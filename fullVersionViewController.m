//
//  fullVersionViewController.m
//  PM Journal Lite
//
//  Created by Peter Pomlett on 29/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "fullVersionViewController.h"

@interface fullVersionViewController ()

@end

@implementation fullVersionViewController


- (IBAction)itunesStore:(id)sender {
    NSString *iTunesLink = @"http://itunes.apple.com/us/app/id690644051?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    
}


- (IBAction)webLink:(id)sender {
    NSString *pMJSupportLink = @"http://www.twigaconsulting.co.uk/pm-journal";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:pMJSupportLink]];
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
