//
//  IssueDetailViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 07/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@class IssueDetailViewController;

@protocol IssueDetailViewControllerDelegate <NSObject>

- (void) upDateBadge;
-(void) dimSuper;
-(void) UndimSuper;
@end

@interface IssueDetailViewController : UIViewController

@property (weak, nonatomic) id <IssueDetailViewControllerDelegate> delegate;
@property(strong, nonatomic)Issue *detailIssue;
@property(strong, nonatomic)Project *project;
- (void)dismissPopovers;
@end
