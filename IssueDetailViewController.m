//
//  IssueDetailViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 07/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "HelpViewController.h"
#import "IssueDetailViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "NewPersonViewController.h"

@interface IssueDetailViewController ()<UITextFieldDelegate, UITextViewDelegate, UIActionSheetDelegate,  NewPersonViewcontrollerDelegate, UIAlertViewDelegate, ABPeoplePickerNavigationControllerDelegate,ABNewPersonViewControllerDelegate, UIPopoverControllerDelegate>
@property (strong, nonatomic) DataHelper *helper;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UILabel *inProjectLabel;
@property (weak, nonatomic) IBOutlet UITextView *summaryTextview;
@property (weak, nonatomic) IBOutlet UITextView *completionTextview;
@property (weak, nonatomic) IBOutlet UILabel *ownerLabel;
@property (weak, nonatomic) IBOutlet UILabel *raisedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *priorityLabel;
@property (weak, nonatomic) IBOutlet UILabel *catagoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *IDLabel;
@property (weak, nonatomic) IBOutlet UILabel *completionHeader;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
@property (weak, nonatomic) IBOutlet UIButton *catagoryButton;
@property (weak, nonatomic) IBOutlet UIButton *priorityButton;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *projectImageView;
@property (weak, nonatomic) IBOutlet UIImageView *summaryImageView;
@property (weak, nonatomic) IBOutlet UIImageView *completionImageView;
@property (weak, nonatomic) IBOutlet UIImageView *ownerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *raisedByImageView;
@property (weak, nonatomic) IBOutlet UIImageView *priorityImageView;
@property (weak, nonatomic) IBOutlet UIImageView *catagoryImageView;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (strong, nonatomic) UIButton *buttonPressed;
@property (strong, nonatomic) UIActionSheet *statusSheet;
@property (strong, nonatomic) UIActionSheet *prioritySheet;
@property (strong, nonatomic) UIActionSheet *catagorySheet;
@property (strong, nonatomic) NSArray *statusList;
@property (strong, nonatomic) NSArray *priorityList;
@property (strong, nonatomic) NSArray *catagoryList;
@property (strong, nonatomic) NSString *peopleKey;
@property (strong, nonatomic) UIPopoverController *addressBookPopover;
@property (strong, nonatomic) UIPopoverController *aNewPersonPopover;
@property (strong, nonatomic) UIPopoverController *UnknownPersonPopover;
@property (strong, nonatomic) UIPopoverController *addNamePopover;
@property (nonatomic, readwrite) ABRecordRef displayedPerson;
@property (nonatomic, strong) NSString *enterFirstName;
@property (nonatomic, strong) NSString *enterLastName;
@property (strong, nonatomic) UIAlertView *unnamedAlert;
@property (strong, nonatomic) UIAlertView *parentClosed;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *helpbuttons;
@property (strong, nonatomic) HelpViewController *helpView;
@property (strong, nonatomic) UIPopoverController *helpPopover;
@property (nonatomic, strong) NSDictionary *helpList;
@property (weak, nonatomic) IBOutlet UIImageView *activeTitleFrame;
@property (weak, nonatomic) IBOutlet UIImageView *activeSummaryFrame;
@property (weak, nonatomic) IBOutlet UIImageView *activeCompFrame;

@end

@implementation IssueDetailViewController



- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
    
    
}



- (IBAction)helpTitle:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"issue.title"] from:sender];
}
- (IBAction)helpProject:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"issue.inProject"] from:sender];
}
- (IBAction)helpSummary:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"issue.summary"] from:sender];
}
- (IBAction)helpCompletion:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"issue.completionSummary"] from:sender];
}
- (IBAction)helpOwner:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"issue.owner"] from:sender];
}
- (IBAction)helpRaised:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"issue.raisedBy"] from:sender];
}
- (IBAction)helpPriority:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"issue.priority"] from:sender];
}
- (IBAction)helpPhase:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"issue.category"] from:sender];
}
- (IBAction)helpStatus:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"issue.status"] from:sender];
}

-(void)loadTheHelpPlist{
    if(self.helpList == nil){
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"IssueHelp" ofType:@"plist"];
        self.helpList = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    }
}

- (void)checkHelpButtons {
    if(![self.helper showHelp]){
          for (UIButton *button in self.helpbuttons) {
             button.hidden = YES;
         }
    }
}


- (void)showHelp:(NSString*)helpString from:(UIButton *)helpButton {
    [self.delegate dimSuper];
    if(self.helpView == nil) {
        self.helpView = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    }
    UITextView *_textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 250, 10)];
    _textView.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
    _textView.textColor = [UIColor lightGrayColor];
    _textView.scrollEnabled = NO;
    _textView.userInteractionEnabled=NO;
    _textView.text =  helpString;
    CGRect frame = _textView.frame;
    frame.size.height =  [self getTextHeight:_textView];
    _textView.frame = frame;
    self.helpView.view.frame = frame;
    [self.helpView.view addSubview:_textView];
    self.helpPopover = [[UIPopoverController alloc] initWithContentViewController:self.helpView];
    self.helpPopover.popoverContentSize =_textView.frame.size;
    [self.helpPopover presentPopoverFromRect:helpButton.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionDown animated:YES];
    self.helpPopover.delegate = self;
}

- (CGFloat)getTextHeight:(UITextView *)textView
{
    NSLayoutManager *lm = textView.layoutManager;
    CGRect boundingRect = [lm usedRectForTextContainer:lm.textContainers[0]];
    CGFloat height = boundingRect.size.height + (textView.textContainerInset.top + textView.textContainerInset.bottom);
    return height;
}

- (void)dismissPopovers{
    [self.delegate UndimSuper];
    
    if (self.parentClosed.isVisible) {
        [self.parentClosed dismissWithClickedButtonIndex:0 animated:NO];
        self.parentClosed = nil;
    }
    if (self.unnamedAlert.isVisible) {
        [self.unnamedAlert dismissWithClickedButtonIndex:0 animated:NO];
        self.unnamedAlert = nil;
    }
    
    if (self.statusSheet != nil) {
        [self.statusSheet dismissWithClickedButtonIndex:self.statusSheet.cancelButtonIndex animated:NO];
        self.statusSheet = nil;
    }
    if (self.prioritySheet != nil) {
        [self.prioritySheet dismissWithClickedButtonIndex:self.prioritySheet.cancelButtonIndex animated:NO];
        self.prioritySheet = nil;
    }
    if (self.catagorySheet != nil) {
        [self.catagorySheet dismissWithClickedButtonIndex:self.catagorySheet.cancelButtonIndex animated:NO];
        self.catagorySheet = nil;
    }
    if ([self.addressBookPopover isPopoverVisible])
    {
        [self.addressBookPopover dismissPopoverAnimated:YES];
    }
    if ([self.aNewPersonPopover isPopoverVisible])
    {
        [self.aNewPersonPopover dismissPopoverAnimated:YES];
    }
    if ([self.addNamePopover isPopoverVisible])
    {
        [self.addNamePopover dismissPopoverAnimated:YES];
    }
    if ([self.helpPopover isPopoverVisible])
    {
        [self.helpPopover dismissPopoverAnimated:YES];
    }
}

//////////////people picker code start/////////////
- (IBAction)owner:(UIButton *)sender {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    self.buttonPressed = sender;
    self.peopleKey = @"owner";
    [self.view endEditing:YES];
    [self pickPerson];
}

- (IBAction)ownerNew:(UIButton *)sender {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    self.buttonPressed = sender;
    self.peopleKey = @"owner";
    [self.view endEditing:YES];
    [self aNewPerson];
}

- (IBAction)RaisedBy:(UIButton *)sender {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    self.buttonPressed = sender;
    self.peopleKey = @"raised";
    [self.view endEditing:YES];
    [self pickPerson];
}

- (IBAction)raisedByNew:(UIButton *)sender {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    self.buttonPressed = sender;
    self.peopleKey = @"raised";
    [self.view endEditing:YES];
    [self aNewPerson];
}

-(void) dismissNewPersonViewcontroller:(NewPersonViewController *)controller {
     [self.delegate UndimSuper];
    [self.addNamePopover dismissPopoverAnimated:YES];
}
-(void) personFirstName:(NSString *)first secondName:(NSString *)second andController:(NewPersonViewController *)controller{
    self.enterFirstName = first;
    self.enterLastName = second;
    [self.addNamePopover dismissPopoverAnimated:YES];
    [self addPersonName];
}

-(void)addPersonName {
    CFErrorRef err = nil;
    ABAddressBookRef adbk = ABAddressBookCreateWithOptions(nil, &err);
    if (nil == adbk) {
         //FUTURE ERROR HANDLING
       // NSLog(@"error: %@", err);
        return;
    }
    ABRecordRef person = self.displayedPerson;
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef)self.enterFirstName, nil);
    ABRecordSetValue(person, kABPersonLastNameProperty,(__bridge CFStringRef)self.enterLastName, nil);
    CFErrorRef error = nil;
    ABAddressBookAddRecord (adbk,person,&error);
    ABAddressBookSave(adbk, &error);
    //stop null from from showing on label
    if(self.enterFirstName == Nil){
        self.enterFirstName = @"";
    }
    if(self.enterLastName == Nil){
        self.enterLastName = @"";
    }
    if([self.peopleKey isEqualToString:@"owner"]){
        self.detailIssue.issueOwner = [NSString stringWithFormat:@"%@ %@", self.enterFirstName ,self.enterLastName];
        if([self.detailIssue.status isEqualToString:@"Unassigned"] || [self.detailIssue.status isEqualToString:@"New"] ){
            self.detailIssue.status = @"Assigned";}
        self.detailIssue.assignedDate = [NSDate date];
        self.detailIssue.lastModifiedDate = [NSDate date];
        [self.helper saveContext];
        self.ownerLabel.text = self.detailIssue.issueOwner;
        self.statusLabel.text = self.detailIssue.status;
    }
    if([self.peopleKey isEqualToString:@"raised"]){
        self.detailIssue.issueRaiser = [NSString stringWithFormat:@"%@ %@", self.enterFirstName ,self.enterLastName];
        self.detailIssue.lastModifiedDate = [NSDate date];
        [self.helper saveContext];
        self.raisedByLabel.text = self.detailIssue.issueRaiser;
    }
    CFRelease(adbk);
}

#pragma mark - Unnamed person alert delegate
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        [self.addressBookPopover dismissPopoverAnimated:YES];
         [self.delegate UndimSuper];
    }
    else{
        [self.addressBookPopover dismissPopoverAnimated:YES];
        NewPersonViewController *aNewPersonViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"NewPerson"];
        aNewPersonViewController.delegate = self;
        self.addNamePopover = [[UIPopoverController alloc] initWithContentViewController:aNewPersonViewController];
        [self.addNamePopover presentPopoverFromRect:self.buttonPressed.frame
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
    }
}

- (void)pickPerson {
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    self.addressBookPopover = [[UIPopoverController alloc] initWithContentViewController:picker];
    [self.addressBookPopover presentPopoverFromRect:self.buttonPressed.frame
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
     self.addressBookPopover.delegate = self;
}

- (void)aNewPerson {
    ABNewPersonViewController *picker = [[ABNewPersonViewController alloc] init];
	picker.newPersonViewDelegate = self;
	UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:picker];
    self.aNewPersonPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
    [self.aNewPersonPopover presentPopoverFromRect: self.buttonPressed.frame
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
      self.aNewPersonPopover.delegate =self;
}

#pragma mark ABPeoplePickerNavigationControllerDelegate methods
// Displays the information of a selected person
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    self.displayedPerson = person;
    NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString *lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    if (firstName == nil && lastName == nil) {
        if(!self.unnamedAlert){
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        }
        [self.unnamedAlert show];
    }
    else{
        //stop null from from showing on label
        if(firstName == Nil){
            firstName = @"";
        }
        if(lastName == Nil){
            lastName = @"";
        }
        if([self.peopleKey isEqualToString:@"owner"]){
            self.detailIssue.issueOwner = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
            if([self.detailIssue.status isEqualToString:@"Unassigned"]||[self.detailIssue.status isEqualToString:@"New"]){
                self.detailIssue.status = @"Assigned";}
            self.detailIssue.assignedDate = [NSDate date];
            self.detailIssue.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
            self.ownerLabel.text = self.detailIssue.issueOwner;
            self.statusLabel.text = self.detailIssue.status;
        }
        if([self.peopleKey isEqualToString:@"raised"]){
            self.detailIssue.issueRaiser = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
            self.detailIssue.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
            self.raisedByLabel.text = self.detailIssue.issueRaiser;
        }
        if ([self.addressBookPopover isPopoverVisible])
        {
            [self.addressBookPopover dismissPopoverAnimated:YES];
             [self.delegate UndimSuper];
        }}
	return NO;
}

// Does not allow users to perform default actions such as dialing a phone number, when they select a person property.
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
								property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
	return NO;
}

// Dismisses the people picker and shows the application when users tap Cancel.
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{
	[self.addressBookPopover dismissPopoverAnimated:YES];
    [self.delegate UndimSuper];
}

#pragma mark ABNewPersonViewControllerDelegate methods
// Dismisses the new-person view controller.
- (void)newPersonViewController:(ABNewPersonViewController *)newPersonViewController didCompleteWithNewPerson:(ABRecordRef)person
{[self.delegate UndimSuper];
    self.displayedPerson = person;
    NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString *lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    if (firstName == nil && lastName == nil) {
        if(self.displayedPerson == nil){
            [self.aNewPersonPopover dismissPopoverAnimated:YES];
            return;
        }
        if(!self.unnamedAlert){
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        }
        [self.unnamedAlert show];
         [self.delegate dimSuper];
    }
    else{
        //stop null from from showing on label
        if(firstName == Nil){
            firstName = @"";
        }
        if(lastName == Nil){
            lastName = @"";
        }
        if([self.peopleKey isEqualToString:@"owner"]){
            self.detailIssue.issueOwner = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
            if([self.detailIssue.status isEqualToString:@"Unassigned"]||[self.detailIssue.status isEqualToString:@"New"]){
                self.detailIssue.status = @"Assigned";}
            self.detailIssue.assignedDate = [NSDate date];
            self.detailIssue.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
            self.ownerLabel.text = self.detailIssue.issueOwner;
            self.statusLabel.text = self.detailIssue.status;
        }
        if([self.peopleKey isEqualToString:@"raised"]){
            self.detailIssue.issueRaiser = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
            self.detailIssue.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
            self.raisedByLabel.text = self.detailIssue.issueRaiser;
        }
    }
	[self.aNewPersonPopover dismissPopoverAnimated:YES];
}
//////////////people picker code end///////////////

-(void)enableCompletionTextView{
    if  ([self.detailIssue.status isEqualToString:@"Completed"]){
        if  (self.detailIssue.completedDate == nil){
            self.detailIssue.completedDate = [NSDate date];}
        self.completionTextview.userInteractionEnabled = YES;
        self.completionTextview.alpha = 1.0;
      //  self.completionImageView.alpha = 1.0;
        self.completionHeader.alpha = 1.0;
         self.activeCompFrame.tintColor = self.view.tintColor;
    }
    else{
        self.completionTextview.userInteractionEnabled = NO;
        self.completionTextview.alpha = 0.5;
      //  self.completionImageView.alpha = 0.5;
        self.completionHeader.alpha = 0.5;
         self.activeCompFrame.tintColor = [UIColor lightGrayColor];
        self.detailIssue.completedDate = nil;
    }
}

-(void)createStatusSheet{
    self.statusList =[ self.helper fetchStatusOptions:self.detailIssue] ;
    self.statusSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:nil
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:nil];
    for  (int i = 0; i < [self.statusList count];  i++) {
        [self.statusSheet addButtonWithTitle:[self.statusList objectAtIndex:i]];
    }
    [self.statusSheet showFromRect:self.statusButton.frame inView:self.view animated:YES];
    
}

-(void)createPrioritySheet{
    self.priorityList = self.helper.fetchPriorityOptions;
    self.prioritySheet = [[UIActionSheet alloc] initWithTitle:nil
                                                     delegate:self
                                            cancelButtonTitle:nil
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:nil];
    for  (int i = 0; i < [self.priorityList count];  i++) {
        [self.prioritySheet addButtonWithTitle:[self.priorityList objectAtIndex:i]];
    }
    [self.prioritySheet showFromRect:self.priorityButton.frame inView:self.view animated:YES];
    
}

-(void)createCatagorySheet{
    self.catagoryList = self.helper.fetchCategoryOptions ;
    self.catagorySheet = [[UIActionSheet alloc] initWithTitle:nil
                                                     delegate:self
                                            cancelButtonTitle:nil
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:nil];
    for  (int i = 0; i < [self.catagoryList count];  i++) {
        [self.catagorySheet addButtonWithTitle:[self.catagoryList objectAtIndex:i]];
    }
    [self.catagorySheet showFromRect:self.catagoryButton.frame inView:self.view animated:YES];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{[self.delegate UndimSuper];
    if (self.statusSheet == actionSheet){
    if (buttonIndex < [self.statusList count]){
        self.detailIssue.status = [self.statusList objectAtIndex:buttonIndex];
        if ([self.detailIssue.status isEqualToString:self.statusLabel.text] == NO) {
            [self enableCompletionTextView];
            self.statusLabel.text = self.detailIssue.status;
            if([self.detailIssue.status isEqualToString:@"Unassigned"]){
                self.detailIssue.issueOwner = nil;
                self.detailIssue.assignedDate = nil;
                self.ownerLabel.text = self.detailIssue.issueOwner;
            }
            self.detailIssue.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
    }
    [self.delegate upDateBadge];
}
    if (self.prioritySheet == actionSheet){
        if (buttonIndex < [self.priorityList count]){
            self.detailIssue.priority = [self.priorityList objectAtIndex:buttonIndex];
            if ([self.detailIssue.priority isEqualToString:self.priorityLabel.text] == NO) {
                self.priorityLabel.text = self.detailIssue.priority;
                self.detailIssue.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
        }
    }
    if (self.catagorySheet == actionSheet){
        if (buttonIndex < [self.catagoryList count]){
            self.detailIssue.category = [self.catagoryList objectAtIndex:buttonIndex];
            if ([self.detailIssue.category isEqualToString:self.catagoryLabel.text] == NO) {
                self.catagoryLabel.text = self.detailIssue.category;
                self.detailIssue.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (self.titleTextField == textField){
        if ([self.detailIssue.title isEqualToString:self.titleTextField.text]==NO){
            self.detailIssue.title = self.titleTextField.text;
            self.detailIssue.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
        if ([self.detailIssue.title isEqualToString:@""]){
            self.detailIssue.title = @"Untitled Issue";
            self.titleTextField.text = self.detailIssue.title;
            [self.helper saveContext];
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (self.summaryTextview == textView){
        if ([self.detailIssue.summary isEqualToString:self.summaryTextview.text]==NO){
            self.detailIssue.summary = self.summaryTextview.text;
            self.detailIssue.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
    }
    if (self.completionTextview == textView){
        if ([self.detailIssue.completionSummary isEqualToString:self.completionTextview.text]==NO){
            self.detailIssue.completionSummary = self.completionTextview.text;
            self.detailIssue.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
    }
}

- (IBAction)PriorityButton {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    [self createPrioritySheet]; 
}

- (IBAction)CatagoryButton {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    [self createCatagorySheet]; 
}

- (IBAction)StatusButton {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    if([self.detailIssue.inProject.status isEqualToString:@"Completed"]){
        if(!self.parentClosed){
        self.parentClosed = [[UIAlertView alloc] initWithTitle:@"Project status completed"
                                                               message:@"Unable to change Issue status as project status is Completed"
                                                              delegate:self                                          cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
        }
        [self.parentClosed show];
    }
    else{
        [self createStatusSheet];
    }
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    UIEdgeInsets tabInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage * textBack = [UIImage imageNamed:@"TextFrame2"];
    textBack = [textBack resizableImageWithCapInsets:tabInsets];
    UIImage* summary = [UIImage imageNamed:@"sumFrame"];
    summary = [summary imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ];
    UIImage* title = [UIImage imageNamed:@"titleframe"];
    title = [title imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ];
    self.activeTitleFrame.image = title;
    self.activeSummaryFrame.image = summary;
    self.activeCompFrame.image = summary;
    
    
    
    self.titleImageView.image = textBack;
   // self.projectImageView.alpha = 0.5;
    self.projectImageView.image = textBack;
    self.summaryImageView.image = textBack;
    self.completionImageView.image = textBack;
    self.ownerImageView.image = textBack;
    self.raisedByImageView.image = textBack;
    self.priorityImageView.image = textBack;
    self.catagoryImageView.image = textBack;
    self.statusImageView.image = textBack;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.titleTextField.text = self.detailIssue.title;
    if([self.titleTextField.text isEqualToString:@""]){
        [self.titleTextField becomeFirstResponder];
    }
    self.summaryTextview.text = self.detailIssue.summary;
    self.completionTextview.text = self.detailIssue.completionSummary;
    self.statusLabel.text = self.detailIssue.status;
    self.IDLabel.text = self.detailIssue.issueID;
    self.priorityLabel.text = self.detailIssue.priority;
    self.catagoryLabel.text = self.detailIssue.category;
    self.inProjectLabel.text = self.detailIssue.inProject.title;
    self.ownerLabel.text = self.detailIssue.issueOwner;
    self.raisedByLabel.text = self.detailIssue.issueRaiser;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    self.titleTextField.delegate = self;
    self.summaryTextview.delegate = self;
    self.completionTextview.delegate = self;
    [self enableCompletionTextView];
    [self checkHelpButtons];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
