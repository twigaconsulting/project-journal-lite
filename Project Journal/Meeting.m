//
//  Meeting.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 18/07/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Meeting.h"
#import "Action.h"
#import "AgendaItem.h"
#import "Media.h"
#import "Note.h"
#import "Person.h"
#import "Project.h"


@implementation Meeting

@dynamic alert;
@dynamic creationDate;
@dynamic deleted;
@dynamic ekEvent;
@dynamic endDateTime;
@dynamic inCalendar;
@dynamic lastModifiedDate;
@dynamic location;
@dynamic meetingID;
@dynamic repeat;
@dynamic startDateTime;
@dynamic status;
@dynamic summary;
@dynamic title;
@dynamic type;
@dynamic url;
@dynamic uuid;
@dynamic oldStatus;
@dynamic inProject;
@dynamic meetingActions;
@dynamic meetingAgenda;
@dynamic meetingAttendees;
@dynamic meetingMedia;
@dynamic meetingNotes;
@dynamic meetingPresent;

@end
