//
//  InitialViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 20/05/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "InitialViewController.h"
#import "ProjectCollectionCell.h"
#import "ProjectFormContainer.h"
#import "PJColour.h"
#import "SettingsTableViewController.h"
#import "ActionFormContainer.h"
#import <iAd/iAd.h>

@interface InitialViewController ()<NSFetchedResultsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,ProjectFormContainerDelegate, SettingsTableViewControllerDelegate, UIActionSheetDelegate, UIAlertViewDelegate, ActionFormContainerDelegate>

@property (nonatomic, strong) DataHelper *helper;
@property (strong, nonatomic) PJColour *cellColour;
@property (nonatomic, strong)Project *project;
@property (weak, nonatomic) IBOutlet UICollectionView *projectCollectionView;
@property (weak, nonatomic) UIPopoverController* settingsPopover;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) UIActionSheet *ProjectSheet;
@property (strong, nonatomic) NSNumberFormatter *formatter;
@property (weak, nonatomic) IBOutlet UILabel *noProjects;
@property (strong,nonatomic) id actionParent;
@property (strong, nonatomic) Action *selectedAction;
@property (strong, nonatomic) UIAlertView *fullVerAlert;
- (NSDateFormatter *)formatterDate;

@end

@implementation InitialViewController

- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

-(void)getActionParent:(id)parent{
    self.actionParent= parent;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"Project" sender: self] ;
    }];
}

- (void)ActionFormContainerDidFinish:(ActionFormContainer *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)getNewAction:(id)itenAction{
    self.actionParent = itenAction;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"parentNewAction" sender: self] ;
    }];
}

-(void)getTheActionForm:(Action *)itemAction{
    self.selectedAction = itemAction;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"actions" sender: self] ;
    }];
}

-(NSString *)convertToLocalCurrency:(NSNumber *)currency{
    if(self.formatter==nil){
        self.formatter = [[NSNumberFormatter alloc] init];
        [self.formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [self.formatter setLocale:[NSLocale currentLocale]];
    }
    return [self.formatter stringFromNumber:currency];
}

- (IBAction)createProject:(UIBarButtonItem *)sender {
    // if to check for projects
    if( [[self.fetchedResultsController fetchedObjects] count] == 0){
    if(!self.ProjectSheet){
        UIActionSheet *proSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:@"Create New Project",nil];
        [proSheet showFromBarButtonItem:sender animated:YES];
        self.ProjectSheet = proSheet;
    }}
    else{
        if(!self.fullVerAlert){
            self.fullVerAlert = [[UIAlertView alloc] initWithTitle:@"PM Journal Lite"
                                                          message:@"This version of Project Manager's Journal only allows for the creation of one project at a time.The full version allows for multiple projects and is ad free. Would you like to view Project Manager's full version in the app store"
                                                         delegate:self                                          cancelButtonTitle:@"Not now"
                                                otherButtonTitles:@"Yes",nil];
        }
        [self.fullVerAlert show];
    }
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex ==1){
   
      
        NSString *iTunesLink = @"http://itunes.apple.com/us/app/id690644051?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }

}



- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        [self performSegueWithIdentifier:@"NewProject" sender:self];
    }
}

-(void)dismissSettings{
    if ([self.settingsPopover isPopoverVisible])
    {
        [self.settingsPopover dismissPopoverAnimated:YES];
    }
}

-(void)updateCollectionView{
    [self fetchRCUtility];
     [self updateNoProjects];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.projectCollectionView reloadData ];
    [self updateNoProjects];
}

- (void)ProjectFormContainerDidFinish:(ProjectFormContainer *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)deleteProject:(ProjectFormContainer *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"settings"]) {
        if (![self.settingsPopover isPopoverVisible]) {
            return YES;
        }
        return NO;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"NewProject"]) {
        [[segue destinationViewController] setDelegate:self];
        self.project = nil;
        [[segue destinationViewController] setProjectObject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"Project"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProjectObject:self.actionParent];
    }
    if ([[segue identifier] isEqualToString:@"Projects"]) {
        [[segue destinationViewController] setProject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"settings"]) {
        self.settingsPopover = [(UIStoryboardPopoverSegue *)segue popoverController];
        UINavigationController *navigationController = segue.destinationViewController;
        SettingsTableViewController *controller = [[navigationController viewControllers] objectAtIndex:0];
        controller.delegate = self;
    }
    if ([[segue identifier] isEqualToString:@"parentNewAction"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.actionParent];
        self.selectedAction = nil;
        [[segue destinationViewController] setContainerAction:self.selectedAction];
    }
    if ([[segue identifier] isEqualToString:@"actions"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        [[segue destinationViewController] setContainerAction:self.selectedAction];
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return  [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    UIColor *redColour = self.cellColour.red;
    UIColor *amberColour = self.cellColour.amber;
    UIColor *greenColour = self.cellColour.green;
    UIColor *blueColour = self.cellColour.blue;
    ProjectCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    Project  *project = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *colour = [project fetchRAGStatus];
    if ([colour isEqualToString:@"Red"]){
        cell.contentView.layer.backgroundColor  = redColour.CGColor;
    }
    if ([colour isEqualToString:@"Green"]){
        cell.contentView.layer.backgroundColor  = greenColour.CGColor;
    }
    if ([colour isEqualToString:@"Amber"]){
        cell.contentView.layer.backgroundColor  = amberColour.CGColor;
    }
    if ([colour isEqualToString:@"Blue"]){
        cell.contentView.layer.backgroundColor  = blueColour.CGColor;
    }
    cell.titleLabel.text = project.title;
    cell.statusLabel.text = project.status;
    cell.budgetLabel.text =[self convertToLocalCurrency: project.targetBudget];
    cell.costLabel.text = [self convertToLocalCurrency: project.actualBudgetToDate];
    cell.ownerLabel.text = project.projectManager;
    if(project.targetCompletionDate){
        cell.completionLabel.text = [[self formatterDate] stringFromDate:project.targetCompletionDate];
    }
    else{
        cell.completionLabel.text =@"Set completion date!";
    }
    if([project isForReview]){
        [ cell.reviewLabel setHidden:NO];
    }
    else{
        [ cell.reviewLabel setHidden:YES];
    }
    if([[project fetchDateRAGStatus] isEqualToString:@"Amber"] ){
        cell.dateBadge.hidden = NO;
        cell.dateBadge.backgroundColor = self.cellColour.amber;
    }
    else if([[project fetchDateRAGStatus] isEqualToString:@"Red"] ){
        cell.dateBadge.hidden = NO;
        cell.dateBadge.backgroundColor = self.cellColour.red;
    }
    else{
        cell.dateBadge.hidden = YES;
    }
    if([[project fetchBudgetRAGStatus] isEqualToString:@"Amber"] ){
        cell.budgetBadge.hidden = NO;
        cell.budgetBadge.backgroundColor = self.cellColour.amber;
    }
    else if([[project fetchBudgetRAGStatus] isEqualToString:@"Red"] ){
        cell.budgetBadge.hidden = NO;
        cell.budgetBadge.backgroundColor = self.cellColour.red;
    }
    else{
        cell.budgetBadge.hidden = YES;
    }
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    self.project =  [_fetchedResultsController objectAtIndexPath:indexPath];
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.canDisplayBannerAds = YES;
    if(self.cellColour == nil){
        self.cellColour = [[PJColour alloc]init];
    }
    [self fetchRCUtility];
}

-(void)updateNoProjects{
    if( [[self.fetchedResultsController fetchedObjects] count] == 0){
        self.noProjects.hidden = NO;
    }
    else{
        self.noProjects.hidden = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated
{ [super viewWillAppear:NO];
    [self fetchRCUtility];
    [self updateNoProjects];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

-(void)refreshFromBackground{
    [self.projectCollectionView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchRCUtility
{
    _helper = [DataHelper sharedInstance];
    _fetchedResultsController = nil;
    if (_helper.hideCompleted) {
        [_helper fetchHCP];
        _fetchedResultsController = _helper.fetchedRCHCP;
    } else {
        [_helper fetchAP];
        _fetchedResultsController = _helper.fetchedRCAP;
    }
    _fetchedResultsController.delegate = self;
    [self.projectCollectionView reloadData ];
}

@end
