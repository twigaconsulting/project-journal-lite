//
//  DetailFormViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 23/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "DetailFormViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "NewPersonViewController.h"
#import "HelpViewController.h"
@interface DetailFormViewController ()<ABPeoplePickerNavigationControllerDelegate,ABNewPersonViewControllerDelegate, NewPersonViewcontrollerDelegate, UITextFieldDelegate, UITextViewDelegate, UIActionSheetDelegate, UIAlertViewDelegate, UIPopoverControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextView *summaryTextView;
@property (weak, nonatomic) IBOutlet UILabel *projectIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectPhaseLabel;
@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
@property (weak, nonatomic) IBOutlet UIImageView *titleActiveFrame;

@property (weak, nonatomic) IBOutlet UIImageView *summaryImage;
@property (weak, nonatomic) IBOutlet UIImageView *summaryActiveFrame;

@property (weak, nonatomic) IBOutlet UIImageView *statusImage;
@property (weak, nonatomic) IBOutlet UIImageView *projectPhaseImageView;
@property (strong, nonatomic) NSArray *phaseList;
@property (strong, nonatomic) NSArray *statusList;
@property (strong, nonatomic) UIActionSheet *statusActionSheet;
@property (strong, nonatomic) UIActionSheet *phaseActionSheet;
@property (weak, nonatomic) IBOutlet UIImageView *ProjectManagerImageView;
//properties for handling project manager field
@property (strong, nonatomic) UIPopoverController *addressBookPopover;
@property (strong, nonatomic) UIPopoverController *aNewPersonPopover;
@property (strong, nonatomic) UIPopoverController *addNamePopover;
@property (strong, nonatomic) UIAlertView *unnamedAlert;
@property (weak, nonatomic) IBOutlet UILabel *PMLabel;
@property (weak, nonatomic) IBOutlet UIButton *PMButton;
@property (weak, nonatomic) IBOutlet UIButton *aNewPersonButton;
@property (nonatomic, readwrite) ABRecordRef displayedPerson;
@property (nonatomic, strong) NSString *enterFirstName;
@property (nonatomic, strong) NSString *enterLastName;
//help buttons
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *helpButtons;
@property (strong, nonatomic) HelpViewController *helpView;
@property (strong, nonatomic) UIPopoverController *helpPopover;
@property (nonatomic, strong) NSDictionary *helpList;
@end

@implementation DetailFormViewController


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
    
    
}


- (IBAction)helpTitle:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.title"] from:sender];
}

- (IBAction)helpPM:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.projectManager"] from:sender];
}

- (IBAction)helpSummary:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.summary"] from:sender];
}

- (IBAction)helpStatus:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.status"] from:sender];
}

//dismiss popovers that are showing when view rotates
- (void)disnissPopovers{
    if (self.unnamedAlert != nil) {
        [self.unnamedAlert dismissWithClickedButtonIndex:0 animated:NO];
        self.unnamedAlert = nil;
    }
    
    if (self.statusActionSheet != nil) {
        [self.statusActionSheet dismissWithClickedButtonIndex:self.statusActionSheet.cancelButtonIndex animated:NO];
        self.statusActionSheet = nil;
    }
    if (self.phaseActionSheet != nil) {
        [self.phaseActionSheet dismissWithClickedButtonIndex:self.statusActionSheet.cancelButtonIndex animated:NO];
        self.phaseActionSheet = nil;
    }
   if ([self.addressBookPopover isPopoverVisible])
   {[self.delegate UndimSuper];
        [self.addressBookPopover dismissPopoverAnimated:YES];
    }
    if ([self.aNewPersonPopover isPopoverVisible])
    {[self.delegate UndimSuper];
        [self.aNewPersonPopover dismissPopoverAnimated:YES];
    }
    if ([self.addNamePopover isPopoverVisible])
    {[self.delegate UndimSuper];
        [self.addNamePopover dismissPopoverAnimated:YES];
    }
    if ([self.helpPopover isPopoverVisible])
    {
        [self.helpPopover dismissPopoverAnimated:YES];
    }
}

#pragma mark methode to add name to contact or new person
-(void) dismissNewPersonViewcontroller:(NewPersonViewController *)controller{
     [self.delegate UndimSuper];
    [self.addNamePopover dismissPopoverAnimated:YES];
   
}

-(void) personFirstName:(NSString *)first secondName:(NSString *)second andController:(NewPersonViewController *)controller{
    self.enterFirstName = first;
    self.enterLastName = second;
    [self.delegate UndimSuper];
    [self.addNamePopover dismissPopoverAnimated:YES];
    [self addPersonName];
}

-(void)addPersonName{
    CFErrorRef err = nil;
    ABAddressBookRef adbk = ABAddressBookCreateWithOptions(nil, &err);
    if (nil == adbk) {
         //FUTURE ERROR HANDLING
      //  NSLog(@"error: %@", err);
       
        return;
    }
    ABRecordRef person = self.displayedPerson;
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef)self.enterFirstName, nil);
    ABRecordSetValue(person, kABPersonLastNameProperty,(__bridge CFStringRef)self.enterLastName, nil);
    CFErrorRef error = nil;
    ABAddressBookAddRecord (adbk,person,&error);
    ABAddressBookSave(adbk, &error);
    //stop null from from showing on label
    if(self.enterFirstName == Nil){
        self.enterFirstName = @"";
    }
    if(self.enterLastName == Nil){
        self.enterLastName = @"";
    }
    self.project.projectManager = [NSString stringWithFormat:@"%@ %@", self.enterFirstName ,self.enterLastName];
    self.project.lastModifiedDate = [NSDate date];
    [ self.helper saveContext];
    self.PMLabel.text =self.project.projectManager;
      [self.delegate upDateTitle];
    CFRelease(adbk);
}

#pragma mark - Unnamed person alert delegate
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        [self.addressBookPopover dismissPopoverAnimated:YES];
        [self.delegate UndimSuper];
    }
    else{
        [self.addressBookPopover dismissPopoverAnimated:YES];
        //show view to allow the adding of names
        NewPersonViewController *aNewPersonViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"NewPerson"];
        aNewPersonViewController.delegate = self;
        self.addNamePopover = [[UIPopoverController alloc] initWithContentViewController:aNewPersonViewController];
        [self.addNamePopover presentPopoverFromRect:self.PMButton.frame
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
        self.addNamePopover.delegate = self;
        
    }
}

//show contact list
- (IBAction)pickPerson {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    self.addressBookPopover = [[UIPopoverController alloc] initWithContentViewController:picker];
    [self.addressBookPopover presentPopoverFromRect:self.PMButton.frame
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
    self.addressBookPopover.delegate = self;
}
//show new person form
- (IBAction)aNewPerson {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    ABNewPersonViewController *picker = [[ABNewPersonViewController alloc] init];
	picker.newPersonViewDelegate = self;
	UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:picker];
    self.aNewPersonPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
    [self.aNewPersonPopover presentPopoverFromRect: self.aNewPersonButton.frame
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
    self.aNewPersonPopover.delegate =self;
}

#pragma mark ABPeoplePickerNavigationControllerDelegate methods
// Displays the information of a selected person
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    self.displayedPerson = person;
    NSString*  firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    if (firstName == nil && lastName == nil) {
        //If the contact picked has no fist or last name show an alert
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        [self.unnamedAlert show];
    }
    else{
        //stop null from from showing on label
        if(firstName == Nil){
            firstName = @"";
        }
        if(lastName == Nil){
            lastName = @"";
        }
        self.project.projectManager = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
        if([self.project.status isEqualToString:@"Unassigned"]||[self.project.status isEqualToString:@"New"]){
            self.project.status = @"Assigned";}
        self.project.lastModifiedDate = [NSDate date];
        [ self.helper saveContext];
        self.PMLabel.text = self.project.projectManager;
         self.statusLabel.text = self.project.status;
          [self.delegate upDateTitle];
        if ([self.addressBookPopover isPopoverVisible])
        {
            [self.addressBookPopover dismissPopoverAnimated:YES];
            [self.delegate UndimSuper];
        }}
	return NO;
}

// Does not allow users to perform default actions such as dialing a phone number, when they select a person property.
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
								property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
	return NO;
}

// Dismisses the people picker and shows the application when users tap Cancel.
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{
	[self.addressBookPopover dismissPopoverAnimated:YES];
    [self.delegate UndimSuper];
}

#pragma mark ABNewPersonViewControllerDelegate methods
// Dismisses the new-person view controller.
- (void)newPersonViewController:(ABNewPersonViewController *)newPersonViewController didCompleteWithNewPerson:(ABRecordRef)person
{ [self.delegate UndimSuper];
    self.displayedPerson = person;
    NSString* firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    
    //If the user does not enter a first or last name show an alert
    if (firstName == nil && lastName == nil) {
        if(self.displayedPerson==nil){
            [self.aNewPersonPopover dismissPopoverAnimated:YES];
            return;
        }
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        [self.unnamedAlert show];
        [self.delegate dimSuper];
    }
    else{
        //stop null from from showing on label
        if(firstName == Nil){
            firstName = @"";
        }
        if(lastName == Nil){
            lastName = @"";
        }
        self.project.projectManager = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
        if([self.project.status isEqualToString:@"Unassigned"]||[self.project.status isEqualToString:@"New"]){
            self.project.status = @"Assigned";}
        self.project.lastModifiedDate = [NSDate date];
        [ self.helper saveContext];
        self.PMLabel.text = self.project.projectManager;
        self.statusLabel.text = self.project.status;
        [self.delegate upDateTitle];
    }
    
	[self.aNewPersonPopover dismissPopoverAnimated:YES];
   // [self.delegate UndimSuper];
}

#pragma mark Action Sheet


- (IBAction)phaseSheet:(UIButton *)sender {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    self.phaseList=self.helper.fetchCategoryOptions ;
    self.phaseActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                  delegate:self
                                         cancelButtonTitle:nil
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:nil];
    
    for  (int i = 0; i < [self.phaseList count];  i++) {
        [self.phaseActionSheet addButtonWithTitle:[self.phaseList objectAtIndex:i]];
    }
    [self.phaseActionSheet showFromRect:sender.frame inView:self.view animated:YES];
}

- (IBAction)statusSheet:(UIButton *)sender {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    self.statusList = [self.helper fetchStatusOptions:self.project];
   self.statusActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    for  (int i = 0; i < [self.statusList count];  i++) {
        [self.statusActionSheet addButtonWithTitle:[self.statusList objectAtIndex:i]];
    }
    [self.statusActionSheet showFromRect:sender.frame inView:self.view animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{    [self.delegate UndimSuper];
    if(self.statusActionSheet == actionSheet){
    if (buttonIndex < [self.statusList count]){
    self.project.status = [self.statusList objectAtIndex:buttonIndex];
    if (![self.project.status isEqualToString:self.statusLabel.text]) {
        self.statusLabel.text = self.project.status;
        //if status is changed to unassigned clear the PM field
        if([self.project.status isEqualToString:@"Unassigned"]){
            self.project.projectManager = nil;
            self.PMLabel.text = self.project.projectManager;
        }
        
          if  ([self.project.status isEqualToString:@"Completed"]){
              self.project.actualCompletionDate = [NSDate date];
          }
          else{
               self.project.actualCompletionDate = nil; 
          }
        self.project.lastModifiedDate = [NSDate date];
        [self.helper saveContext];
    }
}
    //DEL1 this calls the delegate on the form container to update badges and the collection views
    [self.delegate upDateBadge];
}
    if (self.phaseActionSheet == actionSheet){
        if (buttonIndex < [self.phaseList count]){
            self.project.projectPhase = [self.phaseList objectAtIndex:buttonIndex];
            if ([self.project.projectPhase isEqualToString:self.projectPhaseLabel.text] == NO) {
                self.projectPhaseLabel.text = self.project.projectPhase;
                self.project.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
        }
    }
}

#pragma mark Text field/view delegate methods

- (void)textFieldDidEndEditing:(UITextField *)textField{
    //check to see if textfield text is the same as the DB
    //if not update last modified date and save
    if (![self.project.title isEqualToString:self.titleTextField.text]){
        self.project.title = self.titleTextField.text;
        self.project.lastModifiedDate = [NSDate date];
        [self.helper saveContext];
    }
    //If no title has been entered by user set title to Untitled project
    if ([self.project.title isEqualToString:@""]){
        self.project.title = @"Untitled Project";
        self.project.lastModifiedDate = [NSDate date];
        self.titleTextField.text = self.project.title;
        [self.helper saveContext];
   
    }
[self.delegate upDateTitle];
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    if (![self.project.summary isEqualToString:self.summaryTextView.text]){
        self.project.summary = self.summaryTextView.text;
        self.project.lastModifiedDate = [NSDate date];
        [self.helper saveContext];
    }
}

-(void)loadTheHelpPlist{
    if(self.helpList == nil){
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"HelpList" ofType:@"plist"];
        self.helpList = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    }
}

- (void)checkHelpButtons {
    if(![self.helper showHelp]){
          for (UIButton *button in self.helpButtons) {
            button.hidden = YES;
         }
    }
}

- (void)showHelp:(NSString*)helpString from:(UIButton *)helpButton {
    [self.delegate dimSuper];
    if(self.helpView == nil) {
        self.helpView = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    }
    UITextView *_textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 250, 10)];
    _textView.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
    _textView.textColor = [UIColor lightGrayColor];
    _textView.scrollEnabled = NO;
    _textView.userInteractionEnabled=NO;
    _textView.text =  helpString;
    CGRect frame = _textView.frame;
    frame.size.height =  [self getTextHeight:_textView];
    _textView.frame = frame;
    self.helpView.view.frame = frame;
    [self.helpView.view addSubview:_textView];
    self.helpPopover = [[UIPopoverController alloc] initWithContentViewController:self.helpView];
    self.helpPopover.popoverContentSize =_textView.frame.size;
    [self.helpPopover presentPopoverFromRect:helpButton.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionDown animated:YES];
    self.helpPopover.delegate = self;
}

- (CGFloat)getTextHeight:(UITextView *)textView
{
    NSLayoutManager *lm = textView.layoutManager;
    CGRect boundingRect = [lm usedRectForTextContainer:lm.textContainers[0]];
    CGFloat height = boundingRect.size.height + (textView.textContainerInset.top + textView.textContainerInset.bottom);
    return height;
}

#pragma mark ViewController methods
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.titleTextField.text = self.project.title;
    //if self.titleTextfield = nil (new project)
    //show the keyboard to prompt the user to fill the titleField
    if([self.titleTextField.text isEqualToString:@""]){
        
       
        [self.titleTextField becomeFirstResponder];
    }
    self.PMLabel.text = self.project.projectManager;
    self.projectIDLabel.text = self.project.projectID;
   self.summaryTextView.text = self.project.summary;
    self.statusLabel.text = self.project.status;
     self.projectPhaseLabel.text = self.project.projectPhase;
    
  //  self.view.superview. tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    UIEdgeInsets tabInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage * textBack = [UIImage imageNamed:@"TextFrame2"];
    textBack = [textBack resizableImageWithCapInsets:tabInsets];
   UIImage* summary = [UIImage imageNamed:@"sumFrame"];
   summary = [summary imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ];
    UIImage* title = [UIImage imageNamed:@"titleframe"];
    title = [title imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ];
  
    self.titleActiveFrame.image = title;
    self.titleImage.image = textBack;
    self.ProjectManagerImageView.image = textBack;
    self.summaryImage.image = textBack;
    self.summaryActiveFrame.image = summary;
  //  self.summaryActiveFrame.tintColor = self.view.tintColor;
    self.statusImage.image = textBack;
    self.projectPhaseImageView.image = textBack;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    self.titleTextField.delegate = self;
    self.summaryTextView.delegate = self;
    [self checkHelpButtons];
       [self.scroller setContentOffset:CGPointZero animated:YES];
      self.scroller.contentSize=CGSizeMake(1000,758);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
