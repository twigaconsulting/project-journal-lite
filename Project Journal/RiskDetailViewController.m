//
//  RiskDetailViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 27/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "HelpViewController.h"
#import "RiskDetailViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "NewPersonViewController.h"

@interface RiskDetailViewController ()<UITextFieldDelegate, UITextViewDelegate, UIActionSheetDelegate, NewPersonViewcontrollerDelegate, UIAlertViewDelegate, ABPeoplePickerNavigationControllerDelegate, ABNewPersonViewControllerDelegate, UIPopoverControllerDelegate>
@property (strong, nonatomic) UIStoryboardPopoverSegue *popSegue;
@property (strong, nonatomic) DataHelper *helper;
@property (strong, nonatomic) UIPopoverController *addressBookPopover;
@property (strong, nonatomic) UIPopoverController *aNewPersonPopover;
@property (strong, nonatomic) UIPopoverController *UnknownPersonPopover;
@property (strong, nonatomic) UIPopoverController *addNamePopover;
@property (strong, nonatomic) UIAlertView *unnamedAlert;
@property (strong, nonatomic) UIAlertView *parentClosed;
@property (weak, nonatomic) IBOutlet UIButton *personPickerButton;
@property (weak, nonatomic) IBOutlet UIButton *aNewPersonButton;
@property(nonatomic, readwrite) ABRecordRef displayedPerson;
@property (nonatomic, strong) NSString *enterFirstName;
@property (nonatomic, strong) NSString *enterLastName;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UILabel *inProjectLabel;
@property (weak, nonatomic) IBOutlet UITextView *riskEventTextView;
@property (weak, nonatomic) IBOutlet UITextView *impactTextView;
@property (weak, nonatomic) IBOutlet UILabel *ownerLabel;
@property (weak, nonatomic) IBOutlet UILabel *phaseLabel;
@property (weak, nonatomic) IBOutlet UILabel *catagoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *IDLabel;
@property (weak, nonatomic) IBOutlet UIButton *phaseButton;
@property (weak, nonatomic) IBOutlet UIButton *catagoryButton;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *inProjectImageView;
@property (weak, nonatomic) IBOutlet UIImageView *riskEventImageView;
@property (weak, nonatomic) IBOutlet UIImageView *impactImageView;
@property (weak, nonatomic) IBOutlet UIImageView *ownerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *phaseImageView;
@property (weak, nonatomic) IBOutlet UIImageView *catagoryImageView;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (strong, nonatomic) UIActionSheet *statusSheet;
@property (strong, nonatomic) UIActionSheet *phaseSheet;
@property (strong, nonatomic) UIActionSheet *catagorySheet;
@property (strong, nonatomic) NSArray *statusList;
@property (strong, nonatomic) NSArray *phaseList;
@property (strong, nonatomic) NSArray *catagoryList;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *helpButton;
@property (strong, nonatomic) HelpViewController *helpView;
@property (strong, nonatomic) UIPopoverController *helpPopover;
@property (nonatomic, strong) NSDictionary *helpList;
@property (weak, nonatomic) IBOutlet UIImageView *activeTitleFrame;
@property (weak, nonatomic) IBOutlet UIImageView *activeSummaryFrame;
@property (weak, nonatomic) IBOutlet UIImageView *activeCompFrame;
@end

@implementation RiskDetailViewController

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
    
    
}

- (IBAction)helpTitle:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.title"] from:sender];
}

- (IBAction)helpEventStatus:(UIButton *)sender {
     [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.eventDescription"] from:sender]; 
}

- (IBAction)helpImpact:(UIButton *)sender {
     [self loadTheHelpPlist];
     [self showHelp:[self.helpList objectForKey:@"risk.impactDescription"] from:sender];
}

- (IBAction)helpProject:(UIButton *)sender {
     [self loadTheHelpPlist];
     [self showHelp:[self.helpList objectForKey:@"risk.inProject"] from:sender];
}
- (IBAction)helpOwner:(UIButton *)sender {
     [self loadTheHelpPlist];
     [self showHelp:[self.helpList objectForKey:@"risk.owner"] from:sender];
}
- (IBAction)helpPriority:(UIButton *)sender {
     [self loadTheHelpPlist];
     [self showHelp:[self.helpList objectForKey:@"risk.priority"] from:sender];
}
- (IBAction)helpPhase:(UIButton *)sender {
     [self loadTheHelpPlist];
     [self showHelp:[self.helpList objectForKey:@"risk.projectPhase"] from:sender];
}
- (IBAction)helpStatus:(UIButton *)sender {
     [self loadTheHelpPlist];
     [self showHelp:[self.helpList objectForKey:@"risk.status"] from:sender];
}

-(void)loadTheHelpPlist{
    if(self.helpList == nil){
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"RiskHelp" ofType:@"plist"];
        self.helpList = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    }
}

- (void)checkHelpButtons {
    if(![self.helper showHelp]){
          for (UIButton *button in self.helpButton) {
             button.hidden = YES;
          }
    }
}

- (void)showHelp:(NSString*)helpString from:(UIButton *)helpButton {
    [self.delegate dimSuper];
    if(self.helpView == nil) {
        self.helpView = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    }
    UITextView *_textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 250, 10)];
    _textView.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
    _textView.textColor = [UIColor lightGrayColor];
    _textView.scrollEnabled = NO;
    _textView.userInteractionEnabled=NO;
    _textView.text =  helpString;
    CGRect frame = _textView.frame;
    frame.size.height =  [self getTextHeight:_textView];
    _textView.frame = frame;
    self.helpView.view.frame = frame;
    [self.helpView.view addSubview:_textView];
    self.helpPopover = [[UIPopoverController alloc] initWithContentViewController:self.helpView];
    self.helpPopover.popoverContentSize =_textView.frame.size;
    [self.helpPopover presentPopoverFromRect:helpButton.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionDown animated:YES];
    self.helpPopover.delegate = self;
}

- (CGFloat)getTextHeight:(UITextView *)textView
{
    NSLayoutManager *lm = textView.layoutManager;
    CGRect boundingRect = [lm usedRectForTextContainer:lm.textContainers[0]];
    CGFloat height = boundingRect.size.height + (textView.textContainerInset.top + textView.textContainerInset.bottom);
    return height;
}

- (void)dismissPopovers{
    [self.delegate UndimSuper];
    
    if (self.parentClosed.isVisible) {
        [self.parentClosed dismissWithClickedButtonIndex:0 animated:NO];
        self.parentClosed = nil;
    }
    if (self.unnamedAlert.isVisible) {
        [self.unnamedAlert dismissWithClickedButtonIndex:0 animated:NO];
        self.unnamedAlert = nil;
    }
    if (self.statusSheet.isVisible) {
        [self.statusSheet dismissWithClickedButtonIndex:self.statusSheet.cancelButtonIndex animated:NO];
        self.statusSheet = nil;
    }
    if (self.phaseSheet.isVisible) {
        [self.phaseSheet dismissWithClickedButtonIndex:self.phaseSheet.cancelButtonIndex animated:NO];
        self.phaseSheet = nil;
    }
    if (self.catagorySheet.isVisible) {
        [self.catagorySheet dismissWithClickedButtonIndex:self.catagorySheet.cancelButtonIndex animated:NO];
        self.catagorySheet = nil;
    }
    if ([self.addressBookPopover isPopoverVisible])
    {
        [self.addressBookPopover dismissPopoverAnimated:NO];
    }
    if ([self.aNewPersonPopover isPopoverVisible])
    {
        [self.aNewPersonPopover dismissPopoverAnimated:NO];
    }
    if ([self.addNamePopover isPopoverVisible])
    {
        [self.addNamePopover dismissPopoverAnimated:NO];
    }
    if ([self.helpPopover isPopoverVisible])
    {
        [self.helpPopover dismissPopoverAnimated:NO];
    }
}

-(void) dismissNewPersonViewcontroller:(NewPersonViewController *)controller{
     [self.delegate UndimSuper];
    [self.addNamePopover dismissPopoverAnimated:YES];
}

-(void) personFirstName:(NSString *)first secondName:(NSString *)second andController:(NewPersonViewController *)controller{
    self.enterFirstName = first;
    self.enterLastName = second;
    [self.delegate UndimSuper];
    [self.addNamePopover dismissPopoverAnimated:YES];
    [self addPersonName];
}

-(void)addPersonName{
    CFErrorRef err = nil;
    ABAddressBookRef adbk = ABAddressBookCreateWithOptions(nil, &err);
    if (nil == adbk) {
         //FUTURE ERROR HANDLING
      //  NSLog(@"error: %@", err);
        return;
    }
    ABRecordRef person = self.displayedPerson;
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef)self.enterFirstName, nil);
    ABRecordSetValue(person, kABPersonLastNameProperty,(__bridge CFStringRef)self.enterLastName, nil);
    CFErrorRef error = nil;
    ABAddressBookAddRecord (adbk,person,&error);
    ABAddressBookSave(adbk, &error);
    //stop null from from showing on label
    if(self.enterFirstName == Nil){
        self.enterFirstName = @"";
    }
    if(self.enterLastName == Nil){
        self.enterLastName = @"";
    }
    self.risk.riskOwner = [NSString stringWithFormat:@"%@ %@", self.enterFirstName ,self.enterLastName];
    [ self.helper saveContext];
    self.ownerLabel.text =self.risk.riskOwner;
    CFRelease(adbk);
}

#pragma mark - Unnamed person alert delegate
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex ==0){
        [self.addressBookPopover dismissPopoverAnimated:YES];
        [self.delegate UndimSuper];
    }
    else{
        [self.addressBookPopover dismissPopoverAnimated:YES];
        
        NewPersonViewController *aNewPersonViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"NewPerson"];
        aNewPersonViewController.delegate = self;
        self.addNamePopover = [[UIPopoverController alloc] initWithContentViewController:aNewPersonViewController];
        [self.addNamePopover presentPopoverFromRect:self.personPickerButton.frame
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
          self.addNamePopover.delegate = self;
    }
}

- (IBAction)pickPerson {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    self.addressBookPopover = [[UIPopoverController alloc] initWithContentViewController:picker];
    [self.addressBookPopover presentPopoverFromRect:self.personPickerButton.frame
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
    self.addressBookPopover.delegate = self;
}

- (IBAction)aNewPerson:(id)sender {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    ABNewPersonViewController *picker = [[ABNewPersonViewController alloc] init];
	picker.newPersonViewDelegate = self;
	UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:picker];
    self.aNewPersonPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
    [self.aNewPersonPopover presentPopoverFromRect: self.aNewPersonButton.frame
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
    self.aNewPersonPopover.delegate = self;
}

#pragma mark ABPeoplePickerNavigationControllerDelegate methods
// Displays the information of a selected person
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    self.displayedPerson = person;
    NSString*  firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    if (firstName == nil && lastName == nil) {
        if(!self.unnamedAlert){
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        }
        [self.unnamedAlert show];
    }
    else{
        //stop null from from showing on label
        if(firstName == Nil){
            firstName = @"";
        }
        if(lastName == Nil){
            lastName = @"";
        }
        self.risk.riskOwner = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
         self.risk.assignedDate = [NSDate date];
        self.risk.lastModifiedDate = [NSDate date];
         //Update the status field before saving
        if([self.risk.status isEqualToString:@"Unassigned"]||[self.risk.status isEqualToString:@"New"]){
            self.risk.status = @"Assigned";
        }
        [ self.helper saveContext];
        self.ownerLabel.text =  self.risk.riskOwner;
        self.statusLabel.text = self.risk.status;
  
        if ([self.addressBookPopover isPopoverVisible])
        {
            [self.addressBookPopover dismissPopoverAnimated:YES];
             [self.delegate UndimSuper];
        }}
	return NO;
}

// Does not allow users to perform default actions such as dialing a phone number, when they select a person property.
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
								property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
	return NO;
}

// Dismisses the people picker and shows the application when users tap Cancel.
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{
	[self.addressBookPopover dismissPopoverAnimated:YES];
     [self.delegate UndimSuper];
}

#pragma mark ABNewPersonViewControllerDelegate methods
// Dismisses the new-person view controller.
- (void)newPersonViewController:(ABNewPersonViewController *)newPersonViewController didCompleteWithNewPerson:(ABRecordRef)person
{ [self.delegate UndimSuper];
    self.displayedPerson = person;
    NSString* firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    
    //If no first or second name bring up alert
    if (firstName == nil && lastName == nil) {
        if(self.displayedPerson==nil){
            [self.aNewPersonPopover dismissPopoverAnimated:YES];
            return;
        }
        if(!self.unnamedAlert){
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        }
        [self.unnamedAlert show];
        [self.delegate dimSuper];
        
    }
    else{
        //stop null from from showing on label
        if(firstName == Nil){
            firstName = @"";
        }
        if(lastName == Nil){
            lastName = @"";
        }
        self.risk.riskOwner = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
        self.risk.assignedDate = [NSDate date];
        self.risk.lastModifiedDate = [NSDate date];
        
        //Update the status field before saving
        if([self.risk.status isEqualToString:@"Unassigned"]||[self.risk.status isEqualToString:@"New"]){
            self.risk.status = @"Assigned";}
        [ self.helper saveContext];
        self.ownerLabel.text = self.risk.riskOwner;
        self.statusLabel.text = self.risk.status;
    }
    
	[self.aNewPersonPopover dismissPopoverAnimated:YES];
}


- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if (self.riskEventTextView == textView){
        if ([self.risk.eventDescription isEqualToString:self.riskEventTextView.text]==NO){
            self.risk.eventDescription = self.riskEventTextView.text;
        self.risk.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }}
        if (self.impactTextView == textView){
            if ([self.risk.impactDescription isEqualToString:self.impactTextView.text]==NO){
                
                self.risk.impactDescription = self.impactTextView.text;
                self.risk.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (self.titleTextField == textField){
        if ([self.risk.title isEqualToString:self.titleTextField.text]==NO){
            self.risk.title = self.titleTextField.text;
            self.risk.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
        if ([self.risk.title isEqualToString:@""]){
            self.risk.title = @"Untitled Risk";
            self.titleTextField.text = self.risk.title;
            [self.helper saveContext];
        }
    }
}

-(void)createStatusSheet{
    self.statusList = [self.helper fetchStatusOptions:self.risk];
    self.statusSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:nil
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:nil];
    
    for  (int i = 0; i < [self.statusList count];  i++) {
        [self.statusSheet addButtonWithTitle:[self.statusList objectAtIndex:i]];
    }
    [self.statusSheet showFromRect:self.statusButton.frame inView:self.view animated:YES];
    
}

-(void)createPhaseSheet{
    self.phaseList=self.helper.fetchPriorityOptions ;
    self.phaseSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                     delegate:self
                                            cancelButtonTitle:nil
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:nil];
    
    for  (int i = 0; i < [self.phaseList count];  i++) {
        [self.phaseSheet addButtonWithTitle:[self.phaseList objectAtIndex:i]];
    }
    [self.phaseSheet showFromRect:self.phaseButton.frame inView:self.view animated:YES];
}

-(void)createCatagorySheet{
    self.catagoryList=self.helper.fetchCategoryOptions ;
    self.catagorySheet = [[UIActionSheet alloc] initWithTitle:nil
                                                     delegate:self
                                            cancelButtonTitle:nil
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:nil];
    
    for  (int i = 0; i < [self.catagoryList count];  i++) {
        [self.catagorySheet addButtonWithTitle:[self.catagoryList objectAtIndex:i]];
    }
    [self.catagorySheet showFromRect:self.catagoryButton.frame inView:self.view animated:YES];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{[self.delegate UndimSuper];
    if (self.statusSheet == actionSheet){
    if (buttonIndex < [self.statusList count]){
        self.risk.status = [self.statusList objectAtIndex:buttonIndex];
        if ([self.risk.status isEqualToString:self.statusLabel.text] == NO) {
            self.statusLabel.text = self.risk.status;
            if([self.risk.status isEqualToString:@"Unassigned"]){
                self.risk.riskOwner = nil;
                 self.risk.assignedDate = nil;
                self.ownerLabel.text = self.risk.riskOwner;
            }
            if  ([self.risk.status isEqualToString:@"Completed"]){
                self.risk.actualCompletionDate = [NSDate date];
            }
            else{
                self.risk.actualCompletionDate = nil;
            }
            self.risk.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
    }
 [self.delegate upDateBadge];
}
    if (self.phaseSheet == actionSheet){
        if (buttonIndex < [self.phaseList count]){
            self.risk.projectPhase = [self.phaseList objectAtIndex:buttonIndex];
            if ([self.risk.projectPhase isEqualToString:self.phaseLabel.text] == NO) {
                self.phaseLabel.text = self.risk.projectPhase;
                self.risk.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
        }
    }
    
    if (self.catagorySheet == actionSheet){
        if (buttonIndex < [self.catagoryList count]){
            self.risk.category = [self.catagoryList objectAtIndex:buttonIndex];
            if ([self.risk.category isEqualToString:self.catagoryLabel.text] == NO) {
                self.catagoryLabel.text = self.risk.category;
                self.risk.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
        }
    }
}

- (IBAction)PhaseSheet {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    [self createPhaseSheet];
}

- (IBAction)CatagorySHeet {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
     [self createCatagorySheet]; 
}

- (IBAction)StatusSheet {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    if([self.risk.inProject.status isEqualToString:@"Completed"]){
        if(!self.parentClosed){
        self.parentClosed = [[UIAlertView alloc] initWithTitle:@"Project status completed"
                                                               message:@"Unable to change risk status as project status is Completed"
                                                              delegate:self                                          cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
        }
        [self.parentClosed show];
    }
    else{
    [self createStatusSheet];
     }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.titleTextField.text = self.risk.title;
    if([self.titleTextField.text isEqualToString:@""]){
        [self.titleTextField becomeFirstResponder];
    }
    self.riskEventTextView.text = self.risk.eventDescription;
    self.impactTextView.text = self.risk.impactDescription;
    self.phaseLabel.text = self.risk.projectPhase;
    self.catagoryLabel.text = self.risk.category;
    self.statusLabel.text = self.risk.status;
    self.IDLabel.text = self.risk.riskID;
    self.inProjectLabel.text = self.risk.inProject.title;
    self.ownerLabel.text = self.risk.riskOwner;
    
}


-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    UIEdgeInsets tabInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage * textBack = [UIImage imageNamed:@"TextFrame2"];
    textBack = [textBack resizableImageWithCapInsets:tabInsets];
    UIImage* summary = [UIImage imageNamed:@"sumFrame"];
    summary = [summary imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ];
    UIImage* title = [UIImage imageNamed:@"titleframe"];
    title = [title imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ];
    self.activeTitleFrame.image = title;
    self.activeSummaryFrame.image = summary;
    self.activeCompFrame.image = summary;
    
    self.titleImageView.image = textBack;
  //  self.inProjectImageView.alpha = 0.5;
    self.inProjectImageView.image = textBack;
    self.riskEventImageView.image = textBack;
    self.ownerImageView.image = textBack;
    self.phaseImageView.image = textBack;
    self.catagoryImageView.image = textBack;
    self.statusImageView.image = textBack;
    self.impactImageView.image = textBack;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    self.titleTextField.delegate = self;
    self.riskEventTextView.delegate = self;
    self.impactTextView.delegate = self;
    [self checkHelpButtons];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
