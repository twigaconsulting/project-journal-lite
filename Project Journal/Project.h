//
//  Project.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 18/07/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Action, Dependency, Issue, Media, Meeting, Note, Risk, Stakeholder;

@interface Project : NSManagedObject

@property (nonatomic, retain) NSNumber * actualBudgetToDate;
@property (nonatomic, retain) NSDate * actualCompletionDate;
@property (nonatomic, retain) NSNumber * actualCostAtCompletion;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSNumber * hideCompletedActionFlag;
@property (nonatomic, retain) NSNumber * hideCompletedIssueFlag;
@property (nonatomic, retain) NSNumber * hideCompletedMeetingFlag;
@property (nonatomic, retain) NSNumber * hideCompletedRiskFlag;
@property (nonatomic, retain) NSNumber * initialBudget;
@property (nonatomic, retain) NSDate * initialCompletionDate;
@property (nonatomic, retain) NSDate * lastModifiedDate;
@property (nonatomic, retain) NSString * projectID;
@property (nonatomic, retain) NSString * projectManager;
@property (nonatomic, retain) NSString * projectPhase;
@property (nonatomic, retain) NSDate * projectStartDate;
@property (nonatomic, retain) NSString * rag;
@property (nonatomic, retain) NSDate * reviewDate;
@property (nonatomic, retain) NSNumber * revisedBudget;
@property (nonatomic, retain) NSDate * revisedCompletionDate;
@property (nonatomic, retain) NSString * sortActionField;
@property (nonatomic, retain) NSString * sortIssueField;
@property (nonatomic, retain) NSString * sortMeetingField;
@property (nonatomic, retain) NSString * sortRiskField;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSDate * targetCompletionDate;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * oldStatus;
@property (nonatomic, retain) NSSet *projectActions;
@property (nonatomic, retain) NSSet *projectDependencies;
@property (nonatomic, retain) NSSet *projectIssues;
@property (nonatomic, retain) NSSet *projectMedia;
@property (nonatomic, retain) NSSet *projectMeetings;
@property (nonatomic, retain) NSSet *projectNotes;
@property (nonatomic, retain) NSSet *projectRisks;
@property (nonatomic, retain) NSSet *projectStakeholders;
@end

@interface Project (CoreDataGeneratedAccessors)

- (void)addProjectActionsObject:(Action *)value;
- (void)removeProjectActionsObject:(Action *)value;
- (void)addProjectActions:(NSSet *)values;
- (void)removeProjectActions:(NSSet *)values;

- (void)addProjectDependenciesObject:(Dependency *)value;
- (void)removeProjectDependenciesObject:(Dependency *)value;
- (void)addProjectDependencies:(NSSet *)values;
- (void)removeProjectDependencies:(NSSet *)values;

- (void)addProjectIssuesObject:(Issue *)value;
- (void)removeProjectIssuesObject:(Issue *)value;
- (void)addProjectIssues:(NSSet *)values;
- (void)removeProjectIssues:(NSSet *)values;

- (void)addProjectMediaObject:(Media *)value;
- (void)removeProjectMediaObject:(Media *)value;
- (void)addProjectMedia:(NSSet *)values;
- (void)removeProjectMedia:(NSSet *)values;

- (void)addProjectMeetingsObject:(Meeting *)value;
- (void)removeProjectMeetingsObject:(Meeting *)value;
- (void)addProjectMeetings:(NSSet *)values;
- (void)removeProjectMeetings:(NSSet *)values;

- (void)addProjectNotesObject:(Note *)value;
- (void)removeProjectNotesObject:(Note *)value;
- (void)addProjectNotes:(NSSet *)values;
- (void)removeProjectNotes:(NSSet *)values;

- (void)addProjectRisksObject:(Risk *)value;
- (void)removeProjectRisksObject:(Risk *)value;
- (void)addProjectRisks:(NSSet *)values;
- (void)removeProjectRisks:(NSSet *)values;

- (void)addProjectStakeholdersObject:(Stakeholder *)value;
- (void)removeProjectStakeholdersObject:(Stakeholder *)value;
- (void)addProjectStakeholders:(NSSet *)values;
- (void)removeProjectStakeholders:(NSSet *)values;

@end
