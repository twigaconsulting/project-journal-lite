//
//  CurrencyPadViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 20/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CurrencyPadViewController;

@protocol CurrencyPadViewControllerDelegate <NSObject>
- (void) CurrencyPadViewControllerDidFinish:(CurrencyPadViewController *)controller;
- (void) Currency:(NSNumber *)number;
@end

@interface CurrencyPadViewController : UIViewController
@property (weak, nonatomic) id <CurrencyPadViewControllerDelegate> delegate;
@end
