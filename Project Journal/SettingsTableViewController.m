//
//  SettingsTableViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 06/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "DataHelper.h"
#import "settingsPickerViewController.h"
@interface SettingsTableViewController ()< settingsPickerViewControllerDelegate ,UITableViewDelegate>
@property (strong, nonatomic) DataHelper *helper;
@property (weak, nonatomic) IBOutlet UITableViewCell *supportCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *siteCell;

@property (weak, nonatomic) IBOutlet UILabel *redStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *amberStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *budgetLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewProjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewActionsLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewIssuesLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewRiskLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@property (weak, nonatomic) IBOutlet UISwitch *helpSwitch; //may not need
@property (weak, nonatomic) IBOutlet UISwitch *completedSwitch; //may not need
@property (nonatomic, assign) int dayCount;
@property (nonatomic, copy) NSString *key;
@end

@implementation SettingsTableViewController


- (IBAction)done:(id)sender {
    
    [self.delegate dismissSettings];
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView cellForRowAtIndexPath:indexPath] == self.supportCell) {
       [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        /* This method creates the email for the Risk. This email has
         * no attachment the Risk details are with the email body.
         */
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:[NSArray arrayWithObjects:@"support@twigaconsulting.co.uk",nil] forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        
        
        [data setValue:[NSString stringWithFormat:@"SUPPORT - PROJECT JOURNAL %@:",VERSION] forKey:@"subject"];

        [data setValue:nil forKey:@"body"];
        
        [data setValue:nil forKey:@"fileName"];
        
        [self displayMailComposeSheet:data];
        
    }
    
    if ([tableView cellForRowAtIndexPath:indexPath] == self.siteCell) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        NSString *pMJSupportLink = @"http://www.twigaconsulting.co.uk/support";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:pMJSupportLink]];
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"red"]) {
        self.key = @"red";
         [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setPickerBarTitle:@"Red RAG"];
            [[segue destinationViewController] setDayCount:[self.helper.redRAGInterval intValue]];
        }
    if ([[segue identifier] isEqualToString:@"amber"]) {
        self.key = @"amber";
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setPickerBarTitle:@"Amber RAG"];
        [[segue destinationViewController] setDayCount:[self.helper.amberRAGInterval intValue]];
    }
    if ([[segue identifier] isEqualToString:@"budget"]) {
        self.key = @"budget";
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setPickerBarTitle:@"Budget Amber"];
        [[segue destinationViewController] setDayCount:[self.helper.amberRAGBudgetPerCent intValue]];
    }
    
    if ([[segue identifier] isEqualToString:@"projects"]) {
        self.key = @"projects";
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setPickerBarTitle:@"Review Projects"];
        [[segue destinationViewController] setDayCount:[self.helper.projectReviewInterval intValue]];
    }
    if ([[segue identifier] isEqualToString:@"actions"]) {
        self.key = @"actions";
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setPickerBarTitle:@"Review Actions"];
        [[segue destinationViewController] setDayCount:[self.helper.actionReviewInterval intValue]];
    }
    if ([[segue identifier] isEqualToString:@"issues"]) {
        self.key = @"issues";
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setPickerBarTitle:@"Review Issues"];
        [[segue destinationViewController] setDayCount:[self.helper.issueReviewInterval intValue]];
    }
    if ([[segue identifier] isEqualToString:@"risks"]) {
        self.key = @"risks";
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setPickerBarTitle:@"Review Risks"];
        [[segue destinationViewController] setDayCount:[self.helper.riskReviewInterval intValue]];
    }
}

- (void)pickerPickedDays:(int)days{
    if([self.key isEqualToString:@"red"]){
    [self.helper setRedRAGInterval:[NSNumber numberWithInt:days]];
    [self.helper saveContext];
    }
    if([self.key isEqualToString:@"amber"]){
        [self.helper setAmberRAGInterval:[NSNumber numberWithInt:days]];
        [self.helper saveContext];
    }
    if([self.key isEqualToString:@"budget"]){
        [self.helper setAmberRAGBudgetPerCent:[NSNumber numberWithInt:days]];
        [self.helper saveContext];
    }
    if([self.key isEqualToString:@"projects"]){
        [self.helper setProjectReviewInterval:[NSNumber numberWithInt:days]];
        [self.helper saveContext];
    }
    if([self.key isEqualToString:@"actions"]){
        [self.helper setActionReviewInterval:[NSNumber numberWithInt:days]];
        [self.helper saveContext];
    }
    if([self.key isEqualToString:@"issues"]){
        [self.helper setIssueReviewInterval:[NSNumber numberWithInt:days]];
        [self.helper saveContext];
    }
    if([self.key isEqualToString:@"risks"]){
        [self.helper setRiskReviewInterval:[NSNumber numberWithInt:days]];
        [self.helper saveContext];
    }

[self.delegate updateCollectionView];//must check
}

- (IBAction)showHelp:(UISwitch *)sender {
        if ([sender isOn]) {
            [self.helper setShowHelp:YES];
        }
        else{
            [self.helper setShowHelp:NO];
        }
        [self.helper saveContext];
}

- (IBAction)HideCompleted:(UISwitch *)sender {
    if ([sender isOn]) {
        [self.helper setHideCompleted:YES];
    }
    else{
         [self.helper setHideCompleted:NO];
    }
    [self.helper saveContext];
    
    
    [self.delegate updateCollectionView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.redStatusLabel.text = [NSString stringWithFormat:@"%d Days before target completion" ,[self.helper.redRAGInterval intValue]];
    self.amberStatusLabel.text = [NSString stringWithFormat:@"%d Days before red RAG" ,[self.helper.amberRAGInterval intValue]];
       self.budgetLabel.text = [NSString stringWithFormat:@"%d%% of budget",[self.helper.amberRAGBudgetPerCent intValue]];
    self.reviewProjectLabel.text = [NSString stringWithFormat:@"Every %d Days" ,[self.helper.projectReviewInterval intValue]];
     self.reviewActionsLabel.text = [NSString stringWithFormat:@"Every %d Days" ,[self.helper.actionReviewInterval intValue]];
     self.reviewIssuesLabel.text = [NSString stringWithFormat:@"Every %d Days" ,[self.helper.issueReviewInterval intValue]];
     self.reviewRiskLabel.text = [NSString stringWithFormat:@"Every %d Days" ,[self.helper.riskReviewInterval intValue]];
    [ self.helpSwitch setOn:[self.helper showHelp]];
    [ self.completedSwitch setOn:[self.helper hideCompleted]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 self.helper = [DataHelper sharedInstance];
    self.versionLabel.text = VERSION;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Email Methods
// This is used because there are no attachments for a text email.
- (void)displayMailComposeSheet:(NSDictionary *)data
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = (id)self;
	
    
    // Setup recipients if required
    [picker setToRecipients:[data valueForKey:@"toRecipients"]];
	[picker setCcRecipients:[data valueForKey:@"ccRecipients"]];
	[picker setBccRecipients:[data valueForKey:@"bccRecipients"]];
    
    // Fill out the Subject of the email
	[picker setSubject:[data valueForKey:@"subject"]];
    
    if([[data valueForKey:@"fileName"] length] > 0)
    {
        [picker addAttachmentData:[data valueForKey:@"attachmentData"] mimeType:@"text/plain" fileName:[data valueForKey:@"fileName"]];
        
    }
    
    // Fill out the email body text
	NSString *emailBody = [data valueForKey:@"body"];
	[picker setMessageBody:emailBody isHTML:YES];
	
	[self presentViewController:picker animated:YES completion:NULL];
    
    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the
// message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
             //FUTURE ERROR HANDLING
		//	NSLog(@"Result: Mail sending canceled");
			break;
		case MFMailComposeResultSaved:
		//	NSLog(@"Result: Mail saved");
			break;
		case MFMailComposeResultSent:
		//	NSLog(@"Result: Mail sent");
			break;
		case MFMailComposeResultFailed:
		//	NSLog(@"Result: Mail sending failed");
			break;
		default:
		//	NSLog(@"Result: Mail not sent");
			break;
	}
	[controller dismissViewControllerAnimated:YES completion:NULL];
}


@end
