//
//  BudgetFormViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 23/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "HelpViewController.h"
#import "BudgetFormViewController.h"
#import "CurrencyPadViewController.h"

@interface BudgetFormViewController ()<CurrencyPadViewControllerDelegate, UIPopoverControllerDelegate>
{
    NSNumberFormatter *_formatter;
}
@property (strong, nonatomic) DataHelper *helper;
@property (strong, nonatomic) UIStoryboardPopoverSegue* popSegue;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *initialBudgetLabel;
@property (weak, nonatomic) IBOutlet UILabel *revisedBudgetLabel;
@property (weak, nonatomic) IBOutlet UILabel *budgetToDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *costCompletionLabel;
@property (weak, nonatomic) IBOutlet UILabel *earnedValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *foctoredRiskLabel;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *IBImageView;
@property (weak, nonatomic) IBOutlet UIImageView *RBImageView;
@property (weak, nonatomic) IBOutlet UIImageView *BTDImageView;
@property (weak, nonatomic) IBOutlet UIImageView *CACImageView;
@property (weak, nonatomic) IBOutlet UIImageView *EVInageView;
@property (weak, nonatomic) IBOutlet UIImageView *PFRImageView;
@property (weak, nonatomic) IBOutlet UIButton *initialBugetButton;
@property (weak, nonatomic) IBOutlet UIButton *revisedBudgetButton;
@property (weak, nonatomic) IBOutlet UIButton *budgetToDateButton;
@property (weak, nonatomic) IBOutlet UIButton *budgetAtCompletionButton;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *helpButtons;
@property (strong, nonatomic)NSString *key;
@property (strong, nonatomic) HelpViewController *helpView;
@property (strong, nonatomic) UIPopoverController *helpPopover;
@property (nonatomic, strong) NSDictionary *helpList;
@end

@implementation BudgetFormViewController


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
    
    
}

- (IBAction)helpInitial:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.initialBudget"] from:sender];
}

- (IBAction)helpRevised:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.revisedBudget"] from:sender];
}

- (IBAction)helpCost:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.budgetToDate"] from:sender];
}
- (IBAction)helpCostComp:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.costAtCompletion"] from:sender];
}

- (IBAction)helpEarned:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.earnedValue"] from:sender];
}

- (IBAction)helpRisk:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.factoredRisk"] from:sender];
}

-(void)loadTheHelpPlist{
    if(self.helpList == nil){
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"HelpList" ofType:@"plist"];
        self.helpList = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    }
}

- (void)checkHelpButtons {
    if(![self.helper showHelp]){
          for (UIButton *button in self.helpButtons) {
             button.hidden = YES;
          }
    }
}


- (void)showHelp:(NSString*)helpString from:(UIButton *)helpButton {
    [self.delegate dimSuper];
    if(self.helpView == nil) {
        self.helpView = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    }
    UITextView *_textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 250, 10)];
    _textView.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
    _textView.textColor = [UIColor lightGrayColor];
    _textView.scrollEnabled = NO;
    _textView.userInteractionEnabled=NO;
    _textView.text =  helpString;
    CGRect frame = _textView.frame;
    frame.size.height =  [self getTextHeight:_textView];
    _textView.frame = frame;
    self.helpView.view.frame = frame;
    [self.helpView.view addSubview:_textView];
    self.helpPopover = [[UIPopoverController alloc] initWithContentViewController:self.helpView];
    self.helpPopover.popoverContentSize =_textView.frame.size;
    [self.helpPopover presentPopoverFromRect:helpButton.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionDown animated:YES];
    self.helpPopover.delegate = self;
}

- (CGFloat)getTextHeight:(UITextView *)textView
{
    NSLayoutManager *lm = textView.layoutManager;
    CGRect boundingRect = [lm usedRectForTextContainer:lm.textContainers[0]];
    CGFloat height = boundingRect.size.height + (textView.textContainerInset.top + textView.textContainerInset.bottom);
    return height;
}

//dismiss popovers that are showing when view rotates

- (void)dismissPopovers{
    [self.delegate UndimSuper];
    if ([self.popSegue.popoverController isPopoverVisible])
    {
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
    if ([self.helpPopover isPopoverVisible])
    {
        [self.helpPopover dismissPopoverAnimated:YES];
    }
}

-(NSString *)convertToLocalCurrency:(NSNumber *)currency{
    if(_formatter==nil){
        _formatter = [[NSNumberFormatter alloc] init];
        [_formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [_formatter setLocale:[NSLocale currentLocale]];
    }
    return [_formatter stringFromNumber:currency];
}

#pragma mark - delegate methods
- (void) CurrencyPadViewControllerDidFinish:(CurrencyPadViewController *)controller{
    if ([self.popSegue.popoverController isPopoverVisible])
    {[self.delegate UndimSuper];
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
}

- (void) Currency:(NSNumber *)number{
    if([self.key isEqualToString:@"IB"]){
        self.project.initialBudget = number;
        self.project.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
        [self applyBudgetRules];
         self.earnedValueLabel.text=[self convertToLocalCurrency:[self.project earnedValue]];
        self.initialBudgetLabel.text=[self convertToLocalCurrency:self.project.initialBudget];
         [self.delegate updateBudgetBadge];
    }
    if([self.key isEqualToString:@"RB"]){
        self.project.revisedBudget = number;
        self.project.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
         self.earnedValueLabel.text=[self convertToLocalCurrency:[self.project earnedValue]];
        self.revisedBudgetLabel.text=[self convertToLocalCurrency:self.project.revisedBudget];
        [self.delegate updateBudgetBadge];
    }
    if([self.key isEqualToString:@"BTD"]){
        self.project.actualBudgetToDate = number;
        self.project.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
         self.earnedValueLabel.text=[self convertToLocalCurrency:[self.project earnedValue]];
        self.budgetToDateLabel.text=[self convertToLocalCurrency:self.project.actualBudgetToDate];
        [self.delegate updateBudgetBadge];
    }
    if([self.key isEqualToString:@"CAC"]){
        self.project.actualCostAtCompletion = number;
        self.project.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
         self.earnedValueLabel.text=[self convertToLocalCurrency:[self.project earnedValue]];
        self.costCompletionLabel.text=[self convertToLocalCurrency:self.project.actualCostAtCompletion];
    }
}

#pragma mark - View controller methods
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.delegate dimSuper];
    if ([[segue identifier] isEqualToString:@"IB"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        self.popSegue.popoverController.delegate = self;
        self.key=@"IB";
    }
    if ([[segue identifier] isEqualToString:@"RB"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        self.popSegue.popoverController.delegate = self;
        self.key=@"RB";
    }
    if ([[segue identifier] isEqualToString:@"BTD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        self.popSegue.popoverController.delegate = self;
        self.key=@"BTD";
    }
    if ([[segue identifier] isEqualToString:@"CAC"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        self.popSegue.popoverController.delegate = self;
        self.key=@"CAC";
    }
 
}

-(void)applyBudgetRules{
    if([self.project.status isEqualToString:@"Completed"]){
       // self.initialBugetButton.hidden = YES;
        self.initialBugetButton.enabled = NO;
       // self.revisedBudgetButton.hidden = YES;
        self.revisedBudgetButton.enabled = NO;
       // self.budgetToDateButton.hidden = YES;
        self.budgetToDateButton.enabled = NO;
      //  self.budgetAtCompletionButton.hidden = NO;
         self.budgetAtCompletionButton.enabled = YES;
     //   self.RBImageView.alpha = 0.5;
      //  self.IBImageView.alpha = 0.5;
       // self.BTDImageView.alpha = 0.5;
        self.CACImageView.alpha = 1.0;
        self.project.actualCostAtCompletion = self.project.actualBudgetToDate;
    }
    
    else {
        if([self.project.initialBudget floatValue] == 0.00){
           // self.initialBugetButton.hidden = NO;
            self.initialBugetButton.enabled = YES;
           // self.revisedBudgetButton.hidden = YES;
            self.revisedBudgetButton.enabled = NO;
           // self.budgetToDateButton.hidden = YES;
             self.budgetToDateButton.enabled = NO;
          //  self.budgetAtCompletionButton.hidden = YES;
              self.budgetAtCompletionButton.enabled = NO;
          //  self.RBImageView.alpha = 0.5;
            self.IBImageView.alpha = 1.0;
           // self.BTDImageView.alpha = 0.5;
          //  self.CACImageView.alpha = 0.5;
        }
    
        else{
           // self.initialBugetButton.hidden = YES;
            self.initialBugetButton.enabled = NO;
          //  self.revisedBudgetButton.hidden = NO;
            self.revisedBudgetButton.enabled = YES;
           // self.budgetToDateButton.hidden = NO;
            self.budgetToDateButton.enabled = YES;
          //  self.budgetAtCompletionButton.hidden = YES;
              self.budgetAtCompletionButton.enabled = NO;
            self.RBImageView.alpha = 1.0;
          //  self.IBImageView.alpha = 0.5;
            self.BTDImageView.alpha = 1.0;
           // self.CACImageView.alpha = 0.5;
        }
       self.project.actualCostAtCompletion = [NSNumber numberWithFloat:0.00]; 
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self applyBudgetRules];
    self.titleLabel.text = self.project.title;
    self.earnedValueLabel.text=[self convertToLocalCurrency:[self.project earnedValue]];
    self.initialBudgetLabel.text=[self convertToLocalCurrency:self.project.initialBudget];
    self.revisedBudgetLabel.text=[self convertToLocalCurrency:self.project.revisedBudget];
    self.budgetToDateLabel.text=[self convertToLocalCurrency:self.project.actualBudgetToDate];
    self.costCompletionLabel.text=[self convertToLocalCurrency:self.project.actualCostAtCompletion];
    self.foctoredRiskLabel.text =[self convertToLocalCurrency:self.project.factoredRisk];
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    UIEdgeInsets tabInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage * textBack = [UIImage imageNamed:@"TextFrame2"];
    textBack = [textBack resizableImageWithCapInsets:tabInsets];
    self.titleImageView.image = textBack;
   // self.titleImageView.alpha = 0.5;
    self.IBImageView.image = textBack;
    self.RBImageView.image = textBack;
    self.BTDImageView.image = textBack;
    self.CACImageView.image = textBack;
   // self.EVInageView.alpha = 0.5;
    self.EVInageView.image = textBack;
   // self.PFRImageView.alpha = 0.5;
    self.PFRImageView.image = textBack;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    [self checkHelpButtons];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
