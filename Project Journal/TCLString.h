//
//  TCLString.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Meeting;

@interface TCLString : NSManagedObject

@property (nonatomic, retain) NSString * string;
@property (nonatomic, retain) Meeting *inMeetingAgenda;
@property (nonatomic, retain) Meeting *inMeetingAttendees;
@property (nonatomic, retain) Meeting *inMeetingPresent;

@end
