//
//  Dependency.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 18/07/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Media, Note, Project;

@interface Dependency : NSManagedObject

@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSDate * dateRequired;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * dependencyID;
@property (nonatomic, retain) NSString * dependencyReceiver;
@property (nonatomic, retain) NSString * dependencySupplier;
@property (nonatomic, retain) NSString * dependencyType;
@property (nonatomic, retain) NSDate * estimatedDeliveryDate;
@property (nonatomic, retain) NSDate * lastModifiedDate;
@property (nonatomic, retain) NSNumber * receiverAgreement;
@property (nonatomic, retain) NSDate * reviewDate;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSNumber * supplierAgreement;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * oldStatus;
@property (nonatomic, retain) NSSet *dependencyMedia;
@property (nonatomic, retain) NSSet *dependencyNotes;
@property (nonatomic, retain) Project *inProject;
@end

@interface Dependency (CoreDataGeneratedAccessors)

- (void)addDependencyMediaObject:(Media *)value;
- (void)removeDependencyMediaObject:(Media *)value;
- (void)addDependencyMedia:(NSSet *)values;
- (void)removeDependencyMedia:(NSSet *)values;

- (void)addDependencyNotesObject:(Note *)value;
- (void)removeDependencyNotesObject:(Note *)value;
- (void)addDependencyNotes:(NSSet *)values;
- (void)removeDependencyNotes:(NSSet *)values;

@end
