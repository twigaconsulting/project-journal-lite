//
//  Media.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Media.h"
#import "Action.h"
#import "Dependency.h"
#import "Issue.h"
#import "Meeting.h"
#import "Project.h"
#import "Risk.h"
#import "Stakeholder.h"


@implementation Media

@dynamic creationDate;
@dynamic deleted;
@dynamic lastModifiedDate;
@dynamic mediaURL;
@dynamic title;
@dynamic type;
@dynamic uuid;
@dynamic inAction;
@dynamic inDependency;
@dynamic inIssue;
@dynamic inMeeting;
@dynamic inProject;
@dynamic inRisk;
@dynamic inStakeholder;

@end
