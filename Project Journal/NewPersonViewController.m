//
//  NewPersonViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 11/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "NewPersonViewController.h"

@interface NewPersonViewController ()<UITextFieldDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addButton;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) UIAlertView *alert;
@end

@implementation NewPersonViewController

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if(self.firstNameTextfield == textField){
        self.firstName = self.firstNameTextfield.text;
    }
    if(self.lastNameTextField ==textField){
        self.lastName = self.lastNameTextField.text;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    if(self.alert){
        [self.alert dismissWithClickedButtonIndex:0 animated:NO];
        self.alert = nil;
        
    }
}


- (IBAction)addToContact {
    [self.lastNameTextField resignFirstResponder];
    [self.firstNameTextfield resignFirstResponder];
    if([self.firstName isEqualToString:@""] && [self.lastName isEqualToString:@""]){
        if(!self.alert){
       self.alert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        }
        [self.alert show];
    }
    else { 
        [self.delegate personFirstName:self.firstName secondName:self.lastName andController:self];
    }
    }
    
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex ==0){
        [self.delegate dismissNewPersonViewcontroller:self];
    }
  }
        
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.lastName = @"";
    self.firstName = @"";
	self.firstNameTextfield.delegate = self;
    self.lastNameTextField.delegate = self;
    [self.firstNameTextfield becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
