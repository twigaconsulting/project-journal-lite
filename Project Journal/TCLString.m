//
//  TCLString.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "TCLString.h"
#import "Meeting.h"


@implementation TCLString

@dynamic string;
@dynamic inMeetingAgenda;
@dynamic inMeetingAttendees;
@dynamic inMeetingPresent;

@end
