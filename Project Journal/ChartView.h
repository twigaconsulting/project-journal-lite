//
//  ChartView.h
//  Project Journal
//
//  Created by Peter Pomlett on 09/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChartView : UIView
@property(assign, nonatomic) float amber;
@property(assign, nonatomic) float blue;
@property(assign, nonatomic) float red;
@property(assign, nonatomic) float green;
@end
