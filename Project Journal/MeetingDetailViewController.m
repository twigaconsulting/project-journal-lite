//
//  MeetingDetailViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 03/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "HelpViewController.h"
#import "MeetingDetailViewController.h"
#import "MeetingTimeViewController.h"
@interface MeetingDetailViewController ()<UITextFieldDelegate, UITextViewDelegate, MeetingTimeViewControllerDelegate, UIActionSheetDelegate, UIPopoverControllerDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) DataHelper *helper;
@property (strong, nonatomic) UIStoryboardPopoverSegue* popSegue;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextView *summaryTextView;
@property (weak, nonatomic) IBOutlet UILabel *startDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *calendarLabel;
@property (weak, nonatomic) IBOutlet UIImageView *locationImageView;
@property (weak, nonatomic) IBOutlet UILabel *IDLabel;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *summaryImageView;
@property (weak, nonatomic) IBOutlet UIImageView *calendarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *startImageView;
@property (weak, nonatomic) IBOutlet UIButton *calendarEventButton;
@property (weak, nonatomic) IBOutlet UITextField *locationTextField;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *helpButtons;
@property (strong, nonatomic) HelpViewController *helpView;
@property (strong, nonatomic) UIPopoverController *helpPopover;
@property (nonatomic, strong) NSDictionary *helpList;
@property (weak, nonatomic) UIActionSheet *calEvent;
@property (weak, nonatomic) IBOutlet UIImageView *activeTitleFrame;
@property (weak, nonatomic) IBOutlet UIImageView *activeSummaryFrame;
@property (weak, nonatomic) IBOutlet UIImageView *activeLocFrame;
- (NSDateFormatter *)formatterDate;

@end

@implementation MeetingDetailViewController

@synthesize eventsList, eventStore, defaultCalendar, detailViewController;


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
    
    
}


- (IBAction)helpTitle:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"meeting.title"] from:sender];
}
- (IBAction)helpSummary:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"meeting.summary"] from:sender];
}
- (IBAction)helpLocation:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"meeting.location"] from:sender];
}
- (IBAction)helpTime:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"meeting.dateTime"] from:sender];
}
- (IBAction)helpCalender:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"meeting.addToCalendar"] from:sender];
}

-(void)loadTheHelpPlist{
    if(self.helpList == nil){
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"MeetingHelp" ofType:@"plist"];
        self.helpList = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    }
}

- (void)checkHelpButtons {
    if(![self.helper showHelp]){
          for (UIButton *button in self.helpButtons) {
            button.hidden = YES;
          }
    }
}


- (void)showHelp:(NSString*)helpString from:(UIButton *)helpButton {
    [self.delegate dimSuper];
    if(self.helpView == nil) {
        self.helpView = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    }
    UITextView *_textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 250, 10)];
    _textView.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
    _textView.textColor = [UIColor lightGrayColor];
    _textView.scrollEnabled = NO;
    _textView.userInteractionEnabled=NO;
    _textView.text =  helpString;
    CGRect frame = _textView.frame;
    frame.size.height =  [self getTextHeight:_textView];
    _textView.frame = frame;
    self.helpView.view.frame = frame;
    [self.helpView.view addSubview:_textView];
    self.helpPopover = [[UIPopoverController alloc] initWithContentViewController:self.helpView];
    self.helpPopover.popoverContentSize =_textView.frame.size;
    [self.helpPopover presentPopoverFromRect:helpButton.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionDown animated:YES];
    self.helpPopover.delegate = self;
}

- (CGFloat)getTextHeight:(UITextView *)textView
{
    NSLayoutManager *lm = textView.layoutManager;
    CGRect boundingRect = [lm usedRectForTextContainer:lm.textContainers[0]];
    CGFloat height = boundingRect.size.height + (textView.textContainerInset.top + textView.textContainerInset.bottom);
    return height;
}

- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (void)dismissPopovers{
    [self.delegate UndimSuper];
    if ([self.popSegue.popoverController isPopoverVisible])
    {
        [self.popSegue.popoverController dismissPopoverAnimated:NO];
    }
    if ([self.helpPopover isPopoverVisible])
    {
        [self.helpPopover dismissPopoverAnimated:NO];
    }
    if (self.calEvent.isVisible) {
        [self.calEvent dismissWithClickedButtonIndex:self.calEvent.cancelButtonIndex animated:NO];
        self.calEvent = nil;
    }
}

- (IBAction)calendarEvent:(UIButton *)sender {
    [self.view endEditing:YES];
    [self.delegate dimSuper];
    if (self.accessGranted)
    {
    UIActionSheet *calSheet;
    if (!calSheet){
        calSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                  delegate:self
                                         cancelButtonTitle:nil
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"Add to Calendar",nil];
    }
    [calSheet showFromRect:sender.frame inView:self.view  animated:YES];
    self.calEvent = calSheet;
    
    }
    else{
      
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Access Calendar"
                                                              message:@"To allow Project Journal access to your calendar go to the settings app, Privacy, Calendar and Project Journal "
                                                             delegate:self
                                                         cancelButtonTitle:nil
                                                    otherButtonTitles:@"Done",nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self.delegate UndimSuper];
}




- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self.delegate UndimSuper];
    if(buttonIndex == 0){
        
        self.calendarEventButton.hidden = YES;
        self.calendarLabel.text = @"Calendar Event Set";
        

        
        if (self.accessGranted) {
            // Retrieve the calendar event id if saved already
            if (self.detailMeeting.ekEvent) {
                self.event = [eventStore eventWithIdentifier:self.detailMeeting.ekEvent];
                self.detailMeeting.ekEvent = self.event.eventIdentifier;
            } else {
                self.event = [EKEvent eventWithEventStore:eventStore];
                
                if (self.defaultCalendar ==  self.event.calendar) {
                    [self.eventsList addObject:self.event];
                }
                
            }
            
            // Save the event to the default calendar
            self.event.title = self.detailMeeting.title;
            self.event.location = self.detailMeeting.location;
            self.event.notes = self.detailMeeting.summary;
            
            self.event.startDate = self.detailMeeting.startDateTime;
            self.event.endDate = self.detailMeeting.endDateTime;
            // Need to set the allDay flag depending on the start & end times
            if ([self.detailMeeting.endDateTime timeIntervalSinceDate:self.detailMeeting.startDateTime] >= 86400) {
                self.event.allDay = YES;
            } else {
                self.event.allDay = NO;
            }
            
            // Save the EKEvent id
            self.detailMeeting.ekEvent = self.event.eventIdentifier;
            
            
            
            [self.event setCalendar:[self.eventStore defaultCalendarForNewEvents]];
           // NSError *err;
            [self.eventStore saveEvent:self.event span:EKSpanThisEvent error:nil];
            
            self.detailMeeting.ekEvent = self.event.eventIdentifier;
            
            [self.helper saveContext];
        }
        
        self.eventsList = [[NSMutableArray alloc] initWithArray:0];
        
        // Get the default calendar from store.
        self.defaultCalendar = [self.eventStore defaultCalendarForNewEvents];
        
    }}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{[self.view endEditing:YES];
    [self.delegate dimSuper];
    if ([[segue identifier] isEqualToString:@"Date"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setStartDate:self.detailMeeting.startDateTime];
        [[segue destinationViewController] setEndDate:self.detailMeeting.endDateTime];
        self.popSegue.popoverController.delegate =self;
        }}

-(void)pickedStartTime:(NSDate *)sTime endTime:(NSDate *)eTime{
    self.detailMeeting.startDateTime = sTime;
    self.detailMeeting.endDateTime = eTime;
    [self.helper saveContext];
     self.startDateTimeLabel.text = [[self formatterDate] stringFromDate:self.detailMeeting.startDateTime];
    self.endDateTimeLabel.text = [[self formatterDate] stringFromDate:self.detailMeeting.endDateTime];
    if ([self.popSegue.popoverController isPopoverVisible])
    {[self.delegate UndimSuper];
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
        
    }
}

-(void)meetingTimeViewControllerDidFinish:( MeetingTimeViewController *)controller{
    if ([self.popSegue.popoverController isPopoverVisible])
    {[self.delegate UndimSuper];
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (self.titleTextField == textField){
        if ([self.detailMeeting.title isEqualToString:self.titleTextField.text]==NO){
            self.detailMeeting.title = self.titleTextField.text;
            self.detailMeeting.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
        if ([self.detailMeeting.title isEqualToString:@""]){
            self.detailMeeting.title = @"Untitled Meeting";
            self.titleTextField.text = self.detailMeeting.title;
            [self.helper saveContext];
        }
    }
    if (self.locationTextField == textField){
        if ([self.detailMeeting.location isEqualToString:self.locationTextField.text]==NO){
            self.detailMeeting.location = self.locationTextField.text;
            self.detailMeeting.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (self.summaryTextView == textView){
        if ([self.detailMeeting.summary isEqualToString:self.summaryTextView.text]==NO){
            self.detailMeeting.summary = self.summaryTextView.text;
            self.detailMeeting.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
    }
}

- (IBAction)calendarEvent {
    //  Calendar event code
    // When add button is pushed, create an EKEventEditViewController to display the event.
	EKEventEditViewController *addController = [[EKEventEditViewController alloc] initWithNibName:nil bundle:nil];
	
	// set the addController's event store to the current event store.
	addController.eventStore = self.eventStore;
    
    
    // Prep the Calendar event with Meeting title & Summary
    EKEvent *meetingEvent = [EKEvent eventWithEventStore:self.eventStore];
    addController.event = meetingEvent;
    addController.event.title = self.detailMeeting.title;
    addController.event.notes = self.detailMeeting.summary;
                             
	// present EventsAddViewController as a modal view controller
	[self presentViewController:addController animated:YES completion:nil];
	
	addController.editViewDelegate = self;
    
}

-(void)checkForCalendarEvent{
    if ([self.detailMeeting isInCalendar] ) {
        self.calendarEventButton.hidden = YES;
        self.calendarLabel.text = @"Calendar Event Set";
    }
    else{
        self.calendarEventButton.hidden = NO;
        self.calendarLabel.text = @"No Calendar Event Set";
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.titleTextField.text = self.detailMeeting.title;
    if([self.titleTextField.text isEqualToString:@""]){
        [self.titleTextField becomeFirstResponder];
    }
    self.summaryTextView.text = self.detailMeeting.summary;
    self.IDLabel.text = self.detailMeeting.meetingID;
    self.locationTextField.text =self.detailMeeting.location;
  self.startDateTimeLabel.text = [[self formatterDate] stringFromDate:self.detailMeeting.startDateTime];
    self.endDateTimeLabel.text = [[self formatterDate] stringFromDate:self.detailMeeting.endDateTime];
    [self checkForCalendarEvent];
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    UIEdgeInsets tabInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage *textBack = [UIImage imageNamed:@"TextFrame2"];
    textBack = [textBack resizableImageWithCapInsets:tabInsets];
    UIImage* summary = [UIImage imageNamed:@"sumFrame"];
    summary = [summary imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ];
    UIImage* title = [UIImage imageNamed:@"titleframe"];
    title = [title imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ];
    self.activeTitleFrame.image = title;
    self.activeSummaryFrame.image = summary;
    self.activeLocFrame.image = title;
    
    
    self.titleImageView.image = textBack;
    self.startImageView.image = textBack;
    self.locationImageView.image = textBack;
    self.calendarImageView.image = textBack;
    self.summaryImageView.image = textBack;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    self.titleTextField.delegate = self;
    self.summaryTextView.delegate = self;
    self.locationTextField.delegate = self;
    [self checkHelpButtons];
    
    // Check access to the calendar
    self.eventStore = [[EKEventStore alloc] init];
    
    [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        self.accessGranted = granted;
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark EKEventEditViewDelegate

// CALENDER METHODS & DELEGATES
// Overriding EKEventEditViewDelegate method to update event store according to user actions.
// This is essentially a dummy method to satisfy the delegate method
- (void)eventEditViewController:(EKEventEditViewController *)controller
          didCompleteWithAction:(EKEventEditViewAction)action {
	
	switch (action) {
		case EKEventEditViewActionCanceled:
			// Edit action canceled, do nothing.
			break;
			
		case EKEventEditViewActionSaved:
			// When user hit "Done" button, save the newly created event to the event store,
			// and reload table view.
			// If the new event is being added to the default calendar, then update its
			// eventsList.
						
			break;
			
		case EKEventEditViewActionDeleted:
			// When deleting an event, remove the event from the event store,
			// and reload table view.
			// If deleting an event from the currenly default calendar, then update its
			// eventsList.
			if (self.defaultCalendar ==  self.event.calendar) {
				[self.eventsList removeObject:self.event];
			}
            
			break;
		default:
			break;
	}
	// Dismiss the modal view controller
	[controller dismissViewControllerAnimated:YES completion:nil];
	
}


// Set the calendar edited by EKEventEditViewController to our chosen calendar - the default calendar.
- (EKCalendar *)eventEditViewControllerDefaultCalendarForNewEvents:(EKEventEditViewController *)controller {
	EKCalendar *calendarForEdit = self.defaultCalendar;
	return calendarForEdit;
}


@end
