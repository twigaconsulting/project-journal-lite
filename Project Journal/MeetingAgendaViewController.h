//
//  MeetingAgendaViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 03/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h" 


@protocol MeetingAgendaViewControllerDelegate <NSObject>

-(void)updateAgendaBadge;
-(void) dimSuper;
-(void) UndimSuper;
@end

@interface MeetingAgendaViewController : UIViewController
@property (weak, nonatomic) id <MeetingAgendaViewControllerDelegate> delegate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property(strong, nonatomic) Meeting *meeting;
@property (strong,nonatomic) DataHelper *helper;
@property (strong,nonatomic) AgendaItem *agendaItem;
- (void)dismissPopovers;

@end
