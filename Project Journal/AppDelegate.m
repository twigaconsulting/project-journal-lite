//
//  AppDelegate.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 18/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "AppDelegate.h"

//#import "MasterViewController.h"
#import "DataHelper.h"

@class DataHelper;

@implementation AppDelegate




- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
 //  self.window.tintColor = [UIColor colorWithRed:52.0f/255.0f green:73.0f/255.0f blue:94.0f/255.0f alpha:1.0f];
    // Initialise the Data Helper instance to be used through the application
    self.helper = [DataHelper sharedInstance];
    
  ///  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
    ///    UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
     ///   UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
    ///    splitViewController.delegate = (id)navigationController.topViewController;
        
        //UINavigationController *masterNavigationController = splitViewController.viewControllers[0];
        //MasterViewController *controller = (MasterViewController *)masterNavigationController.topViewController;
        //controller.managedObjectContext = self.helper.managedObjectContext;
        
        // Allocate the helper instnce to the coontroller
        //controller.helper = self.helper;
  ///  } else {
        //UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
        //MasterViewController *controller = (MasterViewController *)navigationController.topViewController;
        //controller.managedObjectContext = self.helper.managedObjectContext;
        
        // Allocate the helper instnce to the coontroller
        //controller.helper = self.helper;
 ///   }
    
    NSString *defaultPrefsFile = [[NSBundle mainBundle] pathForResource:@"defaultPrefs" ofType:@"plist"];
    NSDictionary *defaultPreferences = [NSDictionary dictionaryWithContentsOfFile:defaultPrefsFile];
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultPreferences];


    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self.helper saveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self.helper saveContext];
}
/*
-(BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
    return YES;
}

-(BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
    return YES;
}
 */
@end
