//
//  Person.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 18/07/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Person.h"
#import "Meeting.h"


@implementation Person

@dynamic firstName;
@dynamic surname;
@dynamic email;
@dynamic inMeetingAttendee;
@dynamic inMeetingPresent;

@end
