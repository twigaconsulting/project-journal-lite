//
//  CurrencyPadViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 20/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "CurrencyPadViewController.h"

@interface CurrencyPadViewController ()

@property (weak, nonatomic) IBOutlet UILabel *displayLabel;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
//@property (weak, nonatomic) IBOutlet UIButton *enterButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *enterButton;
//
@property (strong, nonatomic) NSString *displayString;
@property (strong, nonatomic) NSString *holdingString;
@property (strong, nonatomic) NSNumberFormatter *formatDisplay;
@property (strong, nonatomic) UIImage *buttonImage;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end

@implementation CurrencyPadViewController

#pragma mark - setters
-(NSNumberFormatter *)formatDisplay {
    if (_formatDisplay == nil) {
        _formatDisplay = [[NSNumberFormatter alloc] init];
        [_formatDisplay setNumberStyle:NSNumberFormatterDecimalStyle];
        [_formatDisplay setLocale:[NSLocale currentLocale]];
    }
    return _formatDisplay;
}

-(NSString *)holdingString {
    if (_holdingString == nil) {
        _holdingString = [[NSString alloc] init];
    }
    return _holdingString;
}

#pragma mark - IBActions keys
- (IBAction)DigitPressed:(UIButton *)sender {
    NSString *digit = [[sender titleLabel] text];
    self.displayString = [self.holdingString stringByAppendingString:digit];
    self.displayLabel.text =  [self.formatDisplay stringFromNumber:[self.formatDisplay numberFromString:self.displayString]];
    self.holdingString = self.displayString;
    if([self.holdingString isEqualToString:@""]){
        self.clearButton.enabled = NO;
        self.enterButton.enabled = NO;
    }
    else{
        self.clearButton.enabled = YES;
        self.enterButton.enabled = YES;
    }
}

- (IBAction)clearKey {
    if([self.displayString length]>=1){
        self.displayString = [self.displayString substringToIndex:[self.displayString length]-1];
        self.displayLabel.text =  [self.formatDisplay stringFromNumber:[self.formatDisplay numberFromString:self.displayString]];
        self.holdingString = self.displayString;
        if([self.holdingString isEqualToString:@""]){
            self.clearButton.enabled = NO;
            self.enterButton.enabled = NO;
        }
        else{
            self.clearButton.enabled = YES;
            self.enterButton.enabled = YES;
        }
    }
}

- (IBAction)EnterKey {
    [self.delegate Currency: [self.formatDisplay numberFromString:self.displayString]];
    [self.delegate CurrencyPadViewControllerDidFinish:self];
}

- (IBAction)Cancel {
    [self.delegate CurrencyPadViewControllerDidFinish:self];
}

#pragma mark - View controller methods
- (void)viewDidLoad {
    [super viewDidLoad];
    self.clearButton.enabled = NO;
    self.enterButton.enabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
