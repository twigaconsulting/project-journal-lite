//
//  NewPersonViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 11/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NewPersonViewController;
@protocol NewPersonViewcontrollerDelegate <NSObject>

-(void) dismissNewPersonViewcontroller:(NewPersonViewController *)controller;
-(void) personFirstName:(NSString *)first secondName:(NSString *)second andController:(NewPersonViewController *)controller;

@end

@interface NewPersonViewController : UIViewController
@property (weak, nonatomic) id <NewPersonViewcontrollerDelegate> delegate;
@end
