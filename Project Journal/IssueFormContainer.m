//
//  ItemFormViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "PJColour.h"

#import "IssueFormContainer.h"
#import "IssueDetailViewController.h"
#import "IssueDateViewController.h"
#import "ActionsListViewController.h"
#import "ListNotesViewController.h"
@interface IssueFormContainer ()< ActionsListViewControllerDelegate, NoteListViewControllerDelegate, IssueDateViewControllerDelegate, IssueDetailViewControllerDelegate, UIActionSheetDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *container;
@property (strong, nonatomic)ListNotesViewController *notesFormViewController;
@property (strong, nonatomic)IssueDetailViewController *issueDetailViewController;
@property (strong, nonatomic)IssueDateViewController *issueDateViewController;
@property (strong, nonatomic)ActionsListViewController *issueActionsViewController;

@property (weak, nonatomic) IBOutlet UIImageView *barImageView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UIButton *datesButton;
@property (weak, nonatomic) IBOutlet UIButton *notesButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (strong, nonatomic) DataHelper *helper;
@property (strong,nonatomic) NSString *key;
@property (strong,nonatomic) UIActionSheet *sendSheet;
@property (strong,nonatomic) PJColour *projectColour;
@property (strong,nonatomic) UILabel *noteCount;
@property (strong,nonatomic) UILabel *actionCount;
@property (strong,nonatomic) UIImageView *imageComplete;
@property (strong,nonatomic) UIImageView *imageReview;
@property (strong,nonatomic) UIImageView *image;
@property (strong,nonatomic) UIAlertView *deleteAlert;

@end

@implementation IssueFormContainer

-(void) dimSuper{
    self.view.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
}
-(void) UndimSuper{
    self.view.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
}


-(void)getNewAction:(id)itenAction{
    [self.delegate getNewAction:itenAction];
}

-(void)getAction:(Action*)itenAction{
    [self.delegate getTheActionForm:itenAction];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self.issueDateViewController dismissPopovers];
    [self.notesFormViewController dismissPopovers];
    [self.issueDetailViewController dismissPopovers];
    if (self.sendSheet.isVisible) {
         [self UndimSuper];
        [self.sendSheet dismissWithClickedButtonIndex:self.sendSheet.cancelButtonIndex animated:NO];
        self.sendSheet = nil;
    }
    if (self.deleteAlert.isVisible) {
        [self.deleteAlert dismissWithClickedButtonIndex:0 animated:NO];
        self.deleteAlert = nil;
    }

    
}

- (BOOL)disablesAutomaticKeyboardDismissal{
    return NO;
}

#pragma mark - toolbar action sheet methods
-(void)createSendSheet{
    if(!self.sendSheet){
    self.sendSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                 delegate:self
                                        cancelButtonTitle:nil
                                   destructiveButtonTitle:nil
                                        otherButtonTitles:@"Print",@"Email Text",@"Email Attachment",nil];
    }
    [self.sendSheet showFromRect:self.sendButton.frame inView:self.view animated:YES];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{ [self UndimSuper];
    if (buttonIndex == 0) {
        // Check if printing is avaliable
        if ([UIPrintInteractionController isPrintingAvailable]) {
            UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
            pic.delegate = self;
            
            UIPrintInfo *printInfo = [UIPrintInfo printInfo];
            printInfo.outputType = UIPrintInfoOutputGeneral;
            printInfo.jobName = self.containerIssue.title;
            pic.printInfo = printInfo;
            
            UIMarkupTextPrintFormatter *textFormatter = [[UIMarkupTextPrintFormatter alloc] initWithMarkupText:self.containerIssue.getDetailHTML];
            textFormatter.startPage = 0;
            textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
            textFormatter.maximumContentWidth = 6 * 72.0;
            pic.printFormatter = textFormatter;
            pic.showsPageRange = YES;
            [pic presentFromRect:self.sendButton.frame inView:self.view animated:YES completionHandler:nil];
            //  [pic presentAnimated:YES completionHandler:nil];
        }
        
    }
    
    if(buttonIndex == 1){
        // Email with detail text
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.containerIssue.title forKey:@"subject"];
        [data setValue:self.containerIssue.getDetailHTML forKey:@"body"];
        
        [data setValue:nil forKey:@"fileName"];
        
        
        [self displayMailComposeSheet:data];
        
    }
    
    if(buttonIndex == 2){
        //Email with attachement
        XMLWriter *writer = [[XMLWriter alloc] init];
        NSString *xmlString = [writer writeXMLForExcel:self.containerIssue];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //make a file name to write the data to using the documents directory:
        NSString *theFileName = [NSString stringWithFormat:@"%@/%@.xls",
                                 documentsDirectory,self.containerIssue.issueID];

        
        NSString *shortFileName = [NSString stringWithFormat:@"%@.xls",self.containerIssue.issueID];
        
        [xmlString writeToFile:theFileName
                    atomically:NO
                      encoding:NSUTF8StringEncoding
                         error:nil];
        
        //Setup email
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.containerIssue.title forKey:@"subject"];
        [data setValue:@"Issue attachment for Microsoft Excel" forKey:@"body"];
        [data setValue:[NSData dataWithContentsOfFile:theFileName] forKey:@"attachmentData"];
        [data setValue:[NSString stringWithFormat:@"%@",shortFileName] forKey:@"fileName"];
        
        [self displayMailComposeSheet:data];
        
        [[NSFileManager defaultManager] removeItemAtPath:theFileName error:nil];
    }
    
}

#pragma mark - button badge methods
//date view delegate method
-(void) upDateRagBadge{
    [self createCompletedBadge];
    [self createDateRagBadge];
}
//detail view delegate method
- (void) upDateBadge{
    [self createCompletedBadge];
    [self createReviewBadge];
    [self createDateRagBadge];
}
//date view delegate
-(void) upDateReviewBadge{
    
    [self createReviewBadge];
}
//Action list Delegate method
-(void)updateActionBadge{
    [self createActionBadge];
}

-(void)createDateRagBadge{
    if(!self.image){
        self.image = [[UIImageView alloc] initWithFrame:CGRectMake(23, 32,11, 11)];
        [self.datesButton addSubview:self.image];
    }
    NSString *ragColour= [self.containerIssue fetchDateRAGStatus];
    if([ragColour isEqualToString:@"Amber"]){
        self.image.image = [UIImage imageNamed:@"AmberBadje"];
    }
    if([ragColour isEqualToString:@"Red"]){
        self.image.image = [UIImage imageNamed:@"redBadje"];
    }
    if([ragColour isEqualToString:@"Green"]){
        self.image.image = [UIImage imageNamed:@"greenBadge"];
    }
    if([ragColour isEqualToString:@"Blue"]){
        self.image.image = [UIImage imageNamed:@"blueBadje"];
    }
}

-(void)createCompletedBadge{
    if(!self.imageComplete){
        self.imageComplete = [[UIImageView alloc] initWithFrame:CGRectMake(29, 32,11, 11)];
        [self.detailButton addSubview:self.imageComplete];
    }
    
    if([self.containerIssue.status isEqualToString:@"Completed"]) {
        self.imageComplete.image = [UIImage imageNamed:@"blueBadje"];
    }
    else{
        self.imageComplete.image = [UIImage imageNamed:@"noBadje"];
    }
}

-(void)createNotesBadgeIfNeeded{
    int noteCount = [[self.helper fetchNotesList:self.containerIssue]count];
    
    if(!self.noteCount){
        self.noteCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, self.notesButton.frame.size.width, 10)];
        self.noteCount.backgroundColor = [UIColor clearColor];
        self.noteCount.textAlignment = NSTextAlignmentCenter;
        self.noteCount.numberOfLines=1;
        [self.noteCount setFont:[UIFont systemFontOfSize:12]];
        self.noteCount.textColor = [UIColor darkGrayColor];
        [self.notesButton addSubview:self.noteCount];
    }
    
    if(noteCount > 0){
        self.noteCount.textColor = [UIColor darkGrayColor];
    }
    else {
        self.noteCount.textColor = [UIColor lightGrayColor];
    }
    self.noteCount.text = [NSString stringWithFormat:@"%d",noteCount];
    }
    


-(void)createActionBadge{
    int actionCount = [[self.containerIssue issueActions ]count];
    if (!self.actionCount) {
        self.actionCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, self.actionButton.frame.size.width, 10)];
        self.actionCount.backgroundColor = [UIColor clearColor];
        self.actionCount.textAlignment = NSTextAlignmentCenter;
        self.actionCount.numberOfLines=1;
        [self.actionCount setFont:[UIFont systemFontOfSize:12]];
        
        [self.actionButton addSubview:self.actionCount];
    }
    if(actionCount > 0){
        self.actionCount.textColor = [UIColor darkGrayColor];
    }
    else {
        self.actionCount.textColor = [UIColor lightGrayColor];
    }
    self.actionCount.text = [NSString stringWithFormat:@"%d",actionCount];
}

- (void) createReviewBadge{
    if(!self.imageReview){
        self.imageReview = [[UIImageView alloc] initWithFrame:CGRectMake(35, 32,11, 11)];
        [self.datesButton addSubview:self.imageReview];
    }
    if(self.containerIssue.isForReview){
        self.imageReview.image = [UIImage imageNamed:@"reviewBadjge"];
    }
    else{
        self.imageReview.image = [UIImage imageNamed:@"noReviewBadge"];
    }
}

-(void)enableButtons
{
    self.detailButton.enabled =YES;
    self.notesButton.enabled =YES;
    self.datesButton.enabled =YES;
    self.actionButton.enabled =YES;
}

- (IBAction)Detail
{
    if(self.issueDetailViewController == nil) {
        self.issueDetailViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"IssueDetail"];
        self.issueDetailViewController.detailIssue = self.containerIssue;
        self.issueDetailViewController.delegate = self;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.issueDetailViewController and:@"Detail"];
    
    self.detailButton.enabled = NO;
}

- (IBAction)Dates
{
    if(self.issueDateViewController == nil) {
        self.issueDateViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"IssueDate"];
        self.issueDateViewController.issue = self.containerIssue;
        self.issueDateViewController.delegate = self;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.issueDateViewController and:@"Date"];
    self.datesButton.enabled = NO;
}

- (IBAction)Notes
{
    if(self.notesFormViewController == nil) {
        self.notesFormViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"NotesTable"];
        self.notesFormViewController.item =  self.containerIssue;
        self.notesFormViewController.delegate = self;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.notesFormViewController and:@"Note"];
    self.notesButton.enabled = NO;
}

- (IBAction)Actions
{
    if(self.issueActionsViewController == nil) {
        self.issueActionsViewController= [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"MAction"];
        self.issueActionsViewController.item = self.containerIssue;
        self.issueActionsViewController.delegate = self;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.issueActionsViewController and:@"Action"];
    self.actionButton.enabled = NO;
}

//Use a key to keep track of what comtroller is loaded
-(void)SwapOldControllerForNew:(UIViewController*)controller and:(NSString *)aKey
{
    if([self.key isEqualToString:@"Detail"]){
        [self.issueDetailViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.issueDetailViewController.view removeFromSuperview]; [self.container addSubview:controller.view]; }
                        completion:^(BOOL finished) {
                            [self.issueDetailViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
    
    else if([self.key isEqualToString:@"Action"]){
        [self.issueActionsViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.issueActionsViewController.view removeFromSuperview]; [self.container addSubview:controller.view]; }
                        completion:^(BOOL finished) {
                            [self.issueActionsViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
    
    else if([self.key isEqualToString:@"Date"])
    { [self.issueDateViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.issueDateViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.issueDateViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
    
    else if([self.key isEqualToString:@"Note"]){
        [self.notesFormViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.notesFormViewController.view removeFromSuperview]; [self.container addSubview:controller.view]; }
                        completion:^(BOOL finished) {
                            [self.notesFormViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)refreshFromBackground{
    [self createReviewBadge];
    [self createDateRagBadge];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self createCompletedBadge];
    [self createNotesBadgeIfNeeded];
    [self createActionBadge];
    [self createReviewBadge];
    [self createDateRagBadge];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    
    if(self.projectColour == nil){
        self.projectColour = [[PJColour alloc]init];
    }
    //create new issue if it is nil
    if (self.containerIssue == nil){
        self.containerIssue = [self.helper newIssue:self.project] ;
    }
    //add detail controller when view loads
    if(self.issueDetailViewController == nil){
        self.issueDetailViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"IssueDetail"];
        self.issueDetailViewController.detailIssue = self.containerIssue;
        self.issueDetailViewController.delegate = self;
    }
    [self addChildViewController:self.issueDetailViewController];
    [self.container addSubview:self.issueDetailViewController.view];
    [self.issueDetailViewController didMoveToParentViewController:self];
    self.key =@"Detail";
    [self enableButtons];
    self.detailButton.enabled = NO;
    
}

-(void) UpdateNotesBadge{
    [self createNotesBadgeIfNeeded];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)Send {
    [self.view endEditing:YES];
    [self dimSuper];
    [self createSendSheet];
}


- (IBAction)Delete {
    [self.view endEditing:YES];
    [self dimSuper];
     if(!self.deleteAlert){
    self.deleteAlert = [[UIAlertView alloc] initWithTitle:@"Delete Issue"
                                                          message:@"Deleting this issue will delete all of its actions and notes "
                                                         delegate:self                                          cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Delete",nil];
     }
    [self.deleteAlert show];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self UndimSuper];
    if(buttonIndex == 1){
        [self.helper deleteObject:self.containerIssue];
        
        [self.helper saveContext];
        [self.delegate IssueFormContainerDidFinish:self];
        
        
    }}

- (IBAction)Done {
    [self.delegate IssueFormContainerDidFinish:self];
}


@end
