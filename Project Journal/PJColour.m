//
//  PJColour.m
//  Project Journal
//
//  Created by Peter Pomlett on 14/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "PJColour.h"

@implementation PJColour


-(UIColor *)sea{
    _sea = [UIColor colorWithRed:22.0f/255.0f green:160.0f/255.0f blue:133.0f/255.0f alpha:1.0f];
    return _sea;
}

-(UIColor *)grey{
    _grey = [UIColor colorWithRed:149.0f/255.0f green:165.0f/255.0f blue:166.0f/255.0f alpha:1.0f];
    return _grey;
}


-(UIColor *)darkBlue{
   // _darkBlue = [UIColor colorWithRed:52.0f/255.0f green:73.0f/255.0f blue:94.0f/255.0f alpha:1.0f];
      _darkBlue = [UIColor colorWithRed:149.0f/255.0f green:165.0f/255.0f blue:166.0f/255.0f alpha:1.0f];
    return _darkBlue;
}


-(UIColor *)amber{
    _amber = [UIColor colorWithRed:243.0f/255.0f green:156.0f/255.0f blue:18.0f/255.0f alpha:1.0f];
    return _amber;
}

-(UIColor *)red{
    _red = [UIColor colorWithRed:231.0f/255.0f green:76.0f/255.0f blue:60.0f/255.0f alpha:1.0f];
  
    return _red;
}
-(UIColor *)green{
    _green = [UIColor colorWithRed:39.0f/255.0f green:174.0f/255.0f blue:96.0f/255.0f alpha:1.0f];
    return _green;
}
-(UIColor *)blue{
    _blue = [UIColor colorWithRed:41.0f/255.0f green:128.0f/255.0f blue:185.0f/255.0f alpha:1.0f];
    return _blue;
}

-(UIColor *)purple{
    _purple = [UIColor colorWithRed:142.0f/255.0f green:68.0f/255.0f blue:173.0f/255.0f alpha:1.0f];
    return _purple;
}

-(UIColor *)palePurple{
    _palePurple = [UIColor colorWithRed:222.0f/255.0f green:202.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
    return _palePurple;
}


-(UIColor *)paleBlue{
    _paleBlue = [UIColor colorWithRed:218.0f/255.0f green:230.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
    return _paleBlue;
}

-(UIColor *)paleGreen{
    _paleGreen = [UIColor colorWithRed:214.0f/255.0f green:247.0f/255.0f blue:200.0f/255.0f alpha:1.0f];
    return _paleGreen;
}

-(UIColor *)paleRed{
    _paleRed =  [UIColor colorWithRed:255.0f/255.0f green:221.0f/255.0f blue:227.0f/255.0f alpha:1.0f];
    return _paleRed;
}

-(UIColor *)paleAmber{
    _paleRed =  [UIColor colorWithRed:253.0f/255.0f green:219.0f/255.0f blue:199.0f/255.0f alpha:1.0f];
    return _paleRed;
}


@end
