//
//  Config.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 25/07/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Config.h"


@implementation Config

@dynamic actionReviewDateInterval;
@dynamic amberRAGBudgetPerCent;
@dynamic amberRAGDateInterval;
@dynamic configID;
@dynamic hideCompleted;
@dynamic issuesReviewDateInterval;
@dynamic projectReviewDateInterval;
@dynamic redRAGDateInterval;
@dynamic risksReviewDateInterval;
@dynamic showHelp;
@dynamic projectCount;
@dynamic actionCount;
@dynamic issueCount;
@dynamic riskCount;
@dynamic meetingCount;

@end
