//
//  MeetingFormContainer.m
//  Project Journal
//
//  Created by Peter Pomlett on 03/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "MeetingFormContainer.h"
#import "MeetingDetailViewController.h"
#import "MeetingAttendeeViewController.h"
#import "MeetingAgendaViewController.h"
#import "ListNotesViewController.h"
@interface MeetingFormContainer ()<UITextFieldDelegate, UITextViewDelegate, UIActionSheetDelegate, NoteListViewControllerDelegate,  MeetingAttendeeViewControllerDelegate,MeetingAgendaViewControllerDelegate, MeetingDetailViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *container;
@property (strong, nonatomic)ListNotesViewController *notesFormViewController;
@property (strong, nonatomic)MeetingDetailViewController *meetingDetailViewController;
@property (strong, nonatomic)MeetingAttendeeViewController *meetingAttendeeViewController;
@property (strong, nonatomic)MeetingAgendaViewController *meetingAgendaViewController;
@property (weak, nonatomic) IBOutlet UIImageView *barImageView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UIButton *attendeeButton;
@property (weak, nonatomic) IBOutlet UIButton *agendaButton;
@property (weak, nonatomic) IBOutlet UIButton *notesButton;
@property (strong,nonatomic) UIActionSheet *sendSheet;
@property (strong, nonatomic) DataHelper *helper;
@property (strong,nonatomic) NSString *key;
@property (strong,nonatomic) UILabel *noteCount;
@property (strong,nonatomic) UILabel *agendaCount;
@property (strong,nonatomic) UILabel *inviteCount;
@property (strong,nonatomic) UIAlertView *deleteAlert;

-(void)SwapOldControllerForNew:(UIViewController *)controller and:(NSString *)aKey;

@end

@implementation MeetingFormContainer

-(void) dimSuper{
    self.view.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
}
-(void) UndimSuper{
    self.view.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self.meetingAgendaViewController dismissPopovers];
    [self.notesFormViewController dismissPopovers];
    [self.meetingAttendeeViewController dismissPopovers];
     [self.meetingDetailViewController dismissPopovers];
    if (self.sendSheet.isVisible) {
         [self UndimSuper];
        [self.sendSheet dismissWithClickedButtonIndex:self.sendSheet.cancelButtonIndex animated:NO];
        self.sendSheet = nil;
    }
    if (self.deleteAlert.isVisible) {
        [self.deleteAlert dismissWithClickedButtonIndex:0 animated:NO];
        self.deleteAlert = nil;
    }
}

-(void)updateAgendaBadge{
    [self createAgendaBadge];
}

-(void)updateAttendeeBadge{
    [self createInviteBadge];
}

-(void)createNotesBadgeIfNeeded{
    int noteCount = [[self.helper fetchNotesList:self.containerMeeting]count];
    
    if(!self.noteCount){
        self.noteCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, self.notesButton.frame.size.width, 10)];
        self.noteCount.backgroundColor = [UIColor clearColor];
        self.noteCount.textAlignment = NSTextAlignmentCenter;
        self.noteCount.numberOfLines=1;
        [self.noteCount setFont:[UIFont systemFontOfSize:12]];
        self.noteCount.textColor = [UIColor darkGrayColor];
        [self.notesButton addSubview:self.noteCount];
    }
    if(noteCount > 0){
        self.noteCount.textColor = [UIColor darkGrayColor];
    }
    else {
        self.noteCount.textColor = [UIColor lightGrayColor];
    }
    self.noteCount.text = [NSString stringWithFormat:@"%d",noteCount];
}

-(void)createAgendaBadge{

    int agenCount = [[self.containerMeeting meetingAgenda]count];
    
    if(!self.agendaCount){
        self.agendaCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, self.agendaButton.frame.size.width, 10)];
        self.agendaCount.backgroundColor = [UIColor clearColor];
        self.agendaCount.textAlignment = NSTextAlignmentCenter;
        self.agendaCount.numberOfLines=1;
        [self.agendaCount setFont:[UIFont systemFontOfSize:12]];
        self.agendaCount.textColor = [UIColor darkGrayColor];
        [self.agendaButton addSubview:self.agendaCount];
    }
    if(agenCount > 0){
        self.agendaCount.textColor = [UIColor darkGrayColor];
    }
    else {
        self.agendaCount.textColor = [UIColor lightGrayColor];
    }
    self.agendaCount.text = [NSString stringWithFormat:@"%d",agenCount];
    
}

-(void)createInviteBadge{

    int invCount = [[self.containerMeeting meetingAttendees]count];
    
    if(!self.inviteCount){
        self.inviteCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, self.attendeeButton.frame.size.width, 10)];
        self.inviteCount.backgroundColor = [UIColor clearColor];
        self.inviteCount.textAlignment = NSTextAlignmentCenter;
        self.inviteCount.numberOfLines=1;
        [self.inviteCount setFont:[UIFont systemFontOfSize:12]];
        self.inviteCount.textColor = [UIColor darkGrayColor];
        [self.attendeeButton addSubview:self.inviteCount];
    }
    if(invCount > 0){
        self.inviteCount.textColor = [UIColor darkGrayColor];
    }
    else {
        self.inviteCount.textColor = [UIColor lightGrayColor];
    }
    self.inviteCount.text = [NSString stringWithFormat:@"%d",invCount];
    
}

@synthesize currentView,toRecipients,ccRecipients,bccRecipients;
@synthesize subject,body,fileName,fileType,mimeType,picker;


- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

-(void)createSendSheet{
    self.sendSheet = [[UIActionSheet alloc] initWithTitle:nil
                                             delegate:self
                                    cancelButtonTitle:nil
                               destructiveButtonTitle:nil
                                    otherButtonTitles:@"print",@"email text",@"email attachment",nil];
    [self.sendSheet showFromRect:self.sendButton.frame inView:self.view animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{ [self UndimSuper];
    if (buttonIndex == 0) {
        // Check if printing is avaliable
        if ([UIPrintInteractionController isPrintingAvailable]) {
            UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
            pic.delegate = self;
            
            UIPrintInfo *printInfo = [UIPrintInfo printInfo];
            printInfo.outputType = UIPrintInfoOutputGeneral;
            printInfo.jobName = self.containerMeeting.title;
            pic.printInfo = printInfo;
            
            UIMarkupTextPrintFormatter *textFormatter = [[UIMarkupTextPrintFormatter alloc] initWithMarkupText:self.containerMeeting.getDetailHTML];
            textFormatter.startPage = 0;
            textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
            textFormatter.maximumContentWidth = 6 * 72.0;
            pic.printFormatter = textFormatter;
            pic.showsPageRange = YES;
            [pic presentFromRect:self.sendButton.frame inView:self.view animated:YES completionHandler:nil];
          //  [pic presentAnimated:YES completionHandler:nil];
        }
    }
    
    if  (buttonIndex == 1) {
       
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.containerMeeting.title forKey:@"subject"];
        [data setValue:self.containerMeeting.getDetailHTML forKey:@"body"];
    
        [data setValue:nil forKey:@"fileName"];
    
    
        [self displayMailComposeSheet:data];
    }
 
    if (buttonIndex == 2) {
         //Replace log with export code
        // NSLog(@"export");
         XMLWriter *writer = [[XMLWriter alloc] init];
         NSString *xmlString = [writer writeXMLForExcel:self.containerMeeting];
         
         NSArray *paths = NSSearchPathForDirectoriesInDomains
         (NSDocumentDirectory, NSUserDomainMask, YES);
         NSString *documentsDirectory = [paths objectAtIndex:0];
         
         
         //make a file name to write the data to using the documents directory:
         NSString *theFileName = [NSString stringWithFormat:@"%@/%@.xls",
                                  documentsDirectory,self.containerMeeting.meetingID];
         //create content - four lines of text
         //NSString *content = @"One\nTwo\nThree\nFour\nFive";
         //save content to the documents directory
        
        NSString *shortFileName = [NSString stringWithFormat:@"%@.xls",self.containerMeeting.meetingID];
        
         [xmlString writeToFile:theFileName
                     atomically:NO
                       encoding:NSStringEncodingConversionAllowLossy
                          error:nil];
         
          //Setup email
          NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
          
          [data setValue:nil forKey:@"toRecipients"];
          [data setValue:nil forKey:@"ccRecipients"];
          [data setValue:nil forKey:@"bccRecipients"];
          [data setValue:self.containerMeeting.title forKey:@"subject"];
          [data setValue:@"Meeting attachment for Microsoft Excel" forKey:@"body"];
          [data setValue:[NSData dataWithContentsOfFile:theFileName] forKey:@"attachmentData"];
          [data setValue:[NSString stringWithFormat:@"%@",shortFileName] forKey:@"fileName"];
          
          [self displayMailComposeSheet:data];
          
          [[NSFileManager defaultManager] removeItemAtPath:theFileName error:nil];
        
    }
    
    
}

-(void)enableButtons
{    self.detailButton.enabled =YES;
    self.notesButton.enabled =YES;
    self.agendaButton.enabled = YES;
    self.attendeeButton.enabled =YES;
}


- (IBAction)Detail {
    if(self.meetingDetailViewController == nil) {
        self.meetingDetailViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"MeetingDetail"];
        self.meetingDetailViewController.delegate = self;
       self.meetingDetailViewController.detailMeeting =self.containerMeeting;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.meetingDetailViewController and:@"Detail"];
    self.detailButton.enabled = NO;
}

- (IBAction)Attendee {
    if(self.meetingAttendeeViewController == nil) {
        self.meetingAttendeeViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"MAttendee"];
        self.meetingAttendeeViewController.delegate = self;
         self.meetingAttendeeViewController.meeting = self.containerMeeting;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.meetingAttendeeViewController and:@"Attendee"];
     self.attendeeButton.enabled = NO;
}

- (IBAction)Agenda {
    if(self.meetingAgendaViewController == nil) {
        self.meetingAgendaViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"MeetingAgenda"];
        self.meetingAgendaViewController.delegate = self;
         self.meetingAgendaViewController.meeting =self.containerMeeting;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.meetingAgendaViewController and:@"Agenda"];
    self.agendaButton.enabled = NO;
}

- (IBAction)Notes {
    if(self.notesFormViewController == nil) {
        self.notesFormViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"NotesTable"];
        self.notesFormViewController.delegate = self;
       self.notesFormViewController.item =  self.containerMeeting;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.notesFormViewController and:@"Note"];
    self.notesButton.enabled = NO;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
   [self createNotesBadgeIfNeeded];
    [self createAgendaBadge];
    [self createInviteBadge];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    if (self.containerMeeting == nil){
        self.containerMeeting = [self.helper newMeeting:self.project] ;
    }
    self.meetingDetailViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"MeetingDetail"];
     self.meetingDetailViewController.delegate = self;
    self.meetingDetailViewController.detailMeeting = self.containerMeeting;
     [self addChildViewController:self.meetingDetailViewController];
    [self.container addSubview:self.meetingDetailViewController.view];
    [self.meetingDetailViewController didMoveToParentViewController:self];
   
    self.key =@"Detail";
     [self enableButtons];
    self.detailButton.enabled = NO;
 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) UpdateNotesBadge{
    [self createNotesBadgeIfNeeded];
}

- (IBAction)Send {
     [self.view endEditing:YES];
     [self dimSuper];
    [self createSendSheet];
}

- (IBAction)Delete {
     [self.view endEditing:YES];
    [self dimSuper];
    if(!self.deleteAlert){
   self.deleteAlert = [[UIAlertView alloc] initWithTitle:@"Delete Meeting"
                                                          message:@"Deleting this meeting will delete all of its notes"
                                                         delegate:self                                          cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Delete",nil];
    }
    [self.deleteAlert show];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self UndimSuper];
    if(buttonIndex == 1){
        if (self.containerMeeting.ekEvent) {
            EKEventStore *store = [[EKEventStore alloc] init];
            EKEvent* eventToRemove = [store eventWithIdentifier:self.containerMeeting.ekEvent];
            if (eventToRemove != nil) {
                NSError* error = nil;
                [store removeEvent:eventToRemove span:EKSpanThisEvent error:&error];
            }
        }
        [self.helper deleteObject:self.containerMeeting];
        [self.helper saveContext];
        [self.delegate MeetingFormContainerDidFinish:self];
        }}

- (IBAction)Done {
    [self.delegate MeetingFormContainerDidFinish:self];
}

//Use a key to keep track of what comtroller is loaded
-(void)SwapOldControllerForNew:(UIViewController*)controller and:(NSString *)aKey
{
    if([self.key isEqualToString:@"Detail"]){
        [self.meetingDetailViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.meetingDetailViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.meetingDetailViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
  else if([self.key isEqualToString:@"Attendee"]){
        [self.meetingAttendeeViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.meetingAttendeeViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.meetingAttendeeViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
   else if([self.key isEqualToString:@"Agenda"]){
        [self.meetingAgendaViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.meetingAgendaViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.meetingAgendaViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
   else if([self.key isEqualToString:@"Note"]){
        [self.notesFormViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.notesFormViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.notesFormViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
}


@end
