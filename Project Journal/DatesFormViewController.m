//
//  DatesFormViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 23/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "HelpViewController.h"
#import "DatesFormViewController.h"
#import "DatePickerViewController.h"

@interface DatesFormViewController ()<DatePickerViewControllerDelegate, UIActionSheetDelegate, UIPopoverControllerDelegate , UIAlertViewDelegate>
@property (strong , nonatomic) DataHelper *helper;
@property (strong, nonatomic) UIStoryboardPopoverSegue* popSegue;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *InitialCompletionLabel;
@property (weak, nonatomic) IBOutlet UILabel *revisedCompletionLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdLabel;
@property (weak, nonatomic) IBOutlet UILabel *actualCompletionLabel;
@property (weak, nonatomic) IBOutlet UILabel *modifiedDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *revisedHeader;
@property (weak, nonatomic) IBOutlet UILabel *completionHeader;
@property (weak, nonatomic) IBOutlet UILabel *initialHeader;
@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
@property (weak, nonatomic) IBOutlet UIImageView *reviewImage;
@property (weak, nonatomic) IBOutlet UIImageView *ActualCompletionImage;
@property (weak, nonatomic) IBOutlet UIImageView *revisedImage;
@property (weak, nonatomic) IBOutlet UIImageView *lastModifiedImage;
@property (weak, nonatomic) IBOutlet UIImageView *createdImage;
@property (weak, nonatomic) IBOutlet UIImageView *startImage;
@property (weak, nonatomic) IBOutlet UIImageView *initialImage;
@property (weak, nonatomic) IBOutlet UIButton *completionButton;
@property (weak, nonatomic) IBOutlet UIButton *initialCompletionButton;
@property (weak, nonatomic) IBOutlet UIButton *revisedCompletionButton;
@property (weak, nonatomic) IBOutlet UIButton *startbutton;
@property (weak, nonatomic) IBOutlet UIButton *reviewButton;
@property (weak, nonatomic) IBOutlet UIButton *markReviewedButton;
@property (strong, nonatomic) HelpViewController *helpView;
@property (strong, nonatomic) UIPopoverController *helpPopover;
@property (nonatomic, strong) NSDictionary *helpList;

//helpButtons
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *helpButtons;
@property (weak, nonatomic) UIActionSheet *reviewAction;
@property (strong, nonatomic) NSString *key;
- (NSDateFormatter *)formatterDate;
- (NSDateFormatter *)formatterDateTime;
@property (strong, nonatomic)UIAlertView *alert1;
@property (strong, nonatomic)UIAlertView *alert2;
@property (strong, nonatomic)UIAlertView *alert3;
@property (strong, nonatomic)UIAlertView *alert4;


@end

@implementation DatesFormViewController


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
    
    
}

- (IBAction)helpStart:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.startDate"] from:sender];
}
- (IBAction)helpInitCompletion:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.initialCompletionDate"] from:sender];
}
- (IBAction)helpRevisedCompletion:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.revisedCompletionDate"] from:sender];
}
- (IBAction)helpCompletion:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.completionDate"] from:sender];
}
- (IBAction)helpLastModified:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.lastModifiedDate"] from:sender];
}
- (IBAction)helpCreation:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.creationDate"] from:sender];
}
- (IBAction)helpReview:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"project.reviewDate"] from:sender];
}

-(void)loadTheHelpPlist{
    if(self.helpList == nil){
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"HelpList" ofType:@"plist"];
        self.helpList = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    }
}

- (void)checkHelpButtons {
    if(![self.helper showHelp]){
          for (UIButton *button in self.helpButtons) {
             button.hidden = YES;
         }
    }
}

- (void)showHelp:(NSString*)helpString from:(UIButton *)helpButton {
    [self.delegate dimSuper];
    if(self.helpView == nil) {
        self.helpView = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    }
    UITextView *_textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 250, 10)];
    _textView.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
    _textView.textColor = [UIColor lightGrayColor];
    _textView.scrollEnabled = NO;
    _textView.userInteractionEnabled=NO;
    _textView.text =  helpString;
    CGRect frame = _textView.frame;
    frame.size.height =  [self getTextHeight:_textView];
    _textView.frame = frame;
    self.helpView.view.frame = frame;
    [self.helpView.view addSubview:_textView];
    self.helpPopover = [[UIPopoverController alloc] initWithContentViewController:self.helpView];
    self.helpPopover.popoverContentSize =_textView.frame.size;
    [self.helpPopover presentPopoverFromRect:helpButton.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionDown animated:YES];
    self.helpPopover.delegate = self;
}

- (CGFloat)getTextHeight:(UITextView *)textView
{
    NSLayoutManager *lm = textView.layoutManager;
    CGRect boundingRect = [lm usedRectForTextContainer:lm.textContainers[0]];
    CGFloat height = boundingRect.size.height + (textView.textContainerInset.top + textView.textContainerInset.bottom);
    return height;
}

- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

//dismiss popovers that are showing when view rotates
- (void)dismissPopovers{
    [self.delegate UndimSuper];
    if (self.alert1 != nil) {
        [self.alert1 dismissWithClickedButtonIndex:0 animated:NO];
        self.alert1 = nil;
    }
    if (self.alert2 != nil) {
        [self.alert2 dismissWithClickedButtonIndex:0 animated:NO];
        self.alert2 = nil;
    }
    
    if (self.alert3 != nil) {
        [self.alert3 dismissWithClickedButtonIndex:0 animated:NO];
        self.alert3 = nil;
    }
    if (self.alert4 != nil) {
        [self.alert4 dismissWithClickedButtonIndex:0 animated:NO];
        self.alert4 = nil;
    }
    if (self.reviewAction != nil) {
        [self.reviewAction dismissWithClickedButtonIndex:self.reviewAction.cancelButtonIndex animated:NO];
        self.reviewAction = nil;
    }
    if ([self.popSegue.popoverController isPopoverVisible])
    {
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
    if ([self.helpPopover isPopoverVisible])
    {
        [self.helpPopover dismissPopoverAnimated:YES];
    }
}

-(void)showHideReviewButton{
    //show review button is project is for review
    if([self.project isForReview]){
        self.markReviewedButton.hidden = NO;
    }
    else{
        self.markReviewedButton.hidden = YES;
    }
}

- (IBAction)review:(id)sender {
    UIActionSheet *reviewSheet;
    if (!reviewSheet){
        [self.delegate dimSuper];
        reviewSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                  delegate:self
                                         cancelButtonTitle:nil
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"Mark As Reviewed",nil];
    }
    [reviewSheet showFromRect:self.markReviewedButton.frame inView:self.view  animated:YES];
    self.reviewAction = reviewSheet;
}
//reset the review date and update the UI
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self.delegate UndimSuper];
    if(buttonIndex == 0){
        [ self.project resetReviewDate];
        [self showHideReviewButton];
        self.reviewLabel.text = [[self formatterDate] stringFromDate:self.project.reviewDate];
        [self.delegate upDateReviewBadge];
    }}

//date picker delegate method to set dates
-(void)DatePicked:(NSDate *)date andController:(DatePickerViewController *)controller
{    //REVISED COMPLETION DATE
    if ([self.key isEqualToString:@"RCD"]){
        //check to see if revised completion date is after start date if not show alert
        if( [date timeIntervalSinceDate:self.project.projectStartDate] > 0 ) {
            self.project.revisedCompletionDate = date;
            self.project.lastModifiedDate =[NSDate date];
            [self.helper saveContext];
            self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.project.lastModifiedDate];
            self.revisedCompletionLabel.text = [[self formatterDate] stringFromDate:self.project.revisedCompletionDate];
            [self.delegate upDateRagBadge];
        }
        else{
            if(!self.alert1){
          
            self.alert1 = [[UIAlertView alloc] initWithTitle:@"Cannot Set Date"
                                                            message:@"The revised completion date must be after the start date."
                                                           delegate:self                                         cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            }
              [self.delegate dimSuper];
            [self.alert1 show];
        }
    }
    //INITIAL COMPLETION DATE
    if ([self.key isEqualToString:@"ICD"]){
        //check to see if imitial completion date is after start date if not show alert
        if( [date timeIntervalSinceDate:self.project.projectStartDate] > 0 ) {
            self.project.initialCompletionDate = date;
            self.project.lastModifiedDate =[NSDate date];
            [self.helper saveContext];
            self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.project.lastModifiedDate];
            self.InitialCompletionLabel.text = [[self formatterDate] stringFromDate:self.project.initialCompletionDate];
            [self.delegate upDateRagBadge];
            [self checkCompletionDates];
        }
        else{
              if(!self.alert2){
                 
            self.alert2 = [[UIAlertView alloc] initWithTitle:@"Cannot Set Initial completion Date"
                                                            message:@"The initial completion date must be after the start date. Change the start date before setting the initial completion date."
                                                           delegate:self                                          cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
              }
             [self.delegate dimSuper];
            [self.alert2 show];
        }
    }
    //REVIEW DATE
    if ([self.key isEqualToString:@"RD"]){
        self.project.reviewDate = date;
        self.project.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
        //update UI
        self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.project.lastModifiedDate];
        self.reviewLabel.text = [[self formatterDate] stringFromDate:self.project.reviewDate];
        [self showHideReviewButton];
        [self.delegate upDateReviewBadge];
    }
    //START DATE
    if ([self.key isEqualToString:@"SD"]){
        //check to see if the revised completion date has been set
        if(self.project.revisedCompletionDate == nil){
            //if revised completion date has not been set check to see if start date is after initial completion date if so show alert
            if( [date timeIntervalSinceDate: self.project.initialCompletionDate] > 0 ) {
                
                  if(!self.alert3){
                      
                self.alert3 = [[UIAlertView alloc] initWithTitle:@"Cannot Set Start Date"
                                                                message:@"You must enter a revised completion date before you can set a start date later than the initial completion date."
                                                               delegate:self                                          cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                  }
                [self.delegate dimSuper];
                [self.alert3 show];
                return;
            }
        }
        //if revised completion date has been set check to see if it is after the start date if not show alert
        if( [date timeIntervalSinceDate: self.project.revisedCompletionDate] > 0 ) {
                if(!self.alert4){
                    
            self.alert4 = [[UIAlertView alloc] initWithTitle:@"Cannot Set Start Date"
                                                            message:@"The start date must be earlier than the revised completion date. Change the revised completion date before setting a new start date "
                                                           delegate:self                                          cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
                }
            [self.delegate dimSuper];
            [self.alert4 show];
        }
        //if start date is before initial and revised completion date set the start date
        else{
            self.project.projectStartDate = date;
            self.project.lastModifiedDate =[NSDate date];
            [self.helper saveContext];
            self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.project.lastModifiedDate];
            self.startDateLabel.text = [[self formatterDate] stringFromDate:self.project.projectStartDate];
        }}
    //ACTUAL COMPLETION DATE
    if ([self.key isEqualToString:@"CD"]){
        self.project.actualCompletionDate = date;
        self.project.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
        self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.project.lastModifiedDate];
        self.actualCompletionLabel.text = [[self formatterDate] stringFromDate:self.project.actualCompletionDate];
        
    }
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.delegate UndimSuper];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{[self.delegate dimSuper];
    if ([[segue identifier] isEqualToString:@"RCD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        
        [[segue destinationViewController] setDelegate:self];
        if(self.project.revisedCompletionDate){

            [[segue destinationViewController] setPickerValue:self.project.revisedCompletionDate];
        }
         self.popSegue.popoverController.delegate =self;
        self.key=@"RCD";
    }
    if ([[segue identifier] isEqualToString:@"ICD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        
        [[segue destinationViewController] setDelegate:self];
        if(self.project.initialCompletionDate){
            
            [[segue destinationViewController] setPickerValue:self.project.initialCompletionDate];
        }
        self.popSegue.popoverController.delegate =self;
        self.key=@"ICD";
    }
    if ([[segue identifier] isEqualToString:@"RD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        
        [[segue destinationViewController] setDelegate:self];
        if(self.project.reviewDate){
            [[segue destinationViewController] setPickerValue:self.project.reviewDate];
        }
         self.popSegue.popoverController.delegate =self;
        self.key=@"RD";
    }
    if ([[segue identifier] isEqualToString:@"SD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        
        [[segue destinationViewController] setDelegate:self];
        if(self.project.projectStartDate){
            [[segue destinationViewController] setPickerValue:self.project.projectStartDate];
        }
        self.popSegue.popoverController.delegate =self;
        self.key=@"SD";
    }
    if ([[segue identifier] isEqualToString:@"CD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
       
        [[segue destinationViewController] setDelegate:self];
        if(self.project.actualCompletionDate){
            [[segue destinationViewController] setPickerValue:self.project.actualCompletionDate];
        }
         self.popSegue.popoverController.delegate =self;
        self.key=@"CD";
    }
}

- (void)DatePickerViewControllerControllerDidFinish:(DatePickerViewController *)controller{
    if ([self.popSegue.popoverController isPopoverVisible])
    {[self.delegate UndimSuper];
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
}

-(void)checkCompletionDates{
    //hide the revised completion button untill initial date has been set
    if(self.project.initialCompletionDate == nil){
       // self.initialCompletionButton.hidden = NO;
        self.initialCompletionButton.enabled = YES;
       // self.revisedCompletionButton.hidden = YES;
        self.revisedCompletionButton.enabled = NO;
        self.initialImage.alpha = 1.0;
        self.initialHeader.alpha = 1.0;
       // self.revisedImage.alpha = 0.5;
      //  self.revisedHeader.alpha = 0.5;
    }
    else{
      //  self.initialCompletionButton.hidden = YES;
        self.initialCompletionButton.enabled = NO;
     //   self.revisedCompletionButton.hidden = NO;
        self.revisedCompletionButton.enabled = YES;
       // self.initialImage.alpha = 0.5;
        self.revisedImage.alpha = 1.0;
        self.revisedHeader.alpha = 1.0;
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self checkCompletionDates];
    //show completion button if the status is set to completed or rejected
    //completion date is set in the detail view controller when status is set to completed or rejected
    if  ([self.project.status isEqualToString:@"Completed"]){
      //  self.completionButton.hidden = NO;
        self.completionButton.enabled = YES;
     //   self.revisedImage.alpha = 0.5;
      //  self.revisedCompletionButton.hidden = YES;
        self.revisedCompletionButton.enabled = NO;
       // self.initialCompletionButton.hidden = YES;
        self.initialCompletionButton.enabled = NO;
      //  self.initialImage.alpha = 0.5;
        if(self.project.initialCompletionDate == nil){
       //     self.initialHeader.alpha =0.5;
        }
        self.ActualCompletionImage.alpha = 1.0;
        self.completionHeader.alpha = 1.0;
        //hide start and review buttons if project is completed
     //   self.startbutton.hidden = YES;
        self.startbutton.enabled = NO;
      //  self.reviewButton.hidden = YES;
        self.reviewButton.enabled = NO;
     //   self.startImage.alpha = 0.5;
      //  self.reviewImage.alpha = 0.5;
    }
    else{
      //  self.completionButton.hidden = YES;
        self.completionButton.enabled = NO;
      //  self.ActualCompletionImage.alpha = 0.5;
      //  self.completionHeader.alpha = 0.5;
      //  self.startbutton.hidden = NO;
        self.startbutton.enabled = YES;
      //  self.reviewButton.hidden = NO;
        self.reviewButton.enabled = YES;
        self.startImage.alpha = 1.0;
        self.reviewImage.alpha = 1.0;
    }
    [self showHideReviewButton];
    self.revisedCompletionLabel.text = [[self formatterDate] stringFromDate:self.project.revisedCompletionDate];
    self.InitialCompletionLabel.text = [[self formatterDate] stringFromDate:self.project.initialCompletionDate];
    self.actualCompletionLabel.text = [[self formatterDate] stringFromDate:self.project.actualCompletionDate];
    self.reviewLabel.text = [[self formatterDate] stringFromDate:self.project.reviewDate];
    self.createdLabel.text = [[self formatterDateTime] stringFromDate:self.project.creationDate];
    self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.project.lastModifiedDate];
    self.startDateLabel.text = [[self formatterDate] stringFromDate:self.project.projectStartDate];
    self.titleLabel.text = self.project.title;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
     
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)refreshFromBackground{
    [self showHideReviewButton];
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    UIEdgeInsets tabInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage * textBack = [UIImage imageNamed:@"TextFrame2"];
    textBack = [textBack resizableImageWithCapInsets:tabInsets];
    self.titleImage.image = textBack;
  //  self.titleImage.alpha = 0.5;
    self.reviewImage.image = textBack;
    self.ActualCompletionImage.image = textBack;
    self.revisedImage.image = textBack;
    self.lastModifiedImage.image = textBack;
 //   self.lastModifiedImage.alpha = 0.5;
    self.createdImage.image = textBack;
  //  self.createdImage.alpha = 0.5;
    self.startImage.image = textBack;
    self.initialImage.image = textBack;
}

- (void)viewDidLoad
{  
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
   [self checkHelpButtons];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
