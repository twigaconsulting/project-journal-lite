//
//  ActionDatesViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 05/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
//@class ActionDatesViewController;

@protocol ActionDatesViewControllerDelegate <NSObject>

-(void) upDateRagBadge;
-(void) upDateReviewBadge;
-(void) dimSuper;
-(void) UndimSuper;
 
@end
@interface ActionDatesViewController : UIViewController
@property (weak, nonatomic) id <ActionDatesViewControllerDelegate> delegate;
@property (strong , nonatomic) Action *action;
@property (strong , nonatomic) DataHelper *helper;
- (void)dismissPopovers;
@end
