//
//  DataHelper.m
//  PJPrototype4
//
//  Created by Kynaston Pomlett on 14/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "DataHelper.h"

NSString *const VERSION = @"V2.0";

@implementation DataHelper

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

static DataHelper *sharedInstance = nil;

#pragma mark - Initialisation Methods
+ (DataHelper *)sharedInstance
{
    if (sharedInstance == nil) {
        sharedInstance = [[DataHelper alloc] init];
    } 
    
    return sharedInstance;
}
- (id)init
{
    self = [super init];
    
    if (self) {

                
        // Setup Core Data
        _managedObjectModel = [self managedObjectModel];
        _managedObjectContext = [self managedObjectContext];
        _persistentStoreCoordinator = [self persistentStoreCoordinator];
    
        // Check for existnace of a single Config record
        
        if ([self fetchConfigItem]) {
            self.settings = [self fetchConfigItem];
        } else {
            [self newSettings];
            

        }
        
        

      
    }
    return self;
}



#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Project_Journal" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Project_Journal.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
         //FUTURE ERROR HANDLING
       // NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
      //  abort();
    }
    
    return _persistentStoreCoordinator;
}
- (void)saveContext
{
    
    
    NSError *error = nil;
    //NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (_managedObjectContext != nil) {
        if ([_managedObjectContext hasChanges] && ![_managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             //FUTURE ERROR HANDLING
          //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
          //  abort();
        }
    }
    
    
}


// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark -  Settings Methods
- (void)newSettings
{
    /* This method creates a new record with defaults, sets a pointer to the config
     * and saves the defaults. There should only be 1 record in the table.
     * Pre-Condition: None
     * Post-Condition: Saved entity and pointer to self.settings
     * This is a private method.
     */
    
    
    self.settings = [NSEntityDescription insertNewObjectForEntityForName:@"Config"
                                                  inManagedObjectContext:_managedObjectContext];
    self.settings.redRAGDateInterval = [NSNumber numberWithInt:1];
    self.settings.amberRAGDateInterval = [NSNumber numberWithInt:7];
    self.settings.projectReviewDateInterval = [NSNumber numberWithInt:7];
    self.settings.actionReviewDateInterval = [NSNumber numberWithInt:7];
    self.settings.issuesReviewDateInterval = [NSNumber numberWithInt:7];
    self.settings.risksReviewDateInterval = [NSNumber numberWithInt:7];
    self.settings.amberRAGBudgetPerCent = [NSNumber numberWithInt:5];
    self.settings.showHelp = [NSNumber numberWithBool:YES];
    
    [self saveContext];
    
}

- (Config *)fetchConfigItem
{
    /* Fetch the single config item , if not in database then return nil
     */
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Config" inManagedObjectContext:_managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    [fetchRequest setFetchBatchSize:0];
    
    // Apply sort order YES=ASCENDING
 
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"hideCompleted" ascending:YES];
    NSArray *descriptors = @[sortDescriptor];
    fetchRequest.sortDescriptors = descriptors;
  
    // Perform the fetch
    NSError __autoreleasing *error;
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                               managedObjectContext:_managedObjectContext
                                                                                                 sectionNameKeyPath:nil cacheName:nil];
    
    if (![fetchedResultsController performFetch:&error]) {
         //FUTURE ERROR HANDLING
       // NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }
    
    int count = 0;
    
    if (fetchedResultsController) {
        count = [[fetchedResultsController fetchedObjects] count];
        if (count == 1) {
            Config *item = [fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            return item;
        } else {
            return nil;
        }
    } else {
        return nil;
    }
    
    
    return nil;
}

// Settings Getter and Setter Methods
- (NSNumber *)redRAGInterval
{
    return self.settings.redRAGDateInterval;
}
- (NSNumber *)amberRAGInterval
{
    return self.settings.amberRAGDateInterval;
}
- (void)setRedRAGInterval:(NSNumber *)number
{
    self.settings.redRAGDateInterval = number;
    [self saveContext];
}
- (void)setAmberRAGInterval:(NSNumber *)number
{
    self.settings.amberRAGDateInterval = number;
    [self saveContext];
}

- (NSNumber *)projectReviewInterval
{
    return self.settings.projectReviewDateInterval;
}
- (NSNumber *)actionReviewInterval
{
    return self.settings.actionReviewDateInterval;
}
- (NSNumber *)issueReviewInterval
{
    return self.settings.issuesReviewDateInterval;
}
- (NSNumber *)riskReviewInterval
{
    return self.settings.risksReviewDateInterval;
}
- (void)setProjectReviewInterval:(NSNumber *)number
{
    self.settings.projectReviewDateInterval = number;
    [self saveContext];
}
- (void)setActionReviewInterval:(NSNumber *)number
{
    self.settings.actionReviewDateInterval = number;
    [self saveContext];
}
- (void)setIssueReviewInterval:(NSNumber *)number
{
    self.settings.issuesReviewDateInterval = number;
    [self saveContext];
}
- (void)setRiskReviewInterval:(NSNumber *)number
{
    self.settings.risksReviewDateInterval = number;
    [self saveContext];
}

- (NSNumber *)amberRAGBudgetPerCent
{
    return self.settings.amberRAGBudgetPerCent;
}
- (void)setAmberRAGBudgetPerCent:(NSNumber *)number
{
    self.settings.amberRAGBudgetPerCent = number;
    [self saveContext];
}

- (BOOL)showHelp
{
    return [self.settings.showHelp boolValue];
}
- (void)setShowHelp:(BOOL)flag
{
    self.settings.showHelp = [NSNumber numberWithBool:flag];
    [self saveContext];
}

- (BOOL)hideCompleted
{
    return [self.settings.hideCompleted boolValue];
}
- (void)setHideCompleted:(BOOL)flag
{
    self.settings.hideCompleted = [NSNumber numberWithBool:flag];
    [self saveContext];
}


#pragma mark - List Methods

- (NSArray *)allProjects
{
    return [[self fetchItemsMatching:nil forEntityType:@"Project" sortingBy:@"title" ascendingBy:YES includeDeleted:NO] fetchedObjects];
}



// LIST MEHTODS

- (NSArray *)fetchStatusOptions:(id)object
{
    // Allocate to a parent object
    // Construct Predicate for each object type
    if ([object isKindOfClass:[Action class]]) {
        Action *entity = (Action *)object;
        
        // Determine contextual status dropdown
        if ([entity.status isEqualToString:@"New"]) {
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Unassigned"]) {
            return [NSArray arrayWithObjects:@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Assigned"]) {
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"On Hold"] && entity.actionOwner == nil) {
            return [NSArray arrayWithObjects:@"Unassigned",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"On Hold"]){
                return [NSArray arrayWithObjects:@"Unassigned",@"Assigned",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Completed"] && entity.actionOwner == nil) {
            //return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",nil];
            if (entity.inMeeting) {
                if ([entity.inMeeting.status isEqualToString:@"Completed"])
                    return [NSArray arrayWithObjects:nil];
            } else if (entity.inRisk) {
                if ([entity.inRisk.status isEqualToString:@"Completed"]) {
                    return [NSArray arrayWithObjects:nil];
                }
            } else if (entity.inIssue) {
                if ([entity.inIssue.status isEqualToString:@"Completed"]) {
                    return [NSArray arrayWithObjects:nil];
                }
            } else if (entity.inProject) {
                if ([entity.inProject.status isEqualToString:@"Completed"]) {
                    return [NSArray arrayWithObjects:nil];
                }
            }
            
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",nil];
            
        } else if ([entity.status isEqualToString:@"Completed"]) {
            if (entity.inMeeting) {
                if ([entity.inMeeting.status isEqualToString:@"Completed"])
                     return [NSArray arrayWithObjects:nil];
            } else if (entity.inRisk) {
                if ([entity.inRisk.status isEqualToString:@"Completed"]) {
                    return [NSArray arrayWithObjects:nil];
                }
            } else if (entity.inIssue) {
                if ([entity.inIssue.status isEqualToString:@"Completed"]) {
                    return [NSArray arrayWithObjects:nil];
                }
            } else if (entity.inProject) {
                if ([entity.inProject.status isEqualToString:@"Completed"]) {
                    return [NSArray arrayWithObjects:nil];
                }
            }
            
            return [NSArray arrayWithObjects:@"Unassigned",@"Assigned",@"On Hold",nil];
            
        }
    }
    
    
    if ([object isKindOfClass:[Issue class]]) {
        Issue *entity= (Issue *)object;
        
        // Determine contextual status dropdown
        if ([entity.status isEqualToString:@"New"]) {
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Unassigned"]) {
            return [NSArray arrayWithObjects:@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Assigned"]) {
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"On Hold"] && entity.issueOwner == nil) {
            return [NSArray arrayWithObjects:@"Unassigned",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"On Hold"]){
            return [NSArray arrayWithObjects:@"Unassigned",@"Assigned",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Completed"] && entity.issueOwner == nil) {
            if (entity.inProject) {
                if ([entity.inProject.status isEqualToString:@"Completed"]) {
                    return [NSArray arrayWithObjects:nil];
                }
            }
            
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",nil];
            
        } else if ([entity.status isEqualToString:@"Completed"]) {
            if (entity.inProject) {
                if ([entity.inProject.status isEqualToString:@"Completed"]) {
                    return [NSArray arrayWithObjects:nil];
                }
            }
            
            return [NSArray arrayWithObjects:@"Unassigned",@"Assigned",@"On Hold",nil];
            
        }
    }
    if ([object isKindOfClass:[Risk class]]) {
        Risk *entity = (Risk *)object;
        
        // Determine contextual status dropdown
        if ([entity.status isEqualToString:@"New"]) {
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Unassigned"]) {
            return [NSArray arrayWithObjects:@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Assigned"]) {
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"On Hold"] && entity.riskOwner == nil) {
            return [NSArray arrayWithObjects:@"Unassigned",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"On Hold"]){
            return [NSArray arrayWithObjects:@"Unassigned",@"Assigned",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Completed"] && entity.riskOwner == nil) {
            if (entity.inProject) {
                if ([entity.inProject.status isEqualToString:@"Completed"]) {
                    return [NSArray arrayWithObjects:nil];
                }
            }
            
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",nil];
            
        } else if ([entity.status isEqualToString:@"Completed"]) {
            if (entity.inProject) {
                if ([entity.inProject.status isEqualToString:@"Completed"]) {
                    return [NSArray arrayWithObjects:nil];
                }
            }
            
            return [NSArray arrayWithObjects:@"Unassigned",@"Assigned",@"On Hold",nil];
            
        }
    }
    if ([object isKindOfClass:[Project class]]) {
        Project *entity = (Project *)object;
        
        // Determine contextual status dropdown
        if ([entity.status isEqualToString:@"New"]) {
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Unassigned"]) {
            return [NSArray arrayWithObjects:@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Assigned"]) {
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"On Hold"] && entity.projectManager == nil) {
            return [NSArray arrayWithObjects:@"Unassigned",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"On Hold"]){
            return [NSArray arrayWithObjects:@"Unassigned",@"Assigned",@"Completed",nil];
            
        }  else if ([entity.status isEqualToString:@"Completed"] && entity.projectManager == nil) {
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",nil];
            
        } else if ([entity.status isEqualToString:@"Completed"]) {
            return [NSArray arrayWithObjects:@"Unassigned",@"Assigned",@"On Hold",nil];
            
        }
    }
    if ([object isKindOfClass:[Dependency class]]) {
        Dependency *entity = (Dependency *)object;
     
        // Determine contextual status dropdown
        if ([entity.status isEqualToString:@"New"]) {
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Unassigned"]) {
            return [NSArray arrayWithObjects:@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"Assigned"]) {
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"On Hold"] && entity.dependencyReceiver == nil) {
            return [NSArray arrayWithObjects:@"Unassigned",@"Completed",nil];
            
        } else if ([entity.status isEqualToString:@"On Hold"]){
            return [NSArray arrayWithObjects:@"Unassigned",@"Assigned",@"Completed",nil];
            
        }  else if ([entity.status isEqualToString:@"Completed"] && entity.dependencyReceiver == nil) {
            if (entity.inProject) {
                if ([entity.inProject.status isEqualToString:@"Completed"]) {
                    return [NSArray arrayWithObjects:nil];
                }
            }
            
            return [NSArray arrayWithObjects:@"Unassigned",@"On Hold",nil];
            
        } else if ([entity.status isEqualToString:@"Completed"]) {
            if (entity.inProject) {
                if ([entity.inProject.status isEqualToString:@"Completed"]) {
                    return [NSArray arrayWithObjects:nil];
                }
            }
            
            return [NSArray arrayWithObjects:@"Unassigned",@"Assigned",@"On Hold",nil];
            
        }

     
    }

    
  
    // Default
    return [NSArray arrayWithObjects:@"Unassigned",@"Assigned",@"On Hold",@"Completed",nil];
    
}
- (NSArray *)fetchViewpointOptions
{
    return [NSArray arrayWithObjects:@"Detractor",@"Neutral",@"Supporter",nil];
}
- (NSArray *)fetchInfluenceOptions
{
    return [NSArray arrayWithObjects:@"Low",@"Medium",@"High",nil];
}
- (NSArray *)fetchPriorityOptions
{
    return [NSArray arrayWithObjects:@"Low",@"Medium",@"High",nil];
}
- (NSArray *)fetchCategoryOptions
{
    return [NSArray arrayWithObjects:@"Initiation",@"Definition",@"Execution",@"Project Control",@"Closure",nil];
}
- (NSArray *)fetchDependencyOptions
{
    return [NSArray arrayWithObjects:@"Interproject",@"Interfunction",@"External",nil];    
}
- (NSArray *)fetchMitigationOptions
{
    return [NSArray arrayWithObjects:@"Acceptance",@"Avoidance",@"Mitigation",@"Monitoring",@"Transfer",nil];   
}

- (NSArray *)fetchPersonList
{
    return [[self fetchItemsMatching:nil forEntityType:@"Person" sortingBy:@"surname" ascendingBy:YES includeDeleted:NO] fetchedObjects];
}

#pragma mark - New Object Creation Methods
// NEW ITEMS CREATED
- (Project *)newProject
{
    Project *object = [NSEntityDescription insertNewObjectForEntityForName:@"Project"
                                               inManagedObjectContext:_managedObjectContext];
    object.type = @"Project";
    object.uuid = self.newUUID;
    object.status = @"New";
    object.creationDate = [NSDate date];
    object.projectStartDate = [NSDate date];
    object.reviewDate = [object.creationDate dateByAddingTimeInterval:[[self projectReviewInterval] intValue] * 86400];
    object.sortActionField = @"RAG";
    object.sortIssueField = @"RAG";
    object.sortRiskField = @"RAG";
    object.deleted = [NSNumber numberWithBool:NO];
    object.projectID = [NSString stringWithFormat:@"P%d",[self.settings.projectCount integerValue] + 1];
    self.settings.projectCount = [NSNumber numberWithInt:[self.settings.projectCount integerValue] + 1];


    
    return object;
}
- (Action *)newAction:(id)object
{
    Action *item = [NSEntityDescription insertNewObjectForEntityForName:@"Action"
                                               inManagedObjectContext:_managedObjectContext];
    // Setting Defaults
    item.type = @"Action";
    item.uuid = self.newUUID;
    item.status = @"New";
    item.priority = @"Medium";
    item.category = @"Initiation";
    item.targetCompletionDate = [[NSDate date] dateByAddingTimeInterval:86400];
    item.creationDate = [NSDate date];
    item.reviewDate = [item.creationDate dateByAddingTimeInterval:[[self actionReviewInterval] intValue] * 86400];
    item.deleted = [NSNumber numberWithBool:NO];
    
    if ([object isKindOfClass:[Risk class]]) {
        Risk *entity = (Risk *)object;
        item.inProject = entity.inProject;
        item.inRisk = entity;
        item.actionID = [NSString stringWithFormat:@"%@-AC%d",entity.riskID,[self.settings.actionCount integerValue] + 1];
        self.settings.actionCount = [NSNumber numberWithInt:[self.settings.actionCount integerValue] + 1];
        item.section = @"D. Risk";
    } else if ([object isKindOfClass:[Issue class]]) {
        Issue *entity = (Issue *)object;
        item.inProject = entity.inProject;
        item.inIssue = entity;
        item.actionID = [NSString stringWithFormat:@"%@-AC%d",entity.issueID,[self.settings.actionCount integerValue] + 1];
        self.settings.actionCount = [NSNumber numberWithInt:[self.settings.actionCount integerValue] + 1];
        item.section = @"C. Issue";
    } else if ([object isKindOfClass:[Meeting class]]) {
        Meeting *entity = (Meeting *)object;
        item.inProject = entity.inProject;
        item.inMeeting = entity;
        item.actionID = [NSString stringWithFormat:@"%@-AC%d",entity.meetingID,[self.settings.actionCount integerValue] + 1];
        self.settings.actionCount = [NSNumber numberWithInt:[self.settings.actionCount integerValue] + 1];
        item.section = @"B. Meeting";
    } else if ([object isKindOfClass:[Project class]]) {
        Project *entity = (Project *)object;
        item.inProject = entity;
        [entity addProjectActionsObject:item];
        item.actionID = [NSString stringWithFormat:@"%@-AC%d",entity.projectID,[self.settings.actionCount integerValue] + 1];
        self.settings.actionCount = [NSNumber numberWithInt:[self.settings.actionCount integerValue] + 1];
        item.section = @"A. Project";
    }


    return item;
}
- (Issue *)newIssue:(Project *)project
{
    Issue *object = [NSEntityDescription insertNewObjectForEntityForName:@"Issue"
                                                   inManagedObjectContext:_managedObjectContext];
    object.type = @"Issue";
    object.uuid = self.newUUID;
    object.status = @"New";
    object.priority = @"Medium";
    object.category = @"Initiation";
    object.creationDate = [NSDate date];
    object.reviewDate = [object.creationDate dateByAddingTimeInterval:[[self issueReviewInterval] intValue] * 86400];
    object.inProject = project;
    [project addProjectIssuesObject:object];
    object.deleted = [NSNumber numberWithBool:NO];
    object.issueID = [NSString stringWithFormat:@"%@-IS%d",project.projectID,[self.settings.issueCount integerValue] + 1];
    self.settings.issueCount = [NSNumber numberWithInt:[self.settings.issueCount integerValue] + 1];
    
    return object;
}
- (Risk *)newRisk:(Project *)project
{
    Risk *object = [NSEntityDescription insertNewObjectForEntityForName:@"Risk"
                                                   inManagedObjectContext:_managedObjectContext];
    object.type = @"Risk";
    object.uuid = self.newUUID;
    object.status = @"New";
    object.creationDate = [NSDate date];
    object.reviewDate = [object.creationDate dateByAddingTimeInterval:[[self riskReviewInterval] intValue] * 86400];
    object.inProject = project;
    [project addProjectRisksObject:object];
    object.deleted = [NSNumber numberWithBool:NO];
    object.riskID = [NSString stringWithFormat:@"%@-RI%lu", project.projectID,
                       (unsigned long)[[[self fetchItemsMatching:project
                                                   forEntityType:@"Risk"
                                                       sortingBy:@"title"
                                                     ascendingBy:YES
                                                  includeDeleted:YES] fetchedObjects] count]];
    object.riskID = [NSString stringWithFormat:@"%@-RI%d",project.projectID,[self.settings.riskCount integerValue] + 1];
    self.settings.riskCount = [NSNumber numberWithInt:[self.settings.riskCount integerValue] + 1];

    
    return object;
}
- (Dependency *)newDependency:(Project *)project
{
    Dependency *object = [NSEntityDescription insertNewObjectForEntityForName:@"Dependency"
                                                   inManagedObjectContext:_managedObjectContext];
    object.type = @"Dependency";
    object.uuid = self.newUUID;
    object.status = @"New";
    object.creationDate = [NSDate date];
    object.reviewDate = [object.creationDate dateByAddingTimeInterval:[[self riskReviewInterval] intValue] * 86400];
    object.inProject = project;
    [project addProjectDependenciesObject:object];
    object.deleted = [NSNumber numberWithBool:NO];
    object.dependencyID = [NSString stringWithFormat:@"%@-DE%lu", project.projectID,
                       (unsigned long)[[[self fetchItemsMatching:project
                                                   forEntityType:@"Dependency"
                                                       sortingBy:@"title"
                                                     ascendingBy:YES
                                                  includeDeleted:YES] fetchedObjects] count]];

    
    return object;
}
- (Meeting *)newMeeting:(Project *)project
{
    Meeting *object = [NSEntityDescription insertNewObjectForEntityForName:@"Meeting"
                                                   inManagedObjectContext:_managedObjectContext];
    object.type = @"Meeting";
    object.uuid = self.newUUID;
    object.creationDate = [NSDate date];
    object.inProject = project;
    [project addProjectMeetingsObject:object];
    object.deleted = [NSNumber numberWithBool:NO];
    object.meetingID = [NSString stringWithFormat:@"%@-ME%lu", project.projectID,
                       (unsigned long)[[[self fetchItemsMatching:project
                                                   forEntityType:@"Meeting"
                                                       sortingBy:@"title"
                                                     ascendingBy:YES
                                                  includeDeleted:YES] fetchedObjects] count]];
    object.meetingID = [NSString stringWithFormat:@"%@-ME%d",project.projectID,[self.settings.meetingCount integerValue] + 1];
    self.settings.meetingCount = [NSNumber numberWithInt:[self.settings.meetingCount integerValue] + 1];

    // Setting default meeting start & end times. Start = now + 1 hour. End = Start + 1 hour.
    object.startDateTime = [[NSDate date] dateByAddingTimeInterval:3600];
    object.endDateTime = [object.startDateTime dateByAddingTimeInterval:3600];
    object.status = @"notCompleted";

    return object;
}
-(void)createDates
{

}

- (Stakeholder *)newStakeholder:(Project *)project
{
    Stakeholder *object = [NSEntityDescription insertNewObjectForEntityForName:@"Stakeholder"
                                                   inManagedObjectContext:_managedObjectContext];
    object.type = @"Stakeholder";
    object.uuid = self.newUUID;
    object.creationDate = [NSDate date];
    object.inProject = project;
    [project addProjectStakeholdersObject:object];
    object.deleted = [NSNumber numberWithBool:NO];
    object.stakeholderID = [NSString stringWithFormat:@"%@-ST%lu", project.projectID,
                       (unsigned long)[[[self fetchItemsMatching:project
                                                   forEntityType:@"Stakeholder"
                                                       sortingBy:@"title"
                                                     ascendingBy:YES
                                                  includeDeleted:YES] fetchedObjects] count]];

    return object;
}
- (Note *)newNote:(id)object
{   // TO DO: Check how we can decode which object to setup relationship
    Note *item = [NSEntityDescription insertNewObjectForEntityForName:@"Note"
                                                        inManagedObjectContext:_managedObjectContext];
    item.type = @"Note";
    item.uuid = self.newUUID;
    item.creationDate = [NSDate date];
    item.lastModifiedDate = item.creationDate;
    item.deleted = [NSNumber numberWithBool:NO];
    item.status = @"notCompleted";
    
    // Allocate to a parent object
    // Construct Predicate for each object type
    if ([object isKindOfClass:[Action class]]) {
        Action *entity = (Action *)object;
        item.inAction = entity;
        item.inProject = entity.inProject;
        item.section = @"C. Action";
    }
    if ([object isKindOfClass:[Dependency class]]) {
        Dependency *entity = (Dependency *)object;
        item.inDependency = entity;
        item.inProject = entity.inProject;
        item.section = @"F. Dependency";
    }
    if ([object isKindOfClass:[Issue class]]) {
        Issue *entity= (Issue *)object;
        item.inIssue = entity;
        item.inProject = entity.inProject;
        item.section = @"D. Issue";
    }
    if ([object isKindOfClass:[Meeting class]]) {
        Meeting *entity = (Meeting *)object;
        item.inMeeting = entity;
        item.inProject = entity.inProject;
        item.section = @"B. Meeting";
    }
    if ([object isKindOfClass:[Risk class]]) {
        Risk *entity = (Risk *)object;
        item.inRisk = entity;
        item.inProject = entity.inProject;
        item.section = @"E. Risk";
    }
    if ([object isKindOfClass:[Project class]]) {
        Project *entity = (Project *)object;
        item.inProject = entity;
        item.section = @"A. Project";
        
    }
    if ([object isKindOfClass:[Stakeholder class]]) {
        Stakeholder *entity = (Stakeholder *)object;
        item.inStakeholder = entity;
        item.inProject = entity.inProject;
        item.section = @"Stakeholder";
    }

    
    return item;
}

- (AgendaItem *)newAgendaItem:(Meeting *)meeting
{
   AgendaItem *object = [NSEntityDescription insertNewObjectForEntityForName:@"AgendaItem"
                                                  inManagedObjectContext:_managedObjectContext];

    object.inMeeting = (Meeting *)meeting;

    return object;

}
- (Media *)newMedia
{   // TO DO: Check how we can decode which object to setup relationship
    Media *object = [NSEntityDescription insertNewObjectForEntityForName:@"Media"
                                                 inManagedObjectContext:_managedObjectContext];
    object.type = @"Media";
    object.uuid = self.newUUID;
    object.creationDate = [NSDate date];
    object.deleted = [NSNumber numberWithBool:NO];
    
    return object;
}

- (NSString *)newUUID
{
    return [[NSProcessInfo processInfo] globallyUniqueString];
}


// DELETE OBJECTS
- (BOOL)deleteObject:(NSManagedObject *)object
{
    // This method flags the item to be deleted. 

    
    if ([object isKindOfClass:[Action class]]) {
        [self.managedObjectContext deleteObject:object];
    }
    
    if ([object isKindOfClass:[Dependency class]]) {
        [self.managedObjectContext deleteObject:object];
    }

    if ([object isKindOfClass:[Issue class]]) {
        // Mark any actions for review so that the user is presented
        // with the orphaned actions.
        Issue *thing = (Issue *)object;
        if ([thing.issueActions count] > 0) {
            for (Action *item in thing.issueActions) {
                //item.reviewDate = [NSDate date];
                [self deleteObject:item];
            }
        }
        [self.managedObjectContext deleteObject:object];
    }
    
    if ([object isKindOfClass:[Media class]]) {
        [self.managedObjectContext deleteObject:object];
    }
    
    if ([object isKindOfClass:[Meeting class]]) {
        // Mark any actions for review so that the user is presented
        // with the orphaned actions.
        Meeting *thing = (Meeting *)object;
        if ([thing.meetingActions count] > 0) {
            for (Action *item in thing.meetingActions) {
                //item.reviewDate = [NSDate date];
                [self deleteObject:item];
            }
        }
        [self.managedObjectContext deleteObject:object];
    }
    
    if ([object isKindOfClass:[Note class]]) {
        [self.managedObjectContext deleteObject:object];
    }
    
    if ([object isKindOfClass:[Project class]]) {
        [self.managedObjectContext deleteObject:object];
    }
    
    if ([object isKindOfClass:[Risk class]]) {
        // Mark any actions for review so that the user is presented
        // with the orphaned actions.
        Risk *thing = (Risk *)object;
        if ([thing.riskActions count] > 0) {
            for (Action *item in thing.riskActions) {
                //item.reviewDate = [NSDate date];
                [self deleteObject:item];
            }
        }
        [self.managedObjectContext deleteObject:object];
    }
    
    if ([object isKindOfClass:[Stakeholder class]]) {
        [self.managedObjectContext deleteObject:object];
    }
    
    return  YES;
}


// Return the CONFIG ITEMS
/*
- (NSNumber *)amberRAGDateInterval
{
    return [NSNumber numberWithInt:14];
}


- (NSNumber *)redRAGDateInterval
{
    return [NSNumber numberWithInt:7];
}

- (NSNumber *)defaultReviewDateInterval
{
    return [NSNumber numberWithInt:7];
}
 */

// MESSAGING METHODS
- (NSString *)emailBodyFromItem:(id)object
{
    return @"Test body message. This functionality is in progress";
}
- (NSString *)emailSubjectFromItem:(id)object
{
    return @"Subject body message. This functionality is in progress.";
}

// FETCH METHODS

- (NSArray *)fetchNotesList:(id)object
{
    // Setup the Fetch request
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Note"
                                                         inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    // Construct Predicate for each object type
    if ([object isKindOfClass:[Action class]]) {
        Action *item = (Action *)object;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inAction == %@ && deleted == 0" ,item];
    }
    if ([object isKindOfClass:[Dependency class]]) {
        Dependency *item = (Dependency *)object;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inDependency == %@ && deleted == 0" ,item];
    }
    if ([object isKindOfClass:[Issue class]]) {
        Issue *item = (Issue *)object;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inIssue == %@ && deleted == 0" ,item];
    }
    if ([object isKindOfClass:[Meeting class]]) {
        Meeting *item = (Meeting *)object;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inMeeting == %@ && deleted == 0" ,item];
    }
    if ([object isKindOfClass:[Risk class]]) {
        Risk *item = (Risk *)object;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inRisk == %@ && deleted == 0" ,item];
    }
    if ([object isKindOfClass:[Project class]]) {
        Project *item = (Project *)object;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@ && deleted == 0 && inAction == nil && inIssue == nil && inRisk == nil && inMeeting == nil",item];
    }
    if ([object isKindOfClass:[Stakeholder class]]) {
        Stakeholder *item = (Stakeholder *)object;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inStakeholder == %@ && deleted == 0" ,item];
    }
    
    // Perform Core Data Fetch
    [fetchRequest setFetchBatchSize:0];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastModifiedDate" ascending:NO];
    NSArray *descriptors = @[sortDescriptor];
    fetchRequest.sortDescriptors = descriptors;
    NSError __autoreleasing *error;
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc]
                                                    initWithFetchRequest:fetchRequest
                                                    managedObjectContext:_managedObjectContext
                                                    sectionNameKeyPath:nil cacheName:nil];
    
    if (![fetchedResultsController performFetch:&error]) {
         //FUTURE ERROR HANDLING
       // NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }

    return [fetchedResultsController fetchedObjects];
}

- (NSFetchedResultsController *)fetchItemsMatching:(Project *)project
                                     forEntityType:(NSString *)entity
                                         sortingBy:(NSString *)sortAttribute
                                       ascendingBy:(BOOL)ascending
                                  includeDeleted:(BOOL)inclDeleted
{
    // TO DO put the fetch code in here and put results into _fetchedResultsController
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entity inManagedObjectContext:_managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    
    // Setup the predicate string
    if ([entity isEqualToString:@"Note"]) {
        if (project && !self.hideCompleted) {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@",project];
        } else {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@ && status != %@",project,@"Completed"];
            
        }
    } else if ([entity isEqualToString:@"Meeting"]) {
        if (project && !self.hideCompleted) {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@",project];
            
        } else {
            //fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@ && status != %@",project,@"Completed"];
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@ && endDateTime >= %@",project,[NSDate date]];
        }
    } else if (project && self.hideCompleted) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@ && status != %@",project,@"Completed"];
       
    } else if (project && !self.hideCompleted) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@",project];
        
    } else if (!project && self.hideCompleted) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"status != %@",@"Completed"];
        
    } else if (!project && !self.hideCompleted) {
        // Do nothing with Predicate
    }
    

    
    
    [fetchRequest setFetchBatchSize:100];
    
    // Apply sort order YES=ASCENDING
    
    // Setup the Sort descriptor
    _currentProject = project;
    NSString *sortString;
    BOOL sortAscending;
    NSSortDescriptor *sortDescriptor;
    NSArray *descriptors;
    
    if ([entity isEqualToString:@"Note"]) {
        sortString = @"lastModifiedDate";
        sortAscending = NO;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending];
        descriptors = @[sortDescriptor];
    } else if ([entity isEqualToString:@"Action"]) {
        sortString = [self getSortString:@"Action"];
        sortAscending = [self getSortAscending:@"Action"];
        if ([sortString isEqualToString:@"title"] || [sortString isEqualToString:@"owner"]) {
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending selector:@selector(localizedCompare:)];
        } else {
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending];
        }
        descriptors = @[sortDescriptor];
    } else if ([entity isEqualToString:@"Issue"]) {
        sortString = [self getSortString:@"Issue"];
        sortAscending = [self getSortAscending:@"Issue"];
        if ([sortString isEqualToString:@"title"] || [sortString isEqualToString:@"owner"]) {
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending selector:@selector(localizedCompare:)];
        } else {
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending];
        }
        descriptors = @[sortDescriptor];
    } else if ([entity isEqualToString:@"Risk"]) {
        sortString = [self getSortString:@"Risk"];
        sortAscending = [self getSortAscending:@"Risk"];
        if ([sortString isEqualToString:@"title"] || [sortString isEqualToString:@"owner"]) {
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending selector:@selector(localizedCompare:)];
        } else {
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending];
        }
        descriptors = @[sortDescriptor];
    } else if ([entity isEqualToString:@"Meeting"]) {
        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"status" ascending:NO];
        NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"startDateTime" ascending:YES];
        descriptors = @[sd1,sd2];
    } else {
        sortString = sortAttribute;
        sortAscending = ascending;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending];
        descriptors = @[sortDescriptor];
    }

    

    fetchRequest.sortDescriptors = descriptors;
    
    // Perform the fetch
    NSError __autoreleasing *error;
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                               managedObjectContext:_managedObjectContext
                                                                                                 sectionNameKeyPath:nil cacheName:nil];

    if (![fetchedResultsController performFetch:&error]) {
         //FUTURE ERROR HANDLING
       // NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }

    return fetchedResultsController;
}


- (NSFetchedResultsController *)fetchItemsMatching:(Project *)project
                                     forEntityType:(NSString *)entity
                                         sortingBy:(NSString *)sortAttribute
                                         sectionBy:(NSString *)sectionAttribute
                                       ascendingBy:(BOOL)ascending
                                    includeDeleted:(BOOL)inclDeleted
{
    // TO DO put the fetch code in here and put results into _fetchedResultsController
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entity inManagedObjectContext:_managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    _currentProject = project;
    
    // Setup the predicate string
    if ([entity isEqualToString:@"Meeting"]) {
        if (project) {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@",project];
        } else {
            // Do nothing with Prdicate
        }
        
    }  else if ([entity isEqualToString:@"Note"]){
        if (project && !self.hideCompleted) {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@",project];
        } else {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@ && status != %@",project,@"Completed"];
            
        }
    } else if (project && self.hideCompleted) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@ && status != %@",project,@"Completed"];
        //fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@",project];
    } else if (project && !self.hideCompleted) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@",project];
        
    } else if (!project && self.hideCompleted) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"status != %@",@"Completed"];
        
    } else if (!project && !self.hideCompleted) {
        // Do nothing with Predicate
    }
    
    [fetchRequest setFetchBatchSize:100];
    
    // Apply sort order YES=ASCENDING
    
    // Setup the Sort descriptor
    _currentProject = project;
    NSString *sortString;
    BOOL sortAscending;
    NSSortDescriptor *sd1;
    NSSortDescriptor *sd2;
    NSArray *descriptors;
    
    if ([entity isEqualToString:@"Note"]) {
        sd1 = [[NSSortDescriptor alloc] initWithKey:@"section" ascending:YES];
        sd2 = [[NSSortDescriptor alloc] initWithKey:@"lastModifiedDate" ascending:NO];
        descriptors = @[sd1,sd2];
    } else if ([entity isEqualToString:@"Action"]) {
        sortString = [self getSortString:@"Action"];
        sortAscending = [self getSortAscending:@"Action"];
        sd1 = [[NSSortDescriptor alloc] initWithKey:@"section" ascending:YES];
        if ([sortString isEqualToString:@"title"] || [sortString isEqualToString:@"owner"]) {
            sd2 = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending selector:@selector(localizedCompare:)];
        } else {
            sd2 = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending];     
        }
        descriptors = @[sd1,sd2];
        
    } else if ([entity isEqualToString:@"Issue"]) {
        sortString = [self getSortString:@"Issue"];
        sortAscending = [self getSortAscending:@"Issue"];
        sd1 = [[NSSortDescriptor alloc] initWithKey:@"section" ascending:YES];
        if ([sortString isEqualToString:@"title"] || [sortString isEqualToString:@"owner"]) {
            sd2 = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending selector:@selector(localizedCompare:)];
        } else {
            sd2 = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending];
        }
        descriptors = @[sd1,sd2];
        
    } else if ([entity isEqualToString:@"Risk"]) {
        sortString = [self getSortString:@"Risk"];
        sortAscending = [self getSortAscending:@"Risk"];
        sd1 = [[NSSortDescriptor alloc] initWithKey:@"section" ascending:YES];
        if ([sortString isEqualToString:@"title"] || [sortString isEqualToString:@"owner"]) {
            sd2 = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending selector:@selector(localizedCompare:)];
        } else {
            sd2 = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending];
        }
        descriptors = @[sd1,sd2];
        
    } else if ([entity isEqualToString:@"Meeting"]) {
        sd1 = [[NSSortDescriptor alloc] initWithKey:@"section" ascending:YES];
        sd2 = [[NSSortDescriptor alloc] initWithKey:@"status" ascending:NO];
        NSSortDescriptor *sd3 = [[NSSortDescriptor alloc] initWithKey:@"startDateTime" ascending:YES];
        descriptors = @[sd1,sd2,sd3];
        
    } else {
        sortString = sortAttribute;
        sortAscending = ascending;
        sd1 = [[NSSortDescriptor alloc] initWithKey:@"section" ascending:YES];
        sd2 = [[NSSortDescriptor alloc] initWithKey:sortString ascending:sortAscending selector:@selector(localizedCompare:)];
        descriptors = @[sd1,sd2];
        

    }
    

    
       fetchRequest.sortDescriptors = descriptors;
    
    // Perform the fetch
    [NSFetchedResultsController deleteCacheWithName:nil];
    
    NSError __autoreleasing *error;
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:_managedObjectContext
                                                                                                 sectionNameKeyPath:@"section"
                                                                                                          cacheName:nil];
    
    if (![fetchedResultsController performFetch:&error]) {
         //FUTURE ERROR HANDLING
      //  NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }
    
    return fetchedResultsController;
}

- (NSFetchedResultsController *)fetchActionsMatching:(id)object
                                         sortingBy:(NSString *)sortAttribute
                                       ascendingBy:(BOOL)ascending
                                    includeDeleted:(BOOL)inclDeleted
{
    // TO DO put the fetch code in here and put results into _fetchedResultsController
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Action" inManagedObjectContext:_managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    
    // Setup the predicate string
    if ([object isKindOfClass:[Issue class]]) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inIssue == %@",object];
    } else if ([object isKindOfClass:[Risk class]]) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inRisk == %@",object];
    } else if ([object isKindOfClass:[Meeting class]]) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inMeeting == %@",object];
    }
    
    
    [fetchRequest setFetchBatchSize:100];
    
    // Apply sort order YES=ASCENDING
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortAttribute ascending:ascending];
    NSArray *descriptors = @[sortDescriptor];
    fetchRequest.sortDescriptors = descriptors;
    
    // Perform the fetch
    NSError __autoreleasing *error;
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                               managedObjectContext:_managedObjectContext
                                                                                                 sectionNameKeyPath:nil cacheName:nil];
    
    if (![fetchedResultsController performFetch:&error]) {
         //FUTURE ERROR HANDLING
      //  NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }
    
    return fetchedResultsController;
}

#pragma mark - Data Model Fetch Methods
- (void)fetchHCP
{
    /* Results a List of Projects which do not include Projects
     * with a status of completed.
     */
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:_managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    // Setup the predicate string
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"status != %@",@"Completed"];
        
    [fetchRequest setFetchBatchSize:0];
    
    // Apply sort order YES=ASCENDING
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES selector:@selector(localizedCompare:)];
    NSArray *descriptors = @[sortDescriptor];
    fetchRequest.sortDescriptors = descriptors;
    
    // Perform the fetch
    NSError __autoreleasing *error;
    
    _fetchedRCHCP = nil;
    
    _fetchedRCHCP = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                        managedObjectContext:_managedObjectContext
                                                          sectionNameKeyPath:nil
                                                                   cacheName:nil];
    
    if (![_fetchedRCHCP performFetch:&error]) {
         //FUTURE ERROR HANDLING
       // NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }

    
}


- (void)fetchAP
{
    /* Results a List of all Projects including Projects
     * with a status of completed.
     */
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:_managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    [fetchRequest setFetchBatchSize:0];
    
    // Apply sort order YES=ASCENDING
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES selector:@selector(localizedCompare:)];
    NSArray *descriptors = @[sortDescriptor];
    fetchRequest.sortDescriptors = descriptors;
    
    // Perform the fetch
    NSError __autoreleasing *error;
    
    _fetchedRCAP = nil;
    
    _fetchedRCAP = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                        managedObjectContext:_managedObjectContext
                                                          sectionNameKeyPath:nil
                                                                   cacheName:nil];
    
    if (![_fetchedRCAP performFetch:&error]) {
         //FUTURE ERROR HANDLING
       // NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }
    

}


#pragma mark - Utility Methods
// Utility Date String formatter
- (NSString *)convertToLocalDate:(NSDate *)aDate
{
    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"dd MMM yyyy"
                                                             options:0
                                                              locale:[NSLocale currentLocale]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formatString];
    NSString *dateString = [dateFormatter stringFromDate:aDate];
    
    return dateString;
}

- (NSString *)convertToLocalDateAndTime:(NSDate *)aDate
{
    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"dd MMM yyyy HH mm"
                                                             options:0
                                                              locale:[NSLocale currentLocale]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formatString];
    NSString *dateString = [dateFormatter stringFromDate:aDate];
    
    return dateString;
}

- (NSString *)convertToLocalReferenceDateAndTime:(NSDate *)aDate
{
    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"dd MMM yyyy"
                                                             options:0
                                                              locale:[NSLocale currentLocale]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formatString];
    NSString *dateString = [dateFormatter stringFromDate:aDate];
    
    NSMutableString *final = [[NSMutableString alloc] initWithFormat:@"%@ 01:00",dateString];
    
    return final;
}

- (NSString *)convertToLocalCurrency:(NSNumber *)currency withFormat:(NSNumberFormatter *)formatter
{
    if(formatter==nil){
        formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [formatter setLocale:[NSLocale currentLocale]];
    }
    return [formatter stringFromNumber:currency];
}

- (NSString *)getSortString:(NSString *)item
{
    NSString *final;
    
    
    if ([item isEqualToString:@"Action"]) {
        
        if (_currentProject.sortActionField == nil) {
            final = @"title";
        } else if ([_currentProject.sortActionField isEqualToString:@"Title"]){
            final = @"title";
        } else if ([_currentProject.sortActionField isEqualToString:@"Owner"]){
            final = @"actionOwner";
        } else if ([_currentProject.sortActionField isEqualToString:@"RAG"]){
            final = @"rag";
        } else if ([_currentProject.sortActionField isEqualToString:@"Target Completion Date"]){
            final = @"targetCompletionDate";
        } else if ([_currentProject.sortActionField isEqualToString:@"Last Modified Date"]){
            final = @"lastModifiedDate";
        }
        
    } else if ([item isEqualToString:@"Issue"]) {
        if (_currentProject.sortIssueField == nil) {
            final = @"title";
        } else if ([_currentProject.sortIssueField isEqualToString:@"Title"]){
            final = @"title";
        } else if ([_currentProject.sortIssueField isEqualToString:@"Owner"]){
            final = @"issueOwner";
        } else if ([_currentProject.sortIssueField isEqualToString:@"RAG"]){
            final = @"rag";
        } else if ([_currentProject.sortIssueField isEqualToString:@"Target Completion Date"]){
            final = @"targetCompletionDate";
        } else if ([_currentProject.sortIssueField isEqualToString:@"Last Modified Date"]){
            final = @"lastModifiedDate";
        }
        
    } else if ([item isEqualToString:@"Risk"]) {
        if (_currentProject.sortRiskField == nil) {
            final = @"title";
        } else if ([_currentProject.sortRiskField isEqualToString:@"Title"]){
            final = @"title";
        } else if ([_currentProject.sortRiskField isEqualToString:@"Owner"]){
            final = @"riskOwner";
        } else if ([_currentProject.sortRiskField isEqualToString:@"RAG"]){
            final = @"rag";
        } else if ([_currentProject.sortRiskField isEqualToString:@"Target Completion Date"]){
            final = @"plannedCompletionDate";
        } else if ([_currentProject.sortRiskField isEqualToString:@"Last Modified Date"]){
            final = @"lastModifiedDate";
        }
        
    } else if ([item isEqualToString:@"Meeting"]) {
        if (_currentProject.sortMeetingField == nil) {
            final = @"title";
        } else if ([_currentProject.sortMeetingField isEqualToString:@"Title"]){
            final = @"title";
        } else if ([_currentProject.sortMeetingField isEqualToString:@"Owner"]){
            final = @"meetingOwner";
        } else if ([_currentProject.sortMeetingField isEqualToString:@"RAG"]){
            final = @"rag";
        } else if ([_currentProject.sortMeetingField isEqualToString:@"Target Completion Date"]){
            final = @"targetCompletionDate";
        } else if ([_currentProject.sortMeetingField isEqualToString:@"Last Modified Date"]){
            final = @"lastModifiedDate";
        }
        
    }
    
    return final;
    
}

- (BOOL)getSortAscending:(NSString *)item
{
    BOOL final = YES;
    
    if ([item isEqualToString:@"Action"]) {
        if ([_currentProject.sortActionField isEqualToString:@"Last Modified Date"]) {
            final = NO;
        }
        
    } else if ([item isEqualToString:@"Issue"]) {
        if ([_currentProject.sortIssueField isEqualToString:@"Last Modified Date"]) {
            final = NO;
        }
        
    } else if ([item isEqualToString:@"Risk"] && [_currentProject.sortRiskField isEqualToString:@"Last Modified Date"]) {
        final = NO;
        
    } 
    
    return final;
}


#pragma mark - DATA SETUP
// DATA SETUP - One off to populate database
- (void)setupData:(int)count
{
    // TO-DO steps........
    // 1. Check existence of one Config object
    // 2. Check existence of Projects in DB if so then exit
    
    int projectCount = count;
    int actionCount = 100;
    int issueCount = 100;
    int riskCount = 100;
    int meetingCount = 100;
    int notesCount = 100;
    
    int actionNoteCount = 0;
    int issueNoteCount = 0;
    int riskNoteCount = 0;
    int meetingNoteCount = 0;
    
    
    
    if ([[[self fetchItemsMatching:nil forEntityType:@"Project" sortingBy:@"title" ascendingBy:YES includeDeleted:NO] fetchedObjects] count] < 1) {
        
   
        
        // Create Project Objects x 10
        for (int i = 1; i <= projectCount; i++) {
            Project *p = [self newProject];
            p.title = [NSString stringWithFormat:@"Project Number %d", i];
            p.deleted = [NSNumber numberWithBool:NO];
            
            p.initialBudget = [NSNumber numberWithInt:100000];
            
            if (i <= (projectCount * .6)) {
                p.initialCompletionDate = [NSDate dateWithTimeIntervalSinceNow:2000000];
            }
            if (i <= (projectCount * .2)) {
                p.initialCompletionDate = [NSDate dateWithTimeIntervalSinceNow:400000];
            }
            
            //Create Actions for each Project x 5/project
            for (int j = 1; j <= actionCount; j++) {
                Action *a = [self newAction:p];
                a.title = [NSString stringWithFormat:@"Action number %d for Project %d", j, i];
                if (j <= (actionCount * .6)) {
                    a.targetCompletionDate = [NSDate dateWithTimeIntervalSinceNow:800000];
                }
                if (j <= (actionCount * .2)) {
                    a.targetCompletionDate = [NSDate dateWithTimeIntervalSinceNow:200000];
                }
               
                a.deleted = [NSNumber numberWithBool:NO];
                
                // Add Notes
                for (int k = 1; k <= actionNoteCount; k++) {
                    Note *n = [self newNote:a];
                    n.title = [NSString stringWithFormat:@"Note number %d for Action %d for Project %d", k, j, i];
                    
                }
            }
            
            //Create Issues for each Project x 5/project
            for (int j = 1; j <= issueCount; j++) {
                Issue *is = [self newIssue:p];
                is.title = [NSString stringWithFormat:@"Issue number %d for Project %d", j, i];
                is.deleted = [NSNumber numberWithBool:NO];
                
                if (j <= (issueCount * .6)) {
                    is.targetCompletionDate = [NSDate dateWithTimeIntervalSinceNow:800000];
                } else if (j <= (issueCount * .2)) {
                    is.targetCompletionDate = [NSDate dateWithTimeIntervalSinceNow:200000];
                } else {
                    is.targetCompletionDate = [NSDate dateWithTimeIntervalSinceNow:60];
                }
                
                // Add Notes
                for (int k = 1; k <= issueNoteCount; k++) {
                    Note *n = [self newNote:is];
                    n.title = [NSString stringWithFormat:@"Note number %d for Issue %d for Project %d", k, j, i];
                    
                }
            }
            
            //Create Risks for each Project x 5/project
            for (int j = 1; j <= riskCount; j++) {
                Risk *r = [self newRisk:p];
                r.title = [NSString stringWithFormat:@"Risk number %d for Project %d", j, i];
                r.deleted = [NSNumber numberWithBool:NO];
                
                if (j <= (issueCount * .6)) {
                    r.plannedCompletionDate = [NSDate dateWithTimeIntervalSinceNow:800000];
                } else if (j <= (issueCount * .2)) {
                    r.plannedCompletionDate = [NSDate dateWithTimeIntervalSinceNow:200000];
                } else {
                    r.plannedCompletionDate = [NSDate dateWithTimeIntervalSinceNow:60];
                }
                
                
                // Add Notes
                for (int k = 1; k <= riskNoteCount; k++) {
                    Note *n = [self newNote:r];
                    n.title = [NSString stringWithFormat:@"Note number %d for Risk %d for Project %d", k, j, i];
                    
                }
            }
            
            
            //Create Meetings for each Project x 5/project
            for (int j = 1; j <= meetingCount; j++) {
                Meeting *m = [self newMeeting:p];
                m.title = [NSString stringWithFormat:@"Meeting number %d for Project %d", j, i];
                m.deleted = [NSNumber numberWithBool:NO];
                
                
                // Add Notes
                for (int k = 1; k <= meetingNoteCount; k++) {
                    Note *n = [self newNote:m];
                    n.title = [NSString stringWithFormat:@"Note number %d for Meeting %d for Project %d", k, j, i];
                    
                }
            }
            
            //Create Notes for each Project x 5/project
            for (int j = 1; j <= notesCount; j++) {
                Note *n = [self newNote:p];
                n.title = [NSString stringWithFormat:@"Note number %d for Project %d", j, i];

            }
            
            
              
            //Create Stakeholders for each Project x 5/project
            
        }
        
        [self saveContext];
        
        
        
    }
    
}
- (BOOL)isHelpVisible
{
    return YES;
}




@end

/*
CLASS FILE CHANGES AND BUG FIXES
format =  date, method, comment/change/amendment,  persons initials, = latest first
-----------------------------------------------------------------------------------
 24/04/2013 convertToLocalDate      Created new method. KP
 08/04/2013 fetchAddressBookList    created new method. KP
 08/04/2013 saveToAddressBook       Created new method. KP
 08/03/2013 fetchItemsMatching      Included Predicate check for deleted = NO. KP.
 08/03/2013 new<ITEMs>              Included deleted = No for all new item methods. KP.
 08/03/2013 setupData               Included deleted = No for all item creation. KP.
 08/03/2013 deleteObject            Changed to flag delete attributes for selected entities. KP.

 
*/
