//
//  IssueCollectionController.m
//  Project Journal
//
//  Created by Peter Pomlett on 10/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "IssueFormContainer.h"
#import <QuartzCore/QuartzCore.h>
#import "IssueCollectionController.h"
#import "ItemCollectionCell.h"
#import "PJColour.h"
#import "ActionFormContainer.h"

@interface IssueCollectionController ()<NSFetchedResultsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIActionSheetDelegate, IssueFormControllerDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSFetchedResultsController *issueResultsController;
@property (weak, nonatomic) IBOutlet UICollectionView *issueCollection;
@property (strong, nonatomic) Issue *selectedIssue;
@property (strong, nonatomic)  PJColour *cellColour;
@property (nonatomic, strong) DataHelper *helper;
@property (strong, nonatomic) UIActionSheet *itemSheet;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addItem;
@property (strong,nonatomic) id actionParent;
@property (strong, nonatomic) Action *selectedAction;
- (NSDateFormatter *)formatterDate;
@property float scaleIssue;
@property float scaleIssue2;
@property (weak, nonatomic) IBOutlet UIStepper *issueStepper;
@end

@implementation IssueCollectionController



-(void)getCellSize{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.scaleIssue =  [defaults floatForKey:@"scaleIssueView"];
    self.scaleIssue2 = [defaults floatForKey:@"scaleIssue2View"];
    self.issueStepper.value = [defaults integerForKey:@"issueStepperView"];
}
- (IBAction)issueSize:(UIStepper *)sender {
    int cellSize = sender.value;
    if(cellSize == 3){
        self.scaleIssue =1;
        self.scaleIssue2 =1;
        [self.issueCollection.collectionViewLayout invalidateLayout];
    }
    
    if(cellSize == 2){
        self.scaleIssue =0.228;
        self.scaleIssue2 =1;
        [self.issueCollection.collectionViewLayout invalidateLayout];
        
    }
    if(cellSize == 1){
        self.scaleIssue =0.228;
        self.scaleIssue2 =0.118;
        [self.issueCollection.collectionViewLayout invalidateLayout];
        
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:self.scaleIssue forKey:@"scaleIssueView"];
    [defaults setFloat:self.scaleIssue2  forKey:@"scaleIssue2View"];
    [defaults setInteger:self.issueStepper.value forKey:@"issueStepperView"];
    [defaults synchronize];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

        return CGSizeMake(240 *self.scaleIssue2, 110*self.scaleIssue);

}






- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

-(void)getActionParent:(id)parent{
    self.selectedIssue = parent;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"issue" sender: self] ;
    }];
}

- (void)ActionFormContainerDidFinish:(ActionFormContainer *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)getNewAction:(id)itenAction{
    self.actionParent = itenAction;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"parentNewAction" sender: self] ;
    }];
}

-(void)getTheActionForm:(Action *)itemAction{
    self.selectedAction = itemAction;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"actions" sender: self] ;
    }];
}

- (void)IssueFormContainerDidFinish:(IssueFormContainer *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)newIssue:(id)sender {
    if (!self.itemSheet){
        self.itemSheet = [[UIActionSheet alloc] initWithTitle:@"In Project"
                                                     delegate:self
                                            cancelButtonTitle:nil
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:@"New Issue",nil];
    }
    [self.itemSheet showFromBarButtonItem:sender animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(self.itemSheet == actionSheet){
        if(buttonIndex == 0){
            [self performSegueWithIdentifier:@"NewIssue" sender:self];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"issue"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        [[segue destinationViewController] setContainerIssue:self.selectedIssue];
    }
    if ([[segue identifier] isEqualToString:@"NewIssue"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        self.selectedIssue = nil;
        [[segue destinationViewController] setContainerIssue:self.selectedIssue];
    }
    if ([[segue identifier] isEqualToString:@"parentNewAction"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.actionParent];
        self.selectedAction = nil;
        [[segue destinationViewController] setContainerAction:self.selectedAction];
    }
    if ([[segue identifier] isEqualToString:@"actions"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        [[segue destinationViewController] setContainerAction:self.selectedAction];
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.issueCollection reloadData];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo;
    sectionInfo = [self.issueResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    ItemCollectionCell *cell ;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    Issue *issue = [self.issueResultsController objectAtIndexPath:indexPath];
    if([issue isForReview]){
        [ cell.reviewLabel setHidden:NO];
    }
    else{
        [ cell.reviewLabel setHidden:YES];
    }
    if([[issue fetchDateRAGStatus] isEqualToString:@"Amber"] ){
        cell.dateBadge.hidden = NO;
        cell.dateBadge.backgroundColor = self.cellColour.amber;
    }
    else if([[issue fetchDateRAGStatus] isEqualToString:@"Red"] ){
        cell.dateBadge.hidden = NO;
        cell.dateBadge.backgroundColor = self.cellColour.red;
    }
    else{
        cell.dateBadge.hidden = YES;
    }
    if ([[issue fetchRAGStatus] isEqualToString:@"Red"]){
        cell.contentView.layer.backgroundColor  = self.cellColour.red.CGColor;
    }
    if ([[issue fetchRAGStatus] isEqualToString:@"Green"]){
        cell.contentView.layer.backgroundColor  = self.cellColour.green.CGColor;
    }
    if ([[issue fetchRAGStatus] isEqualToString:@"Amber"]){
        cell.contentView.layer.backgroundColor  = self.cellColour.amber.CGColor;
    }
    if ([[issue fetchRAGStatus] isEqualToString:@"Blue"]){
        cell.contentView.layer.backgroundColor  = self.cellColour.blue.CGColor;
    }
    cell.titleLabel.text = [issue title];
    cell.ownerLabel.text = [issue issueOwner];
    cell.priorityLabel.text = [issue priority];
    if([issue targetCompletionDate]){
        cell.dateLabel.text = [[self formatterDate] stringFromDate:[issue targetCompletionDate]];
    }
    else{
        cell.dateLabel.text =@"Set completion date!";
    }
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(self.issueCollection == collectionView){
        self.selectedIssue = [self.issueResultsController objectAtIndexPath:indexPath];
    }
    return YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    if (self.itemSheet != nil) {
        [self.itemSheet dismissWithClickedButtonIndex:self.itemSheet.cancelButtonIndex animated:NO];
        self.itemSheet = nil;
    }
    
  [[NSNotificationCenter defaultCenter] removeObserver:self];   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    if([self.project.status isEqualToString:@"Completed"]){
        self.navigationItem.rightBarButtonItem = nil;
    }
    [self getCellSize];
    [self.issueStepper setDecrementImage:[UIImage imageNamed:@"stepperSmall"] forState:UIControlStateNormal];
    [self.issueStepper setIncrementImage:[UIImage imageNamed:@"stepperLarge"] forState:UIControlStateNormal];
    if(self.cellColour == nil){
        self.cellColour = [[PJColour alloc]init];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.issueResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Issue"
                                                       sortingBy:@"title" ascendingBy:YES includeDeleted:NO];
    self.issueResultsController.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
    
}

-(void)refreshFromBackground{
    [self.issueCollection reloadData];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
