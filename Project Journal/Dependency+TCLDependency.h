//
//  Dependency+TCLDependency.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Dependency.h"
#import "DataHelper.h"

@interface Dependency (TCLDependency)


- (NSString *)fetchRAGStatus;
- (NSString *)getStatus;
- (NSString *)getDetailText;
- (NSString *)getSummaryText;

@end
