//
//  IssueDateViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 07/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@class IssueDateViewController;

@protocol IssueDateViewControllerDelegate <NSObject>

-(void) upDateRagBadge;
-(void) upDateReviewBadge;
-(void) dimSuper;
-(void) UndimSuper;
@end

@interface IssueDateViewController : UIViewController
@property (weak, nonatomic) id <IssueDateViewControllerDelegate> delegate;
@property (strong , nonatomic) Issue *issue;
- (void)dismissPopovers;
@end
