//
//  Project.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 18/07/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Project.h"
#import "Action.h"
#import "Dependency.h"
#import "Issue.h"
#import "Media.h"
#import "Meeting.h"
#import "Note.h"
#import "Risk.h"
#import "Stakeholder.h"


@implementation Project

@dynamic actualBudgetToDate;
@dynamic actualCompletionDate;
@dynamic actualCostAtCompletion;
@dynamic creationDate;
@dynamic deleted;
@dynamic hideCompletedActionFlag;
@dynamic hideCompletedIssueFlag;
@dynamic hideCompletedMeetingFlag;
@dynamic hideCompletedRiskFlag;
@dynamic initialBudget;
@dynamic initialCompletionDate;
@dynamic lastModifiedDate;
@dynamic projectID;
@dynamic projectManager;
@dynamic projectPhase;
@dynamic projectStartDate;
@dynamic rag;
@dynamic reviewDate;
@dynamic revisedBudget;
@dynamic revisedCompletionDate;
@dynamic sortActionField;
@dynamic sortIssueField;
@dynamic sortMeetingField;
@dynamic sortRiskField;
@dynamic status;
@dynamic summary;
@dynamic targetCompletionDate;
@dynamic title;
@dynamic type;
@dynamic uuid;
@dynamic oldStatus;
@dynamic projectActions;
@dynamic projectDependencies;
@dynamic projectIssues;
@dynamic projectMedia;
@dynamic projectMeetings;
@dynamic projectNotes;
@dynamic projectRisks;
@dynamic projectStakeholders;

@end
