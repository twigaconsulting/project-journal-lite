//
//  Meeting+TCLMeeting.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Meeting+TCLMeeting.h"

@interface Meeting (PrimitiveAccessors)
- (void)setPrimitiveEndDateTime:(NSDate *)endDateTime;
- (void)setPrimitiveStatus:(NSString *)string;
- (NSString *)primitiveStatus;
@end

@implementation Meeting (TCLMeeting)


- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    return formatter;
}


//date and time
- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    
    return formatter;
}

#pragma mark - Primitive Methods
- (void)setEndDateTime:(NSDate *)endDateTime
{
    [self willAccessValueForKey:@"endDateTime"];
    [self setPrimitiveEndDateTime:endDateTime];
    [self didChangeValueForKey:@"endDateTime"];
    [self validateMeetingIsCompleted];
}

- (void)setStatus:(NSString *)string
{
    
    // If Completed then set child items to to be reviewed & notes to completed
    if ([string isEqualToString:@"Completed"] &&
        ([self.endDateTime compare:[NSDate date]] == NSOrderedAscending ||
         [self.endDateTime compare:[NSDate date]] == NSOrderedSame ||
         [self.inProject.status isEqualToString:@"Completed"])) {

            if ([self.meetingActions count] > 0) {
                for (Action *item in self.meetingActions) {
                    item.status = @"Completed";
                }
            }
            if ([self.meetingNotes count] > 0) {
                for (Note *item in self.meetingNotes) {
                    item.status = @"Completed";
                }
            }
            self.oldStatus = self.status;
            
            [self willAccessValueForKey:@"status"];
            [self setPrimitiveStatus:string];
            [self didChangeValueForKey:@"status"];
            
    } else if ([self.endDateTime compare:[NSDate date]] == NSOrderedDescending){
        if ([self.meetingActions count] > 0) {
            for (Action *item in self.meetingActions) {
                item.status = item.oldStatus;
            }
        }
        if ([self.meetingNotes count] > 0) {
            for (Note *item in self.meetingNotes) {
                item.status = @"notCompleted";
            }
        }
        
        self.oldStatus = self.status;
        
        [self willAccessValueForKey:@"status"];
        [self setPrimitiveStatus:string];
        [self didChangeValueForKey:@"status"];
        
    }

    
    
}

- (NSString *)status
{
    [self willAccessValueForKey:@"status"];
    NSString *final = [self primitiveStatus];
    [self didAccessValueForKey:@"status"];
    
    if (([self.endDateTime compare:[NSDate date]] == NSOrderedAscending || [self.endDateTime compare:[NSDate date]] == NSOrderedSame) && [final isEqualToString:@"notCompleted"]) {
        
        self.oldStatus = final;
        
        [self willAccessValueForKey:@"status"];
        [self setPrimitiveStatus:@"Completed"];
        [self didChangeValueForKey:@"status"];
    }
    
    
    return final;
    
}




#pragma mark - HTML Methods
// Get Textual representation of the Project for email, printing, and file storage
- (NSString *)getDetailHTML
{
    
    // BUILD WEB PAGE IN A STRING
    NSMutableString *result = [[NSMutableString alloc] initWithString:@"<!DOCTYPE html>"];
    [result appendFormat:@"<html>"];
    [result appendFormat:@"<body>"];
    
    
    [result appendFormat:@"<br><br><b><u>MEETING INFORMATION</u></b><br>"];
    [result appendFormat:@"<b>ID:</b> %@<br>",self.meetingID];
    [result appendFormat:@"<b>Title:</b> %@<br>",self.title];
    if ([self.summary length] > 0) {
        [result appendFormat:@"<b>Summary:</b> %@<br>",self.summary];
    }
    [result appendFormat:@"<b>Start Date & Time:</b> %@<br>",[[self formatterDateTime] stringFromDate:self.startDateTime]];
    [result appendFormat:@"<b>End Time:</b>          %@<br>",[[self formatterDateTime] stringFromDate:self.endDateTime]];
    
    // List attendees
    [result appendFormat:@"<br><b><u>ATTENDEES INVITED</u></b><br>"];
    NSArray *attendees = [[NSArray alloc] initWithArray:[self.meetingAttendees allObjects]];
    for (int i = 0; i < [attendees count]; i++) {
        [result appendFormat:@"%@<br>",[[attendees objectAtIndex:i] fullName]];
    }
    
    // List Presents
    [result appendFormat:@"<br><b><u>PRESENT</u></b><br>"];
    NSArray *present = [[NSArray alloc] initWithArray:[self.meetingPresent allObjects]];
    for (int i = 0; i < [present count]; i++) {
        [result appendFormat:@"%@<br>",[[present objectAtIndex:i] fullName]];
    }
    
    
    //List Agenda
    [result appendFormat:@"<br><br><b><u>AGENDA</u></b><br>"];
    //NSArray *agenda = [[NSArray alloc] initWithArray:self.meetingAgenda];
    NSEnumerator *enumerator = [self.meetingAgenda objectEnumerator];
    id agenda;
    while (agenda = [enumerator nextObject]) {
        [result appendFormat:@"%lu. ",(unsigned long)[self.meetingAgenda indexOfObject:agenda] + 1];
        [result appendFormat:@"%@<br>",[agenda title]];
            

    }
    
    // List Notes
    [result appendFormat:@"<br><br><b><u>NOTES</u></b><br>"];
    NSArray *notes = [[NSArray alloc] initWithArray:[self.meetingNotes allObjects]];
    for (int i = 0; i < [notes count]; i++) {
        if (![[[notes objectAtIndex:i] deleted] boolValue]) {
            [result appendFormat:@"<b>%@</b><br>",[[self formatterDateTime] stringFromDate:[[notes objectAtIndex:i] lastModifiedDate]]];
            [result appendFormat:@"%@<br><br>",[[notes objectAtIndex:i] summary]];
        }
    }

    
    //Footer in page
    [result appendFormat:@"</html>"];
    [result appendFormat:@"</body>"];
    
    //Advert for Project Journal
    [result appendFormat:@"<br><br><br><br><a href:\"http://www.twigaconsulting.co.uk\">This information is provided by the Project Journal iPad app.</a><br><br>"];
    
    return result;

}


- (BOOL)isInAttendeeList:(Person *)person
{
    
    NSArray *attendees = [[NSArray alloc] initWithArray:[self.meetingAttendees allObjects]];
    for (int i = 0; i < [attendees count]; i++) {
        if ([[[attendees objectAtIndex:i] fullName] isEqualToString:[person fullName]]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isInPresentList:(Person *)person
{
    
    NSArray *attendees = [[NSArray alloc] initWithArray:[self.meetingPresent allObjects]];
    for (int i = 0; i < [attendees count]; i++) {
        if ([[[attendees objectAtIndex:i] fullName] isEqualToString:[person fullName]]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isInCalendar
{
    BOOL result =  NO;
    
    if (self.ekEvent) {
        result = YES;
    }
    
    return result;
}

- (void)moveAgendaFromIndex:(NSNumber *)from toIndex:(NSNumber *)to
{
   
}

- (void)validateMeetingIsCompleted
{
    if ([self.endDateTime compare:[NSDate date]] == NSOrderedAscending || [self.endDateTime compare:[NSDate date]] == NSOrderedSame) {
        self.status = @"Completed";
    } else {
        self.status = @"notCompleted";
    }

    
}

@end


/*
 CLASS FILE CHANGES AND BUG FIXES
 format =  date, method, comment/change/amendment,  persons initials, = latest first
 ---------------------------------------------
 14/05/2013  moveAgendaFromIndex Created new methos for use by view controllers.KP
 24/04/2013  isInAttendeeList    Created new method for use by view cotrollers.KP.
 24/04/2013  isInPresentList     Created new method for use by view cotrollers.KP.
 24/04/2013  getDetailHTML       Create new method. KP.
 
 */

