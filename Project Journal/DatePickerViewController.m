//
//  DatePickerViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 27/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "DataHelper.h"
#import "DatePickerViewController.h"

@interface DatePickerViewController ()

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) DataHelper *helper;
- (NSDateFormatter *)formatterDate;
@end

@implementation DatePickerViewController


- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (IBAction)dateChanged:(UIDatePicker *)sender {
     self.dateLabel.text = [[self formatterDate] stringFromDate:self.picker.date];
}

- (IBAction)Done {
[self.delegate DatePickerViewControllerControllerDidFinish:self];
[self.delegate DatePicked:[self.picker date] andController:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
      self.helper = [DataHelper sharedInstance];
    if(self.pickerValue){
        self.picker.date=self.pickerValue;
        
    }
     self.dateLabel.text = [[self formatterDate] stringFromDate:self.picker.date];
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
