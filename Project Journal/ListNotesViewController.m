//
//  ListNotesViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 18/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "ListNotesViewController.h"
#import "NoteCell.h"
#import "NotePopover.h"
@interface ListNotesViewController ()<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, NotePopoverDelegate, UIPopoverControllerDelegate>
@property (strong, nonatomic) UIStoryboardPopoverSegue* popSegue;
@property (strong, nonatomic) DataHelper *helper;
@property (strong, nonatomic) Note *note;
@property (strong, nonatomic) NoteCell *noteCell;
@property (weak, nonatomic) IBOutlet UIImageView *tableImageView;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *buttonImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleHeader;
- (NSDateFormatter *)formatterDateTime;
@end

@implementation ListNotesViewController


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
    
    
}


- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}
//dismiss popovers that are showing when view rotates
- (void)dismissPopovers{
    [self.delegate UndimSuper];
    if ([self.popSegue.popoverController isPopoverVisible])
    {
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
}


-(void) NotePopoverDidfinish:(NotePopover *)controller{
    [self.delegate UndimSuper];
    if ([self.popSegue.popoverController isPopoverVisible])
    {
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
}

#pragma mark - VC methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{[self.delegate dimSuper];
    if ([[segue identifier] isEqualToString:@"newNote"]) {
        self.note = nil;
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        if([self.item isKindOfClass:[Action class]]){
            Action *action = self.item;
            [[segue destinationViewController] setItem:action];}
        if([self.item isKindOfClass:[Meeting class]]){
            Meeting *meeting = self.item;
            [[segue destinationViewController] setItem:meeting];}
        if([self.item isKindOfClass:[Issue class]]){
            Issue *issue = self.item;
            [[segue destinationViewController] setItem:issue];}
        if([self.item isKindOfClass:[Project class]]){
            Project *project = self.item;
            [[segue destinationViewController] setItem:project];
        }
        if([self.item isKindOfClass:[Risk class]]){
            Risk *risk = self.item;
            [[segue destinationViewController] setItem:risk];
        }
    }
    
    if ([[segue identifier] isEqualToString:@"oldNote"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        if([self.item isKindOfClass:[Meeting class]]){
            Meeting *meeting = self.item;
            [[segue destinationViewController] setNote:self.note];
            [[segue destinationViewController] setItem:meeting];}
        if([self.item isKindOfClass:[Issue class]]){
            Issue *issue = self.item;
            [[segue destinationViewController] setNote:self.note];
            [[segue destinationViewController] setItem:issue];}
        if([self.item isKindOfClass:[Action class]]){
            Action *action = self.item;
            [[segue destinationViewController] setNote:self.note];
            [[segue destinationViewController] setItem:action];}
        if([self.item isKindOfClass:[Project class]]){
            Project *project = self.item;
            [[segue destinationViewController] setNote:self.note];
            [[segue destinationViewController] setItem:project];
        }
        if([self.item isKindOfClass:[Risk class]]){
            Risk *risk = self.item;
            [[segue destinationViewController] setNote:self.note];
            [[segue destinationViewController] setItem:risk];
        }
    }
    self.popSegue.popoverController.delegate = self;
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    UIEdgeInsets toolInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage *tableImage = [UIImage imageNamed:@"TextFrame2"];
    tableImage = [tableImage resizableImageWithCapInsets:toolInsets];
    self.tableImageView.image = tableImage;
   // self.titleImageView.alpha = 0.5;
    self.titleImageView.image = tableImage;
    self.buttonImageView.image = tableImage;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if([self.item isKindOfClass:[Issue class]]){
        self.titleHeader.text = @"Issue Title";
        Issue *issue = self.item;
        self.titleLabel.text = issue.title;
    }
    if([self.item isKindOfClass:[Risk class]]){
        self.titleHeader.text = @"Risk Title";
        Risk *risk = self.item;
        self.titleLabel.text = risk.title;
    }
    if([self.item isKindOfClass:[Meeting class]]){
        self.titleHeader.text = @"Meeting Title";
        Meeting *meeting = self.item;
        self.titleLabel.text = meeting.title;
    }
    if([self.item isKindOfClass:[Action class]]){
        self.titleHeader.text = @"Action Title";
        Action *action = self.item;
        self.titleLabel.text = action.title;
    }
    if([self.item isKindOfClass:[Project class]]){
        self.titleHeader.text = @"Project Title";
        Project *project = self.item;
        self.titleLabel.text = project.title;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    
    if([self.item isKindOfClass:[Meeting class]]){
        Meeting *meeting = self.item;
        self.managedObjectContext = meeting.managedObjectContext;
    }
    
    if([self.item isKindOfClass:[Issue class]]){
        Issue *issue = self.item;
        self.managedObjectContext = issue.managedObjectContext;
    }
    if([self.item isKindOfClass:[Project class]]){
        Project *project = self.item;
        self.managedObjectContext = project.managedObjectContext;
    }
    if([self.item isKindOfClass:[Action class]]){
        Action *action = self.item;
        self.managedObjectContext = action.managedObjectContext;
    }
    if([self.item isKindOfClass:[Risk class]]){
        Risk *risk = self.item;
        self.managedObjectContext = risk.managedObjectContext;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Notes";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.noteCell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:self.noteCell atIndexPath:indexPath];
    return self.noteCell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.managedObjectContext deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             //FUTURE ERROR HANDLING
          //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
          //  abort();
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}


#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    //fetch code in here and put results into _fetchedResultsController
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Note" inManagedObjectContext:self.managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    if([self.item isKindOfClass:[Action class]]){
        Action *action = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inAction == %@",action];
    }
    
    
    if([self.item isKindOfClass:[Meeting class]]){
        Meeting *meeting = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inMeeting == %@",meeting];
    }
    
    if([self.item isKindOfClass:[Issue class]]){
        Issue *issue = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inIssue == %@",issue];
    }
    
    if([self.item isKindOfClass:[Risk class]]){
        Risk *risk = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inRisk == %@",risk];
    }
    
    if([self.item isKindOfClass:[Project class]]){
        Project *project = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@ && inAction == nil && inIssue == nil && inRisk == nil && inMeeting == nil",project];
    }
    
    
    
    [fetchRequest setFetchBatchSize:0];
    
    // Apply sort order YES=ASCENDING
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastModifiedDate" ascending:NO];
    NSArray *descriptors = @[sortDescriptor];
    fetchRequest.sortDescriptors = descriptors;
    
    // Perform the fetch
    NSError __autoreleasing *error;
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:self.managedObjectContext
                                                                          sectionNameKeyPath:nil cacheName:nil];
    self.fetchedResultsController.delegate = self;
    if (![self.fetchedResultsController performFetch:&error]) {
         //FUTURE ERROR HANDLING
      //  NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }
    
    return self.fetchedResultsController;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    
    [self.tableView reloadData];
    [self.delegate UpdateNotesBadge];
}

- (void)configureCell:(NoteCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.noteTextView.text = [object valueForKey:@"summary"];
    cell.dateLabel.text = [[self formatterDateTime] stringFromDate:[object valueForKey:@"lastModifiedDate"]];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if([object isKindOfClass:[Note class]]){
        self.note = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    return indexPath;
    
}

@end
