//
//  MeetingAttendeeViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 17/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "MeetingAttendeeViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "NewPersonViewController.h"

@interface MeetingAttendeeViewController ()<NewPersonViewcontrollerDelegate, UIAlertViewDelegate, ABPeoplePickerNavigationControllerDelegate,ABNewPersonViewControllerDelegate, UIPopoverControllerDelegate >
@property (strong , nonatomic) DataHelper *helper;
@property (strong,nonatomic) UIPopoverController *addressBookPopover;
@property (strong,nonatomic) UIPopoverController *aNewPersonPopover;
@property (strong,nonatomic) UIPopoverController *addNamePopover;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIAlertView *unnamedAlert;
@property (weak, nonatomic) IBOutlet UIImageView *tableImageView;
@property (weak, nonatomic) IBOutlet UIImageView *buttonImageView;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIButton *contactsButton;
@property (weak, nonatomic) IBOutlet UIButton *aNewPersonButton;
@property(nonatomic, readwrite) ABRecordRef displayedPerson;
@property (nonatomic,strong)NSString *enterFirstName;
@property (nonatomic,strong)NSString *enterLastName;

@end

@implementation MeetingAttendeeViewController


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
}

- (void)dismissPopovers{
    if (self.unnamedAlert.isVisible) {
        [self.unnamedAlert dismissWithClickedButtonIndex:0 animated:NO];
        self.unnamedAlert = nil;
    }
    
    if ([self.addressBookPopover isPopoverVisible])
    {
        [self.addressBookPopover dismissPopoverAnimated:YES];
    }
    if ([self.aNewPersonPopover isPopoverVisible])
    {
        [self.aNewPersonPopover dismissPopoverAnimated:YES];
    }
    if ([self.addNamePopover isPopoverVisible])
    {
        [self.addNamePopover dismissPopoverAnimated:YES];
    }
}

-(void)viewWillLayoutSubviews{
    UIEdgeInsets toolInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage *tableImage = [UIImage imageNamed:@"TextFrame2"];
    tableImage = [tableImage resizableImageWithCapInsets:toolInsets];
    self.tableImageView.image = tableImage;
    self.titleImageView.image = tableImage;
    self.buttonImageView.image = tableImage;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.titleLabel.text = self.meeting.title;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    self.managedObjectContext = self.meeting.managedObjectContext;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject
{
    // KP's Meeting Attendee
    
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    
    ABRecordRef person = self.displayedPerson;
    NSString* firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    Person *attendee= [NSEntityDescription insertNewObjectForEntityForName:@"Person"
                                                    inManagedObjectContext:context];
    attendee.firstName = firstName;
    attendee.surname = lastName;
    attendee.inMeetingAttendee = self.meeting;
    
    // This is a test
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         //FUTURE ERROR HANDLING
      //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
      //  abort();
    }
    
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return @"Invitees";
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.managedObjectContext deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             //FUTURE ERROR HANDLING
          //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
           // abort();
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}


#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    //code in here and put results into _fetchedResultsController
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:self.managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inMeetingAttendee == %@",self.meeting];
    
    
    [fetchRequest setFetchBatchSize:0];
    
    // Apply sort order YES=ASCENDING
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"surname" ascending:YES];
    NSArray *descriptors = @[sortDescriptor];
    fetchRequest.sortDescriptors = descriptors;
    
    // Perform the fetch
    NSError __autoreleasing *error;
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:self.managedObjectContext
                                                                          sectionNameKeyPath:nil cacheName:nil];
    
    self.fetchedResultsController.delegate = self;
    
    if (![self.fetchedResultsController performFetch:&error]) {
         //FUTURE ERROR HANDLING
       // NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }
    
    return self.fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{[self.delegate updateAttendeeBadge];
    [self.tableView endUpdates];
    
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [[object valueForKey:@"fullName"] description];
    if([self.meeting.meetingPresent containsObject:object])
    {  cell.detailTextLabel.text = @"Present";
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;}
    else{
        cell.detailTextLabel.text = @"" ;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.meeting.meetingPresent containsObject:[[self fetchedResultsController] objectAtIndexPath:indexPath]]){
        [self.meeting removeMeetingPresentObject:[[self fetchedResultsController] objectAtIndexPath:indexPath]];
    }
    else{
        [self.meeting addMeetingPresentObject:[[self fetchedResultsController] objectAtIndexPath:indexPath]];
    }
    [self.helper saveContext];
    [self.tableView reloadData];
    
}

// CONTACTS VIEWS - NEW & PICKER
// Delegate for Person View

- (void) personFirstName:(NSString *)first secondName:(NSString *)second andController:(NewPersonViewController *)controller
{    [self.delegate UndimSuper];
    [self.addNamePopover dismissPopoverAnimated:YES];
    
    CFErrorRef err = nil;
    ABAddressBookRef adbk = ABAddressBookCreateWithOptions(nil, &err);
    if (nil == adbk) {
         //FUTURE ERROR HANDLING
       // NSLog(@"error: %@", err);
        return;
    }
    ABRecordRef person = self.displayedPerson;
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef)first, nil);
    ABRecordSetValue(person, kABPersonLastNameProperty,(__bridge CFStringRef)second, nil);
    CFErrorRef er = nil;
    ABAddressBookAddRecord (adbk,person,&er);
    ABAddressBookSave(adbk, &er);
    
    
    CFRelease(adbk);
    
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    Person *attendee= [NSEntityDescription insertNewObjectForEntityForName:@"Person"
                                                    inManagedObjectContext:context];
    attendee.firstName = first;
    attendee.surname = second;
    attendee.inMeetingAttendee = self.meeting;
    
    
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         //FUTURE ERROR HANDLING
       // NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
      //  abort();
    }
  //  NSLog(@"insertNewObject method run");
    
    
}

/////////////////////Start of people picker code//////////////////
-(void) dismissNewPersonViewcontroller:(NewPersonViewController *)controller{
    [self.delegate UndimSuper];
    [self.addNamePopover dismissPopoverAnimated:YES];
}

-(void)addPersonName
{
    CFErrorRef err = nil;
    ABAddressBookRef adbk = ABAddressBookCreateWithOptions(nil, &err);
    if (nil == adbk) {
         //FUTURE ERROR HANDLING
      //  NSLog(@"error: %@", err);
        return;
    }
    ABRecordRef person = self.displayedPerson;
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef)self.enterFirstName, nil);
    ABRecordSetValue(person, kABPersonLastNameProperty,(__bridge CFStringRef)self.enterLastName, nil);
    CFErrorRef error = nil;
    ABAddressBookAddRecord (adbk,person,&error);
    ABAddressBookSave(adbk, &error);
    
    
    CFRelease(adbk);
}

#pragma mark - Unnamed person alert delegate
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex ==0){
        [self.delegate UndimSuper];
        [self.addressBookPopover dismissPopoverAnimated:YES];
    }
    else{
        [self.addressBookPopover dismissPopoverAnimated:YES];
        
        NewPersonViewController *aNewPersonViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"NewPerson"];
        aNewPersonViewController.delegate = self;
        self.addNamePopover = [[UIPopoverController alloc] initWithContentViewController:aNewPersonViewController];
        [self.addNamePopover presentPopoverFromRect:self.contactsButton.frame
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
        self.addNamePopover.delegate = self;
        
    }
}

- (IBAction)pickPerson
{
    [self.delegate dimSuper];
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    self.addressBookPopover = [[UIPopoverController alloc] initWithContentViewController:picker];
    [self.addressBookPopover presentPopoverFromRect:self.contactsButton.frame
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
    self.addressBookPopover.delegate = self;
    
}

- (IBAction)aNewPerson
{
    [self.delegate dimSuper];
    ABNewPersonViewController *picker = [[ABNewPersonViewController alloc] init];
	picker.newPersonViewDelegate = self;
	UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:picker];
    self.aNewPersonPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
    [self.aNewPersonPopover presentPopoverFromRect: self.aNewPersonButton.frame
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
    self.aNewPersonPopover.delegate = self;
}

#pragma mark ABPeoplePickerNavigationControllerDelegate methods
// Displays the information of a selected person
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    self.displayedPerson = person;
    NSString*  firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    if (firstName == nil && lastName == nil) {
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                         otherButtonTitles:@"Add Name",nil];
        [self.delegate dimSuper];
        [self.unnamedAlert show];
    }
    else{
        
        // KP's Meeting Attendee
        Person *attendee= [NSEntityDescription insertNewObjectForEntityForName:@"Person"
                                                        inManagedObjectContext:self.managedObjectContext];
        attendee.firstName = firstName;
        attendee.surname = lastName;
        [self.meeting addMeetingAttendeesObject:attendee];
        
        // Save the context.
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             //FUTURE ERROR HANDLING
          //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
           // abort();
        }
        
        if ([self.addressBookPopover isPopoverVisible])
        {[self.delegate UndimSuper];
            [self.addressBookPopover dismissPopoverAnimated:YES];
        }}
    
    
    
	return NO;
}

// Does not allow users to perform default actions such as dialing a phone number, when they select a person property.
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
								property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
	return NO;
}

// Dismisses the people picker and shows the application when users tap Cancel.
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{ [self.delegate UndimSuper];
	[self.addressBookPopover dismissPopoverAnimated:YES];
}

#pragma mark ABNewPersonViewControllerDelegate methods
// Dismisses the new-person view controller.
- (void)newPersonViewController:(ABNewPersonViewController *)newPersonViewController didCompleteWithNewPerson:(ABRecordRef)person
{ [self.delegate UndimSuper];
    
    
    
    self.displayedPerson = person;
    NSString* firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    
    if (firstName == nil && lastName == nil) {
        if(self.displayedPerson==nil){
            [self.aNewPersonPopover dismissPopoverAnimated:YES];
            return;
        }
        
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self
                                             cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        [self.delegate dimSuper];
        [self.unnamedAlert show];
    }
    else{
        // KP's Meeting Attendee
        [self insertNewObject];
        
        //--- End of KP's edit
    }
   // [self.delegate UndimSuper];
	[self.aNewPersonPopover dismissPopoverAnimated:YES];
}



@end



