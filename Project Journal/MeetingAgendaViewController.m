//
//  MeetingAgendaViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 03/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "AgendaItem.h"
#import "MeetingAgendaViewController.h"
#import "AgendaItemViewController.h"


@interface MeetingAgendaViewController ()<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, AgendaItemViewControllerDelegate, UIPopoverControllerDelegate>
@property (strong, nonatomic) UIStoryboardPopoverSegue* popSegue;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *tableImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *buttonImageView;

@end

@implementation MeetingAgendaViewController


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
}

- (void)dismissPopovers{
    if ([self.popSegue.popoverController isPopoverVisible])
    {[self.delegate UndimSuper];
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
}

-(void) AgendaItemViewControllerDidFinish:(AgendaItemViewController *)controller{
    if ([self.popSegue.popoverController isPopoverVisible])
    {[self.delegate UndimSuper];
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
}

-(void)AgendaItem:(NSString *)item{
    
    /* This method is called from the new Agenda Item button in the form.
     * The method creates a new Agnenda Item, saves it and refreshes the tableview data
     */
    
    self.helper = [DataHelper sharedInstance];
    self.agendaItem = [self.helper newAgendaItem:self.meeting];
    self.agendaItem.title = item;
    [self.helper saveContext];
    if ([self.popSegue.popoverController isPopoverVisible])
    {
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
    [self.tableView reloadData];
    [self.delegate updateAgendaBadge];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{[self.delegate dimSuper];
    if ([[segue identifier] isEqualToString:@"AgendItem"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        self.popSegue.popoverController.delegate = self;
    }}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Agenda Items";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.meeting.meetingAgenda count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        /* This scope deletes the AgendaItem */
        NSMutableOrderedSet *agenda = [NSMutableOrderedSet orderedSetWithOrderedSet:self.meeting.meetingAgenda];
        [agenda removeObjectAtIndex:indexPath.row];
        self.meeting.meetingAgenda = agenda;
        [self.helper saveContext];
        [self.tableView reloadData];
        [self.delegate updateAgendaBadge];
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    /* Reorder the Meeting Object Agenda items as a result of the
     * TableView user reorder functionality. A mutable oredered set is created and
     * copied as a working set.
     */
    
    NSMutableOrderedSet *final = [NSMutableOrderedSet orderedSetWithOrderedSet:self.meeting.meetingAgenda];
    AgendaItem *item = [self.meeting.meetingAgenda objectAtIndex:fromIndexPath.row];
    [final removeObjectAtIndex:fromIndexPath.row];
    [final insertObject:item atIndex:toIndexPath.row];
    self.meeting.meetingAgenda = final;
    [self.helper saveContext];
    [self.tableView reloadData];
}


#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    return nil;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
    [self.delegate updateAgendaBadge];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    /* This method displays the Cell details within the Agenda list.
     */
    AgendaItem *object = [self.meeting.meetingAgenda objectAtIndex:indexPath.row];
    
    NSString *cellString = [NSString stringWithFormat:@"%d. %@",indexPath.row + 1, object.title];
    cell.textLabel.text =  cellString ;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)ediit:(UIButton *)sender {
    if (self.tableView.editing) {
        [self.tableView setEditing:NO animated:YES];
        [sender setTitle: @"Edit" forState: UIControlStateNormal];
    } else { [self.tableView setEditing:YES animated:YES];
        [sender setTitle: @"Done" forState: UIControlStateNormal];
    }
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    UIEdgeInsets tabInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage *textBack = [UIImage imageNamed:@"TextFrame2"];
    textBack = [textBack resizableImageWithCapInsets:tabInsets];
    self.titleImageView.image = textBack;
    self.buttonImageView.image = textBack;
    self.tableImageView.image =textBack;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.titleLabel.text = self.meeting.title;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    self.managedObjectContext = self.helper.managedObjectContext;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
