//
//  ActionDatesViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 05/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "HelpViewController.h"
#import "ActionDatesViewController.h"
#import "DatePickerViewController.h"
@interface ActionDatesViewController ()<DatePickerViewControllerDelegate, UIActionSheetDelegate, UIPopoverControllerDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) UIStoryboardPopoverSegue* popSegue;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *assigndLabel;
@property (weak, nonatomic) IBOutlet UILabel *targetLabel;
@property (weak, nonatomic) IBOutlet UILabel *completionLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdLabel;
@property (weak, nonatomic) IBOutlet UILabel *modifiedLabel;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *assignedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *targetImageView;
@property (weak, nonatomic) IBOutlet UIImageView *completionImageView;
@property (weak, nonatomic) IBOutlet UIImageView *reviewImageView;
@property (weak, nonatomic) IBOutlet UIImageView *createdImageView;
@property (weak, nonatomic) IBOutlet UIImageView *modifiedImageView;
@property (weak, nonatomic) IBOutlet UIButton *completionButton;
@property (weak, nonatomic) IBOutlet UIButton *targetCompletionDateButton;
@property (weak, nonatomic) IBOutlet UIButton *reviewButton;
@property (weak, nonatomic) IBOutlet UIButton *assignedButton;
@property (weak, nonatomic) IBOutlet UIButton *markReviewedButton;
@property (weak, nonatomic) UIActionSheet *reviewAction;
@property (strong, nonatomic)NSString *key;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *helpButtons;
@property (strong, nonatomic) HelpViewController *helpView;
@property (strong, nonatomic) UIPopoverController *helpPopover;
@property (nonatomic, strong) NSDictionary *helpList;
@property (nonatomic, strong) UIAlertView *alert;

- (NSDateFormatter *)formatterDate;
- (NSDateFormatter *)formatterDateTime;
@end

@implementation ActionDatesViewController

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
    
    
}


- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (IBAction)helpAssigned:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.assignedDate"] from:sender];
}
- (IBAction)helpTarget:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.targetCompletionDate"] from:sender];
}
- (IBAction)helpCompletion:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.completionDate"] from:sender];
}
- (IBAction)helpModified:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.lastModifiedDate"] from:sender];
}
- (IBAction)helpCreation:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.creationDate"] from:sender];
}
- (IBAction)helpReview:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.reviewDate"] from:sender];
}

-(void)loadTheHelpPlist{
    if(self.helpList == nil){
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"ActionHelp" ofType:@"plist"];
        self.helpList = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    }
}

- (void)checkHelpButtons {
    if(![self.helper showHelp]){
          for (UIButton *button in self.helpButtons) {
            button.hidden = YES;
          }
    }
}

- (void)showHelp:(NSString*)helpString from:(UIButton *)helpButton {
    [self.delegate dimSuper];
    if(self.helpView == nil) {
        self.helpView = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    }
    UITextView *_textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 250, 10)];
    _textView.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
    _textView.textColor = [UIColor lightGrayColor];
    _textView.scrollEnabled = NO;
    _textView.userInteractionEnabled=NO;
    _textView.text =  helpString;
    CGRect frame = _textView.frame;
    frame.size.height =  [self getTextHeight:_textView];
    _textView.frame = frame;
    self.helpView.view.frame = frame;
    [self.helpView.view addSubview:_textView];
    self.helpPopover = [[UIPopoverController alloc] initWithContentViewController:self.helpView];
    self.helpPopover.popoverContentSize =_textView.frame.size;
    [self.helpPopover presentPopoverFromRect:helpButton.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionDown animated:YES];
    self.helpPopover.delegate = self;
}

- (CGFloat)getTextHeight:(UITextView *)textView
{
    NSLayoutManager *lm = textView.layoutManager;
    CGRect boundingRect = [lm usedRectForTextContainer:lm.textContainers[0]];
    CGFloat height = boundingRect.size.height + (textView.textContainerInset.top + textView.textContainerInset.bottom);
    return height;
}

- (void)dismissPopovers{
      [self.delegate UndimSuper];
    
    if (self.alert.isVisible) {
        [self.alert dismissWithClickedButtonIndex:0 animated:NO];
        self.alert = nil;
    }
    if (self.reviewAction.isVisible) {
        [self.reviewAction dismissWithClickedButtonIndex:self.reviewAction.cancelButtonIndex animated:NO];
        self.reviewAction = nil;
    }
    if ([self.popSegue.popoverController isPopoverVisible])
    {
        [self.popSegue.popoverController dismissPopoverAnimated:NO];
    }
    if ([self.helpPopover isPopoverVisible])
    {
        [self.helpPopover dismissPopoverAnimated:NO];
    }
}

- (IBAction)resetReview:(id)sender {
    UIActionSheet *reviewSheet;
    if (!reviewSheet){
        
        reviewSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                  delegate:self
                                         cancelButtonTitle:nil
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"Mark As Reviewed",nil];
    }
     [self.delegate dimSuper];
    [reviewSheet showFromRect:self.markReviewedButton.frame inView:self.view  animated:YES];
     self.reviewAction = reviewSheet;
}
//reset the review date and update the UI
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self.delegate UndimSuper];
    if(buttonIndex == 0){
        [ self.action resetReviewDate];
        [self showHideReviewButton];
          self.reviewLabel.text = [[self formatterDate] stringFromDate:self.action.reviewDate];
        [self.delegate upDateReviewBadge];
    }}

-(void)showHideReviewButton{
    //show review button is project is for review
    if([self.action isForReview]){
        self.markReviewedButton.hidden = NO;
    }
    else{
        self.markReviewedButton.hidden = YES;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{[self.delegate dimSuper];
      if ([[segue identifier] isEqualToString:@"TCD"]) {
         self.popSegue = (UIStoryboardPopoverSegue*)segue;
         [[segue destinationViewController] setDelegate:self];
         if (self.action.targetCompletionDate){
            [[segue destinationViewController] setPickerValue:self.action.targetCompletionDate];
        }
         self.popSegue.popoverController.delegate =self;
        self.key=@"TCD";
    }
    if ([[segue identifier] isEqualToString:@"CD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        if (self.action.completionDate){
            [[segue destinationViewController] setPickerValue:self.action.completionDate];
        }
        self.popSegue.popoverController.delegate =self;
        self.key=@"CD";
    }
    if ([[segue identifier] isEqualToString:@"RD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        if (self.action.reviewDate){
            [[segue destinationViewController] setPickerValue:self.action.reviewDate];
        }
        self.popSegue.popoverController.delegate =self;
        self.key=@"RD";
    }
    if ([[segue identifier] isEqualToString:@"AD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        if (self.action.assignedDate){
            [[segue destinationViewController] setPickerValue:self.action.assignedDate];
        }
        self.popSegue.popoverController.delegate =self;
        self.key=@"AD";
    }
}

-(void)DatePicked:(NSDate *)date andController:(DatePickerViewController *)controller
{
    
    
    if ([self.key isEqualToString:@"TCD"]){
        
        // **** KYN'S DATE CHECKING CODE
        NSCalendar *calendar = [NSCalendar currentCalendar];
        unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
        NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
        NSDateComponents *check = [calendar components:unitFlags fromDate:self.action.creationDate];
    
        // Change date
        [comps setHour:0];
        [comps setMinute:0];
        [comps setSecond:1];
    
        [check setHour:0];
        [check setMinute:0];
        [check setSecond:1];
    
        NSDate *theDate = [calendar dateFromComponents:comps];
        NSDate *theReview = [calendar dateFromComponents:check];
    
        BOOL flag = NO;

        // Compare the Creation date  with the Picker and flag YES if picker date is same or later
        if ([theDate compare:theReview] == NSOrderedDescending || [theDate compare:theReview] == NSOrderedSame) {
            flag = YES;
        }

        // **** END OF CHECKING CODE
    
        //check to see if target completion date is after creation date if not show alert
        if(flag) {
            self.action.targetCompletionDate = date;
            self.action.lastModifiedDate =[NSDate date];
            [self.helper saveContext];
        
            self.targetLabel.text = [[self formatterDate] stringFromDate:self.action.targetCompletionDate];
            self.modifiedLabel.text = [[self formatterDateTime] stringFromDate:self.action.lastModifiedDate];

            [self.delegate upDateRagBadge];
        } else{
            if(!self.alert){
            self.alert = [[UIAlertView alloc] initWithTitle:@"Cannot Set Date"
                                                        message:@"The target completion date must be after the creation date."
                                                       delegate:self                                          cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
            }
            [self.delegate dimSuper];
            [self.alert show];
        }
    }
    
    if ([self.key isEqualToString:@"CD"]){
        self.action.completionDate = date;
        self.action.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
         self.completionLabel.text = [[self formatterDate] stringFromDate:self.action.completionDate];
         self.modifiedLabel.text = [[self formatterDateTime] stringFromDate:self.action.lastModifiedDate];
    }
    if ([self.key isEqualToString:@"RD"]){
        self.action.reviewDate = date;
        self.action.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
         self.reviewLabel.text = [[self formatterDate] stringFromDate:self.action.reviewDate];
         self.modifiedLabel.text = [[self formatterDateTime] stringFromDate:self.action.lastModifiedDate];
        [self.delegate upDateReviewBadge];
        [self showHideReviewButton];
    }
    if ([self.key isEqualToString:@"AD"]){
        self.action.assignedDate = date;
        self.action.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
         self.assigndLabel.text = [[self formatterDate] stringFromDate:self.action.assignedDate];
         self.modifiedLabel.text = [[self formatterDateTime] stringFromDate:self.action.lastModifiedDate];
    }
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.delegate UndimSuper];
}


- (void)DatePickerViewControllerControllerDidFinish:(DatePickerViewController *)controller{
    if ([self.popSegue.popoverController isPopoverVisible])
    {[self.delegate UndimSuper];
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
}

-(void)applyDateRules{
    if([self.action.status isEqualToString:@"Completed"]){
        //action completed
        self.completionButton.enabled = YES;
        self.reviewButton.enabled = NO;
        self.targetCompletionDateButton.enabled = NO;
        self.assignedButton.enabled = NO;
        self.completionImageView.alpha = 1.0;
      //  self.reviewImageView.alpha = 0.5;
      //  self.targetImageView.alpha = 0.5;
       // self.assignedImageView.alpha = 0.5;
        
        
    }
    else{
        //Action not completed
        
        if(self.action.actionOwner == nil){
           //Action not completed and not assigned
            self.completionButton.enabled = NO;
            self.reviewButton.enabled = YES;
            self.targetCompletionDateButton.enabled = YES;
            self.assignedButton.enabled = NO;
          //  self.completionImageView.alpha = 0.5;
            self.reviewImageView.alpha = 1.0;
            self.targetImageView.alpha = 1.0;
            self.assignedImageView.alpha = 0.5;
            
        }
        
    else{
        //Action not completed and assigned
        self.completionButton.enabled = NO;
        self.reviewButton.enabled = YES;
        self.targetCompletionDateButton.enabled = YES;
        self.assignedButton.enabled = YES;
      //  self.completionImageView.alpha = 0.5;
        self.reviewImageView.alpha = 1.0;
        self.targetImageView.alpha = 1.0;
        self.assignedImageView.alpha = 1.0;
    }
    }
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    UIEdgeInsets tabInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage *textBack = [UIImage imageNamed:@"TextFrame2"];
    textBack = [textBack resizableImageWithCapInsets:tabInsets];
  //  self.titleImageView.alpha = 0.5;
    self.titleImageView.image = textBack;
    self.assignedImageView.image = textBack;
    self.targetImageView.image = textBack;
    self.completionImageView.image = textBack;
    self.reviewImageView.image = textBack;
    self.createdImageView.image = textBack;
  //  self.createdImageView.alpha = 0.5;
    self.modifiedImageView.image = textBack;
  //  self.modifiedImageView.alpha = 0.5;
}

-(void)viewWillAppear:(BOOL)animated{
     [super viewWillAppear:animated];
    self.titleLable.text = self.action.title;
     self.assigndLabel.text = [[self formatterDate] stringFromDate:self.action.assignedDate];
     self.targetLabel.text = [[self formatterDate] stringFromDate:self.action.targetCompletionDate];
     self.completionLabel.text = [[self formatterDate] stringFromDate:self.action.completionDate];
     self.reviewLabel.text = [[self formatterDate] stringFromDate:self.action.reviewDate];
     self.createdLabel.text = [[self formatterDateTime] stringFromDate:self.action.creationDate];
     self.modifiedLabel.text = [[self formatterDateTime] stringFromDate:self.action.lastModifiedDate];
    [self applyDateRules];
    [self showHideReviewButton];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
     [[NSNotificationCenter defaultCenter] removeObserver:self];   
}

-(void)refreshFromBackground{
    [self showHideReviewButton];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	 self.helper = [DataHelper sharedInstance];
    [self checkHelpButtons];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
