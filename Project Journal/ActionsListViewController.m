//
//  MeetingActionsViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 17/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "ActionsListViewController.h"
#import "ActionFormContainer.h"
#import "ActionCell.h"
#import "PJColour.h"
@interface ActionsListViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *tableImageView;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *buttonImageView;
@property (strong, nonatomic) ActionCell *actionCell;
@property (strong, nonatomic) Action *action;
@property (strong, nonatomic) DataHelper *helper;
@property (strong, nonatomic) PJColour *cellColour;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleHeader;
- (NSDateFormatter *)formatterDate;
@end

@implementation ActionsListViewController

- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

#pragma mark - Delegate method
- (IBAction)newAction:(id)sender {
    if([self.item isKindOfClass:[Issue class]]){
        Issue *issue = self.item;
        if([issue.status isEqualToString:@"Completed"]){
            UIAlertView *parentClosed = [[UIAlertView alloc] initWithTitle:@"Issue status completed"
                                                                   message:@"Unable to create new actions for completed issues"
                                                                  delegate:self                                          cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
            [parentClosed show];
        }
        else{
            [self.delegate getNewAction:self.item];
        }
    }
    if([self.item isKindOfClass:[Risk class]]){
        Risk *risk = self.item;
        if([risk.status isEqualToString:@"Completed"]){
            UIAlertView *parentClosed = [[UIAlertView alloc] initWithTitle:@"Risk status completed"
                                                                   message:@"Unable to create new actions for completed risks"
                                                                  delegate:self                                          cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
            [parentClosed show];
        }
        
        else{
            [self.delegate getNewAction:self.item];
        }
    }
    
    if([self.item isKindOfClass:[Project class]]){
        Project *project = self.item;
        if([project.status isEqualToString:@"Completed"]){
            UIAlertView *parentClosed = [[UIAlertView alloc] initWithTitle:@"Project status completed"
                                                                   message:@"Unable to create new actions for completed projects"
                                                                  delegate:self                                          cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
            [parentClosed show];
        }
        else{
            [self.delegate getNewAction:self.item];
        }
    }
}

- (void)ActionFormContainerDidFinish:(ActionFormContainer *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - VC methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ActionNew"]) {
        [[segue destinationViewController] setDelegate:self];
        if([self.item isKindOfClass:[Meeting class]]){
            Meeting *meeting = self.item;
            Project *project = meeting.inProject;
            [[segue destinationViewController] setProject:project];
            [[segue destinationViewController] setItem:meeting];}
        if([self.item isKindOfClass:[Issue class]]){
            Issue *issue = self.item;
            Project *project = issue.inProject;
            [[segue destinationViewController] setProject:project];
            [[segue destinationViewController] setItem:issue];}
        if([self.item isKindOfClass:[Risk class]]){
            Risk *risk = self.item;
            Project *project = risk.inProject;
            [[segue destinationViewController] setProject:project];
            [[segue destinationViewController] setItem:risk];}
        if([self.item isKindOfClass:[Project class]]){
            Project *project = self.item;
            [[segue destinationViewController] setProject:project];
            [[segue destinationViewController] setItem:project];
        }
    }
    if ([[segue identifier] isEqualToString:@"Action"]) {
        [[segue destinationViewController] setDelegate:self];
        if([self.item isKindOfClass:[Meeting class]]){
            Meeting *meeting = self.item;
            Project *project = meeting.inProject;
            [[segue destinationViewController] setProject:project];
            [[segue destinationViewController] setItem:meeting];}
        if([self.item isKindOfClass:[Issue class]]){
            Issue *issue = self.item;
            Project *project = issue.inProject;
            [[segue destinationViewController] setProject:project];
            [[segue destinationViewController] setItem:issue];}
        [[segue destinationViewController] setContainerAction:self.action];
    }
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    UIEdgeInsets toolInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage *tableImage = [UIImage imageNamed:@"TextFrame2"];
    tableImage = [tableImage resizableImageWithCapInsets:toolInsets];
    self.tableImageView.image = tableImage;
   // self.titleImageView.alpha = 0.5;
    self.titleImageView.image = tableImage;
    self.buttonImageView.image = tableImage;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if([self.item isKindOfClass:[Issue class]]){
        self.titleHeader.text = @"Issue Title";
        Issue *issue = self.item;
        self.titleLabel.text = issue.title;
    }
    if([self.item isKindOfClass:[Risk class]]){
        self.titleHeader.text = @"Risk Title";
        Risk *risk = self.item;
        self.titleLabel.text = risk.title;
    }
    if([self.item isKindOfClass:[Meeting class]]){
        self.titleHeader.text = @"Meeting Title";
        Meeting *meeting = self.item;
        self.titleLabel.text = meeting.title;
    }
    if([self.item isKindOfClass:[Project class]]){
        self.titleHeader.text = @"Project Title";
        Project *project = self.item;
        self.titleLabel.text = project.title;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)refreshFromBackground{
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    if(!self.cellColour){
        self.cellColour = [[PJColour alloc]init];
    }
    if([self.item isKindOfClass:[Meeting class]]){
        Meeting *meeting = self.item;
        self.managedObjectContext = meeting.managedObjectContext;
    }
    
    if([self.item isKindOfClass:[Issue class]]){
        Issue *issue = self.item;
        self.managedObjectContext = issue.managedObjectContext;
    }
    if([self.item isKindOfClass:[Project class]]){
        Project *project = self.item;
        self.managedObjectContext = project.managedObjectContext;
    }
    
    if([self.item isKindOfClass:[Risk class]]){
        Risk *risk= self.item;
        self.managedObjectContext = risk.managedObjectContext;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Actions";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.actionCell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:self.actionCell atIndexPath:indexPath];
    return self.actionCell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.managedObjectContext deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            
            //FUTURE ERROR HANDLING 
           // NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
          //  abort();
            
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    //fetch code in here and put results into _fetchedResultsController
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Action" inManagedObjectContext:self.managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    if([self.item isKindOfClass:[Meeting class]]){
        Meeting *meeting = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inMeeting == %@",meeting];
    }
    
    if([self.item isKindOfClass:[Issue class]]){
        Issue *issue = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inIssue == %@",issue];
    }
    
    if([self.item isKindOfClass:[Risk class]]){
        Risk *risk = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inRisk == %@",risk];
    }
    
    if([self.item isKindOfClass:[Project class]]){
        Project *project = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@ && inIssue == nil && inRisk == nil && inMeeting == nil",project];
    }
    
    [fetchRequest setFetchBatchSize:0];
    
    // Apply sort order YES=ASCENDING
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    NSArray *descriptors = @[sortDescriptor];
    fetchRequest.sortDescriptors = descriptors;
    
    // Perform the fetch
    NSError __autoreleasing *error;
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:self.managedObjectContext
                                                                          sectionNameKeyPath:nil cacheName:nil];
    self.fetchedResultsController.delegate = self;
    if (![self.fetchedResultsController performFetch:&error]) {
        //FUTURE ERROR HANDLING
      //  NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }
    
    return self.fetchedResultsController;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    
    [self.tableView reloadData];
    [self.delegate updateActionBadge];
}

- (void)configureCell:(ActionCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if([object isKindOfClass:[Action class]]){
        Action *action = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSString *ragColour = [action fetchRAGStatus];
        UIColor *redColour = self.cellColour.red;
        UIColor *amberColour = self.cellColour.amber;
        UIColor *greenColour = self.cellColour.green;
        UIColor *blueColour = self.cellColour.blue;
        if([ragColour isEqualToString:@"Red"]){
            cell.titleLabel.textColor = redColour;
            cell.ownerLabel.textColor = redColour;
            cell.dateLabel.textColor = redColour;
        }
        if([ragColour isEqualToString:@"Amber"]){
            cell.titleLabel.textColor = amberColour;
            cell.ownerLabel.textColor = amberColour;
            cell.dateLabel.textColor = amberColour;
        }
        if([ragColour isEqualToString:@"Green"]){
            cell.titleLabel.textColor = greenColour;
            cell.ownerLabel.textColor = greenColour;
            cell.dateLabel.textColor = greenColour;
        }
        if([ragColour isEqualToString:@"Blue"]){
            cell.titleLabel.textColor = blueColour;
            cell.ownerLabel.textColor = blueColour;
            cell.dateLabel.textColor = blueColour;
        }
        cell.titleLabel.text = [action title];
        cell.ownerLabel.text = [action actionOwner];
        cell.dateLabel.text = [[self formatterDate] stringFromDate:[action targetCompletionDate]];
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if([object isKindOfClass:[Action class]]){
        self.action = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [self.delegate getAction:self.action];
    }
    return indexPath;
}

@end
