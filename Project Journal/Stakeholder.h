//
//  Stakeholder.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Media, Note, Project;

@interface Stakeholder : NSManagedObject

@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * influence;
@property (nonatomic, retain) NSDate * lastModifiedDate;
@property (nonatomic, retain) NSString * priority;
@property (nonatomic, retain) NSString * role;
@property (nonatomic, retain) NSString * stakeholderID;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * viewpoint;
@property (nonatomic, retain) NSString * person;
@property (nonatomic, retain) Project *inProject;
@property (nonatomic, retain) NSSet *stakeholderMedia;
@property (nonatomic, retain) NSSet *stakeholderNotes;
@end

@interface Stakeholder (CoreDataGeneratedAccessors)

- (void)addStakeholderMediaObject:(Media *)value;
- (void)removeStakeholderMediaObject:(Media *)value;
- (void)addStakeholderMedia:(NSSet *)values;
- (void)removeStakeholderMedia:(NSSet *)values;

- (void)addStakeholderNotesObject:(Note *)value;
- (void)removeStakeholderNotesObject:(Note *)value;
- (void)addStakeholderNotes:(NSSet *)values;
- (void)removeStakeholderNotes:(NSSet *)values;

@end
