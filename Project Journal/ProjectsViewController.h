//
//  ProjectsViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 20/05/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@interface ProjectsViewController : UIViewController<ProjectFormContainerDelegate>
@property (strong, nonatomic) Project *project;
- (void)updateProjectView:(ProjectFormContainer *)controller;
@end
