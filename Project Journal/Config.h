//
//  Config.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 25/07/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Config : NSManagedObject

@property (nonatomic, retain) NSNumber * actionReviewDateInterval;
@property (nonatomic, retain) NSNumber * amberRAGBudgetPerCent;
@property (nonatomic, retain) NSNumber * amberRAGDateInterval;
@property (nonatomic, retain) NSString * configID;
@property (nonatomic, retain) NSNumber * hideCompleted;
@property (nonatomic, retain) NSNumber * issuesReviewDateInterval;
@property (nonatomic, retain) NSNumber * projectReviewDateInterval;
@property (nonatomic, retain) NSNumber * redRAGDateInterval;
@property (nonatomic, retain) NSNumber * risksReviewDateInterval;
@property (nonatomic, retain) NSNumber * showHelp;
@property (nonatomic, retain) NSNumber * projectCount;
@property (nonatomic, retain) NSNumber * actionCount;
@property (nonatomic, retain) NSNumber * issueCount;
@property (nonatomic, retain) NSNumber * riskCount;
@property (nonatomic, retain) NSNumber * meetingCount;

@end
