//
//  ProjectsViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 20/05/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
//
#import <iAd/iAd.h>
#import <QuartzCore/QuartzCore.h>
#import "IssueFormContainer.h"
#import "ProjectFormContainer.h"
#import "ActionFormContainer.h"
#import "RiskFormContainer.h"
#import "MeetingFormContainer.h"
#import "Note+TCLNote.h"
#import "NotesHeader.h"
#import "ItemCollectionCell.h"
#import "ProjectsViewController.h"
#import "ChartView.h"
#import "PJColour.h"
#import "NotePopover.h"

@interface ProjectsViewController ()<NSFetchedResultsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIActionSheetDelegate, IssueFormControllerDelegate, ActionFormContainerDelegate,  RiskFormContainerDelegate, MeetingFormContainerDelegate, NotePopoverDelegate, UIPopoverControllerDelegate>

@property (nonatomic, strong) DataHelper *helper;
@property (strong, nonatomic) PJColour *projectColour;
@property (strong, nonatomic) UIStoryboardPopoverSegue *popSegue;
@property (weak, nonatomic) IBOutlet ChartView *actionChart;
@property (weak, nonatomic) IBOutlet ChartView *issueChart;
@property (weak, nonatomic) IBOutlet ChartView *riskChart;
@property (weak, nonatomic) IBOutlet UILabel *actioncountLabel;
@property (weak, nonatomic) IBOutlet UILabel *issueCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *riskCountLabel;
@property (strong, nonatomic) Action *selectedAction;
@property (strong, nonatomic) Issue *selectedIssue;
@property (strong, nonatomic) Risk *selectedRisk;
@property (strong, nonatomic) Meeting *selectedMeeting;
@property (strong, nonatomic) Note *selectedNote;
@property (strong, nonatomic) NSFetchedResultsController *actionResultsController;
@property (strong, nonatomic) NSFetchedResultsController *issueResultsController;
@property (strong, nonatomic) NSFetchedResultsController *riskResultsController;
@property (strong, nonatomic) NSFetchedResultsController *meetingResultsController;
@property (strong, nonatomic) NSFetchedResultsController *notesResultsController;
@property (weak, nonatomic) IBOutlet UICollectionView *actionCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *issueCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *riskCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *meetingCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *notesCollection;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ownerLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UILabel *budgetLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet UILabel *budgetBadge;
@property (weak, nonatomic) IBOutlet UILabel *dateBadge;
@property (weak, nonatomic) IBOutlet UIButton *ActionsButton;
@property (weak, nonatomic) IBOutlet UIButton *sortActionsButton;
@property (weak, nonatomic) IBOutlet UIButton *notesButton;
@property (weak, nonatomic) IBOutlet UIButton *issuesButton;
@property (weak, nonatomic) IBOutlet UIButton *risksButton;
@property (weak, nonatomic) IBOutlet UIButton *meetingsButton;
@property (weak, nonatomic) IBOutlet UIButton *sortIssuesButton;
@property (weak, nonatomic) IBOutlet UIButton *sortRisksButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) UIActionSheet *itemSheet;
@property (strong, nonatomic) UIActionSheet *riskSortSheet;
@property (strong, nonatomic) UIActionSheet *actionSortSheet;
@property (strong, nonatomic) UIActionSheet *issueSortSheet;
@property (strong, nonatomic) NSArray *riskSortList;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UIView *projectView;
@property (strong, nonatomic) NSNumberFormatter *formatter;
//@property (weak, nonatomic) IBOutlet UINavigationItem *nav;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addItem;
@property (strong, nonatomic) id actionParent;
- (NSDateFormatter *)formatterDate;
- (NSDateFormatter *)formatterDateTime;
- (NSDateFormatter *)formatterTime;
@property float scaleAction;
@property float scaleAction2;
@property float scaleIssue;
@property float scaleIssue2;
@property float scaleNote;
@property float scaleRisk;
@property float scaleRisk2;
@property float scaleMeeting;
@property (weak, nonatomic) IBOutlet UIStepper *notestepper;
@property (weak, nonatomic) IBOutlet UIStepper *actionStepper;
@property (weak, nonatomic) IBOutlet UIStepper *issueStepper;
@property (weak, nonatomic) IBOutlet UIStepper *riskStepper;
@property (weak, nonatomic) IBOutlet UIStepper *meetingStepper;
///
@property (strong, nonatomic) NotePopover *noteView;
@property (strong, nonatomic) UIPopoverController *cellPop;


@end

@implementation ProjectsViewController
- (IBAction)mettingSize:(UIStepper *)sender {
    int cellSize = sender.value;
    if(cellSize == 3){
        self.scaleMeeting =1;
      
        [self.meetingCollection.collectionViewLayout invalidateLayout];
    }
    
    if(cellSize == 2){
        self.scaleMeeting =0.41;
       
        [self.meetingCollection.collectionViewLayout invalidateLayout];
        
    }
    if(cellSize == 1){
        self.scaleMeeting =0.25;
        [self.meetingCollection.collectionViewLayout invalidateLayout];
   
    }
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:self.scaleMeeting forKey:@"scaleMeeting"];
    [defaults setInteger:self.meetingStepper.value forKey:@"meetingStepper"];
    [defaults synchronize];
}

- (IBAction)noteSize:(UIStepper *)sender {
    int cellSize = sender.value;
    if(cellSize == 2){
        self.scaleNote =1;
        [self.notesCollection.collectionViewLayout invalidateLayout];
    }
    if(cellSize == 1){
        self.scaleNote = 0.41;
        [self.notesCollection.collectionViewLayout invalidateLayout];
    }
      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:self.scaleNote forKey:@"scaleNote"];
    [defaults setInteger:self.notestepper.value forKey:@"noteStepper"];
 [defaults synchronize];
}
- (IBAction)riskSize:(UIStepper *)sender {
      int cellSize = sender.value;
    if(cellSize == 3){
        self.scaleRisk =1;
        self.scaleRisk2 =1;
        [self.riskCollection.collectionViewLayout invalidateLayout];
    }

    if(cellSize == 2){
        self.scaleRisk =0.25;
        self.scaleRisk2 =1;
        [self.riskCollection.collectionViewLayout invalidateLayout];
        
    }
    if(cellSize == 1){
        self.scaleRisk =0.25;
        self.scaleRisk2 =0.1;
        [self.riskCollection.collectionViewLayout invalidateLayout];
    }
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:self.scaleRisk forKey:@"scaleRisk"];
    [defaults setFloat:self.scaleRisk2  forKey:@"scaleRisk2"];
    [defaults setInteger:self.riskStepper.value forKey:@"riskStepper"];
      [defaults synchronize];
}

- (IBAction)issueSize:(UIStepper *)sender {
    int cellSize = sender.value;
    if(cellSize == 3){
        self.scaleIssue =1;
        self.scaleIssue2 =1;
        [self.issueCollection.collectionViewLayout invalidateLayout];
    }

    if(cellSize == 2){
        self.scaleIssue =0.25;
        self.scaleIssue2 =1;
        [self.issueCollection.collectionViewLayout invalidateLayout];
        
    }
    if(cellSize == 1){
        self.scaleIssue =0.25;
        self.scaleIssue2 =0.1;
        [self.issueCollection.collectionViewLayout invalidateLayout];
   
    }
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:self.scaleIssue forKey:@"scaleIssue"];
    [defaults setFloat:self.scaleIssue2  forKey:@"scaleIssue2"];
    [defaults setInteger:self.issueStepper.value forKey:@"issueStepper"];
    [defaults synchronize];
}

- (IBAction)actionSize:(UIStepper *)sender {
    int cellSize = sender.value;
    if(cellSize == 3){
        self.scaleAction =1;
        self.scaleAction2 =1;
        [self.actionCollection.collectionViewLayout invalidateLayout];
    }
  
    if(cellSize == 2){
        self.scaleAction =0.25;
        self.scaleAction2 =1;
        [self.actionCollection.collectionViewLayout invalidateLayout];
    
    }
    if(cellSize == 1){
        self.scaleAction =0.25;
        self.scaleAction2 =0.1;
        [self.actionCollection.collectionViewLayout invalidateLayout];
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:self.scaleAction forKey:@"scaleAction"];
    [defaults setFloat:self.scaleAction2  forKey:@"scaleAction2"];
    [defaults setInteger:self.actionStepper.value forKey:@"actionStepper"];
    [defaults synchronize];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.actionCollection == collectionView){
    return CGSizeMake(280 *self.scaleAction2, 108*self.scaleAction);
}
    if(self.issueCollection == collectionView){
        return CGSizeMake(280 *self.scaleIssue2, 108*self.scaleIssue);
    }
    if(self.riskCollection == collectionView){
        return CGSizeMake(280 *self.scaleRisk2, 108*self.scaleRisk);
    }
    if(self.notesCollection == collectionView){
        return CGSizeMake(280 , 108*self.scaleNote);
    }
    if(self.meetingCollection == collectionView){
        return CGSizeMake(280 , 108*self.scaleMeeting);
    }
  else return CGSizeMake(280, 108);
}



#pragma mark - date formatters
- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}
- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (NSFormatter *)formatterTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
    });
    return formatter;
}




#pragma mark - methods for actions and action parent

-(void)getActionParent:(id)parent{
    if([parent isKindOfClass:[Project class ]]){
        [self dismissViewControllerAnimated:YES completion:^{
            [self performSegueWithIdentifier: @"Project" sender: self] ;
        }];
    }
    if([parent isKindOfClass:[Issue class ]]){
        self.selectedIssue = parent;
        [self dismissViewControllerAnimated:YES completion:^{
            [self performSegueWithIdentifier: @"IssueContainer" sender: self] ;
        }];
    }
    if([parent isKindOfClass:[Risk class ]]){
        self.selectedRisk = parent;
        [self dismissViewControllerAnimated:YES completion:^{
            [self performSegueWithIdentifier: @"RiskContainer" sender: self] ;
        }];
    }
}

-(void)getNewAction:(id)itenAction{
    self.actionParent = itenAction;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"NewAction" sender: self] ;
    }];
    
}

-(void)getTheActionForm:(Action *)itemAction{
    
    
    self.selectedAction = itemAction;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"ActionConainer" sender: self] ;
    }];
}

-(NSString *)convertToLocalCurrency:(NSNumber *)currency{
    if(self.formatter==nil){
        self.formatter = [[NSNumberFormatter alloc] init];
        [self.formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [self.formatter setLocale:[NSLocale currentLocale]];
    }
    return [self.formatter stringFromNumber:currency];
}


#pragma mark - methods for sorting action sheets
- (IBAction)sortActions:(UIButton *)sender {
    self.riskSortList = [self.project getSortStatusListForObject:@"Action"];
    self.actionSortSheet = [[UIActionSheet alloc] initWithTitle:@"Sort Actions By"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:nil];
    for  (int i = 0; i < [self.riskSortList count];  i++) {
        [self.actionSortSheet addButtonWithTitle:[self.riskSortList objectAtIndex:i]];
    }
    [self.actionSortSheet showFromRect:sender.frame inView:self.scrollView animated:YES];
}

- (IBAction)sortIssues:(UIButton *)sender {
    self.riskSortList = [self.project getSortStatusListForObject:@"Issue"];
    self.issueSortSheet = [[UIActionSheet alloc] initWithTitle:@"Sort Issues By"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                        destructiveButtonTitle:nil
                                             otherButtonTitles:nil];
    for  (int i = 0; i < [self.riskSortList count];  i++) {
        [self.issueSortSheet addButtonWithTitle:[self.riskSortList objectAtIndex:i]];
    }
    [self.issueSortSheet showFromRect:sender.frame inView:self.scrollView animated:YES];
}

- (IBAction)sortRisks:(UIButton *)sender {
    self.riskSortList = [self.project getSortStatusListForObject:@"Risk"];
    self.riskSortSheet = [[UIActionSheet alloc] initWithTitle:@"Sort Risks By"
                                                     delegate:self
                                            cancelButtonTitle:nil
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:nil];
    for  (int i = 0; i < [self.riskSortList count];  i++) {
        [self.riskSortSheet addButtonWithTitle:[self.riskSortList objectAtIndex:i]];
    }
    [self.riskSortSheet showFromRect:sender.frame inView:self.scrollView animated:YES];
}

#pragma mark - project container delegate
// called from DEL1 
- (void)updateProjectView:(ProjectFormContainer *)controller{
    [self setupProjectview];
    [self.actionCollection reloadData];
    [self.notesCollection reloadData];
    [self.riskCollection reloadData];// test
    [self.issueCollection reloadData];// test
    [self.meetingCollection reloadData];// test
}

#pragma mark - note controller delegate
-(void) NotePopoverDidfinish:(NotePopover *)controller{
    if ([self.popSegue.popoverController isPopoverVisible])
    {
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
       // self.noteLabel.hidden = YES;
    }
    
     [self.cellPop dismissPopoverAnimated:YES];
}

#pragma mark - layout of project view (cell)
-(void)setupProjectview{
    if([[self.project fetchDateRAGStatus] isEqualToString:@"Amber"] ){
        self.dateBadge.hidden = NO;
        self.dateBadge.backgroundColor = self.projectColour.amber;
        self.dateBadge.layer.borderWidth = 1.0;
        self.dateBadge.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    else if([[self.project fetchDateRAGStatus] isEqualToString:@"Red"] ){
        self.dateBadge.hidden = NO;
        self.dateBadge.backgroundColor = self.projectColour.red;
        self.dateBadge.layer.borderWidth = 1.0;
        self.dateBadge.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    else{
        self.dateBadge.hidden = YES;
    }
    
    if([[self.project fetchBudgetRAGStatus] isEqualToString:@"Amber"] ){
        
        self.budgetBadge.hidden = NO;
        self.budgetBadge.backgroundColor = self.projectColour.amber;
        self.budgetBadge.layer.borderWidth = 1.0;
        self.budgetBadge.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    else if([[self.project fetchBudgetRAGStatus] isEqualToString:@"Red"] ){
        self.budgetBadge.hidden = NO;
        self.budgetBadge.backgroundColor = self.projectColour.red;
        self.budgetBadge.layer.borderWidth = 1.0;
        self.budgetBadge.layer.borderColor = [UIColor whiteColor].CGColor;
        
    }
    else{
        self.budgetBadge.hidden = YES;
    }
    
    if ([[self.project fetchRAGStatus] isEqualToString:@"Red"]){
        self.projectView.backgroundColor =self.projectColour.red;
    }
    
    if ([[self.project fetchRAGStatus] isEqualToString:@"Green"]){
        self.projectView.backgroundColor =self.projectColour.green;
    }
    
    if ([[self.project fetchRAGStatus] isEqualToString:@"Amber"]){
        self.projectView.backgroundColor =self.projectColour.amber;
    }
    if ([[self.project fetchRAGStatus] isEqualToString:@"Blue"]){
        self.projectView.backgroundColor =self.projectColour.blue;
    }
    self.titleLabel.text = self.project.title;
    self.ownerLabel.text = self.project.projectManager;
    self.budgetLabel.text =[self convertToLocalCurrency: [self.project targetBudget]];
    self.costLabel.text = [self convertToLocalCurrency:self.project.actualBudgetToDate];
    
    if(self.project.targetCompletionDate){
        self.dateLabel.text = [[self formatterDate] stringFromDate:self.project.targetCompletionDate];
    }
    else{
        self.dateLabel.text = @"Set completion date!";
    }
    
    self.statusLabel.text = self.project.status;
    if([self.project isForReview]){
        self.reviewLabel.layer.borderWidth = 1.0;
        self.reviewLabel.layer.borderColor = [UIColor whiteColor].CGColor;
        [ self.reviewLabel setHidden:NO];
    }
    else{
        [ self.reviewLabel setHidden:YES];
    }
    
}






#pragma mark - action sheet for adding items
- (IBAction)addItem:(UIBarButtonItem *)sender {
    if(!self.itemSheet){
        
        if([self.project.status isEqualToString:@"Completed"]){
            UIActionSheet *addSheet = [[UIActionSheet alloc] initWithTitle:@"In Project"
                                                                  delegate:self
                                                         cancelButtonTitle:nil
                                                    destructiveButtonTitle:nil
                                                         otherButtonTitles:@"New Note",nil];
            [addSheet showFromBarButtonItem:sender animated:YES];
            self.itemSheet = addSheet;
        }
        else{
            UIActionSheet *addSheet = [[UIActionSheet alloc] initWithTitle:@"In Project"
                                                                  delegate:self
                                                         cancelButtonTitle:nil
                                                    destructiveButtonTitle:nil
                                                         otherButtonTitles:@"New Note",@"New Action",@"New Issue",@"New Risk",@"New Meeting",nil];
            [addSheet showFromBarButtonItem:sender animated:YES];
            self.itemSheet = addSheet;
        }
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(self.itemSheet == actionSheet){
        if(buttonIndex == 0){
            [self performSegueWithIdentifier:@"NewNote" sender:self];
        }
        if(buttonIndex == 1){
            self.actionParent = self.project;/////////////
            [self performSegueWithIdentifier:@"NewAction" sender:self];
        }
        if(buttonIndex == 2){
            [self performSegueWithIdentifier:@"NewIssue" sender:self];
        }
        if(buttonIndex == 3){
            
            [self performSegueWithIdentifier:@"NewRisk" sender:self];
        }
        if(buttonIndex == 4){
            [self performSegueWithIdentifier:@"NewMeeting" sender:self];
        }}
    
    if(self.riskSortSheet == actionSheet){
        if (buttonIndex < [self.riskSortList count]){
            self.project.sortRiskField = [self.riskSortList objectAtIndex:buttonIndex];
            // self.sortRiskLabel.text =[NSString stringWithFormat:@"Sorted by %@", [self.project sortRiskField ]];
            [self.sortRisksButton setTitle:[self.project sortRiskField ] forState:UIControlStateNormal];
            // refresh riskRC with new sort attribute
            self.riskResultsController = nil;
            self.riskResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Risk"
                                                              sortingBy:@"title" ascendingBy:YES includeDeleted:NO];
            self.riskResultsController.delegate = self;
            [self.riskCollection reloadData];
            
        }
    }
    
    if(self.actionSortSheet == actionSheet){
        if (buttonIndex < [self.riskSortList count]){
            self.project.sortActionField = [self.riskSortList objectAtIndex:buttonIndex];
            //  self.sortActionLabel.text =[NSString stringWithFormat:@"Sorted by %@", [self.project sortActionField ]];
            [self.sortActionsButton setTitle:[self.project sortActionField ] forState:UIControlStateNormal];
            self.actionResultsController = nil;
            self.actionResultsController =[ self.helper fetchItemsMatching:self.project forEntityType:@"Action" sortingBy:@"section" sectionBy:@"section" ascendingBy:YES includeDeleted:NO];
            self.actionResultsController.delegate = self;
            [self.actionCollection reloadData];
            
        }
    }
    
    if(self.issueSortSheet == actionSheet){
        if (buttonIndex < [self.riskSortList count]){
            self.project.sortIssueField = [self.riskSortList objectAtIndex:buttonIndex];
            [self.sortIssuesButton setTitle:[self.project sortIssueField ] forState:UIControlStateNormal];
            // refresh issueRC with new sort attribute
            self.issueResultsController = nil;
            self.issueResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Issue"
                                                               sortingBy:@"title" ascendingBy:YES includeDeleted:NO];
            self.issueResultsController.delegate = self;
            [self.issueCollection reloadData];
        }
        
    }
}


#pragma mark -  Delegates container views
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if(controller == self.issueResultsController ){
        [self.issueCollection reloadData ];
        [self updateIssuesLabel];
        [self.actionCollection reloadData];
        [self.notesCollection reloadData];
        [self UpdateIssueChartView];
    }
    if(controller == self.actionResultsController ){
        self.actionResultsController = nil;
        self.actionResultsController =[ self.helper fetchItemsMatching:self.project forEntityType:@"Action" sortingBy:@"section" sectionBy:@"section" ascendingBy:YES includeDeleted:NO];
        self.actionResultsController.delegate = self;
        
        [self.actionCollection reloadData];
        [self.notesCollection reloadData];
        [self updateActionsLabel];
        [self UpdateActionChartView];
    }
    if(controller == self.riskResultsController ){
        [self.riskCollection reloadData];
        [self.actionCollection reloadData];
        [self.notesCollection reloadData];
        [self updateRisksLabel];
        [self UpdateRiskChartView];
    }
    if(controller == self.meetingResultsController ){
        [self.meetingCollection reloadData];
        [self.notesCollection reloadData];
        [self updateMeetingsLabel];
    }
    if(controller == self.notesResultsController ){
        [self.notesCollection reloadData];
        [self updateNotesLabel];
    }
}

- (void)IssueFormContainerDidFinish:(IssueFormContainer *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)deleteProject:(ProjectFormContainer *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)ProjectFormContainerDidFinish:(ProjectFormContainer *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)ActionFormContainerDidFinish:(ActionFormContainer *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)RiskFormContainerDidFinish:(RiskFormContainer *)controller{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)MeetingFormContainerDidFinish:(MeetingFormContainer *)controller{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"Project"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProjectObject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"NewAction"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.actionParent];
        self.selectedAction = nil;
        [[segue destinationViewController] setContainerAction:self.selectedAction];
    }
    if ([[segue identifier] isEqualToString:@"NewRisk"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        self.selectedRisk = nil;
        [[segue destinationViewController] setContainerRisk:self.selectedRisk];
    }
    if ([[segue identifier] isEqualToString:@"NewMeeting"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        self.selectedMeeting = nil;
        [[segue destinationViewController] setContainerMeeting:self.selectedMeeting];
    }
    if ([[segue identifier] isEqualToString:@"NewIssue"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        self.selectedIssue = nil;
        [[segue destinationViewController] setContainerIssue:self.selectedIssue];
    }
    if ([[segue identifier] isEqualToString:@"IssueContainer"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        [[segue destinationViewController] setContainerIssue:self.selectedIssue];
    }
    if ([[segue identifier] isEqualToString:@"ProjectDetail"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProjectObject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"ActionConainer"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        [[segue destinationViewController] setContainerAction:self.selectedAction];
    }
    if ([[segue identifier] isEqualToString:@"RiskContainer"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        [[segue destinationViewController] setContainerRisk:self.selectedRisk];
    }
    if ([[segue identifier] isEqualToString:@"MeetingContainer"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        [[segue destinationViewController] setContainerMeeting:self.selectedMeeting];
    }
    if ([[segue identifier] isEqualToString:@"actions"]) {
        [[segue destinationViewController] setProject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"actionsButton"]) {
        [[segue destinationViewController] setProject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"issues"]) {
        [[segue destinationViewController] setProject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"issuesButton"]) {
        [[segue destinationViewController] setProject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"risks"]) {
        [[segue destinationViewController] setProject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"risksButton"]) {
        [[segue destinationViewController] setProject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"meetings"]) {
        [[segue destinationViewController] setProject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"notes"]) {
        [[segue destinationViewController] setProject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"Note"]) {

     //   self.noteLabel.hidden = NO;
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        
        [[self.popSegue destinationViewController] setDelegate:self];
        [[self.popSegue destinationViewController] setNote:self.selectedNote];
       // self.popSegue.popoverController.delegate = self;
    }
    if ([[segue identifier] isEqualToString:@"NewNote"]) {
      //  self.noteLabel.hidden = NO;
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        
        [[self.popSegue destinationViewController] setDelegate:self];
        [[self.popSegue destinationViewController] setItem:self.project];
        [[self.popSegue destinationViewController] setNote:nil];
      //  self.noteLabel.text =[NSString stringWithFormat:@"Project: %@ ",self.project.title];
    }
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        NotesHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
      //  headerView.backgroundColor  = self.projectColour.grey;
        if(self.notesCollection == collectionView){
            //Hide ordering letter from notes header label
            if([[ [[self.notesResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"A. Project"]){
                headerView.titleLable.text = @"In Project";
                headerView.headerImageview.image = [UIImage imageNamed:@"inproject"];
            }
            if([[ [[self.notesResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"B. Meeting"]){
                headerView.titleLable.text = @"In Meetings";
                headerView.headerImageview.image = [UIImage imageNamed:@"inmeetings"];
            }
            if([[ [[self.notesResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"C. Action"]){
                headerView.titleLable.text = @"In Actions";
                headerView.headerImageview.image = [UIImage imageNamed:@"inactions"];
            }
            if([[ [[self.notesResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"D. Issue"]){
                headerView.titleLable.text = @"In Issues";
                headerView.headerImageview.image = [UIImage imageNamed:@"inissues"];
            }
            if([[ [[self.notesResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"E. Risk"]){
                headerView.titleLable.text = @"In Risks";
                headerView.headerImageview.image = [UIImage imageNamed:@"inrisks"];
            }
        }
        
        if(self.actionCollection == collectionView){
            //Hide ordering letter from notes header label
            if([[ [[self.actionResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"A. Project"]){
                headerView.titleLable.text = @"In Project";
                headerView.headerImageview.image = [UIImage imageNamed:@"inproject"];
            }
            if([[ [[self.actionResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"B. Meeting"]){
                headerView.titleLable.text = @"In Meetings";
                 headerView.headerImageview.image = [UIImage imageNamed:@"inmeetings"];
            }
            
            if([[ [[self.actionResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"D. Risk"]){
                headerView.titleLable.text = @"In Risks";
                 headerView.headerImageview.image = [UIImage imageNamed:@"inrisks"];
            }
            if([[ [[self.actionResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"C. Issue"]){
                headerView.titleLable.text = @"In Issues";
                 headerView.headerImageview.image = [UIImage imageNamed:@"inissues"];
            }
            
        }
        
        reusableview = headerView;
    }
    
    return reusableview;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if(self.notesCollection == collectionView){
        return  [[self.notesResultsController sections] count];
    }
    
    if(self.actionCollection == collectionView){
        return  [[self.actionResultsController sections] count];
    }
    
    else return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo;
    if(self.actionCollection == collectionView){
        sectionInfo = [self.actionResultsController sections][section];
        
    }
    if(self.issueCollection == collectionView){
        sectionInfo = [self.issueResultsController sections][section];
        
    }
    
    if(self.riskCollection == collectionView){
        sectionInfo = [self.riskResultsController sections][section];
        
    }
    
    if(self.meetingCollection == collectionView){
        sectionInfo = [self.meetingResultsController sections][section];
        
    }
    if(self.notesCollection == collectionView){
        sectionInfo = [self.notesResultsController sections][section];
        
    }
    return [sectionInfo numberOfObjects];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    UIColor *redCellColor = self.projectColour.red;
    UIColor *amberCellColor = self.projectColour.amber;
    UIColor *greenCellColor = self.projectColour.green;
    UIColor *blueCellColor = self.projectColour.blue;
    UIColor *darkBlue = self.projectColour.darkBlue;
    
    ItemCollectionCell *cell ;
    if(self.actionCollection == collectionView){
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        Action *action = [self.actionResultsController objectAtIndexPath:indexPath];
        if([action isForReview]){
            [ cell.reviewLabel setHidden:NO];
        }
        else{
            [ cell.reviewLabel setHidden:YES];
        }
        
        if([[action fetchDateRAGStatus] isEqualToString:@"Amber"] ){
            cell.dateBadge.hidden = NO;
            cell.dateBadge.backgroundColor = self.projectColour.amber;
        }
        else if([[action fetchDateRAGStatus] isEqualToString:@"Red"] ){
            cell.dateBadge.hidden = NO;
            cell.dateBadge.backgroundColor = self.projectColour.red;
        }
        else{
            cell.dateBadge.hidden = YES;
        }
        
        //check for actions parent
        if([action inMeeting]){
            cell.actionParentLabel.text = [[action inMeeting] title];
        }
        else if([action inIssue]){
            cell.actionParentLabel.text = [[action inIssue] title];
        }
        else if([action inRisk]){
            cell.actionParentLabel.text = [[action inRisk] title];
        }
        else if([action inProject]){
            cell.actionParentLabel.text = [[action inProject]title];
        }
        if ([[action fetchRAGStatus] isEqualToString:@"Red"]){
            cell.contentView.layer.backgroundColor  =redCellColor.CGColor;
        }
        if ([[action fetchRAGStatus] isEqualToString:@"Green"]){
            cell.contentView.layer.backgroundColor  = greenCellColor.CGColor;
        }
        if ([[action fetchRAGStatus] isEqualToString:@"Amber"]){
            cell.contentView.layer.backgroundColor  = amberCellColor.CGColor;
        }
        if ([[action fetchRAGStatus] isEqualToString:@"Blue"]){
            cell.contentView.layer.backgroundColor  = blueCellColor.CGColor;
        }
        cell.titleLabel.text = [action title];
        cell.ownerLabel.text = [action actionOwner];
        cell.priorityLabel.text = [action priority];
        cell.dateLabel.text = [self.formatterDate stringFromDate:[action targetCompletionDate]];
    }
    if(self.issueCollection == collectionView){
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        Issue *issue = [self.issueResultsController objectAtIndexPath:indexPath];
        if([issue isForReview]){
            [ cell.reviewLabel setHidden:NO];
        }
        else{
            [ cell.reviewLabel setHidden:YES];
        }
        if([[issue fetchDateRAGStatus] isEqualToString:@"Amber"] ){
            cell.dateBadge.hidden = NO;
            cell.dateBadge.backgroundColor = self.projectColour.amber;
        }
        else if([[issue fetchDateRAGStatus] isEqualToString:@"Red"] ){
            cell.dateBadge.hidden = NO;
            cell.dateBadge.backgroundColor = self.projectColour.red;
        }
        else{
            cell.dateBadge.hidden = YES;
        }
        cell.titleLabel.text = [issue valueForKey:@"title"];
        if ([[issue fetchRAGStatus] isEqualToString:@"Red"]){
            cell.contentView.layer.backgroundColor = redCellColor.CGColor;
        }
        
        if ([[issue fetchRAGStatus] isEqualToString:@"Green"]){
            cell.contentView.layer.backgroundColor = greenCellColor.CGColor;
        }
        
        if ([[issue fetchRAGStatus] isEqualToString:@"Amber"]){
            cell.contentView.layer.backgroundColor = amberCellColor.CGColor;
        }
        if ([[issue fetchRAGStatus] isEqualToString:@"Blue"]){
            cell.contentView.layer.backgroundColor = blueCellColor.CGColor;
        }
        cell.ownerLabel.text = [issue issueOwner];
        cell.priorityLabel.text = [issue priority];
        
        if([issue targetCompletionDate]){
            cell.dateLabel.text = [self.formatterDate stringFromDate:[issue targetCompletionDate]];
        }
        else{
            cell.dateLabel.text = @"Set completion date!";
        }
    }
    if(self.riskCollection == collectionView){
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        Risk *risk = [self.riskResultsController objectAtIndexPath:indexPath];
        if([risk isForReview]){
            [cell.reviewLabel setHidden:NO];
        }
        else{
            [cell.reviewLabel setHidden:YES];
        }
        if([[risk fetchDateRAGStatus] isEqualToString:@"Amber"] ){
            cell.dateBadge.hidden = NO;
            cell.dateBadge.backgroundColor = self.projectColour.amber;
        }
        else if([[risk fetchDateRAGStatus] isEqualToString:@"Red"] ){
            cell.dateBadge.hidden = NO;
            cell.dateBadge.backgroundColor = self.projectColour.red;
        }
        else{
            cell.dateBadge.hidden = YES;
        }
        if([[risk fetchAnalysisRAGStatus] isEqualToString:@"Amber"] ){
            cell.analysisBadge.hidden = NO;
            cell.analysisBadge.backgroundColor = self.projectColour.amber;
        }
        else if([[risk fetchAnalysisRAGStatus] isEqualToString:@"Red"] ){
            cell.analysisBadge.hidden = NO;
            cell.analysisBadge.backgroundColor = self.projectColour.red;
        }
        else{
            cell.analysisBadge.hidden = YES;
        }
        if ([[risk fetchRAGStatus] isEqualToString:@"Red"]){
            cell.contentView.layer.backgroundColor = redCellColor.CGColor;
        }
        if ([[risk fetchRAGStatus] isEqualToString:@"Green"]){
            cell.contentView.layer.backgroundColor = greenCellColor.CGColor;
        }
        if ([[risk fetchRAGStatus] isEqualToString:@"Amber"]){
            cell.contentView.layer.backgroundColor = amberCellColor.CGColor;
        }
        if ([[risk fetchRAGStatus] isEqualToString:@"Blue"]){
            cell.contentView.layer.backgroundColor = blueCellColor.CGColor;
        }
        cell.titleLabel.text = [risk title];
        cell.ownerLabel.text = [risk riskOwner];
        cell.priorityLabel.text =[risk projectPhase];
        if([risk plannedCompletionDate]){
            cell.dateLabel.text = [self.formatterDate stringFromDate:[risk plannedCompletionDate]];
        }
        else{
            cell.dateLabel.text = @"Set completion date!";
        }
    }
    if(self.meetingCollection == collectionView){
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        Meeting *meeting = [self.meetingResultsController objectAtIndexPath:indexPath];
        
        if([meeting.status isEqualToString:@"Completed"]){
            cell.contentView.layer.backgroundColor = blueCellColor.CGColor;
        }
        else{
            cell.contentView.layer.backgroundColor = darkBlue.CGColor;
        }
        
        if([meeting isInCalendar]){
            cell.calImage.hidden = NO;
        }
        else{
          cell.calImage.hidden = YES;
        }
        cell.titleLabel.text = [meeting title];
        cell.dateLabel.text = [self.formatterDate stringFromDate:[meeting startDateTime]];
        cell.locationLabel.text = [meeting location];
        cell.startTimeLabel.text =[self.formatterTime stringFromDate:[meeting startDateTime]];
        cell.endTimeLabel.text =[self.formatterTime stringFromDate:[meeting endDateTime]];
    }
    if(self.notesCollection == collectionView){
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        Note *note = [self.notesResultsController objectAtIndexPath:indexPath];
        cell.contentView.layer.backgroundColor = darkBlue.CGColor;
        cell.noteTextView.text = [note summary];
         cell.noteTextView.textColor = [UIColor whiteColor];
        cell.dateLabel.text = [self.formatterDateTime stringFromDate:[note lastModifiedDate]];
        if([note inAction]){
            cell.titleLabel.text = [[note inAction]title];
        }
        else if([note inIssue]){
            cell.titleLabel.text = [[note inIssue]title];
        }
        else if([note inMeeting]){
            cell.titleLabel.text = [[note inMeeting]title];
        }
        else if([note inRisk]){
            cell.titleLabel.text = [[note inRisk]title];
        }
        
        else {
            cell.titleLabel.text = [[note inProject]title];
        }
    }
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(self.actionCollection == collectionView){
        
        self.selectedAction = [self.actionResultsController objectAtIndexPath:indexPath];
        
    }
    
    if(self.issueCollection == collectionView){
        
        self.selectedIssue = [self.issueResultsController objectAtIndexPath:indexPath];
    }
    
    if(self.riskCollection == collectionView){
        
        self.selectedRisk = [self.riskResultsController objectAtIndexPath:indexPath];
        
    }
    if(self.meetingCollection == collectionView){
        
        self.selectedMeeting = [self.meetingResultsController objectAtIndexPath:indexPath];
    }
    
    if(self.notesCollection == collectionView){
        UICollectionViewCell *aCell;
        aCell = [collectionView cellForItemAtIndexPath:indexPath];
        self.selectedNote = [self.notesResultsController objectAtIndexPath:indexPath];
        /////////
        self.noteView = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"noteView"];
        [self.noteView setDelegate:self ];
        self.noteView.note = self.selectedNote;
        self.cellPop = [[UIPopoverController alloc] initWithContentViewController:self.noteView];
        [self.cellPop presentPopoverFromRect:aCell.frame inView:self.notesCollection permittedArrowDirections:  UIPopoverArrowDirectionDown  animated:YES];
        
        ///////////
        
        
        
        
        
    }
    return YES;
}

-(void)updateNotesLabel{
    if( [[self.notesResultsController fetchedObjects] count] == 0){
        self.notesButton.enabled = NO;
        self.notestepper.hidden = YES;
    }
    else{
        self.notesButton.enabled = YES;
        self.notestepper.hidden = NO;
    }
}

-(void)updateActionsLabel{
    if( [[self.actionResultsController fetchedObjects] count] == 0){
        self.ActionsButton.enabled = NO;
        self.actionStepper.hidden = YES;
    }
    else{
        self.ActionsButton.enabled = YES;
        self.actionStepper.hidden = NO;
    }
    
    if( [[self.actionResultsController fetchedObjects] count] < 2){
        self.sortActionsButton.hidden = YES;
    }
    else{
        self.sortActionsButton.hidden = NO;
    }
}

-(void)updateIssuesLabel{
    if( [[self.issueResultsController fetchedObjects] count] == 0){
        self.issuesButton.enabled = NO;
        self.issueStepper.hidden = YES;
    }
    else{
        self.issuesButton.enabled = YES;
        self.issueStepper.hidden = NO;
    }
    
    if( [[self.issueResultsController fetchedObjects] count] < 2){
        self.sortIssuesButton.hidden = YES;
    }
    else{
        self.sortIssuesButton.hidden = NO;
    }
}

-(void)updateRisksLabel{
    if( [[self.riskResultsController fetchedObjects] count] == 0){
        self.risksButton.enabled = NO;
        self.riskStepper.hidden = YES;
    }
    else{
        self.risksButton.enabled = YES;
        self.riskStepper.hidden = NO;
    }
    if( [[self.riskResultsController fetchedObjects] count] < 2){
        self.sortRisksButton.hidden = YES;
    }
    else{
        self.sortRisksButton.hidden = NO;
    }
    
}
-(void)updateMeetingsLabel{
    if( [[self.meetingResultsController fetchedObjects] count] == 0){
        self.meetingsButton.enabled = NO;
        self.meetingStepper.hidden = YES;
    }
    else{
        self.meetingsButton.enabled = YES;
        self.meetingStepper.hidden = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    if (self.itemSheet != nil) {
        [self.itemSheet dismissWithClickedButtonIndex:self.itemSheet.cancelButtonIndex animated:NO];
        self.itemSheet = nil;
    }
   // [self RememberCellSize];
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self getCellSize];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refresh)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
    [self setupProjectview];
}


-(void)refresh{
    [self UpdateRiskChartView];
    [self UpdateIssueChartView];
    [self UpdateRiskChartView];
    [self setupProjectview];
    [self.issueCollection reloadData];
    [self.riskCollection reloadData];
    [self.actionCollection reloadData];
    
   
}

-(void)RememberCellSize{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:self.scaleAction forKey:@"scaleAction"];
     [defaults setFloat:self.scaleAction2  forKey:@"scaleAction2"];
    [defaults setInteger:self.actionStepper.value forKey:@"actionStepper"];
    
    [defaults setFloat:self.scaleIssue forKey:@"scaleIssue"];
    [defaults setFloat:self.scaleIssue2  forKey:@"scaleIssue2"];
    [defaults setInteger:self.issueStepper.value forKey:@"issueStepper"];
    
    [defaults setFloat:self.scaleRisk forKey:@"scaleRisk"];
    [defaults setFloat:self.scaleRisk2  forKey:@"scaleRisk2"];
    [defaults setInteger:self.riskStepper.value forKey:@"riskStepper"];
    
    [defaults setFloat:self.scaleNote forKey:@"scaleNote"];
    [defaults setInteger:self.notestepper.value forKey:@"noteStepper"];
    
    [defaults setFloat:self.scaleMeeting forKey:@"scaleMeeting"];
    [defaults setInteger:self.meetingStepper.value forKey:@"meetingStepper"];
    
    [defaults synchronize];
}

-(void)getCellSize{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.scaleAction =  [defaults floatForKey:@"scaleAction"];
    self.scaleAction2 = [defaults floatForKey:@"scaleAction2"];
    self.actionStepper.value = [defaults integerForKey:@"actionStepper"];
    
    self.scaleIssue =  [defaults floatForKey:@"scaleIssue"];
    self.scaleIssue2 = [defaults floatForKey:@"scaleIssue2"];
    self.issueStepper.value = [defaults integerForKey:@"issueStepper"];
    
    self.scaleRisk =  [defaults floatForKey:@"scaleRisk"];
    self.scaleRisk2 = [defaults floatForKey:@"scaleRisk2"];
    self.riskStepper.value = [defaults integerForKey:@"riskStepper"];
    
    self.scaleNote =  [defaults floatForKey:@"scaleNote"];
    self.notestepper.value = [defaults integerForKey:@"noteStepper"];
    
    self.scaleMeeting =  [defaults floatForKey:@"scaleMeeting"];
    self.meetingStepper.value = [defaults integerForKey:@"meetingStepper"];
    
    
}


-(void)UpdateRiskChartView{
    int green = [self.project greenRAGCountForRisks];
    int amber = [self.project amberRAGCountForRisks];
    int red = [self.project redRAGCountForRisks];
    int blue = [self.project blueRAGCountForRisks];
    int total = green + amber + red + blue;
    
    if(total == 0){
        [self.riskChart setHidden:YES];
    }
    else{
        [self.riskChart setHidden:NO];
        self.riskChart.green = green;
        self.riskChart.amber = amber;
        self.riskChart.red = red;
        self.riskChart.blue = blue;
        [self.riskChart setNeedsDisplay];
        self.riskCountLabel.text = [NSString stringWithFormat:@"%d",total];
    }
}
-(void)UpdateActionChartView{
    int green = [self.project greenRAGCountForActions];
    int amber = [self.project amberRAGCountForActions];
    int red = [self.project redRAGCountForActions];
    int blue = [self.project blueRAGCountForActions];
    int total = green + amber + red + blue;
    
    if(total == 0){
        [self.actionChart setHidden:YES];
    }
    else{
        [self.actionChart setHidden:NO];
        self.actionChart.green = green;
        self.actionChart.amber = amber;
        self.actionChart.red = red;
        self.actionChart.blue = blue;
        [self.actionChart setNeedsDisplay];
        self.actioncountLabel.text = [NSString stringWithFormat:@"%d",total];
    }
}
-(void)UpdateIssueChartView{
    int green = [self.project greenRAGCountForIssues];
    int amber = [self.project amberRAGCountForIssues];
    int red = [self.project redRAGCountForIssues];
    int blue = [self.project blueRAGCountForIssues];
    int total = green + amber + red + blue;
    
    if(total == 0){
        [self.issueChart setHidden:YES];
    }
    else{
        [self.issueChart setHidden:NO];
        self.issueChart.green = green;
        self.issueChart.amber = amber;
        self.issueChart.red = red;
        self.issueChart.blue = blue;
        [self.issueChart setNeedsDisplay];
        self.issueCountLabel.text = [NSString stringWithFormat:@"%d",total];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    self.actionResultsController = nil;
    self.actionResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Action" sortingBy:@"LastModifiedDate" sectionBy:@"section" ascendingBy:NO includeDeleted:NO];
    self.actionResultsController.delegate = self;
    [self.actionCollection reloadData];
    
    self.notesResultsController = nil;
    self.notesResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Note"
                                                       sortingBy:@"lastModifiedDate" sectionBy:@"section" ascendingBy:NO includeDeleted:NO];
    self.notesResultsController.delegate = self;
    [self.notesCollection reloadData];
    // Refresh Issue
    self.issueResultsController = nil;
    self.issueResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Issue"
                                                       sortingBy:@"title" ascendingBy:YES includeDeleted:NO];
    self.issueResultsController.delegate = self;
    [self.issueCollection reloadData];
    // Refresh MeetingRC
    [self.project validateMeetingsAreCompleted];
    //[self.helper saveContext];
    self.meetingResultsController = nil;
    self.meetingResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Meeting"
                                                         sortingBy:@"title" ascendingBy:YES includeDeleted:NO];
    self.meetingResultsController.delegate = self;
    [self.meetingCollection reloadData];
    
    // Refresh RiskRC
    self.riskResultsController = nil;
    self.riskResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Risk"
                                                      sortingBy:@"title" ascendingBy:YES includeDeleted:NO];
    self.riskResultsController.delegate = self;
    [self.riskCollection reloadData];
    
    [self UpdateActionChartView];
    [self UpdateIssueChartView];
    [self UpdateRiskChartView];
  //  self.noteLabel.hidden = YES;
    [self updateNotesLabel];
    [self updateActionsLabel];
    [self updateIssuesLabel];
    [self updateRisksLabel];
    [self updateMeetingsLabel];
    [self.sortActionsButton setTitle:[self.project sortActionField ] forState:UIControlStateNormal];
    [self.sortIssuesButton setTitle:[self.project sortIssueField ] forState:UIControlStateNormal];
    [self.sortRisksButton setTitle:[self.project sortRiskField ] forState:UIControlStateNormal];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.canDisplayBannerAds = YES;
    [self.notestepper setDecrementImage:[UIImage imageNamed:@"stepperSmall"] forState:UIControlStateNormal];
    [self.notestepper setIncrementImage:[UIImage imageNamed:@"stepperLarge"] forState:UIControlStateNormal];
    [self.actionStepper setDecrementImage:[UIImage imageNamed:@"stepperSmall"] forState:UIControlStateNormal];
    [self.actionStepper setIncrementImage:[UIImage imageNamed:@"stepperLarge"] forState:UIControlStateNormal];
    [self.issueStepper setDecrementImage:[UIImage imageNamed:@"stepperSmall"] forState:UIControlStateNormal];
    [self.issueStepper setIncrementImage:[UIImage imageNamed:@"stepperLarge"] forState:UIControlStateNormal];
    [self.riskStepper setDecrementImage:[UIImage imageNamed:@"stepperSmall"] forState:UIControlStateNormal];
    [self.riskStepper setIncrementImage:[UIImage imageNamed:@"stepperLarge"] forState:UIControlStateNormal];
    [self.meetingStepper setDecrementImage:[UIImage imageNamed:@"stepperSmall"] forState:UIControlStateNormal];
    [self.meetingStepper setIncrementImage:[UIImage imageNamed:@"stepperLarge"] forState:UIControlStateNormal];
    self.scaleAction =1;
    self.scaleAction2 = 1;
    self.scaleIssue =1;
    self.scaleIssue2 = 1;
    self.scaleNote =1;
    self.scaleRisk = 1;
    self.scaleRisk2 = 1;
    self.scaleMeeting =1;

    if(self.projectColour == nil){
        self.projectColour = [[PJColour alloc]init];
    }
    self.helper = [DataHelper sharedInstance];
    _managedObjectContext = _helper.managedObjectContext;
    
    self.notesButton.enabled = NO;
    self.ActionsButton.enabled = NO;
    self.issuesButton.enabled = NO;
    self.risksButton.enabled = NO;
    self.meetingsButton.enabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

@end
