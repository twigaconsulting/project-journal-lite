//
//  ListNotesViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 18/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@class ListNotesViewController;
@protocol NoteListViewControllerDelegate <NSObject>
-(void)UpdateNotesBadge;
-(void) dimSuper;
-(void) UndimSuper;
@end

@interface ListNotesViewController : UIViewController
@property (weak, nonatomic) id <NoteListViewControllerDelegate> delegate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) id item;
- (void)dismissPopovers;
@end
