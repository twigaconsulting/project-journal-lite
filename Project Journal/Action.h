//
//  Action.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 18/07/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Issue, Media, Meeting, Note, Project, Risk;

@interface Action : NSManagedObject

@property (nonatomic, retain) NSString * actionID;
@property (nonatomic, retain) NSString * actionOwner;
@property (nonatomic, retain) NSString * actionRaiser;
@property (nonatomic, retain) NSDate * assignedDate;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSDate * completionDate;
@property (nonatomic, retain) NSString * completionSummary;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSDate * lastModifiedDate;
@property (nonatomic, retain) NSString * priority;
@property (nonatomic, retain) NSString * rag;
@property (nonatomic, retain) NSDate * raisedDate;
@property (nonatomic, retain) NSDate * reviewDate;
@property (nonatomic, retain) NSString * section;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSDate * targetCompletionDate;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * oldStatus;
@property (nonatomic, retain) NSSet *actionMedia;
@property (nonatomic, retain) NSSet *actionNotes;
@property (nonatomic, retain) Issue *inIssue;
@property (nonatomic, retain) Meeting *inMeeting;
@property (nonatomic, retain) Project *inProject;
@property (nonatomic, retain) Risk *inRisk;
@end

@interface Action (CoreDataGeneratedAccessors)

- (void)addActionMediaObject:(Media *)value;
- (void)removeActionMediaObject:(Media *)value;
- (void)addActionMedia:(NSSet *)values;
- (void)removeActionMedia:(NSSet *)values;

- (void)addActionNotesObject:(Note *)value;
- (void)removeActionNotesObject:(Note *)value;
- (void)addActionNotes:(NSSet *)values;
- (void)removeActionNotes:(NSSet *)values;

@end
