//
//  MeetingCollectionController.m
//  Project Journal
//
//  Created by Peter Pomlett on 10/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.

#import "MeetingCollectionController.h"
#import <QuartzCore/QuartzCore.h>
#import "MeetingFormContainer.h"
#import "ItemCollectionCell.h"
#import "PJColour.h"

@interface MeetingCollectionController ()<NSFetchedResultsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIActionSheetDelegate, MeetingFormContainerDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSFetchedResultsController *meetingResultsController;
@property (weak, nonatomic) IBOutlet UICollectionView *meetingCollection;
@property (strong, nonatomic) Meeting *selectedMeeting;
@property (strong, nonatomic)  PJColour *cellColour;
@property (nonatomic, strong) DataHelper *helper;
@property (strong, nonatomic) UIActionSheet *itemSheet;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addItem;
- (NSDateFormatter *)formatterDateTime;
- (NSDateFormatter *)formatterTime;
@property float scaleMeeting;
@property (weak, nonatomic) IBOutlet UIStepper *meetingStepper;
@end

@implementation MeetingCollectionController


- (IBAction)mettingSize:(UIStepper *)sender {
    int cellSize = sender.value;
    if(cellSize == 3){
        self.scaleMeeting =1;
        
        [self.meetingCollection.collectionViewLayout invalidateLayout];
    }
    
    if(cellSize == 2){
        self.scaleMeeting =0.42;
        
        [self.meetingCollection.collectionViewLayout invalidateLayout];
        
    }
    if(cellSize == 1){
        self.scaleMeeting =0.228;
        [self.meetingCollection.collectionViewLayout invalidateLayout];
        
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:self.scaleMeeting forKey:@"scaleMeetingView"];
    [defaults setInteger:self.meetingStepper.value forKey:@"meetingStepperView"];
    [defaults synchronize];
}

-(void)getCellSize{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.scaleMeeting =  [defaults floatForKey:@"scaleMeetingView"];
    self.meetingStepper.value = [defaults integerForKey:@"meetingStepperView"];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
        return CGSizeMake(240 , 110*self.scaleMeeting);
}
- (NSFormatter *)formatterTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
    });
    return formatter;
}

- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (void)MeetingFormContainerDidFinish:(MeetingFormContainer *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)newMeeting:(id)sender {
    if (!self.itemSheet){
        self.itemSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                     delegate:self
                                            cancelButtonTitle:nil
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:@"New Meeting",nil];
    }
    [self.itemSheet showFromBarButtonItem:sender animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(self.itemSheet == actionSheet){
        if(buttonIndex == 0){
            [self performSegueWithIdentifier:@"NewMeeting" sender:self];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"meeting"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        [[segue destinationViewController] setContainerMeeting:self.selectedMeeting];
    }
    if ([[segue identifier] isEqualToString:@"NewMeeting"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        self.selectedMeeting = nil;
        [[segue destinationViewController] setContainerMeeting:self.selectedMeeting];
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.meetingCollection reloadData];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo;
    sectionInfo = [self.meetingResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    ItemCollectionCell *cell ;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    Meeting *meeting = [self.meetingResultsController objectAtIndexPath:indexPath];
    if([meeting.status isEqualToString:@"Completed"]){
        cell.contentView.layer.backgroundColor  = self.cellColour.blue.CGColor;
    }
    else{
        cell.contentView.layer.backgroundColor  = self.cellColour.darkBlue.CGColor;
    }
    
    if([meeting isInCalendar]){
        cell.calImage.hidden = NO;
    }
    else{
        cell.calImage.hidden = YES;
    }
    cell.titleLabel.text = [meeting title];
    cell.locationLabel.text = [meeting location];
    cell.dateLabel.text = [[self formatterDateTime] stringFromDate:[meeting startDateTime]];
    cell.startTimeLabel.text =[self.formatterTime stringFromDate:[meeting startDateTime]];
    cell.endTimeLabel.text =[self.formatterTime stringFromDate:[meeting endDateTime]];
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(self.meetingCollection == collectionView){
        self.selectedMeeting = [self.meetingResultsController objectAtIndexPath:indexPath];
    }
    return YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    if (self.itemSheet != nil) {
        [self.itemSheet dismissWithClickedButtonIndex:self.itemSheet.cancelButtonIndex animated:NO];
        self.itemSheet = nil;
    }
     [[NSNotificationCenter defaultCenter] removeObserver:self]; 
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    if(self.cellColour == nil){
        self.cellColour = [[PJColour alloc]init];
    }
    if([self.project.status isEqualToString:@"Completed"]){
        self.navigationItem.rightBarButtonItem = nil;
    }
    [self getCellSize];
    [self.meetingStepper setDecrementImage:[UIImage imageNamed:@"stepperSmall"] forState:UIControlStateNormal];
    [self.meetingStepper setIncrementImage:[UIImage imageNamed:@"stepperLarge"] forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.meetingResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Meeting"
                                                         sortingBy:@"startDateTime" ascendingBy:YES includeDeleted:NO];
    self.meetingResultsController.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
}

-(void)refreshFromBackground{
    [self.meetingCollection reloadData];
   
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
