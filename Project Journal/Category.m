//
//  Category.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Category.h"


@implementation Category

@dynamic creationDate;
@dynamic title;
@dynamic uuid;

@end
