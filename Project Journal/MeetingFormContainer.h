//
//  MeetingFormContainer.h
//  Project Journal
//
//  Created by Peter Pomlett on 03/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "DataHelper.h"
#import "UIViewController+TCLViewController.h"
#import "XMLWriter.h"


@class MeetingFormContainer;
@protocol MeetingFormContainerDelegate <NSObject>

- (void)MeetingFormContainerDidFinish:(MeetingFormContainer *)controller;

@end

@interface MeetingFormContainer : UIViewController <MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, UIPrintInteractionControllerDelegate> 

@property (weak, nonatomic) id < MeetingFormContainerDelegate> delegate;
@property (strong,nonatomic) Project *project;
@property (strong,nonatomic) Meeting *containerMeeting;
@property (strong,retain) NSArray *toRecipients;
@property (strong,retain) NSArray *ccRecipients;
@property (strong,retain) NSArray *bccRecipients;
@property (strong,retain) NSString *subject;
@property (strong,retain) NSString *body;
@property (strong,retain) NSString *fileName;
@property (strong,retain) NSString *fileType;
@property (strong,retain) NSString *mimeType;
@property (strong,retain) UIViewController *currentView;
@property (strong,retain) MFMailComposeViewController *picker;

@end
