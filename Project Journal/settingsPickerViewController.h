//
//  settingsPickerViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 08/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class settingsPickerViewController;

@protocol settingsPickerViewControllerDelegate <NSObject>
- (void)pickerPickedDays:(int)days;
@end

@interface settingsPickerViewController : UIViewController
@property (weak, nonatomic) id < settingsPickerViewControllerDelegate> delegate;
@property (nonatomic, assign) int dayCount;
@property (nonatomic, strong) NSString *pickerBarTitle;
@end
