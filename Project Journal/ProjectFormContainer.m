//
//  ProjectDetailViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "PJColour.h"
#import "ProjectFormContainer.h"
#import "DetailFormViewController.h"
#import "BudgetFormViewController.h"
#import "DatesFormViewController.h"
#import "ActionsListViewController.h"
#import "ListNotesViewController.h"
@interface ProjectFormContainer ()<ActionsListViewControllerDelegate, BudgetFormViewControllerDelegate, NoteListViewControllerDelegate, UIAlertViewDelegate, UIActionSheetDelegate, DetailFormViewControllerDelegate, DatesFormViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *container;
@property (strong, nonatomic) DetailFormViewController *detailFormViewController;
@property (strong, nonatomic) BudgetFormViewController *budgetFormViewController;
@property (strong, nonatomic) DatesFormViewController *datesFormViewController;
@property (strong, nonatomic) ActionsListViewController *actionsListViewController;
@property (strong, nonatomic) ListNotesViewController *notesFormViewController;
@property (copy, nonatomic) NSString *key;
@property (nonatomic,strong) PJColour *badgeColour;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UIButton *budgetButton;
@property (weak, nonatomic) IBOutlet UIButton *datesButton;
@property (weak, nonatomic) IBOutlet UIButton *actionsButton;
@property (weak, nonatomic) IBOutlet UIButton *notesButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (strong,nonatomic) UIImage *whiteTab;
@property (strong,nonatomic) UIImage *greyTab;
@property (strong, nonatomic) DataHelper *helper;
@property (strong,nonatomic) UIActionSheet *sendSheet;
@property (strong,nonatomic) UILabel *noteCount;
@property (strong,nonatomic) UILabel *actionCount;
@property (strong,nonatomic) UIImageView *imageComplete;
@property (strong,nonatomic) UIImageView *imageReview;
@property (strong,nonatomic) UIImageView *image;
@property (strong,nonatomic) UIImageView *imageBudget;
@property (strong,nonatomic) UIAlertView *deleteAlert;
@end

@implementation ProjectFormContainer


-(void) dimSuper{
  self.view.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
}
-(void) UndimSuper{
  self.view.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
}

-(void)getNewAction:(id)itenAction{
    [self.delegate getNewAction:itenAction];
}

-(void)getAction:(Action*)itenAction{
    [self.delegate getTheActionForm:itenAction];
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self.datesFormViewController dismissPopovers];
    [self.budgetFormViewController dismissPopovers];
    [self.notesFormViewController dismissPopovers];
    [self.detailFormViewController disnissPopovers];
    if (self.sendSheet != nil) {
        [self.sendSheet dismissWithClickedButtonIndex:self.sendSheet.cancelButtonIndex animated:NO];
        self.sendSheet = nil;
  
    }
      if (self.deleteAlert != nil) {
    [self.deleteAlert dismissWithClickedButtonIndex:0 animated:NO];
          self.deleteAlert = nil;
      }
}



-(void)createSendSheet{
    self.sendSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                 delegate:self
                                        cancelButtonTitle:nil
                                   destructiveButtonTitle:nil
                                        otherButtonTitles:@"Print",@"Email Text",@"Email Attachment",nil];
    [self.sendSheet showFromRect:self.sendButton.frame inView:self.view animated:YES];
   
}
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self UndimSuper];
    if (buttonIndex == 0) {
        /* This scope prints the Risk details to a selectable printer
         */
        
        // Check if printing is avaliable
        if ([UIPrintInteractionController isPrintingAvailable]) {
            UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
            pic.delegate = self;
            
            UIPrintInfo *printInfo = [UIPrintInfo printInfo];
            printInfo.outputType = UIPrintInfoOutputGeneral;
            printInfo.jobName = self.projectObject.title;
            pic.printInfo = printInfo;
            
            UIMarkupTextPrintFormatter *textFormatter = [[UIMarkupTextPrintFormatter alloc] initWithMarkupText:self.projectObject.getDetailHTML];
            textFormatter.startPage = 0;
            textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
            textFormatter.maximumContentWidth = 6 * 72.0;
            pic.printFormatter = textFormatter;
            pic.showsPageRange = YES;
            [pic presentFromRect:self.sendButton.frame inView:self.view animated:YES completionHandler:nil];
            
        }

    }
    if (buttonIndex == 1) {
        /* This method creates the email for the Risk. This email has
         * no attachment the Risk details are with the email body.
         */
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.projectObject.title forKey:@"subject"];
        [data setValue:self.projectObject.getDetailHTML forKey:@"body"];
        
        [data setValue:nil forKey:@"fileName"];
        
        [self displayMailComposeSheet:data];
        
    }
    
    if (buttonIndex == 2) {
        /* This method creates the email for the Risk. This email has
         * an XLS attachment of the Risk Details.
         */
        
        XMLWriter *writer = [[XMLWriter alloc] init];
        NSString *xmlString = [writer writeXMLForExcel:self.projectObject];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //make a file name to write the data to using the documents directory:
        NSString *theFileName = [NSString stringWithFormat:@"%@/%@.xls",
                                 documentsDirectory,self.projectObject.projectID];

        
        NSString *shortFileName = [NSString stringWithFormat:@"%@.xls",self.projectObject.projectID];

        [xmlString writeToFile:theFileName
                    atomically:NO
                      encoding:NSUTF8StringEncoding
                         error:nil];
       
        //Setup email
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.projectObject.title forKey:@"subject"];
        [data setValue:@"Project attachment for Microsoft Excel" forKey:@"body"];
        [data setValue:[NSData dataWithContentsOfFile:theFileName] forKey:@"attachmentData"];
        [data setValue:[NSString stringWithFormat:@"%@",shortFileName] forKey:@"fileName"];
        
        [self displayMailComposeSheet:data];
        
        [[NSFileManager defaultManager] removeItemAtPath:theFileName error:nil];
    }
}

- (BOOL)disablesAutomaticKeyboardDismissal{
    return NO;
}

//child controller delegate methods to update badges
-(void)upDateTitle{
    if ( [self.delegate respondsToSelector:@selector(updateProjectView:)] )
    {  [self.delegate updateProjectView:self];
    }
}
//date form delegate
-(void) upDateRagBadge{
    [self createCompletedBadge];
    [self createDateRagBadge];
    if ( [self.delegate respondsToSelector:@selector(updateProjectView:)] )
    {  [self.delegate updateProjectView:self];
    }
}
//DEL1 
- (void) upDateBadge{
    [self createCompletedBadge];
     [self createReviewBadge];
     [self createBudgetRagBadge];
       [self createDateRagBadge];
    //this calls the delegate on the project collection view
    if ( [self.delegate respondsToSelector:@selector(updateProjectView:)] )
    {  [self.delegate updateProjectView:self];
    }
}

-(void) upDateReviewBadge{
    [self createReviewBadge];
    if ( [self.delegate respondsToSelector:@selector(updateProjectView:)] )
    {  [self.delegate updateProjectView:self];
    }
}

-(void)updateBudgetBadge{
    [self createBudgetRagBadge];
    if ( [self.delegate respondsToSelector:@selector(updateProjectView:)] )
    {  [self.delegate updateProjectView:self];
    }
}

//Action list Delegate method
-(void)updateActionBadge{
    [self createActionBadge];
}

-(void)createDateRagBadge{
    
  
    if(!self.image){
    self.image = [[UIImageView alloc] initWithFrame:CGRectMake(23, 32,11, 11)];
    [self.datesButton addSubview:self.image];
    }
      NSString *ragColour= [self.projectObject fetchDateRAGStatus];
    if([ragColour isEqualToString:@"Amber"]){
        self.image.image = [UIImage imageNamed:@"AmberBadje"];
    }
    if([ragColour isEqualToString:@"Red"]){
        self.image.image = [UIImage imageNamed:@"redBadje"];
    }
    if([ragColour isEqualToString:@"Green"]){
        self.image.image = [UIImage imageNamed:@"greenBadge"];
    }
    if([ragColour isEqualToString:@"Blue"]){
        self.image.image = [UIImage imageNamed:@"blueBadje"];
    }
}

-(void)createBudgetRagBadge{
    
    if(!self.imageBudget){
    self.imageBudget = [[UIImageView alloc] initWithFrame:CGRectMake(29, 32,11, 11)];
        [self.budgetButton addSubview:self.imageBudget];
    }
    NSString *ragColour= [self.projectObject fetchBudgetRAGStatus];
        if([ragColour isEqualToString:@"Amber"]){
              self.imageBudget.image = [UIImage imageNamed:@"AmberBadje"];
        }
        if([ragColour isEqualToString:@"Red"]){
              self.imageBudget.image = [UIImage imageNamed:@"redBadje"];
        }
    
         if([ragColour isEqualToString:@"Green"]){
            self.imageBudget.image = [UIImage imageNamed:@"greenBadge"];
        }
    if([ragColour isEqualToString:@"Blue"]){
        self.imageBudget.image = [UIImage imageNamed:@"blueBadje"];
    }
}


-(void)createActionBadge{
    int actionCount = [self.projectObject actionCountForProject];
    if (!self.actionCount) {
   self.actionCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, self.actionsButton.frame.size.width, 10)];
    self.actionCount.backgroundColor = [UIColor clearColor];
   self.actionCount.textAlignment = NSTextAlignmentCenter;
    self.actionCount.numberOfLines=1;
    [self.actionCount setFont:[UIFont systemFontOfSize:12]];
    
    [self.actionsButton addSubview:self.actionCount];
    }
    if(actionCount > 0){
      self.actionCount.textColor = [UIColor darkGrayColor];
    }
    else {
      self.actionCount.textColor = [UIColor lightGrayColor];
    }
    self.actionCount.text = [NSString stringWithFormat:@"%d",actionCount];
}

-(void)createCompletedBadge{
    if(!self.imageComplete){
         self.imageComplete = [[UIImageView alloc] initWithFrame:CGRectMake(29, 32,11, 11)];
     [self.detailButton addSubview:self.imageComplete];
    }
 
    if([self.projectObject.status isEqualToString:@"Completed"]) {
        self.imageComplete.image = [UIImage imageNamed:@"blueBadje"];
    }
    else{
         self.imageComplete.image = [UIImage imageNamed:@"noBadje"];
    }
}

- (void) createReviewBadge{
    if(!self.imageReview){
   self.imageReview = [[UIImageView alloc] initWithFrame:CGRectMake(35, 32,11, 11)];
    [self.datesButton addSubview:self.imageReview];
    }
    if(self.projectObject.isForReview){
         self.imageReview.image = [UIImage imageNamed:@"reviewBadjge"];
    }
    else{
        self.imageReview.image = [UIImage imageNamed:@"noReviewBadge"];
    }
}

-(void)createNotesBadgeIfNeeded{
    int noteCount = [[self.helper fetchNotesList:self.projectObject]count];
    
    if(!self.noteCount){
   self.noteCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, self.notesButton.frame.size.width, 10)];
    self.noteCount.backgroundColor = [UIColor clearColor];
    self.noteCount.textAlignment = NSTextAlignmentCenter;
    self.noteCount.numberOfLines=1;
    [self.noteCount setFont:[UIFont systemFontOfSize:12]];
    self.noteCount.textColor = [UIColor darkGrayColor];
    [self.notesButton addSubview:self.noteCount];
    }
    
    if(noteCount > 0){
        self.noteCount.textColor = [UIColor darkGrayColor];
    }
    else {
        self.noteCount.textColor = [UIColor lightGrayColor];
    }
    self.noteCount.text = [NSString stringWithFormat:@"%d",noteCount];
}

-(void)enableButtons{
    self.detailButton.enabled = YES;
    self.budgetButton.enabled = YES;
    self.actionsButton.enabled = YES;
    self.notesButton.enabled = YES;
    self.datesButton.enabled = YES;
    
}

#pragma mark - IBActions load views

- (IBAction)Detail {
    if(self.detailFormViewController == nil) {
        self.detailFormViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Detail"];
        self.detailFormViewController.project = self.projectObject;
        self.detailFormViewController.delegate = self;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.detailFormViewController and:@"Detail"];
    self.detailButton.enabled = NO;
}

- (IBAction)Budget {
    if(self.budgetFormViewController == nil) {
        self.budgetFormViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Budget"];
        self.budgetFormViewController.project = self.projectObject;
        self.budgetFormViewController.delegate = self;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.budgetFormViewController and:@"Budget"];
    self.budgetButton.enabled = NO;
}

- (IBAction)Dates {
    if(self.datesFormViewController == nil) {
        self.datesFormViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Dates"];
        self.datesFormViewController.project = self.projectObject;
        self.datesFormViewController.delegate = self;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.datesFormViewController and:@"Dates"];
    self.datesButton.enabled = NO;
}

- (IBAction)Actions {
    if(self.actionsListViewController == nil) {
        self.actionsListViewController= [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"MAction"];
        self.actionsListViewController.item = self.projectObject;
        self.actionsListViewController.delegate = self;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.actionsListViewController and:@"Action"];
    
    self.actionsButton.enabled = NO;
}

- (IBAction)Notes {
    if(self.notesFormViewController == nil) {
        self.notesFormViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"NotesTable"];
        self.notesFormViewController.item = self.projectObject;
        self.notesFormViewController.delegate = self;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.notesFormViewController and:@"Notes"];
    self.notesButton.enabled = NO;
}

//Use a key to keep track of what comtroller is loaded
-(void)SwapOldControllerForNew:(UIViewController*)controller and:(NSString *)aKey
{
    if([self.key isEqualToString:@"Detail"]){
        [self.detailFormViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            [self.detailFormViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
    }
                       completion:^(BOOL finished) {
                            [self.detailFormViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
 else if([self.key isEqualToString:@"Budget"]){
        [self.budgetFormViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                          options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            [self.budgetFormViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                       completion:^(BOOL finished){
                            [self.budgetFormViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                      }];
        self.key = aKey;
    }
 else if([self.key isEqualToString:@"Dates"])
    {
        [self.datesFormViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            [self.datesFormViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.datesFormViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
 else if([self.key isEqualToString:@"Action"]){
        [self.actionsListViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                       animations:^{
                            [self.actionsListViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.actionsListViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        
        self.key = aKey;
    }
 else if([self.key isEqualToString:@"Notes"]){
        [self.notesFormViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                         duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
        [self.notesFormViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
    }
                        completion:^(BOOL finished) {
                            [self.notesFormViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
}

- (void)viewWillAppear:(BOOL)animated
{ [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

-(void)refreshFromBackground{
    [self createReviewBadge];
    [self createDateRagBadge];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self createNotesBadgeIfNeeded];
    [self createCompletedBadge];
    [self createReviewBadge];
    [self createActionBadge];
   [self createDateRagBadge];
    [self createBudgetRagBadge];
// self.view.tintAdjustmentMode = UIViewTintAdjustmentModeAutomatic;
}




- (void)viewDidLoad
{
    [super viewDidLoad];
 
   
   // [self addToolBarImages];
    self.helper = [DataHelper sharedInstance];
    if(!self.badgeColour){
        self.badgeColour = [[PJColour alloc]init];
    }
    if(self.projectObject==nil){
        
        self.projectObject = [self.helper newProject];
        [self.helper saveContext];
    }
    //add detail controller when view loads
    if(self.detailFormViewController == nil){
        self.detailFormViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Detail"];
        self.detailFormViewController.project = self.projectObject;
        self.detailFormViewController.delegate = self;
    }
    [self addChildViewController:self.detailFormViewController];
    [self.container addSubview:self.detailFormViewController.view];
    [self.detailFormViewController didMoveToParentViewController:self];
    self.key = @"Detail";
    [self enableButtons];
    self.detailButton.enabled = NO;
 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) UpdateNotesBadge{
    [self createNotesBadgeIfNeeded];
}

- (IBAction)send:(id)sender {
    [self.view endEditing:YES];
    [self dimSuper];
    [self createSendSheet];
}

- (IBAction)done:(id)sender {
    if ( [self.delegate respondsToSelector:@selector(updateProjectView:)] )
    {  [self.delegate updateProjectView:self];
    }
    [self.delegate ProjectFormContainerDidFinish:self];
    
}


- (IBAction)projectDelete:(id)sender {

    [self.view endEditing:YES];
    [self dimSuper];
   // UIAlertView *deleteAlert;
    if(!self.deleteAlert){
    self.deleteAlert = [[UIAlertView alloc] initWithTitle:@"Delete Project"
                                                          message:@"Deleting this project will delete all of its data"
                                                         delegate:self                                          cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Delete",nil];
    }
    [self.deleteAlert show];
}

//[theAlertView dismissWithClickedButtonIndex:0 animated:YES];



- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self UndimSuper];
    if(buttonIndex == 1){
        [self.helper deleteObject: self.projectObject ];
         [self.helper saveContext];
        
        [self.delegate deleteProject:self];
    }}

@end
