//
//  MeetingTimeViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 24/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "MeetingTimeViewController.h"

@interface MeetingTimeViewController ()<UITableViewDelegate, UITableViewDataSource >
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) NSString *start;
@property (strong, nonatomic) NSString *end;
@property (assign, nonatomic) int selectedRow;
@property (assign, nonatomic) BOOL isValid;
- (NSDateFormatter *)formatterDateTime;
@end

@implementation MeetingTimeViewController

- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (IBAction)showTime:(UISwitch *)sender {
    if(sender.on){
        self.datePicker.datePickerMode = UIDatePickerModeDate;
    }
    
    else{
     self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    }
}

- (IBAction)Cancel {
    [self.delegate meetingTimeViewControllerDidFinish:self];
}

- (IBAction)Done {

    if(self.isValid){
         [self.delegate pickedStartTime:self.startDate endTime:self.endDate ];
    }
    else{
   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cannot Set Dates"
                                                   message:@"The start date must be before the end date."
                                                  delegate:nil                                          cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil];
    [alert show];
    }
}

- (IBAction)dateHasChanged:(UIDatePicker *)sender {
    if(self.selectedRow == 0){
        self.startDate = self.datePicker.date;
        if( [self.startDate timeIntervalSinceDate:self.endDate] >= 0 ) {
            NSTimeInterval meetingLength = 1 * 60 * 60;
           self.endDate = [self.startDate dateByAddingTimeInterval:meetingLength];
    }
       self.isValid = YES;  
    }
    if(self.selectedRow == 1){
        self.endDate = self.datePicker.date;
    if( [self.startDate timeIntervalSinceDate:self.endDate] >= 0 ) {
        
        self.isValid = NO;
    }
    else {
        self.isValid = YES;
    }
    }
    NSIndexPath *ipath = [self.tableView indexPathForSelectedRow];
    [self.tableView reloadData];
    [self.tableView selectRowAtIndexPath:ipath animated:NO scrollPosition:UITableViewScrollPositionNone];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if(self.isValid){
        cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    }
    else{
        cell.detailTextLabel.textColor = [UIColor redColor];
    }
        
        if(indexPath.row == 0){
            cell.textLabel.text =@"Start";
          cell.detailTextLabel.text = [[self formatterDateTime] stringFromDate:self.startDate];
            }
           
        if(indexPath.row == 1){
            cell.textLabel.text =@"End";
              cell.detailTextLabel.text = [[self formatterDateTime] stringFromDate:self.endDate];
            
            }
            
        return cell;
    }
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    if(indexPath.row == 0){
        self.selectedRow = 0;
        if(self.startDate){
            [self.datePicker setDate:self.startDate animated:YES];}
    }
    
    if(indexPath.row == 1){
        self.selectedRow = 1;
        if(self.endDate){
            [self.datePicker setDate:self.endDate animated:YES];}
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isValid = YES;
    self.selectedRow = 0;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    [self.datePicker setDate:self.startDate animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
