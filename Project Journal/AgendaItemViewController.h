//
//  AgendaItemViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 12/05/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AgendaItemViewController;

@protocol AgendaItemViewControllerDelegate <NSObject>

-(void) AgendaItemViewControllerDidFinish:(AgendaItemViewController *)controller;
-(void) AgendaItem:(NSString *)item;
@end

@interface AgendaItemViewController : UIViewController
@property (weak, nonatomic) id <AgendaItemViewControllerDelegate> delegate;
@end
