//
//  DatePickerViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 27/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DatePickerViewController;

@protocol DatePickerViewControllerDelegate <NSObject>
- (void)DatePickerViewControllerControllerDidFinish:(DatePickerViewController *)controller;
-(void)DatePicked:(NSDate *)date andController:(DatePickerViewController *)controller;

@end

@interface DatePickerViewController : UIViewController
@property (weak, nonatomic) id <DatePickerViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIDatePicker *picker;
@property (strong, nonatomic) NSDate *pickerValue;
@end
