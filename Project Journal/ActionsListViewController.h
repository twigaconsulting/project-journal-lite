//
//  MeetingActionsViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 17/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h" 
@class ActionsListViewController ;
@protocol ActionsListViewControllerDelegate <NSObject>

-(void)updateActionBadge;
-(void)getAction:(Action*)itenAction;
-(void)getNewAction:(id)itenAction;
@end


@interface ActionsListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) id <ActionsListViewControllerDelegate> delegate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) id item;
@end
