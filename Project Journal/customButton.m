//
//  customButton.m
//  Project Journal
//
//  Created by Peter Pomlett on 07/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//THIS CLASS INCREASES THE HIT SIZE OF SMALL BUTTONS TO 44 BY 44 pionts
//all buttons on the form fields should be a subclass of this
#import "customButton.h"

@implementation customButton

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    CGRect bounds = self.bounds;
    CGFloat widthDelta =44.0 - bounds.size.width;
    CGFloat heightDelta =44.0 - bounds.size.width;
    bounds = CGRectInset(bounds, -0.5 * widthDelta, -0.5 * heightDelta);
    return CGRectContainsPoint(bounds, point);
}
@end
