//
//  Stakeholder+TCLStakeholder.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Stakeholder+TCLStakeholder.h"

@implementation Stakeholder (TCLStakeholder)

- (NSString *)fetchRAGStatusIntervalsWithAmber:(NSNumber *)amber withRed:(NSNumber *)red
{
    if ([self.influence isEqualToString:@"Low"] &
           [self.priority isEqualToString:@"Low"] &
            [self.viewpoint isEqualToString:@"Low"]) {
        return @"Green";
    } else if ([self.influence isEqualToString:@"High"] &
               [self.priority isEqualToString:@"High"] &
               [self.viewpoint isEqualToString:@"High"]) {
        return @"Red";
    } else {
        return @"Amber";
    }

}



@end
