//
//  NoteCollectionController.m
//  Project Journal
//
//  Created by Peter Pomlett on 03/07/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "NoteCollectionController.h"
#import <QuartzCore/QuartzCore.h>
#import "NotePopover.h"
#import "ItemCollectionCell.h"
#import "PJColour.h"
#import "NotesHeader.h"
@interface NoteCollectionController ()<NSFetchedResultsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIActionSheetDelegate, UIPopoverControllerDelegate, NotePopoverDelegate, UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UIStoryboardPopoverSegue *popSegue;
@property (strong, nonatomic) NSFetchedResultsController *notesResultsController;
@property (weak, nonatomic) IBOutlet UICollectionView *notesCollection;
@property (strong, nonatomic) Note *selectedNote;
@property (strong, nonatomic)  PJColour *cellColour;
@property (nonatomic, strong) DataHelper *helper;
@property (strong, nonatomic) UIActionSheet *itemSheet;
//@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addItem;


- (NSDateFormatter *)formatterDateTime;
////

@property (strong, nonatomic) NotePopover *noteView;
@property (strong, nonatomic) UIPopoverController *cellPop;


@property (weak, nonatomic) IBOutlet UIStepper *notestepper;
@property float scaleNote;
@end

@implementation NoteCollectionController


- (IBAction)noteSize:(UIStepper *)sender {
    int cellSize = sender.value;
    if(cellSize == 2){
        self.scaleNote =1;
        [self.notesCollection.collectionViewLayout invalidateLayout];
    }
    if(cellSize == 1){
        self.scaleNote = 0.45;
        [self.notesCollection.collectionViewLayout invalidateLayout];
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:self.scaleNote forKey:@"scaleNoteView"];
    [defaults setInteger:self.notestepper.value forKey:@"noteStepperView"];
    [defaults synchronize];
}

-(void)getCellSize{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.scaleNote =  [defaults floatForKey:@"scaleNoteView"];
    self.notestepper.value = [defaults integerForKey:@"noteStepperView"];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
        return CGSizeMake(240 , 104*self.scaleNote);
  
}

- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}





-(void) NotePopoverDidfinish:(NotePopover *)controller{
    if ([self.popSegue.popoverController isPopoverVisible])
    {
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
        
    }
    
   [self.cellPop dismissPopoverAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"note"]) {
    
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[self.popSegue destinationViewController] setDelegate:self];
        [[self.popSegue destinationViewController] setNote:self.selectedNote];
       // self.popSegue.popoverController.delegate = self;
    }
    if ([[segue identifier] isEqualToString:@"NewNote"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        
        [[self.popSegue destinationViewController] setDelegate:self];
        [[self.popSegue destinationViewController] setItem:self.project];
        [[self.popSegue destinationViewController] setNote:nil];
        
    }
}

- (IBAction)newNote:(id)sender {
    
    if (!self.itemSheet){
        self.itemSheet = [[UIActionSheet alloc] initWithTitle:@"In Project"
                                                     delegate:self
                                            cancelButtonTitle:nil
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:@"New Note",nil];
    }
    [self.itemSheet showFromBarButtonItem:sender animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(self.itemSheet == actionSheet){
        if(buttonIndex == 0){
            [self performSegueWithIdentifier:@"NewNote" sender:self];
        }
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.notesCollection reloadData];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        NotesHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        if(self.notesCollection == collectionView){
            //Hide ordering letter from notes header label
            if([[ [[self.notesResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"A. Project"]){
                headerView.titleLable.text = @"  In Project";
            }
            if([[ [[self.notesResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"B. Meeting"]){
                headerView.titleLable.text = @"  In Meetings";
            }
            if([[ [[self.notesResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"C. Action"]){
                headerView.titleLable.text = @"  In Actions";
            }
            if([[ [[self.notesResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"D. Issue"]){
                headerView.titleLable.text = @"  In Issues";
            }
            if([[ [[self.notesResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"E. Risk"]){
                headerView.titleLable.text = @"In Risks";
            }
        }
        reusableview = headerView;
    }
    return reusableview;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return  [[self.notesResultsController sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo;
    sectionInfo = [self.notesResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    ItemCollectionCell *cell ;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    Note *note = [self.notesResultsController objectAtIndexPath:indexPath];
    cell.contentView.layer.backgroundColor  = self.cellColour.darkBlue.CGColor;
    if([note inAction]){
        cell.actionParentLabel.text = [[note inAction] title];
    }
    else if([note inIssue]){
        cell.actionParentLabel.text = [[note inIssue] title];
    }
    else if([note inMeeting]){
        cell.actionParentLabel.text = [[note inMeeting] title];
    }
    else if([note inProject]){
        cell.actionParentLabel.text = [[note inProject] title];
    }
    else if([note inRisk]){
        cell.titleLabel.text = [[note inRisk] title];
    }
    cell.noteTextView.text = [note  summary];
    cell.noteTextView.textColor = [UIColor whiteColor];
    cell.dateLabel.text =  [self.formatterDateTime stringFromDate:[note lastModifiedDate]];
    
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *aCell;
    aCell = [collectionView cellForItemAtIndexPath:indexPath];
    if(self.notesCollection == collectionView){
        self.selectedNote = [self.notesResultsController objectAtIndexPath:indexPath];
            self.noteView = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"noteView"];
            [self.noteView setDelegate:self ];
  self.noteView.note = self.selectedNote;
        self.cellPop = [[UIPopoverController alloc] initWithContentViewController:self.noteView];
        [self.cellPop presentPopoverFromRect:aCell.frame inView:self.notesCollection permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionDown | UIPopoverArrowDirectionUp | UIPopoverArrowDirectionRight animated:YES];
        
    
    }
    return YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    if (self.itemSheet != nil) {
        [self.itemSheet dismissWithClickedButtonIndex:self.itemSheet.cancelButtonIndex animated:NO];
        self.itemSheet = nil;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    if(self.cellColour == nil){
        self.cellColour = [[PJColour alloc]init];
    }
    [self getCellSize];
    
    [self.notestepper setDecrementImage:[UIImage imageNamed:@"stepperSmall"] forState:UIControlStateNormal];
    [self.notestepper setIncrementImage:[UIImage imageNamed:@"stepperLarge"] forState:UIControlStateNormal];

    self.notesResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Note"
                                                       sortingBy:@"lastModifiedDate" sectionBy:@"section" ascendingBy:NO includeDeleted:NO];
    self.notesResultsController.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
