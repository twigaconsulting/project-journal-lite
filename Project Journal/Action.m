//
//  Action.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 18/07/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Action.h"
#import "Issue.h"
#import "Media.h"
#import "Meeting.h"
#import "Note.h"
#import "Project.h"
#import "Risk.h"


@implementation Action

@dynamic actionID;
@dynamic actionOwner;
@dynamic actionRaiser;
@dynamic assignedDate;
@dynamic category;
@dynamic completionDate;
@dynamic completionSummary;
@dynamic creationDate;
@dynamic deleted;
@dynamic lastModifiedDate;
@dynamic priority;
@dynamic rag;
@dynamic raisedDate;
@dynamic reviewDate;
@dynamic section;
@dynamic status;
@dynamic summary;
@dynamic targetCompletionDate;
@dynamic title;
@dynamic type;
@dynamic uuid;
@dynamic oldStatus;
@dynamic actionMedia;
@dynamic actionNotes;
@dynamic inIssue;
@dynamic inMeeting;
@dynamic inProject;
@dynamic inRisk;

@end
