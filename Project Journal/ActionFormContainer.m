//
//  ItemDetailViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//


#import "ActionFormContainer.h"
#import "PJColour.h"
#import "ActionDetailViewController.h"
#import "ActionDatesViewController.h"
#import "ListNotesViewController.h"
@interface ActionFormContainer ()< NoteListViewControllerDelegate,  ActionDatesViewControllerDelegate, ActionDetailViewControllerDelegate, UIActionSheetDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *container;
@property (nonatomic,strong) PJColour *badgeColour;;
@property (strong, nonatomic)ListNotesViewController *notesFormViewController;
@property (strong, nonatomic)ActionDetailViewController *actionDetailViewController;
@property (strong, nonatomic)ActionDatesViewController *actionDatesViewController;

@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UIButton *datesButton;
@property (weak, nonatomic) IBOutlet UIButton *notesButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIImageView *barImageView;
@property (strong,nonatomic) UIImage *whiteTab;
@property (strong,nonatomic) UIImage *greyTab;

@property (strong, nonatomic) DataHelper *helper;
@property (strong,nonatomic) NSString *key;
@property (strong,nonatomic) UIActionSheet *sendSheet;
@property (strong,nonatomic) UIActionSheet *deleteSheet;
@property (weak, nonatomic) IBOutlet UIButton *parentButton;
@property (strong,nonatomic) id actionParent;
@property (strong,nonatomic) UILabel *noteCount;

@property (strong,nonatomic) UIImageView *imageComplete;
@property (strong,nonatomic) UIImageView *imageReview;
@property (strong,nonatomic) UIImageView *image;
@property (strong,nonatomic) UIAlertView *deleteAlert;

@end

@implementation ActionFormContainer
-(void) dimSuper{
    self.view.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
}
-(void) UndimSuper{
    self.view.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
}

- (IBAction)goToParent:(UIButton *)sender {
    if(self.containerAction.inIssue){
     self.actionParent = self.containerAction.inIssue;
    }
    else if(self.containerAction.inRisk){
        self.actionParent = self.containerAction.inRisk;
    }
    else {
         self.actionParent = self.containerAction.inProject;
    }
    [self.delegate getActionParent:self.actionParent];
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self.actionDatesViewController dismissPopovers];
   
    [self.notesFormViewController dismissPopovers];
    [self.actionDetailViewController dismissPopovers];
    if (self.sendSheet .isVisible) {
        [self UndimSuper];
        [self.sendSheet dismissWithClickedButtonIndex:self.sendSheet.cancelButtonIndex animated:NO];
        self.sendSheet = nil;
    }
    if (self.deleteAlert.isVisible) {
        [self.deleteAlert dismissWithClickedButtonIndex:0 animated:NO];
        self.deleteAlert = nil;
    }
    
}

- (BOOL)disablesAutomaticKeyboardDismissal{
    return NO;
}

#pragma mark - toolbar action sheet methods

-(void)createDeleteSheet{
    self.deleteSheet = [[UIActionSheet alloc] initWithTitle:nil
                                             delegate:self
                                    cancelButtonTitle:nil
                               destructiveButtonTitle:@"Delete Action"
                                    otherButtonTitles:nil];
    [self.deleteSheet showFromRect:self.deleteButton.frame inView:self.view animated:YES];
    
}

-(void)createSendSheet{
    self.sendSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                 delegate:self
                                        cancelButtonTitle:nil
                                   destructiveButtonTitle:nil
                                        otherButtonTitles:@"Print",@"Email Text",@"Email Attachment",nil];
    [self.sendSheet showFromRect:self.sendButton.frame inView:self.view animated:YES];    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self UndimSuper];
    if(self.sendSheet == actionSheet){
    if (buttonIndex == 0) {
        // Check if printing is avaliable
        if ([UIPrintInteractionController isPrintingAvailable]) {
            UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
            pic.delegate = self;
            
            UIPrintInfo *printInfo = [UIPrintInfo printInfo];
            printInfo.outputType = UIPrintInfoOutputGeneral;
            printInfo.jobName = self.containerAction.title;
            pic.printInfo = printInfo;
            
            UIMarkupTextPrintFormatter *textFormatter = [[UIMarkupTextPrintFormatter alloc] initWithMarkupText:self.containerAction.getDetailHTML];
            textFormatter.startPage = 0;
            textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
            textFormatter.maximumContentWidth = 6 * 72.0;
            pic.printFormatter = textFormatter;
            pic.showsPageRange = YES;
            [pic presentFromRect:self.sendButton.frame inView:self.view animated:YES completionHandler:nil];
        }
    }
    
    if(buttonIndex == 1){
        // Email with detail text
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.containerAction.title forKey:@"subject"];
        [data setValue:self.containerAction.getDetailHTML forKey:@"body"];
        
        [data setValue:nil forKey:@"fileName"];
        
        
        [self displayMailComposeSheet:data];
 
    }
    
    if(buttonIndex == 2){
        //Email with attachement
        XMLWriter *writer = [[XMLWriter alloc] init];
        NSString *xmlString = [writer writeXMLForExcel:self.containerAction];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //make a file name to write the data to using the documents directory:
        NSString *theFileName = [NSString stringWithFormat:@"%@/%@.xls",
                                 documentsDirectory,self.containerAction.actionID];
        //create content - four lines of text
        //NSString *content = @"One\nTwo\nThree\nFour\nFive";
        //save content to the documents directory
        
        NSString *shortFileName = [NSString stringWithFormat:@"%@.xls",self.containerAction.actionID];
        
        [xmlString writeToFile:theFileName
                    atomically:NO
                      encoding:NSUTF8StringEncoding
                         error:nil];
        
        //Setup email
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.containerAction.title forKey:@"subject"];
        [data setValue:@"Action attachment for Microsoft Excel" forKey:@"body"];
        [data setValue:[NSData dataWithContentsOfFile:theFileName] forKey:@"attachmentData"];
        [data setValue:[NSString stringWithFormat:@"%@",shortFileName] forKey:@"fileName"];
        
        [self displayMailComposeSheet:data];
        
        [[NSFileManager defaultManager] removeItemAtPath:theFileName error:nil];
        
    }
    
}
    
    if(self.deleteSheet == actionSheet){
        if (buttonIndex == 0) {
            [self.helper deleteObject:self.containerAction];
            [self.helper saveContext];
            [self.delegate ActionFormContainerDidFinish:self];
        }
    }
}

#pragma mark - button badge methods
-(void) upDateRagBadge{
    [self createCompletedBadge];
    [self createDateRagBadge];
}

- (void) upDateBadge{
    [self createCompletedBadge];
    [self createReviewBadge];
     [self createDateRagBadge];
}

-(void) upDateReviewBadge{
    [self createReviewBadge];
}

-(void)createDateRagBadge{
    if(!self.image){
        self.image = [[UIImageView alloc] initWithFrame:CGRectMake(23, 32,11, 11)];
        [self.datesButton addSubview:self.image];
    }
    NSString *ragColour= [self.containerAction fetchDateRAGStatus];
    if([ragColour isEqualToString:@"Amber"]){
        self.image.image = [UIImage imageNamed:@"AmberBadje"];
    }
    if([ragColour isEqualToString:@"Red"]){
        self.image.image = [UIImage imageNamed:@"redBadje"];
    }
    if([ragColour isEqualToString:@"Green"]){
        self.image.image = [UIImage imageNamed:@"greenBadge"];
    }
    if([ragColour isEqualToString:@"Blue"]){
        self.image.image = [UIImage imageNamed:@"blueBadje"];
    }

}



-(void)createCompletedBadge{
    
    if(!self.imageComplete){
        self.imageComplete = [[UIImageView alloc] initWithFrame:CGRectMake(29, 32,11, 11)];
        [self.detailButton addSubview:self.imageComplete];
    }
    
    if([self.containerAction.status isEqualToString:@"Completed"]) {
        self.imageComplete.image = [UIImage imageNamed:@"blueBadje"];
    }
    else{
        self.imageComplete.image = [UIImage imageNamed:@"noBadje"];
    }
   
}
  
-(void) UpdateNotesBadge{
    
    [self createNotesBadge];
    
}

-(void)createNotesBadge{
    int noteCount = [[self.helper fetchNotesList:self.containerAction]count];
    
    if(!self.noteCount){
        self.noteCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, self.notesButton.frame.size.width, 10)];
        self.noteCount.backgroundColor = [UIColor clearColor];
        self.noteCount.textAlignment = NSTextAlignmentCenter;
        self.noteCount.numberOfLines=1;
        [self.noteCount setFont:[UIFont systemFontOfSize:12]];
        self.noteCount.textColor = [UIColor darkGrayColor];
        [self.notesButton addSubview:self.noteCount];
    }
    
    if(noteCount > 0){
        self.noteCount.textColor = [UIColor darkGrayColor];
    }
    else {
        self.noteCount.textColor = [UIColor lightGrayColor];
    }
    self.noteCount.text = [NSString stringWithFormat:@"%d",noteCount];

}


- (void) createReviewBadge{
    
    if(!self.imageReview){
        self.imageReview = [[UIImageView alloc] initWithFrame:CGRectMake(35, 32,11, 11)];
        [self.datesButton addSubview:self.imageReview];
    }
    if(self.containerAction.isForReview){
        self.imageReview.image = [UIImage imageNamed:@"reviewBadjge"];
    }
    else{
        self.imageReview.image = [UIImage imageNamed:@"noReviewBadge"];
    }

}


-(void)SwapOldControllerForNew:(UIViewController*)controller and:(NSString *)aKey
{
    if([self.key isEqualToString:@"Detail"]){
        [self.actionDetailViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.actionDetailViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.actionDetailViewController removeFromParentViewController];                  
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
 else if([self.key isEqualToString:@"Date"]){
        [self.actionDatesViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.actionDatesViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.actionDatesViewController removeFromParentViewController];                 
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }    
 else if([self.key isEqualToString:@"Note"]){
        [self.notesFormViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.notesFormViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.notesFormViewController removeFromParentViewController];                   
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
}

-(void)enableButtons{
    self.detailButton.enabled =YES;
    self.notesButton.enabled =YES;
    self.datesButton.enabled =YES;    
}

- (IBAction)Detail {
    if(self.actionDetailViewController == nil) {
        self.actionDetailViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"ActionDetail"];
        self.actionDetailViewController.project=self.project;
        self.actionDetailViewController.detailAction = self.containerAction;
        self.actionDetailViewController.item = self.item;
        self.actionDetailViewController.delegate = self;
    }
     [self enableButtons];
     [self SwapOldControllerForNew:self.actionDetailViewController and:@"Detail"];
     self.detailButton.enabled = NO;
}

- (IBAction)Dates {
    if(self.actionDatesViewController == nil) {
        self.actionDatesViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"ActionDates"];
        self.actionDatesViewController.action = self.containerAction;
        self.actionDatesViewController.delegate = self;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.actionDatesViewController and:@"Date"];
    self.datesButton.enabled = NO;
}

- (IBAction)Notes {
    if(self.notesFormViewController == nil) {
        self.notesFormViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"NotesTable"];
        self.notesFormViewController.item = self.containerAction;
        self.notesFormViewController.delegate = self;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.notesFormViewController and:@"Note"];
    self.notesButton.enabled = NO;
}

#pragma mark - view controller methods

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)refreshFromBackground{
    [self createReviewBadge];
    [self createDateRagBadge];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self createCompletedBadge];
    [self createNotesBadge];
    [self createReviewBadge];
    [self createDateRagBadge];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    if(!self.badgeColour){
        self.badgeColour = [[PJColour alloc]init];
    }
    if (self.containerAction == nil) {
        self.containerAction = [self.helper newAction:self.item];
    }
    if(self.containerAction.inIssue){
        
       [self.parentButton setTitle:@"In Issue > " forState:UIControlStateNormal];
    }
    else if(self.containerAction.inRisk){
        
       [self.parentButton setTitle:@"In Risk > " forState:UIControlStateNormal];
    }
    else {
     [self.parentButton setTitle:@"In Project > " forState:UIControlStateNormal];   
    }
   // UIEdgeInsets buttonInsets = UIEdgeInsetsMake(0, 5, 0, 20);
  //  UIImage *backImage = [UIImage imageNamed:@"PJAction"];
  //  backImage = [backImage resizableImageWithCapInsets:buttonInsets];
   // [ self.parentButton setBackgroundImage:backImage forState:UIControlStateNormal];
    self.actionDetailViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"ActionDetail"];
    self.actionDetailViewController.project=self.project;
    self.actionDetailViewController.detailAction = self.containerAction;
    self.actionDetailViewController.item = self.item;
    self.actionDetailViewController.delegate = self;
    [self addChildViewController:self.actionDetailViewController];
    [self.container addSubview:self.actionDetailViewController.view];
    [self.actionDetailViewController didMoveToParentViewController:self];
    self.key = @"Detail";
    [self enableButtons];
    self.detailButton.enabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - toolBar actions
- (IBAction)Send {
     [self.view endEditing:YES];
    [self dimSuper];
    [self createSendSheet];
}

- (IBAction)Delete {
     [self.view endEditing:YES];
    [self dimSuper];
     if(!self.deleteAlert){
    self.deleteAlert = [[UIAlertView alloc] initWithTitle:@"Delete Action"
                                                          message:@"Deleting this action will delete all of its  notes"
                                                         delegate:self                                          cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Delete",nil];
     }
    [self.deleteAlert show];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self UndimSuper];
    if(buttonIndex == 1){
        [self.helper deleteObject:self.containerAction];
        [self.helper saveContext];
        [self.delegate ActionFormContainerDidFinish:self];
        
        
    }}

- (IBAction)Done {    
    [self.delegate ActionFormContainerDidFinish:self];
}

@end
