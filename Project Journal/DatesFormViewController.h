//
//  DatesFormViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 23/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"

@protocol DatesFormViewControllerDelegate <NSObject>

-(void) upDateRagBadge;
-(void) upDateReviewBadge;
-(void) dimSuper;
-(void) UndimSuper;
@end

@interface DatesFormViewController: UIViewController
@property (weak, nonatomic) id <DatesFormViewControllerDelegate> delegate;
@property (strong , nonatomic) Project *project;
- (void)dismissPopovers;
@end
