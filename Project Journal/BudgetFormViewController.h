//
//  BudgetFormViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 23/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"

@protocol BudgetFormViewControllerDelegate <NSObject>

-(void)updateBudgetBadge;
-(void) dimSuper;
-(void) UndimSuper;
@end

@interface BudgetFormViewController : UIViewController
@property (weak, nonatomic) id <BudgetFormViewControllerDelegate> delegate;
@property (strong , nonatomic)Project *project;
- (void)dismissPopovers;
@end
