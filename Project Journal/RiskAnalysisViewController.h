//
//  RiskAnalysisViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 27/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"

@protocol RiskAnalysisViewControllerDelegate <NSObject>

-(void) upDateRagBadge;
-(void) dimSuper;
-(void) UndimSuper;

@end
@interface RiskAnalysisViewController : UIViewController
@property (weak, nonatomic) id <RiskAnalysisViewControllerDelegate> delegate;
@property(strong, nonatomic)Risk *risk;
- (void)dismissPopovers;
@end
