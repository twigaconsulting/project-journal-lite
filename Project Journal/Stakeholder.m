//
//  Stakeholder.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Stakeholder.h"
#import "Media.h"
#import "Note.h"
#import "Project.h"


@implementation Stakeholder

@dynamic creationDate;
@dynamic deleted;
@dynamic influence;
@dynamic lastModifiedDate;
@dynamic priority;
@dynamic role;
@dynamic stakeholderID;
@dynamic type;
@dynamic uuid;
@dynamic viewpoint;
@dynamic person;
@dynamic inProject;
@dynamic stakeholderMedia;
@dynamic stakeholderNotes;

@end
