//
//  AgendaItem.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 20/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "AgendaItem.h"
#import "Meeting.h"


@implementation AgendaItem

@dynamic position;
@dynamic title;
@dynamic inMeeting;

@end
