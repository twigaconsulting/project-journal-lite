//
//  RiskCollectionController.m
//  Project Journal
//
//  Created by Peter Pomlett on 01/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.

#import <QuartzCore/QuartzCore.h>
#import "RiskCollectionController.h"
#import "ItemCollectionCell.h"
#import "PJColour.h"
#import "RiskFormContainer.h"
#import "ActionFormContainer.h"

@interface RiskCollectionController ()<NSFetchedResultsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, RiskFormContainerDelegate, ActionFormContainerDelegate, UIActionSheetDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSFetchedResultsController *riskResultsController;
@property (weak, nonatomic) IBOutlet UICollectionView *riskCollection;
@property (strong, nonatomic) Risk *selectedRisk;
@property (strong, nonatomic)  PJColour *cellColour;
@property (nonatomic, strong) DataHelper *helper;
@property (strong, nonatomic) UIActionSheet *itemSheet;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *addItem;
@property (strong,nonatomic) id actionParent;
@property (strong, nonatomic) Action *selectedAction;
- (NSDateFormatter *)formatterDate;
@property float scaleRisk;
@property float scaleRisk2;
@property (weak, nonatomic) IBOutlet UIStepper *riskStepper;
@end

@implementation RiskCollectionController

- (IBAction)riskSize:(UIStepper *)sender {
    int cellSize = sender.value;
    if(cellSize == 3){
        self.scaleRisk =1;
        self.scaleRisk2 =1;
        [self.riskCollection.collectionViewLayout invalidateLayout];
    }
    
    if(cellSize == 2){
        self.scaleRisk =0.228;
        self.scaleRisk2 =1;
        [self.riskCollection.collectionViewLayout invalidateLayout];
        
    }
    if(cellSize == 1){
        self.scaleRisk =0.228;
        self.scaleRisk2 =0.118;
        [self.riskCollection.collectionViewLayout invalidateLayout];
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:self.scaleRisk forKey:@"scaleRiskView"];
    [defaults setFloat:self.scaleRisk2  forKey:@"scaleRisk2View"];
    [defaults setInteger:self.riskStepper.value forKey:@"riskStepperView"];
    [defaults synchronize];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
        return CGSizeMake(240 *self.scaleRisk2, 110*self.scaleRisk);
}
-(void)getCellSize{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.scaleRisk =  [defaults floatForKey:@"scaleRiskView"];
    self.scaleRisk2 = [defaults floatForKey:@"scaleRisk2View"];
    self.riskStepper.value = [defaults integerForKey:@"riskStepperView"];

}




- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

-(void)getActionParent:(id)parent{
    self.selectedRisk = parent;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"risk" sender: self] ;
    }];
}

- (void)ActionFormContainerDidFinish:(ActionFormContainer *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)getNewAction:(id)itenAction{
    self.actionParent = itenAction;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"parentNewAction" sender: self] ;
    }];
}

-(void)getTheActionForm:(Action *)itemAction{
    
    
    self.selectedAction = itemAction;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"actions" sender: self] ;
    }];
}

- (IBAction)newRisk:(id)sender {
    if (!self.itemSheet){
        self.itemSheet = [[UIActionSheet alloc] initWithTitle:@"In Project"
                                                     delegate:self
                                            cancelButtonTitle:nil
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:@"New Risk",nil];
    }
    [self.itemSheet showFromBarButtonItem:sender animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(self.itemSheet == actionSheet){
        if(buttonIndex == 0){
            [self performSegueWithIdentifier:@"NewRisk" sender:self];
        }
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.riskCollection reloadData];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo;
    sectionInfo = [self.riskResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    ItemCollectionCell *cell ;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    Risk *risk = [self.riskResultsController objectAtIndexPath:indexPath];
    if([risk isForReview]){
        [ cell.reviewLabel setHidden:NO];
    }
    else{
        [ cell.reviewLabel setHidden:YES];
    }
    if([[risk fetchDateRAGStatus] isEqualToString:@"Amber"] ){
        cell.dateBadge.hidden = NO;
        cell.dateBadge.backgroundColor = self.cellColour.amber;
    }
    else if([[risk fetchDateRAGStatus] isEqualToString:@"Red"] ){
        cell.dateBadge.hidden = NO;
        cell.dateBadge.backgroundColor = self.cellColour.red;
    }
    else{
        cell.dateBadge.hidden = YES;
    }
    if([[risk fetchAnalysisRAGStatus] isEqualToString:@"Amber"] ){
        cell.analysisBadge.hidden = NO;
        cell.analysisBadge.backgroundColor = self.cellColour.amber;
    }
    else if([[risk fetchAnalysisRAGStatus] isEqualToString:@"Red"] ){
        cell.analysisBadge.hidden = NO;
        cell.analysisBadge.backgroundColor = self.cellColour.red;
    }
    else{
        cell.analysisBadge.hidden = YES;
    }
    if ([[risk fetchRAGStatus] isEqualToString:@"Red"]){
        cell.contentView.layer.backgroundColor  = self.cellColour.red.CGColor;
    }
    if ([[risk fetchRAGStatus] isEqualToString:@"Green"]){
        cell.contentView.layer.backgroundColor  = self.cellColour.green.CGColor;
    }
    if ([[risk fetchRAGStatus] isEqualToString:@"Amber"]){
        cell.contentView.layer.backgroundColor  = self.cellColour.amber.CGColor;
    }
    if ([[risk fetchRAGStatus] isEqualToString:@"Blue"]){
        cell.contentView.layer.backgroundColor  = self.cellColour.blue.CGColor;
    }
    cell.titleLabel.text = [risk title];
    cell.ownerLabel.text = [risk riskOwner];
    cell.priorityLabel.text = [risk projectPhase];
    if([risk plannedCompletionDate]){
        cell.dateLabel.text= [self.formatterDate stringFromDate:[risk plannedCompletionDate]];
    }
    else{
        cell.dateLabel.text =@"Set completion date!";
    }
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(self.riskCollection == collectionView){
        self.selectedRisk = [self.riskResultsController objectAtIndexPath:indexPath];
    }
    return YES;
}

-(void)RiskFormContainerDidFinish:(RiskFormContainer *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"risk"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        [[segue destinationViewController] setContainerRisk:self.selectedRisk];
    }
    if ([[segue identifier] isEqualToString:@"NewRisk"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        self.selectedRisk = nil;
        [[segue destinationViewController] setContainerRisk:self.selectedRisk];
    }
    if ([[segue identifier] isEqualToString:@"parentNewAction"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.actionParent];
        self.selectedAction = nil;
        [[segue destinationViewController] setContainerAction:self.selectedAction];
    }
    if ([[segue identifier] isEqualToString:@"actions"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        [[segue destinationViewController] setContainerAction:self.selectedAction];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    if (self.itemSheet != nil) {
        [self.itemSheet dismissWithClickedButtonIndex:self.itemSheet.cancelButtonIndex animated:NO];
        self.itemSheet = nil;
    }
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    if(self.cellColour == nil){
        self.cellColour = [[PJColour alloc]init];
    }
    if([self.project.status isEqualToString:@"Completed"]){
        self.navigationItem.rightBarButtonItem = nil;
    }
    [self getCellSize];
    [self.riskStepper setDecrementImage:[UIImage imageNamed:@"stepperSmall"] forState:UIControlStateNormal];
    [self.riskStepper setIncrementImage:[UIImage imageNamed:@"stepperLarge"] forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.riskResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Risk"
                                                      sortingBy:@"title" ascendingBy:YES includeDeleted:NO];
    self.riskResultsController.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
}

-(void)refreshFromBackground{
    [self.riskCollection reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
