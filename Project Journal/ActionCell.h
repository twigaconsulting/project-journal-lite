//
//  ActionCell.h
//  Project Journal
//
//  Created by Peter Pomlett on 30/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//text

#import <UIKit/UIKit.h>

@interface ActionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ownerLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;



@end
