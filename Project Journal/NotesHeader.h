//
//  NotesHeader.h
//  Project Journal
//
//  Created by Peter Pomlett on 25/05/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotesHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UIImageView *headerImageview;

@end
