//
//  AgendaItem.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 20/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Meeting;

@interface AgendaItem : NSManagedObject

@property (nonatomic, retain) NSNumber * position;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Meeting *inMeeting;

@end
