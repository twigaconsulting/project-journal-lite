//
//  ActionDetailViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 05/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
//@class ActionDetailViewController;

@protocol ActionDetailViewControllerDelegate <NSObject>

- (void) upDateBadge;
-(void) dimSuper;
-(void) UndimSuper;
@end

@interface ActionDetailViewController : UIViewController
@property (weak, nonatomic) id <ActionDetailViewControllerDelegate> delegate;
@property (strong,nonatomic) Project *project;
@property (strong,nonatomic) Action *detailAction;
@property (strong,nonatomic) id item;
- (void)dismissPopovers;
@end
