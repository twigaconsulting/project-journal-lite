//
//  MeetingTimeViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 24/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MeetingTimeViewController;

@protocol MeetingTimeViewControllerDelegate <NSObject>

-(void)meetingTimeViewControllerDidFinish:( MeetingTimeViewController *)controller;
-(void)pickedStartTime:(NSDate *)sTime endTime:(NSDate *)eTime ;

@end

@interface MeetingTimeViewController : UIViewController
@property (weak, nonatomic) id <MeetingTimeViewControllerDelegate> delegate;
@property (strong, nonatomic)NSDate *startDate;
@property (strong, nonatomic)NSDate *endDate;

@end
