//
//  ActionDetailViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 05/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "HelpViewController.h"

#import "ActionDetailViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "NewPersonViewController.h"

@interface ActionDetailViewController ()<UITextFieldDelegate, UITextViewDelegate, UIActionSheetDelegate, NewPersonViewcontrollerDelegate, UIAlertViewDelegate, ABPeoplePickerNavigationControllerDelegate,ABNewPersonViewControllerDelegate, UIPopoverControllerDelegate>
@property (strong, nonatomic) UIStoryboardPopoverSegue *popSegue;
@property (strong, nonatomic) DataHelper *helper;
@property (weak, nonatomic) IBOutlet UITextField *titleTextfield;
@property (weak, nonatomic) IBOutlet UILabel *ownerLabel;
@property (weak, nonatomic) IBOutlet UILabel *inProjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *ActionIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *raisedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *catagoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *priorityLabel;
@property (weak, nonatomic) IBOutlet UILabel *raisedfromLabel;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
@property (weak, nonatomic) IBOutlet UIButton *catagoryButton;
@property (weak, nonatomic) IBOutlet UIButton *priorityButton;
@property (weak, nonatomic) IBOutlet UITextView *summaryTextView;
@property (weak, nonatomic) IBOutlet UITextView *completionSummaryTextView;
@property (weak, nonatomic) IBOutlet UILabel *completionHeader;
@property (strong, nonatomic) UIActionSheet *statusSheet;
@property (strong, nonatomic) UIActionSheet *prioritySheet;
@property (strong, nonatomic) UIActionSheet *catagorySheet;
@property (strong, nonatomic) NSArray *statusList;
@property (strong, nonatomic) NSArray *priorityList;
@property (strong, nonatomic) NSArray *catagoryList;
@property (strong, nonatomic) NSString *peopleKey;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *projectImageView;
@property (weak, nonatomic) IBOutlet UIImageView *ownerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *raisedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *fromImageView;
@property (weak, nonatomic) IBOutlet UIImageView *priorityImageView;
@property (weak, nonatomic) IBOutlet UIImageView *catagoryImageView;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet UIImageView *summaryTextImageView;
@property (weak, nonatomic) IBOutlet UIImageView *comSummaryImageView;
@property (strong,nonatomic) UIPopoverController *addressBookPopover;
@property (strong,nonatomic) UIPopoverController *aNewPersonPopover;
@property (strong,nonatomic) UIPopoverController *UnknownPersonPopover;
@property (strong,nonatomic) UIPopoverController *addNamePopover;
@property (nonatomic, readwrite) ABRecordRef displayedPerson;
@property (nonatomic,strong) NSString *enterFirstName;
@property (nonatomic,strong) NSString *enterLastName;
@property (strong, nonatomic) UIAlertView *unnamedAlert;
@property (strong, nonatomic) UIAlertView *parentClosed;
@property (strong, nonatomic) UIButton *buttonPressed;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *helpButtons;
@property (strong, nonatomic) HelpViewController *helpView;
@property (strong, nonatomic) UIPopoverController *helpPopover;
@property (nonatomic, strong) NSDictionary *helpList;
@property (weak, nonatomic) IBOutlet UIImageView *activeTitleFrame;
@property (weak, nonatomic) IBOutlet UIImageView *activeSummaryFrame;
@property (weak, nonatomic) IBOutlet UIImageView *activeCompFrame;

@end

@implementation ActionDetailViewController


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
    
    
}


- (IBAction)helpTitle:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.title"] from:sender];
}

- (IBAction)helpProject:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.project"] from:sender];
}

- (IBAction)helpSummary:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.summary"] from:sender];
}

- (IBAction)helpCompletion:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.completionSummary"] from:sender];
}

- (IBAction)helpOwner:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.owner"] from:sender];
}
- (IBAction)helpRaisedBy:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.raisedBy"] from:sender];
}
- (IBAction)helpInItem:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.raisedIn"] from:sender];
}
- (IBAction)helpPriority:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.priority"] from:sender];
}
- (IBAction)helpPhase:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.category"] from:sender];
}

- (IBAction)helpStatus:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"action.status"] from:sender];
}

-(void)loadTheHelpPlist{
    if(self.helpList == nil){
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"ActionHelp" ofType:@"plist"];
        self.helpList = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    }
}

- (void)checkHelpButtons {
    if(![self.helper showHelp]){
        for (UIButton *button in self.helpButtons) {
           button.hidden = YES;
        }
    }
}

- (void)showHelp:(NSString*)helpString from:(UIButton *)helpButton {
    [self.delegate dimSuper];
    if(self.helpView == nil) {
        self.helpView = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    }
     UITextView *_textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 250, 10)];
   _textView.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
    _textView.textColor = [UIColor lightGrayColor];
   _textView.scrollEnabled = NO;
    _textView.userInteractionEnabled=NO;
    _textView.text =  helpString;
    CGRect frame = _textView.frame;
    frame.size.height =  [self getTextHeight:_textView];
    _textView.frame = frame;
     self.helpView.view.frame = frame;
    [self.helpView.view addSubview:_textView];
   self.helpPopover = [[UIPopoverController alloc] initWithContentViewController:self.helpView];
    self.helpPopover.popoverContentSize =_textView.frame.size;
    [self.helpPopover presentPopoverFromRect:helpButton.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionDown animated:YES];
    self.helpPopover.delegate = self;
}

- (CGFloat)getTextHeight:(UITextView *)textView
{
    NSLayoutManager *lm = textView.layoutManager;
    CGRect boundingRect = [lm usedRectForTextContainer:lm.textContainers[0]];
    CGFloat height = boundingRect.size.height + (textView.textContainerInset.top + textView.textContainerInset.bottom);
    return height;
}




- (void)dismissPopovers{
    [self.delegate UndimSuper];
    
    if (self.parentClosed.isVisible) {
        [self.parentClosed dismissWithClickedButtonIndex:0 animated:NO];
        self.parentClosed = nil;
    }
    
    if (self.unnamedAlert.isVisible) {
        [self.unnamedAlert dismissWithClickedButtonIndex:0 animated:NO];
        self.unnamedAlert = nil;
    }
    if (self.statusSheet.isVisible) {
        [self.statusSheet dismissWithClickedButtonIndex:self.statusSheet.cancelButtonIndex animated:NO];
        self.statusSheet = nil;
    }
    if (self.prioritySheet.isVisible) {
        [self.prioritySheet dismissWithClickedButtonIndex:self.prioritySheet.cancelButtonIndex animated:NO];
        self.prioritySheet = nil;
    }
    if (self.catagorySheet.isVisible) {
        [self.catagorySheet dismissWithClickedButtonIndex:self.prioritySheet.cancelButtonIndex animated:NO];
        self.catagorySheet = nil;
    }
    if ([self.addressBookPopover isPopoverVisible])
    {
        [self.addressBookPopover dismissPopoverAnimated:NO];
    }
    if ([self.aNewPersonPopover isPopoverVisible])
    {
        [self.aNewPersonPopover dismissPopoverAnimated:NO];
    }
    if ([self.addNamePopover isPopoverVisible])
    {
        [self.addNamePopover dismissPopoverAnimated:NO];
    }
    if ([self.helpPopover isPopoverVisible])
    {
        [self.helpPopover dismissPopoverAnimated:NO];
    }
}

-(void)addBackgroundImageViews{
    UIEdgeInsets tabInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage *textBack = [UIImage imageNamed:@"TextFrame2"];
    textBack = [textBack resizableImageWithCapInsets:tabInsets];
    UIImage* summary = [UIImage imageNamed:@"sumFrame"];
    summary = [summary imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ];
    UIImage* title = [UIImage imageNamed:@"titleframe"];
    title = [title imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ];
    self.activeTitleFrame.image = title;
    self.activeSummaryFrame.image = summary;
    self.activeCompFrame.image = summary;
    self.titleImageView.alpha = 1.0;
    self.titleImageView.image = textBack;
   // self.projectImageView.alpha = 0.5;
    self.projectImageView.image = textBack;
    self.ownerImageView.alpha = 1.0;
    self.ownerImageView.image = textBack;
    self.raisedImageView.alpha = 1.0;
    self.raisedImageView.image = textBack;
   // self.fromImageView.alpha = 0.5;
    self.fromImageView.image = textBack;
    self.priorityImageView.alpha = 1.0;
    self.priorityImageView.image = textBack;
    self.catagoryImageView.alpha = 1.0;
    self.catagoryImageView.image = textBack;
    self.statusImageView.alpha = 1.0;
    self.statusImageView.image =textBack;
    self.summaryTextImageView.alpha = 1.0;
    self.summaryTextImageView.image = textBack;
    self.comSummaryImageView.alpha = 1.0;
    self.comSummaryImageView.image = textBack;
    
}

//////////////people picker code start/////////////

- (IBAction)owner:(UIButton *)sender {
     [self.view endEditing:YES];
    [self.delegate dimSuper];
    self.buttonPressed = sender;
    self.peopleKey = @"owner";
    [self pickPerson];
}

- (IBAction)ownerNew:(UIButton *)sender {
     [self.view endEditing:YES];
    [self.delegate dimSuper];
    self.buttonPressed = sender;
    self.peopleKey = @"owner";
    [self aNewPerson];
}

- (IBAction)RaisedBy:(UIButton *)sender {
     [self.view endEditing:YES];
    [self.delegate dimSuper];
    self.buttonPressed = sender;
    self.peopleKey = @"raised";
    [self pickPerson];
}

- (IBAction)raisedByNew:(UIButton *)sender {
     [self.view endEditing:YES];
    [self.delegate dimSuper];
    self.buttonPressed = sender;
    self.peopleKey = @"raised";
    [self aNewPerson];
}

-(void) dismissNewPersonViewcontroller:(NewPersonViewController *)controller{
    [self.delegate UndimSuper];
    [self.addNamePopover dismissPopoverAnimated:YES];
}
-(void) personFirstName:(NSString *)first secondName:(NSString *)second andController:(NewPersonViewController *)controller{
    self.enterFirstName = first;
    self.enterLastName = second;
    [self.delegate UndimSuper];
    [self.addNamePopover dismissPopoverAnimated:YES];
    [self addPersonName];
}

-(void)addPersonName{
    CFErrorRef err = nil;
    ABAddressBookRef adbk = ABAddressBookCreateWithOptions(nil, &err);
    if (nil == adbk) {
      //  NSLog(@"error: %@", err);
       //FUTURE ERROR HANDLING
        return;
    }
    ABRecordRef person = self.displayedPerson;
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef)self.enterFirstName, nil);
    ABRecordSetValue(person, kABPersonLastNameProperty,(__bridge CFStringRef)self.enterLastName, nil);
    CFErrorRef error = nil;
    ABAddressBookAddRecord (adbk,person,&error);
    ABAddressBookSave(adbk, &error);
    //stop null from from showing on label
    if(self.enterFirstName == Nil){
        self.enterFirstName = @"";
    }
    if(self.enterLastName == Nil){
        self.enterLastName = @"";
    }
    if([self.peopleKey isEqualToString:@"owner"]){
        self.detailAction.actionOwner = [NSString stringWithFormat:@"%@ %@", self.enterFirstName ,self.enterLastName];
        
        if([self.detailAction.status isEqualToString:@"Unassigned"] || [self.detailAction.status isEqualToString:@"New"] ){
            self.detailAction.status = @"Assigned";}
        self.detailAction.assignedDate = [NSDate date];
        self.detailAction.lastModifiedDate = [NSDate date];
        [self.helper saveContext];
        self.ownerLabel.text = self.detailAction.actionOwner;
        self.statusLabel.text = self.detailAction.status;
    }
    if([self.peopleKey isEqualToString:@"raised"]){
        self.detailAction.actionRaiser = [NSString stringWithFormat:@"%@ %@", self.enterFirstName ,self.enterLastName];
        self.detailAction.lastModifiedDate = [NSDate date];
        [self.helper saveContext];
        self.raisedByLabel.text = self.detailAction.actionRaiser;
    }
    CFRelease(adbk);
}

#pragma mark - Unnamed person alert delegate
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex ==0){
        [self.addressBookPopover dismissPopoverAnimated:YES];
         [self.delegate UndimSuper];
    }
    else{
        [self.addressBookPopover dismissPopoverAnimated:YES];
        
        NewPersonViewController *aNewPersonViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"NewPerson"];
        aNewPersonViewController.delegate = self;
        self.addNamePopover = [[UIPopoverController alloc] initWithContentViewController:aNewPersonViewController];
        [self.addNamePopover presentPopoverFromRect:self.buttonPressed.frame
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
         self.addNamePopover.delegate = self;
    }
    
}

- (void)pickPerson {
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    self.addressBookPopover = [[UIPopoverController alloc] initWithContentViewController:picker];
    [self.addressBookPopover presentPopoverFromRect:self.buttonPressed.frame
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                           animated:YES];
      self.addressBookPopover.delegate = self;
}

- (void)aNewPerson {
    ABNewPersonViewController *picker = [[ABNewPersonViewController alloc] init];
	picker.newPersonViewDelegate = self;
	UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:picker];
    self.aNewPersonPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
    [self.aNewPersonPopover presentPopoverFromRect: self.buttonPressed.frame
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
    self.aNewPersonPopover.delegate =self;

}

#pragma mark ABPeoplePickerNavigationControllerDelegate methods
// Displays the information of a selected person
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    self.displayedPerson = person;
    NSString*  firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    if (firstName == nil && lastName == nil) {
        if (!self.unnamedAlert){
       
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        }
        [self.unnamedAlert show];
    }
    else{
        //stop null from from showing on label
        if(firstName == Nil){
            firstName = @"";
        }
        if(lastName == Nil){
            lastName = @"";
        }
        if([self.peopleKey isEqualToString:@"owner"]){
            self.detailAction.actionOwner = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
            
            if([self.detailAction.status isEqualToString:@"Unassigned"]||[self.detailAction.status isEqualToString:@"New"]){
                self.detailAction.status = @"Assigned";}
            self.detailAction.assignedDate = [NSDate date];
            self.detailAction.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
            self.ownerLabel.text = self.detailAction.actionOwner;
            self.statusLabel.text = self.detailAction.status;
        }
        if([self.peopleKey isEqualToString:@"raised"]){
            self.detailAction.actionRaiser = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
            self.detailAction.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
            self.raisedByLabel.text = self.detailAction.actionRaiser;
        }
        if ([self.addressBookPopover isPopoverVisible])
        {
            [self.addressBookPopover dismissPopoverAnimated:YES];
             [self.delegate UndimSuper];
        }}
	return NO;
}

// Does not allow users to perform default actions such as dialing a phone number, when they select a person property.
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
								property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
	return NO;
}

// Dismisses the people picker and shows the application when users tap Cancel.
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{
	[self.addressBookPopover dismissPopoverAnimated:YES];
    [self.delegate UndimSuper];
}

#pragma mark ABNewPersonViewControllerDelegate methods
// Dismisses the new-person view controller.
- (void)newPersonViewController:(ABNewPersonViewController *)newPersonViewController didCompleteWithNewPerson:(ABRecordRef)person
{[self.delegate UndimSuper];
    self.displayedPerson = person;
    NSString* firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    if (firstName == nil && lastName == nil) {
        if(self.displayedPerson==nil){
            [self.aNewPersonPopover dismissPopoverAnimated:YES];
            return;
        }
        if(!self.unnamedAlert){
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        }
        [self.unnamedAlert show];
        [self.delegate dimSuper];
    }
    else{
        //stop null from from showing on label
        if(firstName == Nil){
            firstName = @"";
        }
        if(lastName == Nil){
            lastName = @"";
        }
        if([self.peopleKey isEqualToString:@"owner"]){
            self.detailAction.actionOwner = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
            if([self.detailAction.status isEqualToString:@"Unassigned"]||[self.detailAction.status isEqualToString:@"New"]){
                self.detailAction.status = @"Assigned";}
            self.detailAction.assignedDate = [NSDate date];
            self.detailAction.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
            self.ownerLabel.text = self.detailAction.actionOwner;
            self.statusLabel.text = self.detailAction.status;
        }
        if([self.peopleKey isEqualToString:@"raised"]){
            self.detailAction.actionRaiser = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
            self.detailAction.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
            self.raisedByLabel.text = self.detailAction.actionRaiser;
        }
    }
    
	[self.aNewPersonPopover dismissPopoverAnimated:YES];
     //[self.delegate dimSuper];
}
//////////////people picker code end///////////////

-(void)checkCompletionTextView{
    if  ([self.detailAction.status isEqualToString:@"Completed"]){
        self.completionSummaryTextView.userInteractionEnabled = YES;
        self.completionHeader.alpha = 1.0;
        self.completionSummaryTextView.alpha = 1.0;
        self.activeCompFrame.tintColor = self.view.tintColor;
    }
    else{
        self.completionSummaryTextView.userInteractionEnabled = NO;
        self.completionHeader.alpha = 0.5;
        self.completionSummaryTextView.alpha = 0.5;
       self.activeCompFrame.tintColor = [UIColor lightGrayColor];
    }
}

-(void)enableCompletionTextView{
    if  ([self.detailAction.status isEqualToString:@"Completed"]){
          if  (self.detailAction.completionDate == nil){
              self.detailAction.completionDate = [NSDate date];}
        self.completionSummaryTextView.userInteractionEnabled = YES;
        self.completionHeader.alpha = 1.0;
        self.completionSummaryTextView.alpha = 1.0;
         self.activeCompFrame.tintColor = self.view.tintColor;
    }
    else{
        self.completionSummaryTextView.userInteractionEnabled = NO;
        self.completionHeader.alpha = 0.5;
        self.completionSummaryTextView.alpha = 0.5;
      self.activeCompFrame.tintColor = [UIColor lightGrayColor];
        self.detailAction.completionDate = nil;
    }
    [self.helper saveContext];
}

-(void)createStatusSheet{
    self.statusList =[self.helper fetchStatusOptions: self.detailAction];
    if([self.statusList count] == 0){
        if(!self.parentClosed){
        self.parentClosed = [[UIAlertView alloc] initWithTitle:@"Parent Status Completed"
                                                              message:@"Unable to change action status as its parent status is Completed"
                                                             delegate:self                                          cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
        }
        [self.parentClosed show];
    }
    else{
    
    self.statusSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    for  (int i = 0; i < [self.statusList count];  i++) {
        [self.statusSheet addButtonWithTitle:[self.statusList objectAtIndex:i]];
    }
    [self.statusSheet showFromRect:self.statusButton.frame inView:self.view animated:YES];
    }
}

-(void)createPrioritySheet{
    self.priorityList=self.helper.fetchPriorityOptions ;
    self.prioritySheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:nil
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:nil];
    
    for  (int i = 0; i < [self.priorityList count];  i++) {
        [self.prioritySheet addButtonWithTitle:[self.priorityList objectAtIndex:i]];
    }
    [self.prioritySheet showFromRect:self.priorityButton.frame inView:self.view animated:YES];
}

-(void)createCatagorySheet{
    self.catagoryList=self.helper.fetchCategoryOptions ;
    self.catagorySheet = [[UIActionSheet alloc] initWithTitle:nil
                                                     delegate:self
                                            cancelButtonTitle:nil
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:nil];
    
    for  (int i = 0; i < [self.catagoryList count];  i++) {
        [self.catagorySheet addButtonWithTitle:[self.catagoryList objectAtIndex:i]];
    }
    [self.catagorySheet showFromRect:self.catagoryButton.frame inView:self.view animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{[self.delegate UndimSuper];
    if (self.statusSheet == actionSheet){
    if (buttonIndex < [self.statusList count]){
       self.detailAction.status = [self.statusList objectAtIndex:buttonIndex];
        
       if ([self.detailAction.status isEqualToString:self.statusLabel.text] == NO) {
           [self enableCompletionTextView];
          self.statusLabel.text = self.detailAction.status;
           
           if([self.detailAction.status isEqualToString:@"Unassigned"]){
               self.detailAction.actionOwner = nil;
               self.detailAction.assignedDate = nil;
               self.ownerLabel.text = self.detailAction.actionOwner;
           }
          self.detailAction.lastModifiedDate = [NSDate date];
          [self.helper saveContext];
          }
      }
    [self.delegate upDateBadge];
  }
    if (self.prioritySheet == actionSheet){
        if (buttonIndex < [self.priorityList count]){
            self.detailAction.priority = [self.priorityList objectAtIndex:buttonIndex];
            if ([self.detailAction.priority isEqualToString:self.priorityLabel.text] == NO) {
                self.priorityLabel.text = self.detailAction.priority;
                 self.detailAction.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
        }
    }
    if (self.catagorySheet == actionSheet){
        if (buttonIndex < [self.catagoryList count]){
            self.detailAction.category = [self.catagoryList objectAtIndex:buttonIndex];
            if ([self.detailAction.category isEqualToString:self.catagoryLabel.text] == NO) {
                self.catagoryLabel.text = self.detailAction.category;
                 self.detailAction.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
  
      if (self.titleTextfield == textField){
        if ([self.detailAction.title isEqualToString:self.titleTextfield.text]==NO){
            self.detailAction.title = self.titleTextfield.text;
            self.detailAction.lastModifiedDate = [NSDate date];
            
            [self.helper saveContext];
            }
          if ([self.detailAction.title isEqualToString:@""]){
              self.detailAction.title = @"Untitled Action";
              self.titleTextfield.text = self.detailAction.title;
               [self.helper saveContext];
          }
        }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if (self.summaryTextView == textView){
        if ([self.detailAction.summary isEqualToString:self.summaryTextView.text]==NO){
            self.detailAction.summary = self.summaryTextView.text;
            self.detailAction.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
    }
    if (self.completionSummaryTextView == textView){
        if ([self.detailAction.completionSummary isEqualToString:self.completionSummaryTextView.text]==NO){
            self.detailAction.completionSummary = self.completionSummaryTextView.text;
            self.detailAction.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
    }
}

- (IBAction)PriorityButton {
     [self.view endEditing:YES];
    [self.delegate dimSuper];
    [self createPrioritySheet];
}

- (IBAction)CatagoryButton {
     [self.view endEditing:YES];
    [self.delegate dimSuper];
    [self createCatagorySheet];
}

- (IBAction)StatusButton {
     [self.view endEditing:YES];
    [self.delegate dimSuper];
    [self createStatusSheet];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.titleTextfield.text = self.detailAction.title;
    if([self.titleTextfield.text isEqualToString:@""]){
        [self.titleTextfield becomeFirstResponder];
    }
    if(self.detailAction.inIssue){
         self.raisedfromLabel.text = [NSString stringWithFormat:@"Issue: %@",self.detailAction.inIssue.title];
    }
   else if(self.detailAction.inRisk){
         self.raisedfromLabel.text = [NSString stringWithFormat:@"Risk: %@",self.detailAction.inRisk.title];
    }
  else if(self.detailAction.inMeeting){
        self.raisedfromLabel.text = [NSString stringWithFormat:@"Meeting: %@",self.detailAction.inMeeting.title];
    }
   else {
        self.raisedfromLabel.text = @"Project";
    }
    self.summaryTextView.text = self.detailAction.summary;
    self.completionSummaryTextView.text =self.detailAction.completionSummary;
    self.statusLabel.text = self.detailAction.status;
    self.ActionIDLabel.text = self.detailAction.actionID;
    self.priorityLabel.text = self.detailAction.priority;
    self.catagoryLabel.text = self.detailAction.category;
    self.inProjectLabel.text = self.detailAction.inProject.title;
    self.ownerLabel.text = self.detailAction.actionOwner;
    self.raisedByLabel.text = self.detailAction.actionRaiser;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    self.titleTextfield.delegate = self;
    self.summaryTextView.delegate = self;
    self.completionSummaryTextView.delegate = self;
    [self addBackgroundImageViews];
    [self checkCompletionTextView];
    [self checkHelpButtons];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
