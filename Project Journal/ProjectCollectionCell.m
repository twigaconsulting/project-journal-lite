//
//  ProjectCollectionCell.m
//  Project Journal
//
//  Created by Peter Pomlett on 20/05/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "ProjectCollectionCell.h"
#import <QuartzCore/QuartzCore.h>
@implementation ProjectCollectionCell


-(void) awakeFromNib{
    [super awakeFromNib];
    self.dateBadge.layer.borderWidth = 1.0;
    self.dateBadge.layer.borderColor = [UIColor whiteColor].CGColor;
    self.reviewLabel.layer.borderWidth = 1.0;
    self.reviewLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    self.budgetBadge.layer.borderWidth = 1.0;
    self.budgetBadge.layer.borderColor = [UIColor whiteColor].CGColor;
    
    CALayer *layer = [self layer];
    [layer setCornerRadius:6];
    
    
    [layer setRasterizationScale:[[UIScreen mainScreen] scale]];
    [layer setShouldRasterize:YES];
    
}

@end
