//
//  Category.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Category : NSManagedObject

@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * uuid;

@end
