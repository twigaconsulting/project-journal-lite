//
//  NoteCollectionController.h
//  Project Journal
//
//  Created by Peter Pomlett on 03/07/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@interface NoteCollectionController : UIViewController
@property (strong, nonatomic) Project *project;
@end
