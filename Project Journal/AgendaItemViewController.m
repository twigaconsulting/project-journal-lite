//
//  AgendaItemViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 12/05/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "AgendaItemViewController.h"

@interface AgendaItemViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *agendaTextView;
@property (strong, nonatomic) NSString *agendaItem;

@end

@implementation AgendaItemViewController

- (IBAction)Done {

    [ self.delegate AgendaItemViewControllerDidFinish:self];
}

 - (void)textFieldDidEndEditing:(UITextField *)textField{
     self.agendaItem = self.agendaTextView.text;
     if ([self.agendaTextView.text isEqualToString:@""]== NO){
         [self.delegate AgendaItem:self.agendaItem];
     }
 }

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.agendaTextView.delegate = self;
    [self.agendaTextView becomeFirstResponder];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
