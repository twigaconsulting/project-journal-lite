//
//  Risk+TCLRisk.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Risk+TCLRisk.h"


@interface Risk (PrimitiveAccessors)
//- (NSString *)primitiveStatus;
- (void)setPrimitiveStatus:(NSString *)string;
- (void)setPrimitivePlannedCompletionDate:(NSDate *)date;
@end

@implementation Risk (TCLRisk)

- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    return formatter;
}


//date and time
- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    
    return formatter;
}


#pragma mark - Primitive Overidden Methods
- (void)setStatus:(NSString *)string
{
    // If Completed then set child items to to be reviewed & notes to completed
    if ([string isEqualToString:@"Completed"]) {
        if ([self.riskActions count] > 0) {
            for (Action *item in self.riskActions) {
                item.status = @"Completed";
            }
        }
        if ([self.riskNotes count] > 0) {
            for (Note *item in self.riskNotes) {
                item.status = @"Completed";
            }
        }
        self.oldStatus = self.status;
    } else {
        if ([self.riskActions count] > 0) {
            for (Action *item in self.riskActions) {
                item.status = item.oldStatus;
            }
        }
        if ([self.riskNotes count] > 0) {
            for (Note *item in self.riskNotes) {
                item.status = @"notCompleted";
            }
        }
    }

    
    [self willAccessValueForKey:@"status"];
    [self setPrimitiveStatus:string];
    [self didChangeValueForKey:@"status"];
    
    
}

- (void)setPlannedCompletionDate:(NSDate *)date
{
    // DataHelper *helper = [DataHelper sharedInstance];
    // NSDate *refDate = [NSDate dateWithString:[helper convertToLocalReferenceDateAndTime:date]];
    
    //convert time to 01:00
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    // NSDate *redate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    
    //update for the start date
    [comps setHour:1];
    [comps setMinute:0];
    [comps setSecond:0];
    NSDate *refDate = [calendar dateFromComponents:comps];
    
    [self willAccessValueForKey:@"plannedCompletionDate"];
    [self setPrimitivePlannedCompletionDate:refDate];
    [self didChangeValueForKey:@"plannedCompletionDate"];
    
}


#pragma mark - RAG Methods
- (NSString *)fetchRAGStatus
{
    // Compute the Risk RAG Status using Dates and Analysis
    NSString *final = @"Green";
    NSString *test = @"3. Green";
    
    if ([self.status isEqualToString:@"Completed"]) {
        final = @"Blue";
        test = @"4. Blue";
    } else {
        if ([[self fetchDateRAGStatus] isEqualToString:@"Red"] || [[self fetchAnalysisRAGStatus] isEqualToString:@"Red"]) {
            final = @"Red";
            test = @"1. Red";
        } else if ([[self fetchDateRAGStatus] isEqualToString:@"Amber"] || [[self fetchAnalysisRAGStatus] isEqualToString:@"Amber"]) {
            final = @"Amber";
            test = @"2. Amber";
        }
    }
    
    if (![self.rag isEqualToString:test]) {
        if([final isEqualToString:@"Red"]) {
            self.rag = @"1. Red";
        } else if ([final isEqualToString:@"Amber"]) {
            self.rag = @"2. Amber";
        } else if ([final isEqualToString:@"Green"]) {
            self.rag = @"3. Green";
        } else if ([final isEqualToString:@"Blue"]) {
            self.rag = @"4. Blue";
        }
    }
    
    return final;
}
- (NSString *)fetchDateRAGStatus
{
    NSString *final = @"Green";
    
    DataHelper *helper = [DataHelper sharedInstance];
    
    if ([self.status isEqualToString:@"Completed"]) {
        final = @"Blue";
    } else {
        if (self.plannedCompletionDate) {
            NSDate *redRefDate = [NSDate dateWithTimeIntervalSinceNow:([[helper redRAGInterval] integerValue] * 86400)];
            NSDate *amberRefDate = [redRefDate dateByAddingTimeInterval:([[helper amberRAGInterval] integerValue] * 86400)];
            if ([redRefDate compare:self.plannedCompletionDate] == NSOrderedDescending || [redRefDate compare:self.plannedCompletionDate] == NSOrderedSame) {
                final = @"Red";
            } else if ([amberRefDate compare:self.plannedCompletionDate] == NSOrderedDescending || [amberRefDate compare:self.plannedCompletionDate] == NSOrderedSame) {
                final = @"Amber";
            } else {
                final = @"Green";
            }
        } else {
            final = @"Red";
        }
    }
    return final;
}
- (NSString *)fetchAnalysisRAGStatus
{
    NSString *final =  @"Green";
    
    NSString *risk = [self riskLevel];
    
    if ([self.status isEqualToString:@"Completed"]) {
        final = @"Blue";
    } else {
        if ([risk isEqualToString:@"High"]) {
            final =  @"Red";
        } else if ([risk isEqualToString:@"Medium"]) {
            final = @"Amber";
        } else if ([risk isEqualToString:@"Low"]) {
            final = @"Green";
        }
    }
    return final;
}

- (BOOL)isForReview
{
    // This method checks the Project for review. The View Controller will call this to
    // check whether to present a review badge.
    
    
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *comps = [calendar components:unitFlags fromDate:today];
    NSDateComponents *check = [calendar components:unitFlags fromDate:self.reviewDate];
    // Change date
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:1];
    
    [check setHour:0];
    [check setMinute:0];
    [check setSecond:1];
    
    NSDate *theDate = [calendar dateFromComponents:comps];
    NSDate *theReview = [calendar dateFromComponents:check];
    
    BOOL flag = NO;
    
    if (![self.status isEqualToString:@"Completed"]) {
        // Check the Project's review date
        if ([theDate compare:theReview] == NSOrderedDescending || [theDate compare:theReview] == NSOrderedSame) {
            flag = YES;
        } else {
            flag = NO;
        }
    }
    
    
    
    return flag;
}

- (NSDate *)resetReviewDate
{
    /* This method advances the current date by the default review interval
     * and returns the new Review Date. It is called by the view controllers
     */
    DataHelper *helper = [DataHelper sharedInstance];
    
    self.reviewDate = [[NSDate date] dateByAddingTimeInterval:[[helper riskReviewInterval] intValue] * 86400];
    
    return self.reviewDate;
}

- (NSString *)getStatus
{
    return self.status;
}


#pragma mark - List Methods
- (NSArray *)statusList
{
    NSArray *final = [NSArray arrayWithObjects:@"Identification",@"Assessment",@"Response Planning",@"Manage",nil];
    
    if (!self.processStatus) {
        final = [NSArray arrayWithObjects:@"Identification",@"Assessment",@"Response Planning",@"Manage",nil];
    } else if ([self.processStatus isEqualToString:@"Identification"]) {
        final = [NSArray arrayWithObjects:@"Assessment",@"Response Planning",@"Manage",nil];
        
    } else if ([self.processStatus isEqualToString:@"Assessment"]) {
        final = [NSArray arrayWithObjects:@"Identification",@"Response Planning",@"Manage",nil];
        
    } else if ([self.processStatus isEqualToString:@"Response Planning"]) {
        final = [NSArray arrayWithObjects:@"Identification",@"Assessment",@"Manage",nil];
        
    } else if ([self.processStatus isEqualToString:@"Manage"]) {
        final = [NSArray arrayWithObjects:@"Identification",@"Assessment",@"Response Planning",nil];
    }
    
    
    return final;
}


- (NSArray *)strategyList
{
    NSArray *final = [NSArray arrayWithObjects:@"Avoid",@"Transfer", @"Reduce/Mitigate",@"Accept",@"Contigency Plan",nil];
    
    if (!self.responseStrategy) {
        final = [NSArray arrayWithObjects:@"Avoid",@"Transfer", @"Reduce/Mitigate",@"Accept",@"Contigency Plan",nil];

    } else if ([self.responseStrategy isEqualToString:@"Avoid"]) {
        final = [NSArray arrayWithObjects:@"Transfer", @"Reduce/Mitigate",@"Accept",@"Contigency Plan",nil];

    } else if ([self.responseStrategy isEqualToString:@"Transfer"]) {
        final = [NSArray arrayWithObjects:@"Avoid",@"Reduce/Mitigate",@"Accept",@"Contigency Plan",nil];

    } else if ([self.responseStrategy isEqualToString:@"Reduce/Mitigate"]) {
        final = [NSArray arrayWithObjects:@"Avoid",@"Transfer",@"Accept",@"Contigency Plan",nil];

    } else if ([self.responseStrategy isEqualToString:@"Accept"]) {
        final = [NSArray arrayWithObjects:@"Avoid",@"Transfer", @"Reduce/Mitigate",@"Contigency Plan",nil];

    } else if ([self.responseStrategy isEqualToString:@"Contingency Plan"]) {
        final = [NSArray arrayWithObjects:@"Avoid",@"Transfer", @"Reduce/Mitigate",@"Accept",nil];

    }
    
    return final;
}

#pragma mark - Analysis Methods

- (NSString *)riskLevel
{
    double level = ([self.probability intValue] + [self.impact intValue])/2;
    NSString *riskLevel;
    
    if ([self.priority isEqualToString:@"Low"] ) {
        level = (level + 33)/2;
    }
    if ([self.priority isEqualToString:@"Medium"] ) {
        level = (level + 66)/2;
    }
    if ([self.priority isEqualToString:@"High"] ) {
        level = (level + 100)/2;
    }
    
    if (level >= 0 && level <= 33) {
        riskLevel = @"Low";
    }
    if (level > 33 && level <= 66) {
        riskLevel = @"Medium";
    }
    if (level > 66 && level <= 100) {
        riskLevel = @"High";
    }
    
    return riskLevel;
}
- (NSNumber *)factoredRisk
{
    double cost = ([self.probability doubleValue] * [self.responseCost doubleValue])/100;
    
    return [NSNumber numberWithDouble:cost];
}

- (Action *)newMitigationTask
{
    DataHelper *helper = [DataHelper sharedInstance];
    Action *item = [helper newAction:self];
    [self addRiskActionsObject:item];
    
    return item;
}

- (NSArray *)fetchMitigationPlan
{
    return [self.riskActions allObjects];
}

#pragma mark - RAG Status Methods
// MEthod Calls for RAG STATUS

- (NSNumber *)amberRAGInterval
{
    DataHelper *helper = [DataHelper sharedInstance];
    
    return [helper amberRAGInterval];
    
}

- (NSNumber *)redRAGInterval
{
    DataHelper *helper = [DataHelper sharedInstance];
    
    return [helper redRAGInterval];
}

#pragma mark - Output Text Methods
- (NSString *)getDetailHTML
{


    
    // BUILD WEB PAGE IN A STRING
    NSMutableString *result = [[NSMutableString alloc] initWithString:@"<!DOCTYPE html>"];
    [result appendFormat:@"<html>"];
    [result appendFormat:@"<body>"];
    
    
    [result appendFormat:@"<br><br><b><u>RISK INFORMATION</u></b><br>"];
    [result appendFormat:@"<b>ID:</b> %@<br>",self.riskID];
    [result appendFormat:@"<b>Title:</b> %@<br>",self.title];
    [result appendFormat:@"<b>RAG:</b> %@<br>",[self fetchRAGStatus]];
    if ([self.summary length] > 0) {
        [result appendFormat:@"<b>Summary:</b> %@<br>",self.summary];
    }
    if (self.riskOwner) {
        [result appendFormat:@"<b>Owner:</b> %@<br>",self.riskOwner];
    }
    
    [result appendFormat:@"<b>Status:</b> %@<br>",self.status];
    [result appendFormat:@"<b>Category:</b> %@<br>",self.category];   

    [result appendFormat:@"<b>Target Completion Date:</b> %@<br>",[[self formatterDate] stringFromDate:self.plannedCompletionDate]];
    [result appendFormat:@"<b>Review Date:</b> %@<br>",[[self formatterDate] stringFromDate:self.reviewDate]];
    if (self.triggerDate) {
        [result appendFormat:@"<b>Trigger Date:</b> %@<br>",[[self formatterDate] stringFromDate:self.triggerDate]];
    }
    if (self.actualCompletionDate) {
        [result appendFormat:@"<b>Completion Date:</b> %@<br>",[[self formatterDate] stringFromDate:self.actualCompletionDate]];
    }
    if (self.completionSummary) {
        [result appendFormat:@"<b>Completion Summary:</b> %@<br>",self.completionSummary];
    }
    
    
    [result appendFormat:@"<b>In Project:</b> %@<br>",self.inProject.title];
    
    
    [result appendFormat:@"<b>Priority:</b> %@<br>",self.projectPhase];
    [result appendFormat:@"<b>Risk Level:</b> %@<br>",[self riskLevel]];
    [result appendFormat:@"<b>\tProbability:</b> %@<br>",self.probability];
    [result appendFormat:@"<b>\tImpact:</b> %@<br>",self.impact];
    
    [result appendFormat:@"<b>Creation Date:</b> %@<br>",[[self formatterDateTime] stringFromDate:self.creationDate]];
    [result appendFormat:@"<b>Last Modified Date:</b> %@<br>",[[self formatterDateTime] stringFromDate:self.lastModifiedDate]];
    
    
    // List Actions
    [result appendFormat:@"<br><br><b><u>ACTIONS</u></b><br>"];
    NSArray *actions = [[NSArray alloc] initWithArray:[self.riskActions allObjects]];
    for (int i = 0; i < [actions count]; i++) {
        if (![[[actions objectAtIndex:i] deleted] boolValue]) {
            [result appendFormat:@"<b>ID/Title:</b> %@  %@<br>",[[actions objectAtIndex:i] actionID],[[actions objectAtIndex:i] title]];
            [result appendFormat:@"<b>Target Completion Date:</b> %@<br>",[[self formatterDate] stringFromDate:[[actions objectAtIndex:i] targetCompletionDate]]];
            if ([[actions objectAtIndex:i] actionOwner]) {
                [result appendFormat:@"<b>Owner:</b> %@<br>",[[actions objectAtIndex:i] actionOwner]];
            }
            if ([[actions objectAtIndex:i] summary]) {
                [result appendFormat:@"%@<br><br>",[[actions objectAtIndex:i] summary]];
            }
            
        }
    }
    
    
    // List Notes
    [result appendFormat:@"<br><br><b><u>NOTES</u></b><br>"];
    NSArray *notes = [[NSArray alloc] initWithArray:[self.riskNotes allObjects]];
    for (int i = 0; i < [notes count]; i++) {
        if (![[[notes objectAtIndex:i] deleted] boolValue]) {
            [result appendFormat:@"<b>%@</b><br>",[[self formatterDateTime] stringFromDate:[[notes objectAtIndex:i] lastModifiedDate]]];
            [result appendFormat:@"%@<br><br>",[[notes objectAtIndex:i] summary]];
        }
    }
    
    //Footer in page
    [result appendFormat:@"</html>"];
    [result appendFormat:@"</body>"];
    
    //Advert for Project Journal
    [result appendFormat:@"<br><br><br><br><a href:\"http://www.twigaconsulting.co.uk\">This information is provided by the Project Journal iPad app.</a><br><br>"];
    
    return result;
    
}


-(NSString *)spaces:(NSNumber *)count
{
    NSMutableString *final;
    int i = 0;
    while (i < [count intValue]) {
        [final appendFormat:@"&nbsp;"];
        i++;
    }
    
    return final;
}
@end
