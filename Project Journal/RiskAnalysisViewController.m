//
//  RiskAnalysisViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 27/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "HelpViewController.h"
#import "RiskAnalysisViewController.h"
#import "CurrencyPadViewController.h"
@interface RiskAnalysisViewController ()<UIActionSheetDelegate, CurrencyPadViewControllerDelegate, UIPopoverControllerDelegate>
@property (strong, nonatomic) DataHelper *helper;
@property (strong, nonatomic) UIStoryboardPopoverSegue* popSegue;
@property (weak, nonatomic) IBOutlet UISlider *probabilitySlider;
@property (weak, nonatomic) IBOutlet UISlider *impactSlider;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *probabilityLabel;
@property (weak, nonatomic) IBOutlet UILabel *impactLabel;
@property (weak, nonatomic) IBOutlet UILabel *riskLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *processLabel;
@property (weak, nonatomic) IBOutlet UILabel *responceLabel;
@property (weak, nonatomic) IBOutlet UILabel *responseCostLabel;
@property (weak, nonatomic) IBOutlet UILabel *factoredRiskLabel;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *processImageView;
@property (weak, nonatomic) IBOutlet UIImageView *responseImageView;
@property (weak, nonatomic) IBOutlet UIImageView *responseCostImageView;
@property (weak, nonatomic) IBOutlet UIImageView *factoredRiskImageView;
@property (weak, nonatomic) IBOutlet UIImageView *probabilityImageView;
@property (weak, nonatomic) IBOutlet UIImageView *impactImageView;
@property (weak, nonatomic) IBOutlet UIImageView *riskLevelImageView;
@property (strong, nonatomic) UIActionSheet *processSheet;
@property (strong, nonatomic) UIActionSheet *responseSheet;
@property (strong, nonatomic) NSArray *processList;
@property (strong, nonatomic) NSArray *responseList;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *helpButtons;
@property (strong, nonatomic) HelpViewController *helpView;
@property (strong, nonatomic) UIPopoverController *helpPopover;
@property (nonatomic, strong) NSDictionary *helpList;
@end

@implementation RiskAnalysisViewController

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
    
    
}

- (IBAction)helpProcess:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.processStatus"] from:sender];
}
- (IBAction)helpResponce:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.responseStrategy"] from:sender];
}
- (IBAction)helpLevel:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.riskLevel"] from:sender];
}
- (IBAction)helpResponceCost:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.responseCost"] from:sender];
}
- (IBAction)factoredRisk:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.factoredRisk"] from:sender];
}
- (IBAction)helpProbabilaty:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.probability"] from:sender];
}
- (IBAction)helpImpact:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.impact"] from:sender];
}

-(void)loadTheHelpPlist{
    if(self.helpList == nil){
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"RiskHelp" ofType:@"plist"];
        self.helpList = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    }
}


- (void)checkHelpButtons {
    if(![self.helper showHelp]){
          for (UIButton *button in self.helpButtons) {
             button.hidden = YES;
          }
    }
}

- (void)showHelp:(NSString*)helpString from:(UIButton *)helpButton {
    [self.delegate dimSuper];
    if(self.helpView == nil) {
        self.helpView = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    }
    UITextView *_textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 250, 10)];
    _textView.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
    _textView.textColor = [UIColor lightGrayColor];
    _textView.scrollEnabled = NO;
    _textView.userInteractionEnabled=NO;
    _textView.text =  helpString;
    CGRect frame = _textView.frame;
    frame.size.height =  [self getTextHeight:_textView];
    _textView.frame = frame;
    self.helpView.view.frame = frame;
    [self.helpView.view addSubview:_textView];
    self.helpPopover = [[UIPopoverController alloc] initWithContentViewController:self.helpView];
    self.helpPopover.popoverContentSize =_textView.frame.size;
    [self.helpPopover presentPopoverFromRect:helpButton.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionDown animated:YES];
    self.helpPopover.delegate = self;
}

- (CGFloat)getTextHeight:(UITextView *)textView
{
    NSLayoutManager *lm = textView.layoutManager;
    CGRect boundingRect = [lm usedRectForTextContainer:lm.textContainers[0]];
    CGFloat height = boundingRect.size.height + (textView.textContainerInset.top + textView.textContainerInset.bottom);
    return height;
}

- (void)dismissPopovers{
     [self.delegate UndimSuper];
    if (self.responseSheet.isVisible) {
        [self.responseSheet dismissWithClickedButtonIndex:self.responseSheet.cancelButtonIndex animated:NO];
        self.responseSheet = nil;
    }
    if (self.processSheet.isVisible) {
        [self.processSheet dismissWithClickedButtonIndex:self.processSheet.cancelButtonIndex animated:NO];
        self.processSheet = nil;
    }
    if ([self.popSegue.popoverController isPopoverVisible])
    {
        [self.popSegue.popoverController dismissPopoverAnimated:NO];
    }
    if ([self.helpPopover isPopoverVisible])
    {
        [self.helpPopover dismissPopoverAnimated:NO];
    }
}

#pragma mark - delegate methods
- (void) CurrencyPadViewControllerDidFinish:(CurrencyPadViewController *)controller{
    if ([self.popSegue.popoverController isPopoverVisible])
    {[self.delegate UndimSuper];
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
}

- (void) Currency:(NSNumber *)number{
  
        self.risk.responseCost = number;
        self.risk.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
        
        self.responseCostLabel.text = [self convertToLocalCurrency:self.risk.responseCost];
     self.factoredRiskLabel.text = [self convertToLocalCurrency:self.risk.factoredRisk];
    }


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{ [self.delegate dimSuper];
    if ([[segue identifier] isEqualToString:@"currency"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        self.popSegue.popoverController.delegate = self;
    }
}

-(NSString *)convertToLocalCurrency:(NSNumber *)currency{
    NSNumberFormatter *formatter;
    
    if(formatter==nil){
        formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [formatter setLocale:[NSLocale currentLocale]];
    }
    return [formatter stringFromNumber:currency];
}



- (IBAction)processStatus:(UIButton *)sender {
    [self.delegate dimSuper];
    self.processList= [self.risk statusList] ;
    self.processSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                    delegate:self
                                           cancelButtonTitle:nil
                                      destructiveButtonTitle:nil
                                           otherButtonTitles:nil];
    
    for  (int i = 0; i < [self.processList count];  i++) {
        [self.processSheet addButtonWithTitle:[self.processList objectAtIndex:i]];
    }
    [self.processSheet showFromRect:sender.frame inView:self.view animated:YES];
    
}

- (IBAction)responseStrategy:(UIButton *)sender {
    [self.delegate dimSuper];
    self.responseList= [self.risk strategyList] ;
    self.responseSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                    delegate:self
                                           cancelButtonTitle:nil
                                      destructiveButtonTitle:nil
                                           otherButtonTitles:nil];
    
    for  (int i = 0; i < [self.responseList count];  i++) {
        [self.responseSheet addButtonWithTitle:[self.responseList objectAtIndex:i]];
    }
    [self.responseSheet showFromRect:sender.frame inView:self.view animated:YES];
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{[self.delegate UndimSuper];
    if (self.processSheet == actionSheet){
    if (buttonIndex < [self.processList count]){
        self.risk.processStatus = [self.processList objectAtIndex:buttonIndex];
        
        if (![self.risk.processStatus isEqualToString:self.processLabel.text]) {
            self.processLabel.text = self.risk.processStatus;
            self.risk.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        
        }
    }
}
    if (self.responseSheet == actionSheet){
        if (buttonIndex < [self.responseList count]){
            self.risk.responseStrategy = [self.responseList objectAtIndex:buttonIndex];
            
            if (![self.risk.responseStrategy isEqualToString:self.responceLabel.text]) {
                self.responceLabel.text = self.risk.responseStrategy;
                self.risk.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
               
            }
        }
    }
}



- (IBAction)probabilityStoppedIn:(UISlider *)sender {
    int p =sender.value;
    self.risk.probability = [NSNumber numberWithInt:p];
    self.risk.lastModifiedDate = [NSDate date];
    [self.helper saveContext];
    [self.delegate upDateRagBadge];
     self.riskLevelLabel.text = self.risk.riskLevel;
    self.factoredRiskLabel.text = [self convertToLocalCurrency:self.risk.factoredRisk];
}

- (IBAction)probabilityStoppedOut:(UISlider *)sender {
    self.risk.probability = [NSNumber numberWithInt:sender.value];
    self.risk.lastModifiedDate = [NSDate date];
    [self.helper saveContext];
    [self.delegate upDateRagBadge];
     self.riskLevelLabel.text = self.risk.riskLevel;
    self.factoredRiskLabel.text = [self convertToLocalCurrency:self.risk.factoredRisk];
    
}

- (IBAction)probabilityChanged:(UISlider *)sender {
    int p = sender.value;
    self.probabilityLabel.text = [NSString stringWithFormat:@"%d %%",p];
}


- (IBAction)impactStoppedIn:(UISlider *)sender {
    int p =sender.value;
    self.risk.impact = [NSNumber numberWithInt:p];
    self.risk.lastModifiedDate = [NSDate date];
    [self.helper saveContext];
    [self.delegate upDateRagBadge];
     self.riskLevelLabel.text = self.risk.riskLevel;
    self.factoredRiskLabel.text = [self convertToLocalCurrency:self.risk.factoredRisk];
}

- (IBAction)impactStoppedOut:(UISlider *)sender {
    int p =sender.value;
    self.risk.impact = [NSNumber numberWithInt:p];
    self.risk.lastModifiedDate = [NSDate date];
    [self.helper saveContext];
    [self.delegate upDateRagBadge];
     self.riskLevelLabel.text = self.risk.riskLevel;
    self.factoredRiskLabel.text = [self convertToLocalCurrency:self.risk.factoredRisk];
}

- (IBAction)impactChanged:(UISlider *)sender {
    int p = sender.value;
    self.impactLabel.text = [NSString stringWithFormat:@"%d %%",p];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
     self.probabilitySlider.hidden = NO;
    self.impactSlider.hidden = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   	self.titleLabel.text = self.risk.title;
    self.probabilityLabel.text = [NSString stringWithFormat:@"%d %%" ,[self.risk.probability integerValue]];
    self.probabilitySlider.value = [self.risk.probability integerValue];
    self.impactLabel.text = [NSString stringWithFormat:@"%d %%" ,[self.risk.impact integerValue]];
    self.impactSlider.value = [self.risk.impact integerValue];
    self.riskLevelLabel.text = self.risk.riskLevel;
     self.processLabel.text = self.risk.processStatus;
    self.responceLabel.text = self.risk.responseStrategy;
    self.responseCostLabel.text = [self convertToLocalCurrency:self.risk.responseCost];
    self.factoredRiskLabel.text = [self convertToLocalCurrency:self.risk.factoredRisk];
}


-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    UIEdgeInsets tabInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage * textBack = [UIImage imageNamed:@"TextFrame2"];
    textBack = [textBack resizableImageWithCapInsets:tabInsets];
    self.titleImageView.image = textBack;
  //  self.titleImageView.alpha = 0.5;
    self.riskLevelImageView.image = textBack;
  //  self.riskLevelImageView.alpha = 0.5;
    self.probabilityImageView.image = textBack;
    self.impactImageView.image = textBack;
    self.processImageView.image = textBack;
    self.responseImageView.image = textBack;
    self.factoredRiskImageView.image = textBack;
   // self.factoredRiskImageView.alpha = 0.5;
    self.responseCostImageView.image = textBack;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    [self checkHelpButtons];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
