//
//  ItemDetailViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "UIViewController+TCLViewController.h"
#import "XMLWriter.h"


@class ActionFormContainer;


@protocol ActionFormContainerDelegate <NSObject>
- (void)ActionFormContainerDidFinish:(ActionFormContainer *)controller;
-(void)getActionParent:(id)parent;
@end

@interface ActionFormContainer : UIViewController <MFMailComposeViewControllerDelegate, UINavigationControllerDelegate,UIPrintInteractionControllerDelegate> 

@property (weak, nonatomic) id < ActionFormContainerDelegate > delegate;
@property (strong,nonatomic) Project *project;
@property (strong,nonatomic) Action *containerAction;
//@property (strong,nonatomic) Meeting *meeting;
@property (strong,nonatomic) id item;
@property (assign, nonatomic) BOOL parentIsModal;


@property (strong,retain) NSArray *toRecipients;
@property (strong,retain) NSArray *ccRecipients;
@property (strong,retain) NSArray *bccRecipients;
@property (strong,retain) NSString *subject;
@property (strong,retain) NSString *body;
@property (strong,retain) NSString *fileName;
@property (strong,retain) NSString *fileType;
@property (strong,retain) NSString *mimeType;
@property (strong,retain) UIViewController *currentView;
@property (strong,retain) MFMailComposeViewController *picker;
@end
