//
//  RiskFormViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 26/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "PJColour.h"

#import "RiskFormContainer.h"
#import "RiskDetailViewController.h"
#import "RiskAnalysisViewController.h"
#import "RiskDatesViewController.h"
#import "ListNotesViewController.h"
#import "ActionsListViewController.h"
@interface RiskFormContainer ()<NoteListViewControllerDelegate, UIActionSheetDelegate, RiskDatesViewControllerDelegate, RiskDetailViewControllerDelegate, RiskAnalysisViewControllerDelegate, ActionsListViewControllerDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) DataHelper *helper;
@property (nonatomic,strong) PJColour *badgeColour;
@property (weak, nonatomic) IBOutlet UIView *container;
@property (strong, nonatomic) ListNotesViewController *notesFormViewController;
@property (strong, nonatomic) RiskDetailViewController *riskDetailViewController;
@property (strong, nonatomic) RiskAnalysisViewController *riskAnalysisViewController;
@property (strong, nonatomic) RiskDatesViewController *riskDatesViewController;
@property (strong, nonatomic) ActionsListViewController *riskActionsViewController;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UIButton *datesButton;
@property (weak, nonatomic) IBOutlet UIButton *analysisButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UIButton *notesButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (strong,nonatomic) NSString *key;
@property (strong,nonatomic) UIActionSheet *sendSheet;
@property (strong,nonatomic) UILabel *noteCount;
@property (strong,nonatomic) UILabel *actionCount;
@property (strong,nonatomic) UIImageView *imageComplete;
@property (strong,nonatomic) UIImageView *imageReview;
@property (strong,nonatomic) UIImageView *image;
@property (strong,nonatomic) UIImageView *imageA;
@property (strong,nonatomic) UIAlertView *deleteAlert;
@end

@implementation RiskFormContainer
#pragma mark -
-(void) dimSuper{
    self.view.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
}
-(void) UndimSuper{
    self.view.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
}

-(void)getNewAction:(id)itenAction{
    [self.delegate getNewAction:itenAction];
}

-(void)getAction:(Action*)itenAction{
    [self.delegate getTheActionForm:itenAction];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self.riskDatesViewController dismissPopovers];
    [self.riskAnalysisViewController dismissPopovers];
    [self.notesFormViewController dismissPopovers];
    [self.riskDetailViewController dismissPopovers];
    if (self.sendSheet.isVisible) {
        [self UndimSuper];
        [self.sendSheet dismissWithClickedButtonIndex:self.sendSheet.cancelButtonIndex animated:NO];
        self.sendSheet = nil;
    }
    
    if (self.deleteAlert.isVisible) {
        [self.deleteAlert dismissWithClickedButtonIndex:0 animated:NO];
        self.deleteAlert = nil;
    }
}

//allows keyboard to be dismised on the subviews
- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

#pragma mark - button badge methods
//date view delegate methods
-(void) upDateRagBadge{
    [self createCompletedBadge];
     [self createDateRagBadge];
    [self createAnalysisRagBadge];
}
-(void) upDateReviewBadge{
    [self createReviewBadge];
}

- (void) upDateBadge{
    [self createCompletedBadge];
     [self createDateRagBadge];
    [self createAnalysisRagBadge];
    [self createReviewBadge];
}
//note view delegate method
-(void) UpdateNotesBadge{
    [self createNotesBadge];    
}
-(void)updateActionBadge{
    [self createActionBadge];
}

-(void)createAnalysisRagBadge{
  
    if(!self.imageA){
        self.imageA = [[UIImageView alloc] initWithFrame:CGRectMake(29, 32,11, 11)];
        [self.analysisButton addSubview:self.imageA];
    }
    NSString *budgetColour= [self.containerRisk fetchAnalysisRAGStatus];
    if([budgetColour isEqualToString:@"Amber"]){
        self.imageA.image = [UIImage imageNamed:@"AmberBadje"];
    }
    if([budgetColour isEqualToString:@"Red"]){
        self.imageA.image = [UIImage imageNamed:@"redBadje"];
    }
    
    if([budgetColour isEqualToString:@"Green"]){
        self.imageA.image = [UIImage imageNamed:@"greenBadge"];
    }
    if([budgetColour isEqualToString:@"Blue"]){
        self.imageA.image = [UIImage imageNamed:@"blueBadje"];
    }
    
    
    
    
    
}

-(void)createDateRagBadge{
    if(!self.image){
        self.image = [[UIImageView alloc] initWithFrame:CGRectMake(23, 32,11, 11)];
        [self.datesButton addSubview:self.image];
    }
    NSString *ragColour= [self.containerRisk fetchDateRAGStatus];
    if([ragColour isEqualToString:@"Amber"]){
        self.image.image = [UIImage imageNamed:@"AmberBadje"];
    }
    if([ragColour isEqualToString:@"Red"]){
        self.image.image = [UIImage imageNamed:@"redBadje"];
    }
    if([ragColour isEqualToString:@"Green"]){
        self.image.image = [UIImage imageNamed:@"greenBadge"];
    }
    if([ragColour isEqualToString:@"Blue"]){
        self.image.image = [UIImage imageNamed:@"blueBadje"];
    }
}

-(void)createActionBadge{
    int actionCount = [[self.containerRisk riskActions ]count];
    if (!self.actionCount) {
        self.actionCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, self.actionButton.frame.size.width, 10)];
        self.actionCount.backgroundColor = [UIColor clearColor];
        self.actionCount.textAlignment = NSTextAlignmentCenter;
        self.actionCount.numberOfLines=1;
        [self.actionCount setFont:[UIFont systemFontOfSize:12]];
        
        [self.actionButton addSubview:self.actionCount];
    }
    if(actionCount > 0){
        self.actionCount.textColor = [UIColor darkGrayColor];
    }
    else {
        self.actionCount.textColor = [UIColor lightGrayColor];
    }
    self.actionCount.text = [NSString stringWithFormat:@"%d",actionCount];
}

-(void)createCompletedBadge{
    if(!self.imageComplete){
        self.imageComplete = [[UIImageView alloc] initWithFrame:CGRectMake(29, 32,11, 11)];
        [self.detailButton addSubview:self.imageComplete];
    }
    
    if([self.containerRisk.status isEqualToString:@"Completed"]) {
        self.imageComplete.image = [UIImage imageNamed:@"blueBadje"];
    }
    else{
        self.imageComplete.image = [UIImage imageNamed:@"noBadje"];
    }
}



-(void)createNotesBadge{
    
    int noteCount = [[self.helper fetchNotesList:self.containerRisk]count];
    
    if(!self.noteCount){
        self.noteCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, self.notesButton.frame.size.width, 10)];
        self.noteCount.backgroundColor = [UIColor clearColor];
        self.noteCount.textAlignment = NSTextAlignmentCenter;
        self.noteCount.numberOfLines=1;
        [self.noteCount setFont:[UIFont systemFontOfSize:12]];
        self.noteCount.textColor = [UIColor darkGrayColor];
        [self.notesButton addSubview:self.noteCount];
    }
if(noteCount > 0){
    self.noteCount.textColor = [UIColor darkGrayColor];
}
else {
    self.noteCount.textColor = [UIColor lightGrayColor];
}
self.noteCount.text = [NSString stringWithFormat:@"%d",noteCount];
}

- (void) createReviewBadge{
    if(!self.imageReview){
        self.imageReview = [[UIImageView alloc] initWithFrame:CGRectMake(35, 32,11, 11)];
        [self.datesButton addSubview:self.imageReview];
    }
    if(self.containerRisk.isForReview){
        self.imageReview.image = [UIImage imageNamed:@"reviewBadjge"];
    }
    else{
        self.imageReview.image = [UIImage imageNamed:@"noReviewBadge"];
    }
}

#pragma mark - tool bar action sheets


-(void)createSendSheet{
    self.sendSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                 delegate:self
                                        cancelButtonTitle:nil
                                   destructiveButtonTitle:nil
                                        otherButtonTitles:@"print",@"email text",@"email attachment",nil];
    [self.sendSheet showFromRect:self.sendButton.frame inView:self.view animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{ [self UndimSuper];
    if (buttonIndex == 0) {
        /* This scope prints the Risk details to a selectable printer
         */
        
        // Check if printing is avaliable
        if ([UIPrintInteractionController isPrintingAvailable]) {
            UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
            pic.delegate = self;
            
            UIPrintInfo *printInfo = [UIPrintInfo printInfo];
            printInfo.outputType = UIPrintInfoOutputGeneral;
            printInfo.jobName = self.containerRisk.title;
            pic.printInfo = printInfo;
            
            UIMarkupTextPrintFormatter *textFormatter = [[UIMarkupTextPrintFormatter alloc] initWithMarkupText:self.containerRisk.getDetailHTML];
            textFormatter.startPage = 0;
            textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
            textFormatter.maximumContentWidth = 6 * 72.0;
            pic.printFormatter = textFormatter;
            pic.showsPageRange = YES;
            [pic presentFromRect:self.sendButton.frame inView:self.view animated:YES completionHandler:nil];
            
        }
    }
    if (buttonIndex == 1) {
        /* This method creates the email for the Risk. This email has 
         * no attachment the Risk details are with the email body.
         */
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.containerRisk.title forKey:@"subject"];
        [data setValue:self.containerRisk.getDetailHTML forKey:@"body"];
        
        [data setValue:nil forKey:@"fileName"];
        
        [self displayMailComposeSheet:data];
    }
    if (buttonIndex == 2) {
        /* This method creates the email for the Risk. This email has
         * an XLS attachment of the Risk Details.
         */
        
        XMLWriter *writer = [[XMLWriter alloc] init];
        NSString *xmlString = [writer writeXMLForExcel:self.containerRisk];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //make a file name to write the data to using the documents directory:
        NSString *theFileName = [NSString stringWithFormat:@"%@/%@.xls",
                                 documentsDirectory,self.containerRisk.riskID];

        
        NSString *shortFileName = [NSString stringWithFormat:@"%@.xls",self.containerRisk.riskID];
        
        [xmlString writeToFile:theFileName
                    atomically:NO
                      encoding:NSUTF8StringEncoding
                         error:nil];
        
        //Setup email
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.containerRisk.title forKey:@"subject"];
        [data setValue:@"Risk attachment for Microsoft Excel" forKey:@"body"];
        [data setValue:[NSData dataWithContentsOfFile:theFileName] forKey:@"attachmentData"];
        [data setValue:[NSString stringWithFormat:@"%@",shortFileName] forKey:@"fileName"];
        
        [self displayMailComposeSheet:data];
        
        [[NSFileManager defaultManager] removeItemAtPath:theFileName error:nil];

    }
    
}

#pragma mark - methodes for adding container viewControllers

-(void)enableButtons{
    self.detailButton.enabled = YES;
    self.analysisButton.enabled = YES;
    self.actionButton.enabled = YES;
    self.notesButton.enabled = YES;
    self.datesButton.enabled = YES;
}

- (IBAction)Detail {
    if(self.riskDetailViewController == nil) {
        self.riskDetailViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"RiskDetail"];
        self.riskDetailViewController.risk = self.containerRisk;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.riskDetailViewController and:@"RiskDetail"];
    self.detailButton.enabled = NO;
}

- (IBAction)Dates {
    if(self.riskDatesViewController == nil) {
        self.riskDatesViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"RiskDates"];
        self.riskDatesViewController.delegate = self;
        self.riskDatesViewController.risk = self.containerRisk;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.riskDatesViewController and:@"RiskDates"];
    self.datesButton.enabled = NO;
}

- (IBAction)Analysis {
    if(self.riskAnalysisViewController == nil) {
        self.riskAnalysisViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"RiskAnalysis"];
        self.riskAnalysisViewController.delegate = self;
        self.riskAnalysisViewController.risk = self.containerRisk;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.riskAnalysisViewController and:@"RiskAnalysis"];
    self.analysisButton.enabled = NO;
}


- (IBAction)Plan {
    if(self.riskActionsViewController == nil) {
        self.riskActionsViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"MAction"];
        self.riskActionsViewController.delegate = self;
          self.riskActionsViewController.item = self.containerRisk;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.riskActionsViewController and:@"Action"];
    self.actionButton.enabled = NO;
}

- (IBAction)Notes {
    if(self.notesFormViewController == nil) {
        self.notesFormViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"NotesTable"];
        self.notesFormViewController.item = self.containerRisk;
         self.notesFormViewController.delegate = self;
    }
    [self enableButtons];
    [self SwapOldControllerForNew:self.notesFormViewController and:@"Notes"];
    self.notesButton.enabled = NO;
}

//Use a key to keep track of what comtroller is loaded
-(void)SwapOldControllerForNew:(UIViewController*)controller and:(NSString *)aKey {
    if([self.key isEqualToString:@"RiskDetail"]){
        [self.riskDetailViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.riskDetailViewController.view removeFromSuperview]; [self.container addSubview:controller.view]; }
                        completion:^(BOOL finished) {
                            [self.riskDetailViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
 else if([self.key isEqualToString:@"RiskDates"])
    {  [self.riskDatesViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.riskDatesViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.riskDatesViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
   else if([self.key isEqualToString:@"RiskAnalysis"])
    {  [self.riskAnalysisViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.riskAnalysisViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.riskAnalysisViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
  else if([self.key isEqualToString:@"Action"])
    { [self.riskActionsViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.riskActionsViewController.view removeFromSuperview]; [self.container addSubview:controller.view];
                        }
                        completion:^(BOOL finished) {
                            [self.riskActionsViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
   else if([self.key isEqualToString:@"Notes"]){
        [self.notesFormViewController willMoveToParentViewController:nil];
        [self addChildViewController:controller];
        [UIView transitionWithView:self.container
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [self.notesFormViewController.view removeFromSuperview]; [self.container addSubview:controller.view]; }
                        completion:^(BOOL finished) {
                            [self.notesFormViewController removeFromParentViewController];
                            [controller didMoveToParentViewController:self];
                        }];
        self.key = aKey;
    }
}
#pragma mark - View controller methods

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
  
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)refreshFromBackground{
    [self createReviewBadge];
    [self createDateRagBadge];
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    if (!self.badgeColour){
        self.badgeColour = [[PJColour alloc]init] ;
    }
    if (self.containerRisk == nil){
        self.containerRisk = [self.helper newRisk:self.project] ;
    }
    self.riskDetailViewController = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"RiskDetail"];
    self.riskDetailViewController.risk = self.containerRisk;
    self.riskDetailViewController.delegate = self;
    [self addChildViewController:self.riskDetailViewController];
    
    [self.container addSubview:self.riskDetailViewController.view];
    [self.riskDetailViewController didMoveToParentViewController:self];
    self.key =@"RiskDetail";
    [self enableButtons];
    self.detailButton.enabled = NO;
    [self createCompletedBadge];
    [self createNotesBadge];
    [self createReviewBadge];
    [self createActionBadge];
     [self createDateRagBadge];
    [self createAnalysisRagBadge];
}
#pragma mark - tool bar IBActions
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)send:(id)sender {
     [self.view endEditing:YES];
    [self dimSuper];
    [self createSendSheet];
}


- (IBAction)Done:(id)sender {
    [self.delegate RiskFormContainerDidFinish:self];
}

- (IBAction)Delete:(id)sender {
     [self.view endEditing:YES];
    [self dimSuper];
    if(!self.deleteAlert){
        self.deleteAlert = [[UIAlertView alloc] initWithTitle:@"Delete Risk"
                                                      message:@"Deleting this action will delete all of its actions and notes"
                                                     delegate:self                                          cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Delete",nil];
    }
    [self.deleteAlert show];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
     [self UndimSuper];
    if(buttonIndex == 1){
        [self.helper deleteObject:self.containerRisk];
        
        [self.helper saveContext];
        [self.delegate RiskFormContainerDidFinish:self];
        
        
    }}


@end
