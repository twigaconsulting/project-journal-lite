//
//  IssueCollectionController.h
//  Project Journal
//
//  Created by Peter Pomlett on 10/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@interface IssueCollectionController : UIViewController
@property (strong, nonatomic) Project *project;
@end
