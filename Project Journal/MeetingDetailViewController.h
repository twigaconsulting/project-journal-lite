//
//  MeetingDetailViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 03/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "DataHelper.h"
@protocol MeetingDetailViewControllerDelegate <NSObject>


-(void) dimSuper;
-(void) UndimSuper;

@end

@interface MeetingDetailViewController : UIViewController <UINavigationBarDelegate, UITableViewDelegate, EKEventEditViewDelegate, UINavigationControllerDelegate, UIActionSheetDelegate> {
    
	EKEventViewController *detailViewController;
	EKEventStore *eventStore;
	EKCalendar *defaultCalendar;
	NSMutableArray *eventsList;
}

@property (weak, nonatomic) id <MeetingDetailViewControllerDelegate> delegate;
@property(strong, nonatomic)Meeting *detailMeeting;
@property(strong, nonatomic)Project *project;

// CALENDAR PROPERTIES
@property (nonatomic, retain) EKEventStore *eventStore;
@property (nonatomic, retain) EKCalendar *defaultCalendar;
@property (nonatomic, retain) NSMutableArray *eventsList;
@property (nonatomic, retain) EKEventViewController *detailViewController;
@property (nonatomic, retain) EKEvent *event;
@property (nonatomic) BOOL accessGranted;

- (void)dismissPopovers;

@end
