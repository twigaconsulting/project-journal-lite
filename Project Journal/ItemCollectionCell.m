//
//  ItemCollectionCell.m
//  Project Journal
//
//  Created by Peter Pomlett on 20/05/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "ItemCollectionCell.h"
#import <QuartzCore/QuartzCore.h>
@implementation ItemCollectionCell

-(void) awakeFromNib{
    [super awakeFromNib];
    self.dateBadge.layer.borderWidth = 1.0;
    self.dateBadge.layer.borderColor = [UIColor whiteColor].CGColor;
    self.reviewLabel.layer.borderWidth = 1.0;
    self.reviewLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    self.analysisBadge.layer.borderWidth = 1.0;
    self.analysisBadge.layer.borderColor = [UIColor whiteColor].CGColor;
    self.noteTextView.textColor = [UIColor whiteColor];////test
    CALayer *layer = [self layer];
    [layer setCornerRadius:6];
    
    
    [layer setRasterizationScale:[[UIScreen mainScreen] scale]];
    [layer setShouldRasterize:YES];
    
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if (menuController) {
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
             return NO;
       }

    



@end
