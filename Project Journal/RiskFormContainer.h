//
//  RiskFormViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 26/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "UIViewController+TCLViewController.h"
#import "XMLWriter.h"


@class RiskFormContainer;


@protocol RiskFormContainerDelegate <NSObject>
-(void)RiskFormContainerDidFinish:(RiskFormContainer *)controller;
-(void)getTheActionForm:(Action *)itemAction;
-(void)getNewAction:(id)itenAction;
@end

@interface RiskFormContainer : UIViewController <MFMailComposeViewControllerDelegate, UINavigationControllerDelegate,UIPrintInteractionControllerDelegate> 

@property (weak,nonatomic) id <RiskFormContainerDelegate> delegate;
@property (strong,nonatomic) Project *project;
@property (strong,nonatomic) Risk *containerRisk;

@property (strong,retain) NSArray *toRecipients;
@property (strong,retain) NSArray *ccRecipients;
@property (strong,retain) NSArray *bccRecipients;
@property (strong,retain) NSString *subject;
@property (strong,retain) NSString *body;
@property (strong,retain) NSString *fileName;
@property (strong,retain) NSString *fileType;
@property (strong,retain) NSString *mimeType;
@property (strong,retain) UIViewController *currentView;
@property (strong,retain) MFMailComposeViewController *picker;

@end
