//
//  DBRequest+ExBody.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 09/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "DBRequest+ExBody.h"

@implementation DBRequest (ExBody)

#pragma mark NSURLConnection delegate methods

- (NSInputStream *)connection:(NSURLConnection *)connection needNewBodyStream:(NSURLRequest *)req {
    
    NSString * sourcePath = [userInfo objectForKey:@"sourcePath"];
    NSLog(@"%@ needs newBodyStream!", sourcePath);
    return [NSInputStream inputStreamWithFileAtPath:sourcePath];
}

@end
