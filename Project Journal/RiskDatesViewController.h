//
//  RiskDatesViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 27/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"

@protocol RiskDatesViewControllerDelegate <NSObject>

-(void) upDateRagBadge;
-(void) upDateReviewBadge;
-(void) dimSuper;
-(void) UndimSuper;

@end

@interface RiskDatesViewController : UIViewController
@property (weak, nonatomic) id <RiskDatesViewControllerDelegate> delegate;
@property (strong, nonatomic) Risk *risk;
- (void)dismissPopovers;
@end
