// REVIEWED BY PETER
//  RiskDatesViewController.m
//  Project Journal
//
//  Created by Peter Pomlett on 27/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "HelpViewController.h"
#import "RiskDatesViewController.h"
#import "DatePickerViewController.h"

@interface RiskDatesViewController ()<DatePickerViewControllerDelegate, UIActionSheetDelegate, UIPopoverControllerDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) UIStoryboardPopoverSegue* popSegue;
@property (strong, nonatomic) DataHelper *helper;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *completionLabel;
@property (weak, nonatomic) IBOutlet UILabel *plannedCompletionLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet UILabel *modifiedLabel;
@property (weak, nonatomic) IBOutlet UILabel *assignedLabel;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *completedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *plannedCompletionImageView;
@property (weak, nonatomic) IBOutlet UIImageView *createdImageView;
@property (weak, nonatomic) IBOutlet UIImageView *reviewImageView;
@property (weak, nonatomic) IBOutlet UIImageView *modifiedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *assignedImageView;
@property (weak, nonatomic) IBOutlet UIButton *completionButton;
@property (weak, nonatomic) IBOutlet UIButton *targetCompletionDateButton;
@property (weak, nonatomic) IBOutlet UIButton *reviewButton;
@property (weak, nonatomic) IBOutlet UIButton *assignedButton;
@property (weak, nonatomic) IBOutlet UIButton *markReviewedButton;
@property (weak, nonatomic) UIActionSheet *reviewAction;
@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *helpButtons;
@property (strong, nonatomic) HelpViewController *helpView;
@property (strong, nonatomic) UIPopoverController *helpPopover;
@property (nonatomic, strong) NSDictionary *helpList;
@property (nonatomic, strong) UIAlertView *alert;
- (NSDateFormatter *)formatterDate;
- (NSDateFormatter *)formatterDateTime;
@end

@implementation RiskDatesViewController


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.delegate UndimSuper];
    
    
}

- (IBAction)helpAsigned:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.assignedDate"] from:sender];
}
- (IBAction)helpTarget:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.targetCompletionDate"] from:sender];
}
- (IBAction)helpCompleted:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.completionDate"] from:sender];
}
- (IBAction)lastModified:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.lastModifiedDate"] from:sender];
}
- (IBAction)helpCreation:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.creationDate"] from:sender];
}
- (IBAction)helpReview:(UIButton *)sender {
    [self loadTheHelpPlist];
    [self showHelp:[self.helpList objectForKey:@"risk.reviewDate"] from:sender];
}

-(void)loadTheHelpPlist{
    if(self.helpList == nil){
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"RiskHelp" ofType:@"plist"];
        self.helpList = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    }
}

- (void)checkHelpButtons {
    if(![self.helper showHelp]){
          for (UIButton *button in self.helpButtons) {
             button.hidden = YES;
          }
    }
}


- (void)showHelp:(NSString*)helpString from:(UIButton *)helpButton {
    [self.delegate dimSuper];
    if(self.helpView == nil) {
        self.helpView = [[UIStoryboard storyboardWithName:@"storyBoardIpad" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    }
    UITextView *_textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 250, 10)];
    _textView.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
    _textView.textColor = [UIColor lightGrayColor];
    _textView.scrollEnabled = NO;
    _textView.userInteractionEnabled=NO;
    _textView.text =  helpString;
    CGRect frame = _textView.frame;
    frame.size.height =  [self getTextHeight:_textView];
    _textView.frame = frame;
    self.helpView.view.frame = frame;
    [self.helpView.view addSubview:_textView];
    self.helpPopover = [[UIPopoverController alloc] initWithContentViewController:self.helpView];
    self.helpPopover.popoverContentSize =_textView.frame.size;
    [self.helpPopover presentPopoverFromRect:helpButton.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionDown animated:YES];
    self.helpPopover.delegate = self;
}

- (CGFloat)getTextHeight:(UITextView *)textView
{
    NSLayoutManager *lm = textView.layoutManager;
    CGRect boundingRect = [lm usedRectForTextContainer:lm.textContainers[0]];
    CGFloat height = boundingRect.size.height + (textView.textContainerInset.top + textView.textContainerInset.bottom);
    return height;
}

- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (void)dismissPopovers{
    [self.delegate UndimSuper];
    
    
    if (self.alert.isVisible) {
        [self.alert dismissWithClickedButtonIndex:0 animated:NO];
        self.alert = nil;
    }
    if (self.reviewAction.isVisible) {
        [self.reviewAction dismissWithClickedButtonIndex:self.reviewAction.cancelButtonIndex animated:NO];
        self.reviewAction = nil;
    }
    if ([self.popSegue.popoverController isPopoverVisible])
    {
        [self.popSegue.popoverController dismissPopoverAnimated:NO];
    }
    if ([self.helpPopover isPopoverVisible])
    {
        [self.helpPopover dismissPopoverAnimated:NO];
    }
}

- (IBAction)resetReview:(id)sender {
    UIActionSheet *reviewSheet;
    if (!reviewSheet){
        reviewSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                  delegate:self
                                         cancelButtonTitle:nil
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"Mark As Reviewed",nil];
    }
     [self.delegate dimSuper];
    [reviewSheet showFromRect:self.markReviewedButton.frame inView:self.view  animated:YES];
     self.reviewAction = reviewSheet;
}
//reset the review date and update the UI
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
     [self.delegate UndimSuper];
    if(buttonIndex == 0){
        [ self.risk resetReviewDate];
        [self showHideReviewButton];
        self.reviewLabel.text = [[self formatterDate] stringFromDate:self.risk.reviewDate];
        [self.delegate upDateReviewBadge];
    }}

-(void)showHideReviewButton{
    //show review button is project is for review
    if([self.risk isForReview]){
        self.markReviewedButton.hidden = NO;
    }
    else{
        self.markReviewedButton.hidden = YES;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{[self.delegate dimSuper];
    if ([[segue identifier] isEqualToString:@"RD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        if (self.risk.raisedDate){
            [[segue destinationViewController] setPickerValue:self.risk.raisedDate];
        }
         self.popSegue.popoverController.delegate =self;
        self.key=@"RD";
    }
    if ([[segue identifier] isEqualToString:@"PCD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        if (self.risk.plannedCompletionDate){
            [[segue destinationViewController] setPickerValue:self.risk.plannedCompletionDate];
        }
         self.popSegue.popoverController.delegate =self;
        self.key=@"PCD";
    }
    if ([[segue identifier] isEqualToString:@"RvD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        if (self.risk.reviewDate){
            [[segue destinationViewController] setPickerValue:self.risk.reviewDate];
        }
         self.popSegue.popoverController.delegate =self;
        self.key=@"RvD";
    }
    if ([[segue identifier] isEqualToString:@"CD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        if (self.risk.actualCompletionDate){
            [[segue destinationViewController] setPickerValue:self.risk.actualCompletionDate];
        }
         self.popSegue.popoverController.delegate =self;
        self.key=@"CD";
    }
    if ([[segue identifier] isEqualToString:@"AD"]) {
        self.popSegue = (UIStoryboardPopoverSegue*)segue;
        [[segue destinationViewController] setDelegate:self];
        if (self.risk.assignedDate){
            [[segue destinationViewController] setPickerValue:self.risk.assignedDate];
       }
         self.popSegue.popoverController.delegate =self;
        self.key=@"AD";
    }
}

#pragma mark DatePickerController delegate methodes
//method called when the done button is tapped on the DatePickerController
-(void)DatePicked:(NSDate *)date andController:(DatePickerViewController *)controller
{   //Use key to update correct field with date returned from datePickerController
    if ([self.key isEqualToString:@"PCD"]){
        
        // **** KYN'S DATE CHECKING CODE
        NSCalendar *calendar = [NSCalendar currentCalendar];
        unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
        NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
        NSDateComponents *check = [calendar components:unitFlags fromDate:self.risk.creationDate];
        
        // Change date
        [comps setHour:0];
        [comps setMinute:0];
        [comps setSecond:1];
        
        [check setHour:0];
        [check setMinute:0];
        [check setSecond:1];
        
        NSDate *theDate = [calendar dateFromComponents:comps];
        NSDate *theReview = [calendar dateFromComponents:check];
        
        BOOL flag = NO;
        
        // Compare the Creation date  with the Picker and flag YES if picker date is same or later
        if ([theDate compare:theReview] == NSOrderedDescending || [theDate compare:theReview] == NSOrderedSame) {
            flag = YES;
        }
        
        // **** END OF CHECKING CODE
        
        
        
      if( flag ) {
        self.risk.plannedCompletionDate = date;
        self.risk.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
          self.plannedCompletionLabel.text = [[self formatterDate] stringFromDate:self.risk.plannedCompletionDate];
          self.modifiedLabel.text = [[self formatterDateTime] stringFromDate:self.risk.lastModifiedDate];
           [self.delegate upDateRagBadge];
      }
      else{
          if(!self.alert){
          self.alert = [[UIAlertView alloc] initWithTitle:@"Cannot Set Date"
                                                          message:@"The target completion date must be after the creation date."
                                                         delegate:self                                          cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
          }
          [self.delegate dimSuper];
          [self.alert show];
      }
    }
    if ([self.key isEqualToString:@"RvD"]){
        self.risk.reviewDate = date;
        self.risk.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
         self.reviewLabel.text = [[self formatterDate] stringFromDate:self.risk.reviewDate];
         self.modifiedLabel.text = [[self formatterDateTime] stringFromDate:self.risk.lastModifiedDate];
        [self.delegate upDateReviewBadge];
        [self showHideReviewButton];
        }
    if ([self.key isEqualToString:@"CD"]){
        self.risk.actualCompletionDate = date;
        self.risk.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
         self.completionLabel.text = [[self formatterDate] stringFromDate:self.risk.actualCompletionDate];
         self.modifiedLabel.text = [[self formatterDateTime] stringFromDate:self.risk.lastModifiedDate];
        }
    if ([self.key isEqualToString:@"AD"]){
        self.risk.assignedDate = date;
        self.risk.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
         self.assignedLabel.text = [[self formatterDate] stringFromDate:self.risk.assignedDate];
         self.modifiedLabel.text = [[self formatterDateTime] stringFromDate:self.risk.lastModifiedDate];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.delegate UndimSuper];
}

- (void)DatePickerViewControllerControllerDidFinish:(DatePickerViewController *)controller{
    if ([self.popSegue.popoverController isPopoverVisible])
    {[self.delegate UndimSuper];
        [self.popSegue.popoverController dismissPopoverAnimated:YES];
    }
}

-(void)applyDateRules{
    if([self.risk.status isEqualToString:@"Completed"]){
        //risk completed
        self.completionButton.enabled = YES;
        self.reviewButton.enabled = NO;
        self.targetCompletionDateButton.enabled = NO;
        self.assignedButton.enabled = NO;
        self.completedImageView.alpha = 1.0;
     //   self.reviewImageView.alpha = 0.5;
        self.plannedCompletionImageView.alpha = 0.5;
       self.assignedImageView.alpha = 0.5;
    }
    else{
        //risk not completed
        if(self.risk.riskOwner == nil){
            //risk not completed and not assigned
            self.completionButton.enabled = NO;
            self.reviewButton.enabled = YES;
            self.targetCompletionDateButton.enabled = YES;
           self.assignedButton.enabled = NO;
          //  self.completedImageView.alpha = 0.5;
            self.reviewImageView.alpha = 1.0;
            self.plannedCompletionImageView.alpha = 1.0;
         // self.assignedImageView.alpha = 0.5;
            
        }
        else{
            //risk not completed and assigned
            self.completionButton.enabled = NO;
            self.reviewButton.enabled = YES;
            self.targetCompletionDateButton.enabled = YES;
            self.assignedButton.enabled = YES;
        //    self.completedImageView.alpha = 0.5;
            self.reviewImageView.alpha = 1.0;
            self.plannedCompletionImageView.alpha = 1.0;
            self.assignedImageView.alpha = 1.0;
        }
    }
}

#pragma mark view controller methods

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   
    self.titleLabel.text = self.risk.title;
     self.completionLabel.text = [[self formatterDate] stringFromDate:self.risk.actualCompletionDate];
     self.plannedCompletionLabel.text = [[self formatterDate] stringFromDate:self.risk.plannedCompletionDate];
     self.createdLabel.text = [[self formatterDateTime] stringFromDate:self.risk.creationDate];
     self.reviewLabel.text = [[self formatterDate] stringFromDate:self.risk.reviewDate];
     self.modifiedLabel.text = [[self formatterDateTime] stringFromDate:self.risk.lastModifiedDate];
     self.assignedLabel.text = [[self formatterDate] stringFromDate:self.risk.assignedDate];
    [self applyDateRules];
   [self showHideReviewButton];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
        [[NSNotificationCenter defaultCenter] removeObserver:self];    
}

-(void)refreshFromBackground{
     
    [self showHideReviewButton];
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    UIEdgeInsets tabInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    UIImage * textBack = [UIImage imageNamed:@"TextFrame2"];
    textBack = [textBack resizableImageWithCapInsets:tabInsets];
    self.titleImageView.image = textBack;
   // self.titleImageView.alpha = 0.5;
    self.assignedImageView.image = textBack;
    self.completedImageView.image = textBack;
    self.createdImageView.image = textBack;
  //  self.createdImageView.alpha = 0.5;
    self.modifiedImageView.image = textBack;
  //  self.modifiedImageView.alpha = 0.5;
    self.plannedCompletionImageView.image = textBack;
    self.reviewImageView.image = textBack;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    [self checkHelpButtons];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
