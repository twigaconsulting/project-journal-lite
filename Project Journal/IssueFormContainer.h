//
//  ItemFormViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "UIViewController+TCLViewController.h"
#import "XMLWriter.h"
#import "DataHelper.h"

@class IssueFormContainer;


@protocol IssueFormControllerDelegate <NSObject>
- (void)IssueFormContainerDidFinish:(IssueFormContainer *)controller;
-(void)getTheActionForm:(Action *)itemAction;
-(void)getNewAction:(id)itenAction;
@end

@interface IssueFormContainer : UIViewController <MFMailComposeViewControllerDelegate, UINavigationControllerDelegate,UIPrintInteractionControllerDelegate>

@property (weak, nonatomic) id < IssueFormControllerDelegate> delegate;
@property (strong,nonatomic) Project *project;
@property (strong,nonatomic) Issue *containerIssue;
@property (strong,retain) NSArray *toRecipients;
@property (strong,retain) NSArray *ccRecipients;
@property (strong,retain) NSArray *bccRecipients;
@property (strong,retain) NSString *subject;
@property (strong,retain) NSString *body;
@property (strong,retain) NSString *fileName;
@property (strong,retain) NSString *fileType;
@property (strong,retain) NSString *mimeType;
@property (strong,retain) UIViewController *currentView;
@property (strong,retain) MFMailComposeViewController *picker;

@end
