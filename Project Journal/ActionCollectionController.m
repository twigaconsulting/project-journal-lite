//
//  ActionCollectionController.m
//  Project Journal
//
//  Created by Peter Pomlett on 10/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.

#import <QuartzCore/QuartzCore.h>
#import "ActionCollectionController.h"
#import "ItemCollectionCell.h"
#import "PJColour.h"
#import "NotesHeader.h"
#import "ActionFormContainer.h"
#import "ProjectFormContainer.h"
#import "RiskFormContainer.h"
#import "IssueFormContainer.h"

@interface ActionCollectionController ()<NSFetchedResultsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate,  UIActionSheetDelegate, ActionFormContainerDelegate, ProjectFormContainerDelegate, RiskFormContainerDelegate, IssueFormControllerDelegate,  UICollectionViewDelegateFlowLayout>

- (NSDateFormatter *)formatter;
@property (strong, nonatomic) NSFetchedResultsController *actionResultsController;
@property (weak, nonatomic) IBOutlet UICollectionView *actionCollection;
@property (strong, nonatomic) Action *selectedAction;
@property (strong, nonatomic)  PJColour *cellColour;
@property (nonatomic, strong) DataHelper *helper;
@property (strong, nonatomic) UIActionSheet *itemSheet;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addItem;
@property (strong, nonatomic) UIBarButtonItem *addButton;
@property (strong,nonatomic) id actionParent;

@property float scaleAction;
@property float scaleAction2;
@property (weak, nonatomic) IBOutlet UIStepper *actionStepper;
@end

@implementation ActionCollectionController



-(void)getCellSize{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.scaleAction =  [defaults floatForKey:@"scaleActionView"];
    self.scaleAction2 = [defaults floatForKey:@"scaleAction2View"];
    self.actionStepper.value = [defaults integerForKey:@"actionStepperView"];
}


- (IBAction)actionSize:(UIStepper *)sender {
    int cellSize = sender.value;
    if(cellSize == 3){
        self.scaleAction =1;
        self.scaleAction2 =1;
        [self.actionCollection.collectionViewLayout invalidateLayout];
    }
    
    if(cellSize == 2){
        self.scaleAction =0.228;
        self.scaleAction2 =1;
        [self.actionCollection.collectionViewLayout invalidateLayout];
        
    }
    if(cellSize == 1){
        self.scaleAction =0.228;
        self.scaleAction2 =0.118;
        [self.actionCollection.collectionViewLayout invalidateLayout];
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:self.scaleAction forKey:@"scaleActionView"];
    [defaults setFloat:self.scaleAction2  forKey:@"scaleAction2View"];
    [defaults setInteger:self.actionStepper.value forKey:@"actionStepperView"];
    [defaults synchronize];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
      return CGSizeMake(240 *self.scaleAction2, 110*self.scaleAction);
}









- (void)updateProjectView:(ProjectFormContainer *)controller{
    if([self.project.status isEqualToString:@"Completed"]){
        if(self.navigationItem.rightBarButtonItem){
        self.navigationItem.rightBarButtonItem = nil;
    }
    }
    else{
        if(!self.navigationItem.rightBarButtonItem){
        self.addItem = self.addButton;
     self.navigationItem.rightBarButtonItem = self.addItem;
    
        }}
  
}



-(void)getNewAction:(id)itenAction{
    self.actionParent = itenAction;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"parentNewAction" sender: self] ;
    }];
}

-(void)getTheActionForm:(Action *)itemAction{
    self.selectedAction = itemAction;
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier: @"actions" sender: self] ;
    }];
}

- (void)deleteProject:(ProjectFormContainer *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)ProjectFormContainerDidFinish:(ProjectFormContainer *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
     [self.actionCollection reloadData];
}
-(void)RiskFormContainerDidFinish:(RiskFormContainer *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
     [self.actionCollection reloadData];
}

- (void)IssueFormContainerDidFinish:(IssueFormContainer *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
     [self.actionCollection reloadData];
}

-(void)getActionParent:(id)parent{
    self.actionParent =parent;
    if([parent isKindOfClass:[Project class ]]){
        [self dismissViewControllerAnimated:YES completion:^{
            [self performSegueWithIdentifier: @"projectParent" sender: self] ;
        }];
    }
    if([parent isKindOfClass:[Issue class ]]){
        [self dismissViewControllerAnimated:YES completion:^{
            [self performSegueWithIdentifier: @"issueParent" sender: self] ;
        }];
    }
    if([parent isKindOfClass:[Risk class ]]){
        [self dismissViewControllerAnimated:YES completion:^{
            [self performSegueWithIdentifier: @"riskParent" sender: self] ;
        }];
    }
    
}

- (IBAction)newAction:(id)sender {
    
    if (!self.itemSheet){
        self.itemSheet = [[UIActionSheet alloc] initWithTitle:@"In Project"
                                                     delegate:self
                                            cancelButtonTitle:nil
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:@"New Action",nil];
    }
    [self.itemSheet showFromBarButtonItem:sender animated:YES];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(self.itemSheet == actionSheet){
        if(buttonIndex == 0){
            [self performSegueWithIdentifier:@"NewAction" sender:self];
        }
    }
}

- (void)ActionFormContainerDidFinish:(ActionFormContainer *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"actions"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
        [[segue destinationViewController] setContainerAction:self.selectedAction];
    }
    if ([[segue identifier] isEqualToString:@"NewAction"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.project];
        self.selectedAction = nil;
        [[segue destinationViewController] setContainerAction:self.selectedAction];
    }
    //action parent segues
    if ([[segue identifier] isEqualToString:@"parentNewAction"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.actionParent];
        self.selectedAction = nil;
        [[segue destinationViewController] setContainerAction:self.selectedAction];
    }
    if ([[segue identifier] isEqualToString:@"projectParent"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProjectObject:self.actionParent];
    }
    if ([[segue identifier] isEqualToString:@"issueParent"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setContainerIssue:self.actionParent];
    }
    
    if ([[segue identifier] isEqualToString:@"riskParent"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setContainerRisk:self.actionParent];
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.actionCollection reloadData];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        NotesHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        if(self.actionCollection == collectionView){
            //Hide ordering letter from notes header label
            if([[ [[self.actionResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"A. Project"]){
                headerView.titleLable.text = @"  In Project";
            }
            if([[ [[self.actionResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"B. Meeting"]){
                headerView.titleLable.text = @"  In Meetings";
            }
            
            if([[ [[self.actionResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"D. Risk"]){
                headerView.titleLable.text = @"  In Risks";
            }
            if([[ [[self.actionResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"C. Issue"]){
                headerView.titleLable.text = @"  In Issues";
            }
        }
        reusableview = headerView;
    }
    return reusableview;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return  [[self.actionResultsController sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo;
    sectionInfo = [self.actionResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    ItemCollectionCell *cell ;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    Action *action = [self.actionResultsController objectAtIndexPath:indexPath];
    NSString *DateRagStatus = [action fetchDateRAGStatus];
    NSString *RagStatus = [action fetchRAGStatus];
    UIColor *amber = self.cellColour.amber;
    UIColor *red = self.cellColour.red;
    UIColor *green = self.cellColour.green;
    UIColor *blue = self.cellColour.blue;
    if([action isForReview]){
        [ cell.reviewLabel setHidden:NO];
        cell.reviewLabel.layer.borderWidth = 1.0;
        cell.reviewLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    else{
        [ cell.reviewLabel setHidden:YES];
    }
    if([DateRagStatus isEqualToString:@"Amber"] ){
        cell.dateBadge.hidden = NO;
        cell.dateBadge.backgroundColor = amber;
        cell.dateBadge.layer.borderWidth = 1.0;
        cell.dateBadge.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    else if([DateRagStatus isEqualToString:@"Red"] ){
        cell.dateBadge.hidden = NO;
        cell.dateBadge.backgroundColor = red;
        cell.dateBadge.layer.borderWidth = 1.0;
        cell.dateBadge.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    else{
        cell.dateBadge.hidden = YES;
    }
    if ([RagStatus isEqualToString:@"Red"]){
        cell.contentView.layer.backgroundColor  = red.CGColor;
    }
    if ([RagStatus isEqualToString:@"Green"]){
        cell.contentView.layer.backgroundColor  = green.CGColor;
    }
    if ([RagStatus isEqualToString:@"Amber"]){
        cell.contentView.layer.backgroundColor  = amber.CGColor;
    }
    if ([RagStatus isEqualToString:@"Blue"]){
        cell.contentView.layer.backgroundColor  = blue.CGColor;
    }
    cell.titleLabel.text = [action title];
    cell.ownerLabel.text = [action actionOwner];
    cell.priorityLabel.text = [action priority];
    //check for actions parent
    if([action inMeeting]){
        cell.actionParentLabel.text = [[action inMeeting] title];
    }
    else if([action inIssue]){
        cell.actionParentLabel.text = [[action inIssue] title];
    }
    else if([action inRisk]){
        cell.actionParentLabel.text = [[action inRisk] title];
    }
    else if([action inProject]){
        cell.actionParentLabel.text = [[action inProject]title];
    }
    cell.dateLabel.text= [self.formatter stringFromDate:[action targetCompletionDate]];
    return cell;
}



- (NSFormatter *)formatter {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(self.actionCollection == collectionView){
        self.selectedAction = [self.actionResultsController objectAtIndexPath:indexPath];
    }
    return YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    if (self.itemSheet != nil) {
        [self.itemSheet dismissWithClickedButtonIndex:self.itemSheet.cancelButtonIndex animated:NO];
        self.itemSheet = nil;
    }
      [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.helper = [DataHelper sharedInstance];
    [self getCellSize];
    [self.actionStepper setDecrementImage:[UIImage imageNamed:@"stepperSmall"] forState:UIControlStateNormal];
    [self.actionStepper setIncrementImage:[UIImage imageNamed:@"stepperLarge"] forState:UIControlStateNormal];
    if(self.cellColour == nil){
        self.cellColour = [[PJColour alloc]init];
    }
    if([self.project.status isEqualToString:@"Completed"]){
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.actionResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Action" sortingBy:@"section" sectionBy:@"section" ascendingBy:YES includeDeleted:NO];
    self.actionResultsController.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object: nil];
}

-(void)refreshFromBackground{
    [self.actionCollection reloadData];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
