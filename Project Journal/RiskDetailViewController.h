//
//  RiskDetailViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 27/03/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"


@protocol RiskDetailViewControllerDelegate <NSObject>

- (void) upDateBadge;
-(void) dimSuper;
-(void) UndimSuper;

@end

@interface RiskDetailViewController : UIViewController
@property (weak, nonatomic) id <RiskDetailViewControllerDelegate> delegate;
@property(strong, nonatomic)Risk *risk;
- (void)dismissPopovers;
@end
