//
//  DataHelper.h
//  PJPrototype4
//
//  Created by Kynaston Pomlett on 14/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

// Test Line

#import <Foundation/Foundation.h>
//#import "MasterViewController.h"
#import "Project.h"
#import "Project+TCLProject.h"
#import "Action.h"
#import "Action+TCLAction.h"
#import "Issue.h"
#import "Issue+TCLIssue.h"
#import "Risk.h"
#import "Risk+TCLRisk.h"
#import "Dependency.h"
#import "Dependency+TCLDependency.h"
#import "Meeting.h"
#import "Meeting+TCLMeeting.h"
#import "Stakeholder.h"
#import "Stakeholder+TCLStakeholder.h"
#import "Note.h"
#import "Note+TCLNote.h"
#import "Media.h"
#import "Media+TCLMedia.h"
#import "Header.h"
#import "Config+TCLConfig.h"
#import "Config.h"
#import "AddressBook/AddressBook.h"
#import "AgendaItem+TCLAgenda.h"



@class MainViewController;

extern NSString *const VERSION;

@interface DataHelper : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) MainViewController *mainViewController;
@property (strong, nonatomic) NSFetchedResultsController *fetchedRCHCP;
@property (strong, nonatomic) NSFetchedResultsController *fetchedRCAP;
@property (strong, nonatomic) NSFetchedResultsController *fetchedRCAN;
@property (strong, nonatomic) NSFetchedResultsController *fetchedRCHCA;
@property (strong, nonatomic) NSFetchedResultsController *fetchedRCAA;
@property (strong, nonatomic) NSFetchedResultsController *fetchedRCHCI;
@property (strong, nonatomic) NSFetchedResultsController *fetchedRCAI;
@property (strong, nonatomic) NSFetchedResultsController *fetchedRCHCR;
@property (strong, nonatomic) NSFetchedResultsController *fetchedRCAR;
@property (strong, nonatomic) NSFetchedResultsController *fetchedRCHCM;
@property (strong, nonatomic) NSFetchedResultsController *fetchedRCAM;
// Current Instances
@property (strong, nonatomic) Project *currentProject;
@property (strong, nonatomic) Config *settings;

+ (DataHelper *)sharedInstance;

- (id)init;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

// CALLED BY VIEW CONTROLLERS
- (NSArray *)allProjects;               // Returns all Projects
- (NSArray *)fetchNotesList:(id)object;  // Returns non-deleted notes in an object


// Settings Getter and Setter Methods
- (NSNumber *)redRAGInterval;
- (NSNumber *)amberRAGInterval;
- (void)setRedRAGInterval:(NSNumber *)number;
- (void)setAmberRAGInterval:(NSNumber *)number;

- (NSNumber *)projectReviewInterval;
- (NSNumber *)actionReviewInterval;
- (NSNumber *)issueReviewInterval;
- (NSNumber *)riskReviewInterval;
- (void)setProjectReviewInterval:(NSNumber *)number;
- (void)setActionReviewInterval:(NSNumber *)number;
- (void)setIssueReviewInterval:(NSNumber *)number;
- (void)setRiskReviewInterval:(NSNumber *)number;
- (NSNumber *)amberRAGBudgetPerCent;
- (void)setAmberRAGBudgetPerCent:(NSNumber *)number;


- (BOOL)showHelp;
- (void)setShowHelp:(BOOL)flag;
- (BOOL)hideCompleted;
- (void)setHideCompleted:(BOOL)flag;

// Lists for popovers

- (NSArray *)fetchStatusOptions:(id)object;
- (NSArray *)fetchViewpointOptions;
- (NSArray *)fetchInfluenceOptions;
- (NSArray *)fetchPriorityOptions;
- (NSArray *)fetchCategoryOptions;
- (NSArray *)fetchPersonList;
- (NSArray *)fetchDependencyOptions;
- (NSArray *)fetchMitigationOptions;


- (NSFetchedResultsController *)fetchItemsMatching:(Project *)project
                                     forEntityType:(NSString *)entity
                                         sortingBy:(NSString *)sortAttribute
                                       ascendingBy:(BOOL)ascending
                                    includeDeleted:(BOOL)inclDeleted;
- (NSFetchedResultsController *)fetchItemsMatching:(Project *)project
                                     forEntityType:(NSString *)entity
                                         sortingBy:(NSString *)sortAttribute
                                         sectionBy:(NSString *)sectionAttribute
                                       ascendingBy:(BOOL)ascending
                                    includeDeleted:(BOOL)inclDeleted;


#pragma mark - Data Model Fetch Methods
- (void)fetchHCP;
- (void)fetchAP;


#pragma mark - New Object Methods
// New Items
- (Project *)newProject;
- (Action *)newAction:(id)object;
- (Issue *)newIssue:(Project *)project;
- (Risk *)newRisk:(Project *)project;
- (Dependency *)newDependency:(Project *)project;
- (Meeting *)newMeeting:(Project *)project;
- (Stakeholder *)newStakeholder:(Project *)project;
- (Note *)newNote:(id)object;
- (AgendaItem *)newAgendaItem:(Meeting *)meeting;
- (Media *)newMedia;
- (NSString *)newUUID;


// Delete Items
- (BOOL)deleteObject:(NSManagedObject *)object;

//Config Items
//- (NSNumber *)amberRAGDateInterval;
//- (NSNumber *)redRAGDateInterval;
//- (NSNumber *)defaultReviewDateInterval;

// Messaging Methods
- (NSString *)emailBodyFromItem:(id)object;
- (NSString *)emailSubjectFromItem:(id)object;

#pragma mark - Utility Methods
//Date Utility Methods
- (NSString *)convertToLocalDate:(NSDate *)aDate;
- (NSString *)convertToLocalDateAndTime:(NSDate *)aDate;
- (NSString *)convertToLocalReferenceDateAndTime:(NSDate *)aDate;
- (NSString *)convertToLocalCurrency:(NSNumber *)currency withFormat:(NSNumberFormatter *)formatter;



// Config Methods
- (BOOL)isHelpVisible;
@end
