//
//  DBRequest+ExBody.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 09/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <DropboxSDK/DropboxSDK.h>

@interface DBRequest (ExBody)

- (NSInputStream *)connection:(NSURLConnection *)connection needNewBodyStream:(NSURLRequest *)req;
@end
