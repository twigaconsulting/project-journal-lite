//
//  MeetingAttendeeViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 17/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h" 
#import "Person+TCLPerson.h"

@protocol MeetingAttendeeViewControllerDelegate <NSObject>

-(void)updateAttendeeBadge;
-(void) dimSuper;
-(void) UndimSuper;

@end

@interface MeetingAttendeeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) id <MeetingAttendeeViewControllerDelegate> delegate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) Meeting *meeting;
- (void)dismissPopovers;
@end
