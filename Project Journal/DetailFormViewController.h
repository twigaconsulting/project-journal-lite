//
//  DetailFormViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 23/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"

@protocol DetailFormViewControllerDelegate <NSObject>

- (void) upDateBadge;
- (void) upDateTitle;
-(void) dimSuper;
-(void) UndimSuper;
@end

@interface DetailFormViewController : UIViewController
@property (weak, nonatomic) id <DetailFormViewControllerDelegate> delegate;
@property (strong , nonatomic) Project *project;
@property (strong , nonatomic) DataHelper *helper;
- (void)disnissPopovers;
@end
