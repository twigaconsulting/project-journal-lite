//
//  SettingsTableViewController.h
//  Project Journal
//
//  Created by Peter Pomlett on 06/06/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@protocol SettingsTableViewControllerDelegate <NSObject>

-(void)updateCollectionView;
-(void)dismissSettings;
@end

@interface SettingsTableViewController : UITableViewController <MFMailComposeViewControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) id <SettingsTableViewControllerDelegate> delegate;

@property (strong,retain) NSArray *toRecipients;
@property (strong,retain) NSArray *ccRecipients;
@property (strong,retain) NSArray *bccRecipients;
@property (strong,retain) NSString *subject;
@property (strong,retain) NSString *body;
@property (strong,retain) NSString *fileName;
@property (strong,retain) NSString *fileType;
@property (strong,retain) NSString *mimeType;



@end
